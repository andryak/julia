Place in this folder the datasets used to replicate the experiments related to Julia.

The three benchmarks can be found [here](https://drive.google.com/open?id=0B5BAqcpCRvEPX1pUZFlBTXVTREE).
Download them and place them in this folder into three directories with the following names:

+ lia
+ nra
+ abv
+ str
