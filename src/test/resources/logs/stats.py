import sys

from collections import defaultdict


def parse(content):
    result = defaultdict(lambda: defaultdict(dict))

    current_block = None
    current_subblock = None
    for line in content.split('\n'):
        if not line.strip():
            # Skip empty lines.
            continue
        if line.startswith('@'):
            # Block ident.
            current_block = line.strip('@')
            continue
        elif line.startswith('['):
            # Sub-block ident.
            current_subblock = line.strip('[]')
            continue
        else:
            assert(current_block and current_subblock)
            key, value = line.split(' = ')
            result[current_block][current_subblock][key] = float(value)

    return result


def aggregate_utopia_logs(logs):
    all_formulas = logs['expressions']['checked']
    sat_formulas = logs['expressions']['sat']
    unsat_formulas = logs['expressions']['unsat']
    unknown_formulas = logs['expressions']['unknown']
    assert(all_formulas == sat_formulas + unsat_formulas + unknown_formulas)
    
    sat_reuse = 0.0
    sat_reuse += logs['reuse']['sat-hash-cache-hits']
    sat_reuse += logs['reuse']['satdelta-cache-hits']
    sat_reuse += logs['reuse']['satdelta-is-zero']
    assert(sat_reuse <= sat_formulas)
    
    unsat_reuse = 0.0
    unsat_reuse += logs['reuse']['unsat-hash-cache-hits']
    unsat_reuse += logs['reuse']['cex-cache-hits']
    assert(unsat_reuse <= unsat_formulas)
    
    all_reuse = sat_reuse + unsat_reuse
    
    all_reuse_ratio = all_reuse / all_formulas if all_formulas else 1.0
    sat_reuse_ratio = sat_reuse / sat_formulas if sat_formulas else 1.0
    unsat_reuse_ratio = unsat_reuse / unsat_formulas if unsat_formulas else 1.0
    
    total_time = sum(logs['time(ns)'].values())
    total_time /= 1000000000

    return '{}\t{}\t{}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.3f}'.format(
        int(all_formulas),
        int(sat_formulas),
        int(unsat_formulas),
        sat_reuse_ratio,
        unsat_reuse_ratio,
        all_reuse_ratio,
        total_time
    )


def aggregate_random_logs(logs):
    all_formulas = logs['expressions']['checked']
    sat_formulas = logs['expressions']['sat']
    unsat_formulas = logs['expressions']['unsat']
    unknown_formulas = logs['expressions']['unknown']
    assert(all_formulas == sat_formulas + unsat_formulas + unknown_formulas)
    
    sat_reuse = logs['reuse']['nr-sat-cache-hits']
    assert(sat_reuse <= sat_formulas)
    
    unsat_reuse = logs['reuse']['nr-unsat-cache-hits']
    assert(unsat_reuse <= unsat_formulas)
    
    all_reuse = sat_reuse + unsat_reuse
    
    all_reuse_ratio = all_reuse / all_formulas if all_formulas else 1.0
    sat_reuse_ratio = sat_reuse / sat_formulas if sat_formulas else 1.0
    unsat_reuse_ratio = unsat_reuse / unsat_formulas if unsat_formulas else 1.0
    
    total_time = sum(logs['time(ns)'].values())
    total_time /= 1000000000

    return '{}\t{}\t{}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.3f}'.format(
        int(all_formulas),
        int(sat_formulas),
        int(unsat_formulas),
        sat_reuse_ratio,
        unsat_reuse_ratio,
        all_reuse_ratio,
        total_time
    )


def aggregate_smtsolver_logs(logs):
    all_formulas = logs['expressions']['checked']
    sat_formulas = logs['expressions']['sat']
    unsat_formulas = logs['expressions']['unsat']
    unknown_formulas = logs['expressions']['unknown']
    assert(all_formulas == sat_formulas + unsat_formulas + unknown_formulas)

    total_time = sum(logs['time(ns)'].values())
    total_time /= 1000000000
    
    return '{}\t{}\t{}\t{:.3f}'.format(
        int(all_formulas),
        int(sat_formulas),
        int(unsat_formulas),
        total_time
    )


if __name__ == '__main__':
    if len(sys.argv) < 2:
        pname = sys.argv[0]
        print('Error: missing logfile')
        print(f'Usage: python {pname} <logfile> [--header, -h]')
        exit(-1)

    logfile = sys.argv[1]
    headers = '--header' in sys.argv or '-h' in sys.argv

    with open(logfile, 'r') as fh:
        content = fh.read()

    logs = parse(content)

    if 'utopia-solver' in logs:
        if headers:
            print('\t'.join([
                'all-formulas',
                'sat-formulas',
                'unsat-formulas',
                'sat-reuse-ratio',
                'unsat-reuse-ratio',
                'total-reuse-ratio',
                'total-time',
            ]))
        print(aggregate_utopia_logs(logs['utopia-solver']))

    if 'random-solver' in logs:
        if headers:
            print('\t'.join([
                'all-formulas',
                'sat-formulas',
                'unsat-formulas',
                'sat-reuse-ratio',
                'unsat-reuse-ratio',
                'total-reuse-ratio',
                'total-time',
            ]))
        print(aggregate_random_logs(logs['random-solver']))

    if 'z3-solver' in logs:
        if headers:
            print('\t'.join([
                'all-formulas',
                'sat-formulas',
                'unsat-formulas',
                'total-time',
            ]))
        print(aggregate_smtsolver_logs(logs['z3-solver']))

    if 'z3str-solver' in logs:
        if headers:
            print('\t'.join([
                'all-formulas',
                'sat-formulas',
                'unsat-formulas',
                'total-time',
            ]))
        print(aggregate_smtsolver_logs(logs['z3str-solver']))
