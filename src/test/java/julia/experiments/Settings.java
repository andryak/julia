package julia.experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Configuration class for the experiments.
 */
final class Settings {
   /**
    * Path to the Microsoft Z3 SMT solver executable.
    */
   public final static String Z3_BINARY;

   /**
    * Path to the Z3-Str solver executable.
    */
   public final static String Z3_STR_BINARY;

   // Load the path to Z3 from the entry in the config.properties files.
   static {
      final String propertiesFilename = "./src/main/resources/config.properties";
      File propertiesFile = new File(propertiesFilename);
      FileReader reader;
      try {
         reader = new FileReader(propertiesFile);
      } catch (FileNotFoundException e) {
         throw new IllegalStateException(e.getMessage());
      }
      Properties properties = new Properties();
      try {
         properties.load(reader);
      } catch (IOException e) {
         throw new IllegalStateException(e.getMessage());
      }
      Z3_BINARY = properties.getProperty("z3");
      Z3_STR_BINARY = properties.getProperty("z3-str");
   }

   /**
    * Path to the folder containing the formulas in the NRA benchmark in SEXPR format.
    */
   public final static String NRA_BENCHMARK = "./src/test/resources/benchmarks/nra/";

   /**
    * Path to the folder containing the formulas in the LIA benchmark in SEXPR format.
    */
   public final static String LIA_BENCHMARK = "./src/test/resources/benchmarks/lia/";

   /**
    * Path to the folder containing the formulas in the ABV benchmark in SMT-LIB v2
    * format.
    */
   public final static String ABV_BENCHMARK = "./src/test/resources/benchmarks/abv/";

   /**
    * Path to the folder containing the formulas in the STR benchmark in Z3-Str format.
    */
   public final static String STR_BENCHMARK = "./src/test/resources/benchmarks/str/";

   /**
    * Folder where the log files produced by the experiments will be placed.
    */
   public final static String LOG_DIR = "./src/test/resources/logs/";

   private Settings() {};

   /**
    * Throws an exception if the specified directories or executables do not exist.
    * 
    * @throws IllegalStateException
    *            if the specified directories or executables do not exist.
    */
   public static void validate() {
      File f;

      // Check that the Microsoft Z3 SMT solver executable exists.
      f = new File(Settings.Z3_BINARY);
      if (!f.exists() || f.isDirectory() || !f.canExecute()) {
         throw new IllegalStateException("Cannot find or execute the Microsoft Z3 SMT solver.");
      }

      // Check that the Z3-Str solver executable exists.
      f = new File(Settings.Z3_STR_BINARY);
      if (!f.exists() || f.isDirectory() || !f.canExecute()) {
         throw new IllegalStateException("Cannot find or execute the Microsoft Z3 SMT solver.");
      }

      // Check that logs directory exist.
      f = new File(Settings.LOG_DIR);
      if (!f.exists() || !f.isDirectory()) {
         throw new IllegalStateException("Logs directory does not exist.");
      }

      // Check that benchmark directories exist.
      f = new File(Settings.NRA_BENCHMARK);
      if (!f.exists() || !f.isDirectory()) {
         throw new IllegalStateException("NRA benchmark directory does not exist.");
      }

      f = new File(Settings.LIA_BENCHMARK);
      if (!f.exists() || !f.isDirectory()) {
         throw new IllegalStateException("LIA benchmark directory does not exist.");
      }

      f = new File(Settings.ABV_BENCHMARK);
      if (!f.exists() || !f.isDirectory()) {
         throw new IllegalStateException("ABV benchmark directory does not exist.");
      }

      f = new File(Settings.STR_BENCHMARK);
      if (!f.exists() || !f.isDirectory()) {
         throw new IllegalStateException("STR benchmark directory does not exist.");
      }
   }
}
