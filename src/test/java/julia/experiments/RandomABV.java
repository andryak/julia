package julia.experiments;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import julia.expr.parser.exc.ParserException;
import julia.solver.random.RandomSolver;
import julia.solver.random.RandomSolverStatsCollector;
import julia.solver.z3.Z3Solver;
import julia.tools.ABVExperimenter;
import julia.tools.DatasetExperimenter;
import org.junit.BeforeClass;
import org.junit.Test;

public class RandomABV {
   private static final String LOG_SUBDIR = "abv/random/";

   private void analyze(String path) throws IOException, ParserException {
      Z3Solver Z3 = new Z3Solver(Settings.Z3_BINARY);
      Z3.setLogic("QF_ABV");

      RandomSolver solver = new RandomSolver(0, Z3, 10);
      RandomSolverStatsCollector listener = new RandomSolverStatsCollector();
      solver.addListener(listener);

      DatasetExperimenter experimenter = new ABVExperimenter(path, solver);
      experimenter.analyze();

      System.out.println("@random-solver");
      System.out.println(listener.getStats());
   }

   @BeforeClass
   public static void setup() throws IOException {
      // Check that the data to run the experiments exist.
      Settings.validate();

      // Create sub-directory to hold logs, if necessary.
      Files.createDirectories(Paths.get(Settings.LOG_DIR, LOG_SUBDIR));
   }

   @Test
   public void test_test1() throws IOException, ParserException {
      final String fname = "[.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_base64() throws IOException, ParserException {
      final String fname = "base64.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_basename() throws IOException, ParserException {
      final String fname = "basename.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_cat() throws IOException, ParserException {
      final String fname = "cat.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_chcon() throws IOException, ParserException {
      final String fname = "chcon.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_chgrp() throws IOException, ParserException {
      final String fname = "chgrp.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_chmod() throws IOException, ParserException {
      final String fname = "chmod.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_chown() throws IOException, ParserException {
      final String fname = "chown.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_chroot() throws IOException, ParserException {
      final String fname = "chroot.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_cksum() throws IOException, ParserException {
      final String fname = "cksum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_comm() throws IOException, ParserException {
      final String fname = "comm.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_cp() throws IOException, ParserException {
      final String fname = "cp.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_csplit() throws IOException, ParserException {
      final String fname = "csplit.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_cut() throws IOException, ParserException {
      final String fname = "cut.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_date() throws IOException, ParserException {
      final String fname = "date.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_dd() throws IOException, ParserException {
      final String fname = "dd.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_df() throws IOException, ParserException {
      final String fname = "df.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_dir() throws IOException, ParserException {
      final String fname = "dir.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_dircolors() throws IOException, ParserException {
      final String fname = "dircolors.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_dirname() throws IOException, ParserException {
      final String fname = "dirname.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_du() throws IOException, ParserException {
      final String fname = "du.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_echo() throws IOException, ParserException {
      final String fname = "echo.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_env() throws IOException, ParserException {
      final String fname = "env.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_expand() throws IOException, ParserException {
      final String fname = "expand.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_expr() throws IOException, ParserException {
      final String fname = "expr.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_factor() throws IOException, ParserException {
      final String fname = "factor.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_false() throws IOException, ParserException {
      final String fname = "false.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_fmt() throws IOException, ParserException {
      final String fname = "fmt.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_fold() throws IOException, ParserException {
      final String fname = "fold.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_ginstall() throws IOException, ParserException {
      final String fname = "ginstall.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_groups() throws IOException, ParserException {
      final String fname = "groups.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_head() throws IOException, ParserException {
      final String fname = "head.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_hostid() throws IOException, ParserException {
      final String fname = "hostid.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_id() throws IOException, ParserException {
      final String fname = "id.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_join() throws IOException, ParserException {
      final String fname = "join.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_kill() throws IOException, ParserException {
      final String fname = "kill.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_link() throws IOException, ParserException {
      final String fname = "link.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_ln() throws IOException, ParserException {
      final String fname = "ln.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_logname() throws IOException, ParserException {
      final String fname = "logname.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_ls() throws IOException, ParserException {
      final String fname = "ls.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_md5sum() throws IOException, ParserException {
      final String fname = "md5sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_mkdir() throws IOException, ParserException {
      final String fname = "mkdir.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_mkfifo() throws IOException, ParserException {
      final String fname = "mkfifo.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_mknod() throws IOException, ParserException {
      final String fname = "mknod.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_mktemp() throws IOException, ParserException {
      final String fname = "mktemp.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_mv() throws IOException, ParserException {
      final String fname = "mv.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_nice() throws IOException, ParserException {
      final String fname = "nice.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_nl() throws IOException, ParserException {
      final String fname = "nl.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_nohup() throws IOException, ParserException {
      final String fname = "nohup.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_od() throws IOException, ParserException {
      final String fname = "od.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_paste() throws IOException, ParserException {
      final String fname = "paste.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_pathchk() throws IOException, ParserException {
      final String fname = "pathchk.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_pinky() throws IOException, ParserException {
      final String fname = "pinky.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_pr() throws IOException, ParserException {
      final String fname = "pr.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_printenv() throws IOException, ParserException {
      final String fname = "printenv.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_printf() throws IOException, ParserException {
      final String fname = "printf.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_ptx() throws IOException, ParserException {
      final String fname = "ptx.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_pwd() throws IOException, ParserException {
      final String fname = "pwd.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_readlink() throws IOException, ParserException {
      final String fname = "readlink.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_rm() throws IOException, ParserException {
      final String fname = "rm.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_rmdir() throws IOException, ParserException {
      final String fname = "rmdir.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_runcon() throws IOException, ParserException {
      final String fname = "runcon.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_seq() throws IOException, ParserException {
      final String fname = "seq.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_setuidgid() throws IOException, ParserException {
      final String fname = "setuidgid.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sha1sum() throws IOException, ParserException {
      final String fname = "sha1sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sha224sum() throws IOException, ParserException {
      final String fname = "sha224sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sha256sum() throws IOException, ParserException {
      final String fname = "sha256sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sha384sum() throws IOException, ParserException {
      final String fname = "sha384sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sha512sum() throws IOException, ParserException {
      final String fname = "sha512sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_shred() throws IOException, ParserException {
      final String fname = "shred.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_shuf() throws IOException, ParserException {
      final String fname = "shuf.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sleep() throws IOException, ParserException {
      final String fname = "sleep.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sort() throws IOException, ParserException {
      final String fname = "sort.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_split() throws IOException, ParserException {
      final String fname = "split.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_stat() throws IOException, ParserException {
      final String fname = "stat.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_stty() throws IOException, ParserException {
      final String fname = "stty.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_su() throws IOException, ParserException {
      final String fname = "su.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sum() throws IOException, ParserException {
      final String fname = "sum.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_sync() throws IOException, ParserException {
      final String fname = "sync.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tac() throws IOException, ParserException {
      final String fname = "tac.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tail() throws IOException, ParserException {
      final String fname = "tail.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tee() throws IOException, ParserException {
      final String fname = "tee.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_test2() throws IOException, ParserException {
      final String fname = "test.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_touch() throws IOException, ParserException {
      final String fname = "touch.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tr() throws IOException, ParserException {
      final String fname = "tr.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_true() throws IOException, ParserException {
      final String fname = "true.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tsort() throws IOException, ParserException {
      final String fname = "tsort.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tty() throws IOException, ParserException {
      final String fname = "tty.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_uname() throws IOException, ParserException {
      final String fname = "uname.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_unexpand() throws IOException, ParserException {
      final String fname = "unexpand.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_uniq() throws IOException, ParserException {
      final String fname = "uniq.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_unlink() throws IOException, ParserException {
      final String fname = "unlink.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_uptime() throws IOException, ParserException {
      final String fname = "uptime.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_users() throws IOException, ParserException {
      final String fname = "users.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_vdir() throws IOException, ParserException {
      final String fname = "vdir.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_wc() throws IOException, ParserException {
      final String fname = "wc.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_who() throws IOException, ParserException {
      final String fname = "who.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_whoami() throws IOException, ParserException {
      final String fname = "whoami.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }

   @Test
   public void test_yes() throws IOException, ParserException {
      final String fname = "yes.missed";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.ABV_BENCHMARK, fname).toString());
   }
}
