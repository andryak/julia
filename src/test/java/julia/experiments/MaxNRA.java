package julia.experiments;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.model.Model;
import julia.expr.model.SortModel;
import julia.expr.parser.exc.ParserException;
import julia.solver.utopia.UtopiaSolver;
import julia.solver.utopia.UtopiaSolverStatsCollector;
import julia.solver.z3.Z3Solver;
import julia.tools.DatasetExperimenter;
import julia.tools.NRAExperimenter;
import org.junit.BeforeClass;
import org.junit.Test;

public class MaxNRA {
   private static final String LOG_SUBDIR = "nra/max/";

   private static List<Model> REFERENCE_MODELS;
   static {
      SortModel model0 = new SortModel();
      SortModel model1 = new SortModel();
      SortModel model2 = new SortModel();

      model0.setBoolInter(BoolVal.TRUE);
      model1.setBoolInter(BoolVal.TRUE);
      model2.setBoolInter(BoolVal.TRUE);

      model0.setRealInter(RealVal.ZERO);
      model1.setRealInter(RealVal.create(100));
      model2.setRealInter(RealVal.create(-1000));

      REFERENCE_MODELS = new ArrayList<>();
      REFERENCE_MODELS.add(model0);
      REFERENCE_MODELS.add(model1);
      REFERENCE_MODELS.add(model2);
   }

   private void analyze(String path) throws IOException, ParserException {
      Z3Solver Z3 = new Z3Solver(Settings.Z3_BINARY);
      Z3.setLogic("QF_NRA");

      UtopiaSolver solver = new UtopiaSolver(REFERENCE_MODELS, Z3, Integer.MAX_VALUE);
      UtopiaSolverStatsCollector listener = new UtopiaSolverStatsCollector();
      solver.addListener(listener);

      DatasetExperimenter experimenter = new NRAExperimenter(path, solver);
      experimenter.analyze();

      System.out.println("@utopia-solver");
      System.out.println(listener.getStats());
   }

   @BeforeClass
   public static void setup() throws IOException {
      // Check that the data to run the experiments exist.
      Settings.validate();

      // Create sub-directory to hold logs, if necessary.
      Files.createDirectories(Paths.get(Settings.LOG_DIR, LOG_SUBDIR));
   }

   @Test
   public void test_multigraph() throws IOException, ParserException {
      final String fname = "graphstream_multigraph.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_singlegraph() throws IOException, ParserException {
      final String fname = "graphstream_singlegraph.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_arraylistmultimap() throws IOException, ParserException {
      final String fname = "guava_arraylistmultimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_concurrenthashmultiset() throws IOException, ParserException {
      final String fname = "guava_concurrenthashmultiset.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_duration() throws IOException, ParserException {
      final String fname = "guava_duration.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_hashbimap() throws IOException, ParserException {
      final String fname = "guava_hashbimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_hashmultimap() throws IOException, ParserException {
      final String fname = "guava_hashmultimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_hashmultiset() throws IOException, ParserException {
      final String fname = "guava_hashmultiset.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_immutablebimap() throws IOException, ParserException {
      final String fname = "guava_immutablebimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_immutablelistmultimap() throws IOException, ParserException {
      final String fname = "guava_immutablelistmultimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_immutablemultiset() throws IOException, ParserException {
      final String fname = "guava_immutablemultiset.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_linkedhashmultimap() throws IOException, ParserException {
      final String fname = "guava_linkedhashmultimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_linkedhashmultiset() throws IOException, ParserException {
      final String fname = "guava_linkedhashmultiset.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_linkedlistmultimap() throws IOException, ParserException {
      final String fname = "guava_linkedlistmultimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_treemultimap() throws IOException, ParserException {
      final String fname = "guava_treemultimap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_treemultiset() throws IOException, ParserException {
      final String fname = "guava_treemultiset.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.NRA_BENCHMARK, fname).toString());
   }
}
