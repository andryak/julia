package julia.experiments;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.str.StrVal;
import julia.expr.model.Model;
import julia.expr.model.SortModel;
import julia.expr.parser.exc.ParserException;
import julia.solver.slicing.SlicingSolver;
import julia.solver.slicing.SlicingSolverStatsCollector;
import julia.solver.utopia.UtopiaSolver;
import julia.solver.utopia.UtopiaSolverStatsCollector;
import julia.solver.z3_str.Z3StrSolver;
import julia.tools.DatasetExperimenter;
import julia.tools.STRExperimenter;
import org.junit.BeforeClass;
import org.junit.Test;

public class MaxSTR {
   private static final String LOG_SUBDIR = "str/max/";

   private static final int SOLVER_TIMEOUT = 10;

   private static List<Model> REFERENCE_MODELS;
   static {
      SortModel model0 = new SortModel();
      SortModel model1 = new SortModel();

      model0.setBoolInter(BoolVal.TRUE);
      model1.setBoolInter(BoolVal.TRUE);

      model0.setIntInter(IntVal.create(0));
      model1.setIntInter(IntVal.create(1));

      model0.setStringInter(StrVal.create(""));
      model1.setStringInter(StrVal.create("a"));

      REFERENCE_MODELS = new ArrayList<>();
      REFERENCE_MODELS.add(model0);
      REFERENCE_MODELS.add(model1);
   }

   private void analyze(String path) throws IOException, ParserException {
      Z3StrSolver Z3_STR = new Z3StrSolver(Settings.Z3_STR_BINARY, SOLVER_TIMEOUT);

      // Use the slicing solver for the STR benchmark.
      UtopiaSolver utopiaSolver = new UtopiaSolver(REFERENCE_MODELS, Z3_STR, Integer.MAX_VALUE);
      UtopiaSolverStatsCollector utopiaSolverListener = new UtopiaSolverStatsCollector();
      utopiaSolver.addListener(utopiaSolverListener);

      SlicingSolver solver = new SlicingSolver(utopiaSolver);
      SlicingSolverStatsCollector slicingSolverListener = new SlicingSolverStatsCollector();
      solver.addListener(slicingSolverListener);

      DatasetExperimenter experimenter = new STRExperimenter(path, solver);
      experimenter.analyze();

      System.out.println("@slicing-solver");
      System.out.println(slicingSolverListener.getStats());
      System.out.println();

      System.out.println("@utopia-solver");
      System.out.println(utopiaSolverListener.getStats());
   }

   @BeforeClass
   public static void setup() throws IOException {
      // Check that the data to run the experiments exist.
      Settings.validate();

      // Create sub-directory to hold logs, if necessary.
      Files.createDirectories(Paths.get(Settings.LOG_DIR, LOG_SUBDIR));
   }

   @Test
   public void test_kaluza() throws IOException, ParserException {
      final String fname = "kaluza.smt2";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.STR_BENCHMARK, fname).toString());
   }
}
