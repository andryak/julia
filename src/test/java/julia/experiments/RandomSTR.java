package julia.experiments;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import julia.expr.parser.exc.ParserException;
import julia.solver.random.RandomSolver;
import julia.solver.random.RandomSolverStatsCollector;
import julia.solver.z3_str.Z3StrSolver;
import julia.tools.DatasetExperimenter;
import julia.tools.STRExperimenter;
import org.junit.BeforeClass;
import org.junit.Test;

public class RandomSTR {
   private static final String LOG_SUBDIR = "str/random/";

   private static final int SOLVER_TIMEOUT = 10; // seconds.

   private void analyze(String path) throws IOException, ParserException {
      Z3StrSolver Z3_STR = new Z3StrSolver(Settings.Z3_STR_BINARY, SOLVER_TIMEOUT);

      RandomSolver solver = new RandomSolver(0, Z3_STR, 10);
      RandomSolverStatsCollector listener = new RandomSolverStatsCollector();
      solver.addListener(listener);

      DatasetExperimenter experimenter = new STRExperimenter(path, solver);
      experimenter.analyze();

      System.out.println("@random-solver");
      System.out.println(listener.getStats());
   }

   @BeforeClass
   public static void setup() throws IOException {
      // Check that the data to run the experiments exist.
      Settings.validate();

      // Create sub-directory to hold logs, if necessary.
      Files.createDirectories(Paths.get(Settings.LOG_DIR, LOG_SUBDIR));
   }

   @Test
   public void test_kaluza() throws IOException, ParserException {
      final String fname = "kaluza.smt2";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.STR_BENCHMARK, fname).toString());
   }
}
