package julia.experiments;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import julia.expr.parser.exc.ParserException;
import julia.solver.random.RandomSolver;
import julia.solver.random.RandomSolverStatsCollector;
import julia.solver.slicing.SlicingSolver;
import julia.solver.slicing.SlicingSolverStatsCollector;
import julia.solver.z3.Z3Solver;
import julia.tools.DatasetExperimenter;
import julia.tools.LIAExperimenter;
import org.junit.BeforeClass;
import org.junit.Test;

public class RandomLIA {
   private static final String LOG_SUBDIR = "lia/random/";

   private void analyze(String path) throws IOException, ParserException {
      Z3Solver Z3 = new Z3Solver(Settings.Z3_BINARY);
      Z3.setLogic("QF_LIA");

      // Use the slicing solver for the LIA benchmark.
      RandomSolver randomSolver = new RandomSolver(0, Z3, 10);
      RandomSolverStatsCollector randomSolverListener = new RandomSolverStatsCollector();
      randomSolver.addListener(randomSolverListener);

      SlicingSolver slicingSolver = new SlicingSolver(randomSolver);
      SlicingSolverStatsCollector slicingSolverListener = new SlicingSolverStatsCollector();
      slicingSolver.addListener(slicingSolverListener);

      DatasetExperimenter experimenter = new LIAExperimenter(path, slicingSolver);
      experimenter.analyze();

      System.out.println("@slicing-solver");
      System.out.println(slicingSolverListener.getStats());
      System.out.println();

      System.out.println("@random-solver");
      System.out.println(randomSolverListener.getStats());
   }

   @BeforeClass
   public static void setup() throws IOException {
      // Check that the data to run the experiments exist.
      Settings.validate();

      // Create sub-directory to hold logs, if necessary.
      Files.createDirectories(Paths.get(Settings.LOG_DIR, LOG_SUBDIR));
   }

   @Test
   public void test_afs() throws IOException, ParserException {
      final String fname = "afs.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_avl() throws IOException, ParserException {
      final String fname = "avl.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_ball() throws IOException, ParserException {
      final String fname = "ball.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_block() throws IOException, ParserException {
      final String fname = "block.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_cdaudio() throws IOException, ParserException {
      final String fname = "cdaudio.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_collision() throws IOException, ParserException {
      final String fname = "collision.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_dijkstra() throws IOException, ParserException {
      final String fname = "dijkstra.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_diskperf() throws IOException, ParserException {
      final String fname = "diskperf.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_division() throws IOException, ParserException {
      final String fname = "division.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_floppy() throws IOException, ParserException {
      final String fname = "floppy.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_grep() throws IOException, ParserException {
      final String fname = "grep.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_kbfiltr() throws IOException, ParserException {
      final String fname = "kbfiltr.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_knapsack() throws IOException, ParserException {
      final String fname = "knapsack.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_list() throws IOException, ParserException {
      final String fname = "list.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_multiplication() throws IOException, ParserException {
      final String fname = "multiplication.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_newtax() throws IOException, ParserException {
      final String fname = "new-tax.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_oldtax() throws IOException, ParserException {
      final String fname = "old-tax.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_reverseword() throws IOException, ParserException {
      final String fname = "reverseword.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_swapwords() throws IOException, ParserException {
      final String fname = "swapwords.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_tcas() throws IOException, ParserException {
      final String fname = "tcas.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_treemap() throws IOException, ParserException {
      final String fname = "treemap.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }

   @Test
   public void test_wbs() throws IOException, ParserException {
      final String fname = "wbs.sexpr";
      final String logfile = Paths.get(Settings.LOG_DIR, LOG_SUBDIR, fname).toString();
      final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
      System.setOut(stream);
      System.setErr(stream);
      analyze(Paths.get(Settings.LIA_BENCHMARK, fname).toString());
   }
}
