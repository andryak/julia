package julia.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

public class PairTest {
   @Test
   public void testGetL() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      assertEquals("hello", pair.getL());
   }

   @Test
   public void testGetR() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      assertEquals("world", pair.getR());
   }

   @Test
   public void testSetL() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      pair.setL("hi");
      assertEquals("hi", pair.getL());
   }

   @Test
   public void testSetR() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      pair.setR("man");
      assertEquals("man", pair.getR());
   }

   @Test
   public void testPairEqualsSelf() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      assertEquals(pair, pair);
   }

   @Test
   public void testPairNotEqualsNull() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      assertNotEquals(pair, null);
   }

   @Test
   public void testPairNotEqualsInteger() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      assertNotEquals(pair, new Integer(0));
   }

   @Test
   public void testNonNullPairNotEqualsPairWithLeftNull() {
      Pair<String, String> pair1 = new Pair<>("hello", "world");
      Pair<String, String> pair2 = new Pair<>(null, "world");
      assertNotEquals(pair1, pair2);
      assertNotEquals(pair2, pair1);
   }

   @Test
   public void testNonNullPairNotEqualsPairWithRightNull() {
      Pair<String, String> pair1 = new Pair<>("hello", "world");
      Pair<String, String> pair2 = new Pair<>("hello", null);
      assertNotEquals(pair1, pair2);
      assertNotEquals(pair2, pair1);
   }

   @Test
   public void testPairWithLeftNullEqualsSamePair() {
      Pair<String, String> pair1 = new Pair<>(null, "world");
      Pair<String, String> pair2 = new Pair<>(null, "world");
      assertEquals(pair1, pair2);
   }

   @Test
   public void testPairWithRightNullEqualsSamePair() {
      Pair<String, String> pair1 = new Pair<>("hello", null);
      Pair<String, String> pair2 = new Pair<>("hello", null);
      assertEquals(pair1, pair2);
   }

   @Test
   public void testEqualPairsHaveEqualHashCodes() {
      Pair<String, String> pair1 = new Pair<>("hello", "world");
      Pair<String, String> pair2 = new Pair<>("hello", "world");
      assertEquals(pair1.hashCode(), pair2.hashCode());
   }

   @Test
   public void testDifferentPairsHaveDifferentHashCodes() {
      Pair<String, String> pair1 = new Pair<>("hello", "world");
      Pair<String, String> pair2 = new Pair<>("world", "hello");
      assertNotEquals(pair1.hashCode(), pair2.hashCode());
   }

   @Test
   public void testHashCodeWithNulls() {
      Pair<String, String> pair1 = new Pair<>("hello", null);
      Pair<String, String> pair2 = new Pair<>(null, "world");
      assertNotEquals(pair1.hashCode(), pair2.hashCode());
   }

   @Test
   public void testToString() {
      Pair<String, String> pair = new Pair<>("hello", "world");
      assertEquals("(hello, world)", pair.toString());
   }
}
