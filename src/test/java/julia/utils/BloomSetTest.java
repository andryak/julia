package julia.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Test;

public class BloomSetTest {
   private static Set<Integer> makeSet(Integer... integers) {
      return new HashSet<Integer>(Arrays.asList(integers));
   }

   @Test
   public void testEmptyBloomSet() {
      BloomSet<Integer> set = new BloomSet<>();
      List<Set<Integer>> subsets = set.getSubsets(makeSet(1, 2, 3), 10);
      assertEquals(0, subsets.size());
   }

   @Test
   public void testNegativeNumberOfSubsets() {
      BloomSet<Integer> set = new BloomSet<>();
      List<Set<Integer>> subsets = set.getSubsets(makeSet(1, 2, 3), -1);
      assertEquals(0, subsets.size());
   }

   @Test
   public void testGetSomeSubsets() {
      BloomSet<Integer> set = new BloomSet<>();
      Set<Integer> s1 = makeSet(1, 2, 3, 4, 5);
      Set<Integer> s2 = makeSet(1, 2, 3, 4);
      Set<Integer> s3 = makeSet(1, 2, 3);
      Set<Integer> s4 = makeSet(1, 2);
      Set<Integer> s5 = makeSet(1);
      set.put(s1);
      set.put(s2);
      set.put(s3);
      set.put(s4);
      set.put(s5);
      List<Set<Integer>> subsets = set.getSubsets(makeSet(1, 2, 3), 10);
      assertEquals(3, subsets.size());
      assertTrue(subsets.contains(s3));
      assertTrue(subsets.contains(s4));
      assertTrue(subsets.contains(s5));
   }

   @Test
   public void testGetSomeSubsetsWhereMoreAreAvailable() {
      BloomSet<Integer> set = new BloomSet<>();
      set.put(makeSet(1, 2, 3, 4, 5));
      set.put(makeSet(1, 2, 3, 4));
      set.put(makeSet(1, 2, 3));
      set.put(makeSet(1, 2));
      set.put(makeSet(1));
      List<Set<Integer>> subsets = set.getSubsets(makeSet(1, 2, 3), 2);
      assertEquals(2, subsets.size());
   }

   @Test
   public void testGetSubsetsOfDisjointSet() {
      BloomSet<Integer> set = new BloomSet<>();
      set.put(makeSet(1, 2, 3, 4, 5));
      set.put(makeSet(1, 2, 3, 4));
      set.put(makeSet(1, 2, 3));
      set.put(makeSet(1, 2));
      set.put(makeSet(1));
      List<Set<Integer>> subsets = set.getSubsets(makeSet(6, 7, 8), 10);
      assertEquals(0, subsets.size());
   }

   @Test
   public void testGetSubsetsOfEmptySet1() {
      BloomSet<Integer> set = new BloomSet<>();
      set.put(makeSet(1, 2, 3, 4, 5));
      set.put(makeSet(1, 2, 3, 4));
      set.put(makeSet(1, 2, 3));
      set.put(makeSet(1, 2));
      set.put(makeSet(1));
      List<Set<Integer>> subsets = set.getSubsets(makeSet(), 10);
      assertEquals(0, subsets.size());
   }

   @Test
   public void testGetSubsetsOfEmptySet2() {
      BloomSet<Integer> set = new BloomSet<>();
      Set<Integer> s1 = makeSet();
      Set<Integer> s2 = makeSet(1);
      set.put(s1);
      set.put(s2);
      List<Set<Integer>> subsets = set.getSubsets(makeSet(), 10);
      assertEquals(1, subsets.size());
      assertTrue(subsets.contains(s1));
   }

   @Test
   public void testGetSubsetsOfPartiallyOverlappingSet() {
      BloomSet<Integer> set = new BloomSet<>();
      set.put(makeSet(1, 2, 3));
      set.put(makeSet(3, 4, 5));
      List<Set<Integer>> subsets = set.getSubsets(makeSet(2, 3, 4), 10);
      assertEquals(0, subsets.size());
   }
}
