package julia.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class VarRenamerTest {
   @Test
   public void testGetIndexForOfMissingValue() {
      VarRenamer renamer = new VarRenamer();
      assertThat(renamer.getIndexFor("hello"), is(0));
   }

   @Test
   public void testGetIndexForOfPresentValue() {
      VarRenamer renamer = new VarRenamer();
      renamer.getIndexFor("hello");
      renamer.getIndexFor("world");
      assertThat(renamer.getIndexFor("world"), is(1));
   }
}
