package julia.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class RunningStatsTest {
   private final double TOLERANCE = 0.001;

   @Test
   public void testEmptyRunningStats() {
      RunningStats stats = new RunningStats();
      assertEquals(0, stats.count());
      assertEquals(0, stats.mean(), TOLERANCE);
      assertEquals(0, stats.var(), TOLERANCE);
      assertEquals(0, stats.stdev(), TOLERANCE);
      assertEquals(0, stats.min(), TOLERANCE);
      assertEquals(0, stats.max(), TOLERANCE);
   }

   @Test
   public void testRunningStatsWithOneNullDataPoint() {
      RunningStats stats = new RunningStats();
      stats.add(0);

      assertEquals(1, stats.count());
      assertEquals(0, stats.mean(), TOLERANCE);
      assertEquals(0, stats.var(), TOLERANCE);
      assertEquals(0, stats.stdev(), TOLERANCE);
      assertEquals(0, stats.min(), TOLERANCE);
      assertEquals(0, stats.max(), TOLERANCE);
   }

   @Test
   public void testRunningStatsWithOneNonNullDataPoint() {
      RunningStats stats = new RunningStats();
      stats.add(1);

      assertEquals(1, stats.count());
      assertEquals(1, stats.mean(), TOLERANCE);
      assertEquals(0, stats.var(), TOLERANCE);
      assertEquals(0, stats.stdev(), TOLERANCE);
      assertEquals(1, stats.min(), TOLERANCE);
      assertEquals(1, stats.max(), TOLERANCE);
   }

   @Test
   public void testRunningStatsWithTwoDataPoints() {
      RunningStats stats = new RunningStats();
      stats.add(2);
      stats.add(1);

      assertEquals(2, stats.count());
      assertEquals(1.5, stats.mean(), TOLERANCE);
      assertEquals(0.5, stats.var(), TOLERANCE);
      assertEquals(Math.sqrt(0.5), stats.stdev(), TOLERANCE);
      assertEquals(1, stats.min(), TOLERANCE);
      assertEquals(2, stats.max(), TOLERANCE);
   }

   @Test
   public void testRunningStatsWithManyDataPoints() {
      RunningStats stats = new RunningStats();
      stats.add(1);
      stats.add(3);
      stats.add(2);

      assertEquals(3, stats.count());
      assertEquals(2, stats.mean(), TOLERANCE);
      assertEquals(1, stats.var(), TOLERANCE);
      assertEquals(1, stats.stdev(), TOLERANCE);
      assertEquals(1, stats.min(), TOLERANCE);
      assertEquals(3, stats.max(), TOLERANCE);
   }
}
