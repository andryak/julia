package julia.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class CounterTest {
   @Test
   public void testNoDataInEmptyCounter() {
      Counter<String> counter = new Counter<>();
      assertEquals(0, counter.getData().size());
   }

   @Test
   public void testUpdateOneKey() {
      Counter<String> counter = new Counter<>();
      counter.update("hello", 1);
      counter.update("hello", 2);
      counter.update("hello", 3);
      assertEquals(6, counter.get("hello").intValue());
   }

   @Test
   public void testUpdateManyKeys() {
      Counter<String> counter = new Counter<>();
      counter.update("foo", 1);
      counter.update("fie", 1);
      counter.update("bar", 1);
      counter.update("foo", 2);
      counter.update("fie", 2);
      counter.update("foo", 3);
      assertEquals(6, counter.get("foo").intValue());
      assertEquals(3, counter.get("fie").intValue());
      assertEquals(1, counter.get("bar").intValue());
   }

   @Test
   public void testUpdateOneKeyWithDefault() {
      Counter<String> counter = new Counter<>(42);
      counter.update("hello", 1);
      counter.update("hello", 2);
      counter.update("hello", 3);
      assertEquals(48, counter.get("hello").intValue());
   }

   @Test
   public void testIncrementOneKeyOnce() {
      Counter<String> counter = new Counter<>();
      counter.increment("hello");
      assertEquals(1, counter.get("hello").intValue());
   }

   @Test
   public void testIncrementOneKeyThreeTimes() {
      Counter<String> counter = new Counter<>();
      counter.increment("hello");
      counter.increment("hello");
      counter.increment("hello");
      assertEquals(3, counter.get("hello").intValue());
   }

   @Test
   public void testToString() {
      Counter<String> counter = new Counter<>();
      counter.increment("hello");
      counter.increment("world");
      assertTrue(counter.toString().contains("hello"));
      assertTrue(counter.toString().contains("world"));
   }

   @Test
   public void testHashCode() {
      Counter<String> counter1 = new Counter<>();
      Counter<String> counter2 = new Counter<>();
      counter1.increment("hello");
      counter2.increment("hello");
      assertEquals(counter1.hashCode(), counter2.hashCode());
   }

   @Test
   public void testCounterEqualsItself() {
      Counter<String> counter = new Counter<>();
      assertEquals(counter, counter);
   }

   @Test
   public void testCounterNotEqualsNull() {
      Counter<String> counter = new Counter<>();
      assertNotEquals(counter, null);
   }

   @Test
   public void testCounterNotEqualsString() {
      Counter<String> counter = new Counter<>();
      assertNotEquals(counter, "");
   }

   @Test
   public void testCounterEqualsSameCounter() {
      Counter<String> counter1 = new Counter<>();
      Counter<String> counter2 = new Counter<>();
      counter1.increment("hello");
      counter2.increment("hello");
      assertEquals(counter1, counter2);
   }

   @Test
   public void testEmptyCounterNotEqualsFullCounter() {
      Counter<String> empty = new Counter<>();
      Counter<String> full = new Counter<>();
      full.increment("hello");
      assertNotEquals(full, empty);
   }
}
