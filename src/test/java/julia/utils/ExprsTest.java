package julia.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Not;
import julia.expr.ast.bool.Or;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.ints.IntMul;
import julia.expr.ast.ints.IntNeg;
import julia.expr.ast.ints.IntSub;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.usort.USort;
import julia.expr.sort.DeclareSort;
import julia.utils.Exprs.SmtDecl;
import org.junit.Test;

public class ExprsTest {
   @Test
   public void testCreateIntVal() {
      IExpr expr = Exprs.create("42");
      assertEquals(IntVal.create(42), expr);
   }

   @Test
   public void testCreateInt() {
      IExpr expr = Exprs.create("x0");
      assertEquals(Int.create(0), expr);
   }

   @Test
   public void testCreateIntNeg() {
      IExpr expr = Exprs.create("- x0");
      assertEquals(IntNeg.create(Int.create(0)), expr);
   }

   @Test
   public void testCreateIntNegOddChain() {
      IExpr expr = Exprs.create("- - - x0");
      assertEquals(IntNeg.create(Int.create(0)), expr);
   }

   @Test
   public void testCreateIntNegEvenChain() {
      IExpr expr = Exprs.create("- - - - x0");
      assertEquals(Int.create(0), expr);
   }

   @Test
   public void testCreateIntAdd() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun z = Int.create(2);
      IExpr expr = Exprs.create("x0 + x1 + x2");
      assertEquals(IntAdd.create(x, y, z), expr);
   }

   @Test
   public void testCreateIntSub() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun z = Int.create(2);
      IExpr expr = Exprs.create("x0 - x1 - x2");
      assertEquals(IntSub.create(x, y, z), expr);
   }

   @Test
   public void testCreateIntMul() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun z = Int.create(2);
      IExpr expr = Exprs.create("x0 * x1 * x2");
      assertEquals(IntMul.create(x, y, z), expr);
   }

   @Test
   public void testCreateOpPrecedence1() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun z = Int.create(2);
      IExpr expr = Exprs.create("x0 + x1 * x2");
      assertEquals(IntAdd.create(x, IntMul.create(y, z)), expr);
   }

   @Test
   public void testCreateOpPrecedence2() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun z = Int.create(2);
      IExpr expr = Exprs.create("x0 * x1 + x2");
      assertEquals(IntAdd.create(IntMul.create(x, y), z), expr);
   }

   @Test
   public void testCreateWithParents() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun z = Int.create(2);
      IExpr expr = Exprs.create("(x0 + x1) * x2");
      assertEquals(IntMul.create(IntAdd.create(x, y), z), expr);
   }

   @Test(expected = IllegalArgumentException.class)
   public void testCreateWrongExpr() {
      Exprs.create("x0 + * x1");
   }

   @Test
   public void testGetConjunctsOfAnd() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);

      IExpr c1 = A;
      IExpr c2 = Not.create(B);
      IExpr c3 = Or.create(A, B);

      IExpr expr = And.create(c1, c2, c3);
      Set<IExpr> conjuncts = Exprs.getConjuncts(expr);

      assertEquals(3, conjuncts.size());
      assertTrue(conjuncts.contains(c1));
      assertTrue(conjuncts.contains(c2));
      assertTrue(conjuncts.contains(c3));
   }

   @Test
   public void testGetConjunctsOfOr() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);

      IExpr expr = Or.create(A, B);
      Set<IExpr> conjuncts = Exprs.getConjuncts(expr);

      assertEquals(1, conjuncts.size());
      assertTrue(conjuncts.contains(expr));
   }

   @Test
   public void testCast() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IntAdd expr = Exprs.cast(IntAdd.create(x, y), IntAdd.class);
      assertNotNull(expr);
   }

   @Test
   public void testWrongCast() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      And expr = Exprs.cast(IntAdd.create(x, y), And.class);
      assertNull(expr);
   }

   @Test
   public void testBooleanToExpr1() {
      assertEquals(BoolVal.TRUE, Exprs.toExpr(true));
   }

   @Test
   public void testBooleanToExpr2() {
      assertEquals(BoolVal.FALSE, Exprs.toExpr(false));
   }

   @Test
   public void testIntegerToExpr1() {
      assertEquals(IntVal.ZERO, Exprs.toExpr(0));
   }

   @Test
   public void testIntegerToExpr2() {
      assertEquals(IntVal.ONE, Exprs.toExpr(1));
   }

   @Test
   public void testIntegerToExpr3() {
      assertEquals(IntVal.TWO, Exprs.toExpr(2));
   }

   @Test
   public void testFloatToExpr1() {
      assertEquals(RealVal.ZERO, Exprs.toExpr(0f));
   }

   @Test
   public void testFloatToExpr2() {
      assertEquals(RealVal.ONE, Exprs.toExpr(1f));
   }

   @Test
   public void testFloatToExpr3() {
      assertEquals(RealVal.TWO, Exprs.toExpr(2f));
   }

   @Test
   public void testDoubleToExpr1() {
      assertEquals(RealVal.ZERO, Exprs.toExpr(0d));
   }

   @Test
   public void testDoubleToExpr2() {
      assertEquals(RealVal.ONE, Exprs.toExpr(1d));
   }

   @Test
   public void testDoubleToExpr3() {
      assertEquals(RealVal.TWO, Exprs.toExpr(2d));
   }

   @Test
   public void testToSmtDeclGetFullQuery() {
      Fun a1 = USort.create(0, DeclareSort.A);
      Fun a2 = USort.create(1, DeclareSort.A);
      IExpr expr = Equal.create(a1, a2);

      SmtDecl decl = Exprs.toSmtDecl(expr);
      String[] parts = decl.getFullQuery().split("\n");

      assertEquals(4, parts.length);
      assertEquals("(declare-sort A)", parts[0]);
      assertTrue(parts[1].matches("\\(declare-fun [^ ]+ \\(\\) A\\)"));
      assertTrue(parts[2].matches("\\(declare-fun [^ ]+ \\(\\) A\\)"));
      assertTrue(parts[3].matches("\\(assert \\(= [^ ]+ [^)]+\\)\\)"));
   }

   @Test
   public void testToSmtDeclWithEmptySortDecls() {
      Fun x = Int.create(0);
      SmtDecl decl = Exprs.toSmtDecl(x);
      assertEquals(0, decl.getSortDecls().size());
   }

   @Test
   public void testToSmtDeclGetSortDecls() {
      Fun a1 = USort.create(0, DeclareSort.A);
      Fun a2 = USort.create(1, DeclareSort.A);
      IExpr expr = Equal.create(a1, a2);

      SmtDecl decl = Exprs.toSmtDecl(expr);
      assertEquals(1, decl.getSortDecls().size());
      assertTrue(decl.getSortDecls().contains("(declare-sort A)"));
   }

   @Test
   public void testToSmtDeclGetFunDecls() {
      Fun a1 = USort.create(0, DeclareSort.A);
      Fun a2 = USort.create(1, DeclareSort.A);
      IExpr expr = Equal.create(a1, a2);

      SmtDecl decl = Exprs.toSmtDecl(expr);
      assertEquals(2, decl.getFunDecls().size());

      List<String> decls = new ArrayList<>(decl.getFunDecls());
      assertTrue(decls.get(0).matches("\\(declare-fun [^ ]+ \\(\\) A\\)"));
      assertTrue(decls.get(1).matches("\\(declare-fun [^ ]+ \\(\\) A\\)"));
   }
}
