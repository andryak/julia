package julia.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class StringUtilsTest {
   @Test
   public void testHammingHead1() {
      assertEquals(0, StringUtils.hammingHead("ab", "abc"));
   }

   @Test
   public void testHammingHead2() {
      assertEquals(0, StringUtils.hammingHead("abc", "ab"));
   }

   @Test
   public void testHammingHead3() {
      assertEquals(1, StringUtils.hammingHead("ab", "aa"));
   }

   @Test
   public void testHammingHead4() {
      assertEquals(1, StringUtils.hammingHead("ab", "bb"));
   }

   @Test
   public void testHammingHead5() {
      assertEquals(2, StringUtils.hammingHead("ab", "ba"));
   }

   @Test
   public void testHammingTail1() {
      assertEquals(0, StringUtils.hammingTail("bc", "abc"));
   }

   @Test
   public void testHammingTail2() {
      assertEquals(0, StringUtils.hammingTail("abc", "bc"));
   }

   @Test
   public void testHammingTail3() {
      assertEquals(1, StringUtils.hammingTail("ab", "aa"));
   }

   @Test
   public void testHammingTail4() {
      assertEquals(1, StringUtils.hammingTail("ab", "bb"));
   }

   @Test
   public void testHammingTail5() {
      assertEquals(2, StringUtils.hammingTail("ab", "ba"));
   }
}
