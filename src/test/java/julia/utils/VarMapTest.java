package julia.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import julia.expr.ast.bool.Bool;
import org.junit.Test;

public class VarMapTest {
   @Test
   public void testEmptyMap() {
      VarMap map = new VarMap();
      assertEquals(0, map.size());
   }

   @Test
   public void testPut() {
      VarMap map = new VarMap();
      map.put(Bool.create(0), 1);
      map.put(Bool.create(1), 0);
      assertEquals(2, map.size());
   }

   @Test
   public void testGetIndexForOfMissingValue() {
      VarMap map = new VarMap();
      assertNull(map.getIndexFor(Bool.create(0)));
   }

   @Test
   public void testGetIndexForOfPresentValue() {
      VarMap map = new VarMap();
      map.put(Bool.create(0), 1);
      map.put(Bool.create(1), 0);
      assertThat(map.getIndexFor(Bool.create(1)), is(0));
   }
}
