package julia.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Z3StrUtilsTest {
   @Test
   public void testEscape1() {
      assertEquals("", Z3StrUtils.escape(""));
   }

   @Test
   public void testEscape2() {
      assertEquals("test", Z3StrUtils.escape("test"));
   }

   @Test
   public void testEscape3() {
      assertEquals("-\\\\-\\\"-", Z3StrUtils.escape("-\\-\"-"));
   }

   @Test
   public void testUnescape1() {
      assertEquals("", Z3StrUtils.unescape(""));
   }

   @Test
   public void testUnescape2() {
      assertEquals("test", Z3StrUtils.unescape("test"));
   }

   @Test
   public void testUnescape3() {
      assertEquals("-\\-\"-", Z3StrUtils.unescape("-\\\\-\\\"-"));
   }
}
