package julia.utils;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TimerTest {
   @Test
   public void testElapsedTimeIsNonNegative() {
      Timer timer = new Timer();
      timer.start();
      long elapsed = timer.stop();
      assertTrue(elapsed >= 0);
   }

   @Test(expected = IllegalStateException.class)
   public void testStopWithoutStart() {
      Timer timer = new Timer();
      timer.stop();
   }
}
