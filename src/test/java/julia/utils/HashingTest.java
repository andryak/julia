package julia.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

public class HashingTest {
   @Test
   public void testCombineOneValue() {
      String s = "Hello, world!";
      assertEquals(s.hashCode(), Hashing.combine(s));
   }

   @Test
   public void testCombineManyValues() {
      String s1 = "Hello";
      String s2 = "World";
      assertNotEquals(s1.hashCode(), Hashing.combine(s1, s2));
      assertNotEquals(s2.hashCode(), Hashing.combine(s1, s2));
   }

   @Test
   public void testHash32OfOneIsNotOne() {
      assertNotEquals(1, Hashing.hash32(1));
   }
}
