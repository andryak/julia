package julia.cache.cex;

import static org.junit.Assert.assertEquals;
import java.util.HashSet;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.Bool;
import org.junit.Test;

public class CexCacheTest {
   private static Set<IExpr> varSet(int... numbers) {
      Set<IExpr> result = new HashSet<>();
      for (int n : numbers) {
         result.add(Bool.create(n));
      }
      return result;
   }

   @Test
   public void testGetZeroSubsets() {
      CexCache cache = new CexCache();
      cache.put(varSet(1));
      assertEquals(0, cache.getSubsets(varSet(1), 0).size());
   }

   @Test
   public void testGetOneSubset() {
      CexCache cache = new CexCache();
      cache.put(varSet(1));
      cache.put(varSet(2));
      assertEquals(1, cache.getSubsets(varSet(1, 2), 1).size());
   }

   @Test
   public void testGetAllSubsets() {
      CexCache cache = new CexCache();
      cache.put(varSet(1));
      cache.put(varSet(2));
      assertEquals(2, cache.getSubsets(varSet(1, 2), 2).size());
   }

   @Test
   public void testGetSubsetsWithSomeOverlap() {
      CexCache cache = new CexCache();
      cache.put(varSet(1));
      cache.put(varSet(2));
      cache.put(varSet(3));
      assertEquals(2, cache.getSubsets(varSet(1, 2), 3).size());
   }

   @Test
   public void testGetSubsetsWithNoOverlap() {
      CexCache cache = new CexCache();
      cache.put(varSet(1));
      cache.put(varSet(2));
      cache.put(varSet(1, 2));
      assertEquals(0, cache.getSubsets(varSet(3), 3).size());
   }
}
