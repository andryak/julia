package julia.cache.satdelta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.stream.Collectors;
import julia.expr.model.SortModel;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.expr.utils.BigRational;
import julia.utils.Counter;
import org.junit.Test;

public class SatDeltaCacheTest {
   /**
    * Returns a fake entry representing an expression with no variables.
    * 
    * @param score
    *           the score of the entry.
    * @return a fake entry representing an expression with no variables.
    */
   private static CacheEntry fakeEntry(int score) {
      return fakeEntryWithVars(score, 0);
   }

   /**
    * Returns a fake entry representing an expression with some variables.
    * 
    * @param score
    *           the score of the entry.
    * @param var
    *           the number of variables of the entry.
    * @return a fake entry representing an expression with some variables.
    */
   private static CacheEntry fakeEntryWithVars(int score, int vars) {
      Counter<ExprSort> counter = new Counter<>();
      if (vars > 0) {
         counter.update(IntSort.create(), vars);
      }
      return new CacheEntry(BigRational.create(score), counter, new SortModel());
   }

   /**
    * Syntactic sugar for cache.filter.
    */
   private static List<CacheEntry> filter(SatDeltaCache cache, int score, int k) {
      return filter(cache, score, 0, k);
   }

   /**
    * Syntactic sugar for cache.filter.
    */
   private static List<CacheEntry> filter(SatDeltaCache cache, int score, int vars, int k) {
      Counter<ExprSort> counter = new Counter<>();
      if (vars > 0) {
         counter.update(IntSort.create(), vars);
      }
      return cache.filter(BigRational.create(score), counter, k);
   }

   /**
    * Returns the list of the scores of the given entries.
    * 
    * @return the list of the scores of the given entries.
    */
   private static List<BigRational> scores(List<CacheEntry> entries) {
      return entries.stream().map(e -> e.getScore()).collect(Collectors.toList());
   }

   @Test
   public void testGetSize() {
      SatDeltaCache cache = new SatDeltaCache();
      assertEquals(0, cache.size());
   }

   @Test
   public void testFilterWithNegK() {
      SatDeltaCache cache = new SatDeltaCache();
      assertEquals(0, filter(cache, 4, -5).size());
   }

   @Test
   public void testFilterOnEmptyCache() {
      SatDeltaCache cache = new SatDeltaCache();
      assertEquals(0, filter(cache, 4, 3).size());
   }

   @Test
   public void testFilterTopTwoWithoutTies() {
      SatDeltaCache cache = new SatDeltaCache();
      cache.store(fakeEntry(0));
      cache.store(fakeEntry(1));
      cache.store(fakeEntry(5));
      cache.store(fakeEntry(6));
      cache.store(fakeEntry(9));
      List<BigRational> scores = scores(filter(cache, 4, 2));
      assertEquals(2, scores.size());
      assertEquals(BigRational.create(5), scores.get(0));
      assertEquals(BigRational.create(6), scores.get(1));
   }

   @Test
   public void testFilterTopThreeWithoutTies1() {
      SatDeltaCache cache = new SatDeltaCache();
      cache.store(fakeEntry(1));
      cache.store(fakeEntry(3));
      cache.store(fakeEntry(5));
      cache.store(fakeEntry(7));
      cache.store(fakeEntry(9));
      List<BigRational> scores = scores(filter(cache, 0, 3));
      assertEquals(3, scores.size());
      assertEquals(BigRational.create(1), scores.get(0));
      assertEquals(BigRational.create(3), scores.get(1));
      assertEquals(BigRational.create(5), scores.get(2));
   }

   @Test
   public void testFilterTopThreeWithoutTies2() {
      SatDeltaCache cache = new SatDeltaCache();
      cache.store(fakeEntry(1));
      cache.store(fakeEntry(3));
      cache.store(fakeEntry(5));
      cache.store(fakeEntry(7));
      cache.store(fakeEntry(9));
      List<BigRational> scores = scores(filter(cache, 10, 3));
      assertEquals(3, scores.size());
      assertEquals(BigRational.create(9), scores.get(0));
      assertEquals(BigRational.create(7), scores.get(1));
      assertEquals(BigRational.create(5), scores.get(2));
   }

   @Test
   public void testFilterTopThreeWithTies() {
      SatDeltaCache cache = new SatDeltaCache();
      cache.store(fakeEntry(1));
      cache.store(fakeEntry(3));
      cache.store(fakeEntry(5));
      cache.store(fakeEntry(7));
      cache.store(fakeEntry(9));
      List<BigRational> scores = scores(filter(cache, 5, 3));
      assertEquals(3, scores.size());
      assertEquals(BigRational.create(5), scores.get(0));
      assertTrue(scores.contains(BigRational.create(3)));
      assertTrue(scores.contains(BigRational.create(7)));
   }

   @Test
   public void testFilterTooManyEntries() {
      SatDeltaCache cache = new SatDeltaCache();
      cache.store(fakeEntry(0));
      cache.store(fakeEntry(1));
      cache.store(fakeEntry(5));
      cache.store(fakeEntry(6));
      cache.store(fakeEntry(9));
      List<BigRational> scores = scores(filter(cache, 0, 1000));
      assertEquals(5, scores.size());
      assertEquals(BigRational.create(0), scores.get(0));
      assertEquals(BigRational.create(1), scores.get(1));
      assertEquals(BigRational.create(5), scores.get(2));
      assertEquals(BigRational.create(6), scores.get(3));
      assertEquals(BigRational.create(9), scores.get(4));
   }

   @Test
   public void testFilterTopTreeSkipsIncompatibleEntries() {
      SatDeltaCache cache = new SatDeltaCache();
      cache.store(fakeEntryWithVars(0, 1));
      cache.store(fakeEntryWithVars(1, 1));
      cache.store(fakeEntryWithVars(5, 1));
      cache.store(fakeEntry(6));
      cache.store(fakeEntryWithVars(9, 1));
      List<BigRational> scores = scores(filter(cache, 4, 1, 3));
      assertEquals(3, scores.size());
      assertEquals(BigRational.create(5), scores.get(0));
      assertEquals(BigRational.create(1), scores.get(1));
      assertEquals(BigRational.create(0), scores.get(2));
   }
}
