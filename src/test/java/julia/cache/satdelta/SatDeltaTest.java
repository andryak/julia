package julia.cache.satdelta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import julia.expr.utils.BigRational;
import org.junit.Test;

public class SatDeltaTest {
   @Test
   public void testGetDir() {
      SatDelta delta = new SatDelta(0, 1);
      assertEquals(BigRational.ZERO, delta.dir());
   }

   @Test
   public void testGetInv() {
      SatDelta delta = new SatDelta(0, 1);
      assertEquals(BigRational.ONE, delta.inv());
   }

   @Test
   public void testHashCode() {
      SatDelta delta1 = new SatDelta(0, 1);
      SatDelta delta2 = new SatDelta(0, 1);
      assertEquals(delta1.hashCode(), delta2.hashCode());
   }

   @Test
   public void testDeltaEqualsSelf() {
      SatDelta delta = new SatDelta(0, 1);
      assertEquals(delta, delta);
   }

   @Test
   public void testDeltaNotEqualsNull() {
      SatDelta delta = new SatDelta(0, 1);
      assertNotEquals(delta, null);
   }

   @Test
   public void testDeltaNotEqualsInteger() {
      SatDelta delta = new SatDelta(0, 1);
      assertNotEquals(delta, new Integer(0));
   }

   @Test
   public void testDeltaEqualsSameDelta() {
      SatDelta delta1 = new SatDelta(0, 1);
      SatDelta delta2 = new SatDelta(0, 1);
      assertEquals(delta1, delta2);
   }

   @Test
   public void testNotEquals() {
      SatDelta delta1 = new SatDelta(0, 1);
      SatDelta delta2 = new SatDelta(1, 0);
      assertNotEquals(delta1, delta2);
   }

   @Test
   public void testToString() {
      SatDelta delta = new SatDelta(0, 1);
      assertEquals("SatDelta(0, 1)", delta.toString());
   }
}
