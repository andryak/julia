package julia.cache.satdelta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.model.Model;
import julia.expr.model.SortModel;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BigRational;
import julia.utils.Counter;
import org.junit.Test;

public class CacheEntryTest {
   @Test
   public void testGetScore() {
      CacheEntry entry = new CacheEntry(BigRational.ONE, new Counter<>(), new SortModel());
      assertEquals(BigRational.ONE, entry.getScore());
   }

   @Test
   public void testGetVars() {
      Counter<ExprSort> counter = new Counter<>();
      CacheEntry entry = new CacheEntry(BigRational.ONE, counter, new SortModel());
      assertEquals(counter, entry.getVars());
   }

   @Test
   public void testGetModel() {
      Model model = new SortModel();
      CacheEntry entry = new CacheEntry(BigRational.ONE, new Counter<>(), model);
      assertEquals(model, entry.getModel());
   }

   @Test
   public void testToString() {
      CacheEntry entry = new CacheEntry(BigRational.ONE, new Counter<>(), new SortModel());
      assertTrue(entry.toString().contains("1"));
   }
}
