package julia.solver.utopia;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.List;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.ints.IntVal;
import julia.expr.model.Model;
import julia.expr.model.SortModel;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.dummy.DummyUnknownSolver;
import julia.utils.Exprs;
import org.junit.Test;

public class UtopiaSolverTest {
   private static List<Model> REF_MODELS;

   private static Solver SOLVER = new DummyUnknownSolver();

   static {
      SortModel model1 = new SortModel();
      SortModel model2 = new SortModel();
      model1.setIntInter(IntVal.ZERO);
      model2.setIntInter(IntVal.ONE);
      REF_MODELS = Arrays.asList(model1, model2);
   }

   @Test
   public void testConstructorPositiveNrCandidates() {
      UtopiaSolver cache = new UtopiaSolver(REF_MODELS, SOLVER, 10);
      assertEquals(10, cache.getNrCandidates());
   }

   @Test
   public void testConstructorNegativeNrCandidates() {
      UtopiaSolver cache = new UtopiaSolver(REF_MODELS, SOLVER, -10);
      assertEquals(0, cache.getNrCandidates());
   }

   @Test
   public void testConstructorZeroNrCandidates() {
      UtopiaSolver cache = new UtopiaSolver(REF_MODELS, SOLVER, 0);
      assertEquals(0, cache.getNrCandidates());
   }

   @Test
   public void testCacheOnConstExpr() {
      UtopiaSolver cache = new UtopiaSolver(REF_MODELS, SOLVER, 10);
      assertEquals(CheckResult.UNSAT, cache.check(BoolVal.FALSE));
      assertEquals(CheckResult.SAT, cache.check(BoolVal.TRUE));
   }

   @Test
   public void testExprWithZeroSatDelta() {
      UtopiaSolver cache = new UtopiaSolver(REF_MODELS, SOLVER, 10);
      assertEquals(CheckResult.SAT, cache.check(Exprs.create("x0 = 0")));
      assertEquals(CheckResult.SAT, cache.check(Exprs.create("x0 = 1")));
   }

   @Test
   public void testExprWithNonZeroSatDelta() {
      UtopiaSolver cache = new UtopiaSolver(REF_MODELS, SOLVER, 10);
      assertEquals(CheckResult.UNKNOWN, cache.check(Exprs.create("x0 = 2")));
      assertEquals(CheckResult.UNKNOWN, cache.check(Exprs.create("x0 = 3")));
   }
}
