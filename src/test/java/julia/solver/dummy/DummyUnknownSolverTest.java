package julia.solver.dummy;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.exc.NoModelException;
import julia.solver.exc.NoUnsatCoreException;
import org.junit.Test;

public class DummyUnknownSolverTest {
   private static Solver solver = new DummyUnknownSolver();

   @Test
   public void testCheckSatExpr() {
      Fun x = Int.create(0);
      IExpr expr = IntLt.create(x, IntVal.ZERO);
      assertEquals(CheckResult.UNKNOWN, solver.check(expr));
   }

   @Test
   public void testCheckUnsatExpr() {
      Fun x = Int.create(0);
      IExpr expr1 = IntLt.create(x, IntVal.ZERO);
      IExpr expr2 = IntGt.create(x, IntVal.ZERO);
      assertEquals(CheckResult.UNKNOWN, solver.check(And.create(expr1, expr2)));
   }

   @Test(expected = NoModelException.class)
   public void testGetModel() throws NoModelException {
      solver.getModel();
   }

   @Test(expected = NoUnsatCoreException.class)
   public void testGetUnsatCore() throws NoUnsatCoreException {
      solver.getUnsatCore();
   }
}
