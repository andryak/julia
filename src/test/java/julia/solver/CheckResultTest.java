package julia.solver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import julia.solver.CheckResult;
import org.junit.Test;

public class CheckResultTest {
   @Test
   public void testCheckResultValues() {
      List<CheckResult> values = new ArrayList<>();
      for (final CheckResult cr : CheckResult.values()) {
         values.add(cr);
      }

      assertEquals(3, values.size());
      assertTrue(values.contains(CheckResult.SAT));
      assertTrue(values.contains(CheckResult.UNSAT));
      assertTrue(values.contains(CheckResult.UNKNOWN));
   }

   @Test
   public void testCheckResultValueOf() {
      assertEquals(CheckResult.SAT, CheckResult.valueOf("SAT"));
      assertEquals(CheckResult.UNSAT, CheckResult.valueOf("UNSAT"));
      assertEquals(CheckResult.UNKNOWN, CheckResult.valueOf("UNKNOWN"));
   }

   @Test(expected = IllegalArgumentException.class)
   public void testCheckResultValueOfWrongString() {
      CheckResult.valueOf("A value that does not exists");
   }
}
