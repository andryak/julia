package julia.solver.z3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.array.Array;
import julia.expr.ast.array.ArraySelect;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Distinct;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Not;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecAdd;
import julia.expr.ast.bv.BitVecUgt;
import julia.expr.ast.bv.BitVecUlt;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealGt;
import julia.expr.ast.reals.RealLt;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.reals_ints.ToInt;
import julia.expr.ast.str.Str;
import julia.expr.ast.str.StrConcat;
import julia.expr.ast.str.StrLen;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.uf.FunApp;
import julia.expr.ast.usort.USort;
import julia.expr.model.Model;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.DeclareSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.FunSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.exc.NoModelException;
import julia.solver.exc.NoUnsatCoreException;
import org.junit.BeforeClass;
import org.junit.Test;

public class Z3SolverTest {
   private static String Z3 = null;

   private static Solver getSolverFor(String logic) {
      Z3Solver solver = new Z3Solver(Z3);
      try {
         solver.setLogic(logic);
      } catch (IOException e) {
         fail(String.format("Cannot set logic to %s.", logic));
      }
      return solver;
   }

   private static Model getModelOrNull(Solver solver) {
      try {
         return solver.getModel();
      } catch (NoModelException e) {
         return null;
      }
   }

   private static Set<IExpr> getCoreOrNull(Solver solver) {
      try {
         return solver.getUnsatCore();
      } catch (NoUnsatCoreException e) {
         return null;
      }
   }

   @BeforeClass
   public static void setup() {
      // Check that Microsoft Z3 exists and is executable.
      File config = new File("./src/main/resources/config.properties");

      try (FileReader reader = new FileReader(config)) {
         Properties props = new Properties();
         props.load(reader);

         Z3 = props.getProperty("z3");
         File z3file = new File(Z3);

         assertTrue("Cannot find Z3 binary at " + Z3, z3file.exists());
         assertTrue("Z3 binary is not executable", z3file.canExecute());
      } catch (FileNotFoundException e) {
         fail("Configuration file not found.");
      } catch (IOException e) {
         fail(e.getMessage());
      }
   }

   @Test
   public void testSetLogic() {
      Z3Solver solver = new Z3Solver(Z3);
      try {
         solver.setLogic("QF_LIA");
      } catch (IOException e) {
         fail("Cannot set logic to QF_LIA");
      }
   }

   @Test
   public void testCheckSatIntExpr() {
      Fun x = Int.create(0);
      IExpr expr = IntLt.create(x, IntVal.ZERO);

      Z3Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr c = model.getValueFor(x);
      assertEquals(BoolVal.TRUE, IntLt.create(c, IntVal.ZERO));
   }

   @Test
   public void testCheckSatRealExpr() {
      Fun x = Real.create(0);
      IExpr expr = And.create(RealGt.create(x, RealVal.ZERO), RealLt.create(x, RealVal.ONE));

      Z3Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr c = model.getValueFor(x);
      assertEquals(BoolVal.TRUE, RealGt.create(c, RealVal.ZERO));
      assertEquals(BoolVal.TRUE, RealLt.create(c, RealVal.ONE));
   }

   @Test
   public void testCheckSatMixedExpr() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr clause1 = IntLt.create(x, IntVal.ZERO);
      IExpr clause2 = IntGt.create(y, IntVal.ZERO);
      IExpr expr = And.create(clause1, clause2, A, Not.create(B));

      Z3Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr c0 = model.getValueFor(x);
      IExpr c1 = model.getValueFor(y);
      IConst c2 = model.getValueFor(A);
      IConst c3 = model.getValueFor(B);
      assertEquals(BoolVal.TRUE, IntLt.create(c0, IntVal.ZERO));
      assertEquals(BoolVal.TRUE, IntGt.create(c1, IntVal.ZERO));
      assertEquals(BoolVal.TRUE, c2);
      assertEquals(BoolVal.FALSE, c3);
   }

   @Test
   public void testCheckUnsatIntExpr() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr clause1 = IntLt.create(x, IntVal.ZERO);
      IExpr clause2 = IntGt.create(x, IntVal.ZERO);
      IExpr clause3 = IntLt.create(y, IntVal.ZERO);
      IExpr expr = And.create(clause1, clause2, clause3);

      Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.UNSAT, status);

      Set<IExpr> unsatCore = getCoreOrNull(solver);
      assertNotNull(unsatCore);

      assertTrue(unsatCore.contains(clause1));
      assertTrue(unsatCore.contains(clause2));
   }

   @Test
   public void testCheckUnsatRealExpr() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr clause1 = RealLt.create(x, RealVal.ZERO);
      IExpr clause2 = RealGt.create(x, RealVal.ZERO);
      IExpr clause3 = RealLt.create(y, RealVal.ZERO);
      IExpr expr = And.create(clause1, clause2, clause3);

      Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.UNSAT, status);

      Set<IExpr> unsatCore = getCoreOrNull(solver);
      assertNotNull(unsatCore);

      assertTrue(unsatCore.contains(clause1));
      assertTrue(unsatCore.contains(clause2));
   }

   @Test
   public void testCheckSatRealsIntsExpr() {
      Fun x = Real.create(0);
      Fun y = Int.create(0);
      IExpr expr = IntLt.create(ToInt.create(x), y);

      Z3Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr c0 = model.getValueFor(x);
      IExpr c1 = model.getValueFor(y);
      assertEquals(BoolVal.TRUE, IntLt.create(ToInt.create(c0), c1));
   }

   @Test
   public void testCheckSatBitVecExpr() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecUlt.create(BitVecAdd.create(x, BitVecVal.create("#b0001")), y);

      Solver solver = getSolverFor("QF_BV");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr c0 = model.getValueFor(x);
      IExpr c1 = model.getValueFor(y);
      IExpr cexpr = BitVecUlt.create(BitVecAdd.create(c0, BitVecVal.create("#b0001")), c1);
      assertEquals(BoolVal.TRUE, cexpr);
   }

   @Test
   public void testCheckBoolArrayExpr() {
      Fun array = Array.create(0, ArraySort.INT_BOOL);
      IExpr expr1 = Equal.create(ArraySelect.create(array, IntVal.ZERO), BoolVal.TRUE);
      IExpr expr2 = Equal.create(ArraySelect.create(array, IntVal.ONE), BoolVal.FALSE);
      IExpr expr = And.create(expr1, expr2);

      Solver solver = getSolverFor("QF_ALIA");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr carray = model.getValueFor(array);
      assertEquals(BoolVal.TRUE, ArraySelect.create(carray, IntVal.ZERO));
      assertEquals(BoolVal.FALSE, ArraySelect.create(carray, IntVal.ONE));
   }

   @Test
   public void testCheckIntArrayExpr() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      IExpr expr1 = Equal.create(ArraySelect.create(array, IntVal.ZERO), IntVal.create(42));
      IExpr expr2 = Equal.create(ArraySelect.create(array, IntVal.ONE), IntVal.create(10));
      IExpr expr = And.create(expr1, expr2);

      Solver solver = getSolverFor("QF_ALIA");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr carray = model.getValueFor(array);
      assertEquals(IntVal.create(42), ArraySelect.create(carray, IntVal.ZERO));
      assertEquals(IntVal.create(10), ArraySelect.create(carray, IntVal.ONE));
   }

   @Test
   public void testCheckBitVecArrayExpr() {
      final BitVecVal ZERO = BitVecVal.create("#b0000");
      final BitVecVal ONE = BitVecVal.create("#b0001");
      final BitVecVal TWO = BitVecVal.create("#b0010");

      Fun array = Array.create(0, BitVecSort.SIZE4, BitVecSort.SIZE4);
      IExpr expr1 = BitVecUlt.create(ArraySelect.create(array, ZERO), TWO);
      IExpr expr2 = BitVecUgt.create(ArraySelect.create(array, ONE), TWO);
      IExpr expr = And.create(expr1, expr2);

      Solver solver = getSolverFor("QF_ABV");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr carray = model.getValueFor(array);
      assertEquals(BoolVal.TRUE, BitVecUlt.create(ArraySelect.create(carray, ZERO), TWO));
      assertEquals(BoolVal.TRUE, BitVecUgt.create(ArraySelect.create(carray, ONE), TWO));
   }

   @Test
   public void testCheckSatUSortExpr1() {
      Fun a = USort.create(0, DeclareSort.A);
      Fun b = USort.create(1, DeclareSort.A);
      IExpr expr = Equal.create(a, b);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      assertEquals(model.getValueFor(a), model.getValueFor(b));
   }

   @Test
   public void testCheckSatUSortExpr2() {
      Fun a = USort.create(0, DeclareSort.A);
      Fun b = USort.create(1, DeclareSort.A);
      IExpr expr = Distinct.create(a, b);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      assertNotEquals(model.getValueFor(a), model.getValueFor(b));
   }

   @Test
   public void testCheckSatMixedUSortExpr() {
      Fun a1 = USort.create(0, DeclareSort.A);
      Fun a2 = USort.create(1, DeclareSort.A);
      Fun b1 = USort.create(2, DeclareSort.B);
      Fun b2 = USort.create(3, DeclareSort.B);
      IExpr expr1 = Distinct.create(a1, a2);
      IExpr expr2 = Equal.create(b1, b2);
      IExpr expr = And.create(expr1, expr2);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      assertNotEquals(model.getValueFor(a1), model.getValueFor(a2));
      assertEquals(model.getValueFor(b1), model.getValueFor(b2));
   }

   @Test
   public void testCheckUnsatUSortExpr() {
      Fun a1 = USort.create(0, DeclareSort.A);
      Fun a2 = USort.create(1, DeclareSort.A);
      Fun a3 = USort.create(2, DeclareSort.A);
      IExpr clause1 = Equal.create(a1, a2);
      IExpr clause2 = Equal.create(a2, a3);
      IExpr clause3 = Distinct.create(a1, a3);
      IExpr expr = And.create(clause1, clause2, clause3);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.UNSAT, status);

      Set<IExpr> unsatCore = getCoreOrNull(solver);
      assertNotNull(unsatCore);

      assertTrue(unsatCore.contains(clause1));
      assertTrue(unsatCore.contains(clause2));
      assertTrue(unsatCore.contains(clause3));
   }

   @Test
   public void testCheckSatFunExpr1() {
      Fun a = USort.create(0, DeclareSort.A);
      Fun b = USort.create(1, DeclareSort.A);
      Fun f = Fun.createHomogenous(0, 2, DeclareSort.A);

      IExpr expr1 = Distinct.create(a, b);
      IExpr expr2 = Equal.create(FunApp.create(f, a, a), a);
      IExpr expr3 = Equal.create(FunApp.create(f, a, b), b);
      IExpr expr4 = Equal.create(FunApp.create(f, b, a), a);
      IExpr expr5 = Equal.create(FunApp.create(f, b, b), b);
      IExpr expr = And.create(expr1, expr2, expr3, expr4, expr5);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr fval = model.getValueFor(f);
      IExpr aval = model.getValueFor(a);
      IExpr bval = model.getValueFor(b);
      assertNotEquals(aval, bval);
      assertEquals(aval, FunApp.create(fval, aval, aval));
      assertEquals(bval, FunApp.create(fval, aval, bval));
      assertEquals(aval, FunApp.create(fval, bval, aval));
      assertEquals(bval, FunApp.create(fval, bval, bval));
   }

   @Test
   public void testCheckSatFunExpr2() {
      final DeclareSort A = DeclareSort.A;
      final DeclareSort B = DeclareSort.B;
      final DeclareSort C = DeclareSort.C;

      Fun a = USort.create(0, A);
      Fun b = USort.create(1, B);
      Fun c = USort.create(2, C);
      Fun f = Fun.create(0, FunSort.create(Arrays.asList(A, B), C));
      IExpr expr = Equal.create(FunApp.create(f, a, b), c);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr fval = model.getValueFor(f);
      IExpr aval = model.getValueFor(a);
      IExpr bval = model.getValueFor(b);
      IExpr cval = model.getValueFor(c);
      assertEquals(cval, FunApp.create(fval, aval, bval));
   }

   @Test
   public void testCheckSatFunExpr3() {
      final ExprSort I = IntSort.create();
      final ExprSort BV4 = BitVecSort.create(4);
      final ExprSort R = RealSort.create();

      Fun x = Int.create(0);
      Fun y = BitVec.create(0, 4);
      Fun z = Real.create(0);
      Fun f = Fun.create(0, FunSort.create(Arrays.asList(I, BV4), R));

      IExpr expr1 = Equal.create(FunApp.create(f, x, y), z);
      IExpr expr2 = Equal.create(z, RealVal.create("1/2"));
      IExpr expr = And.create(expr1, expr2);

      Z3Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr fval = model.getValueFor(f);
      IExpr xval = model.getValueFor(x);
      IExpr yval = model.getValueFor(y);
      IExpr zval = model.getValueFor(z);
      assertEquals(zval, FunApp.create(fval, xval, yval));
      assertEquals(RealVal.create("1/2"), zval);
   }

   @Test
   public void testCheckUnsatFunExpr() {
      Fun a0 = USort.create(0, DeclareSort.A);
      Fun a1 = USort.create(1, DeclareSort.A);
      Fun f = Fun.createHomogenous(0, 1, DeclareSort.A);

      IExpr clause1 = Equal.create(FunApp.create(f, a0), a0);
      IExpr clause2 = Equal.create(FunApp.create(f, a0), a1);
      IExpr clause3 = Distinct.create(a0, a1);
      IExpr expr = And.create(clause1, clause2, clause3);

      Solver solver = getSolverFor("QF_UF");
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.UNSAT, status);

      Set<IExpr> unsatCore = getCoreOrNull(solver);
      assertNotNull(unsatCore);

      assertTrue(unsatCore.contains(clause1));
      assertTrue(unsatCore.contains(clause2));
      assertTrue(unsatCore.contains(clause3));
   }

   @Test
   public void testCheckSatStringExpr() {
      Fun s = Str.create(0);

      IExpr e1 = StrConcat.create(s, StrVal.create("a"));
      IExpr e2 = StrConcat.create(StrVal.create("a"), s);

      IExpr expr = And.create(
            Equal.create(e1, e2),
            Equal.create(StrLen.create(s), IntVal.create(4)));

      Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      assertEquals(StrVal.create("aaaa"), model.getValueFor(s));
   }

   @Test
   public void testCheckUnsatStringExpr() {
      // At present time Z3 returns UNKNOWN on this expression.
      Fun s = Str.create(0);

      IExpr e1 = StrConcat.create(s, StrVal.create("a"));
      IExpr e2 = StrConcat.create(StrVal.create("a"), s);
      IExpr e3 = StrConcat.create(s, StrVal.create("b"));
      IExpr e4 = StrConcat.create(StrVal.create("b"), s);

      IExpr clause1 = Equal.create(e1, e2);
      IExpr clause2 = Equal.create(e3, e4);
      IExpr clause3 = Not.create(Equal.create(s, StrVal.EMPTY));

      IExpr expr = And.create(clause1, clause2, clause3);

      Solver solver = new Z3Solver(Z3);
      CheckResult status = solver.check(expr);
      assertNotEquals(CheckResult.SAT, status);
   }
}
