package julia.solver.z3_str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.str.Str;
import julia.expr.ast.str.StrConcat;
import julia.expr.ast.str.StrLen;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.Model;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.exc.NoModelException;
import org.junit.BeforeClass;
import org.junit.Test;

public class Z3StrSolverTest {
   private static String Z3_STR = null;

   private static Model getModelOrNull(Solver solver) {
      try {
         return solver.getModel();
      } catch (NoModelException e) {
         return null;
      }
   }

   @BeforeClass
   public static void setup() {
      // Check that Microsoft Z3 exists and is executable.
      File config = new File("./src/main/resources/config.properties");

      try (FileReader reader = new FileReader(config)) {
         Properties props = new Properties();
         props.load(reader);

         Z3_STR = props.getProperty("z3-str");
         File z3file = new File(Z3_STR);

         assertTrue("Cannot find Z3-Str binary at " + Z3_STR, z3file.exists());
         assertTrue("Z3-Str binary is not executable", z3file.canExecute());
      } catch (FileNotFoundException e) {
         fail("Configuration file not found.");
      } catch (IOException e) {
         fail(e.getMessage());
      }
   }

   @Test
   public void testCheckSatIntExpr() {
      Fun x = Int.create(0);
      IExpr expr = IntLt.create(x, IntVal.ZERO);

      Z3StrSolver solver = new Z3StrSolver(Z3_STR, 3600);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IExpr c = model.getValueFor(x);
      assertEquals(BoolVal.TRUE, IntLt.create(c, IntVal.ZERO));
   }

   @Test
   public void testCheckSatStrExpr() {
      Fun s = Str.create(0);
      IExpr expr = Equal.create(StrConcat.create(s, s), StrVal.create("heyhey"));

      Z3StrSolver solver = new Z3StrSolver(Z3_STR, 3600);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      assertEquals(StrVal.create("hey"), model.getValueFor(s));
   }

   @Test
   public void testCheckMixedExpr() {
      Fun c = Bool.create(0);
      Fun n = Int.create(0);
      Fun s = Str.create(0);
      IExpr expr = And.create(c, Equal.create(StrLen.create(s), n));

      Z3StrSolver solver = new Z3StrSolver(Z3_STR, 3600);
      CheckResult status = solver.check(expr);
      assertEquals(CheckResult.SAT, status);

      Model model = getModelOrNull(solver);
      assertNotNull(model);

      IConst cval = model.getValueFor(c);
      IConst nval = model.getValueFor(n);
      IConst sval = model.getValueFor(s);
      assertEquals(BoolVal.TRUE, cval);
      assertEquals(BoolVal.TRUE, Equal.create(StrLen.create(sval), nval));
   }
}
