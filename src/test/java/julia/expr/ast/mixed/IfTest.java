package julia.expr.ast.mixed;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.reals.Real;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IfTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNonConstIf() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = If.create(IntLt.create(x, y), x, y);
      assertEquals(If.class, expr.getClass());
   }

   @Test
   public void testConstIf1() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = If.create(BoolVal.TRUE, x, y);
      assertEquals(x, expr);
   }

   @Test
   public void testConstIf2() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = If.create(BoolVal.FALSE, x, y);
      assertEquals(y, expr);
   }

   @Test
   public void testConstIf3() {
      Fun x = Int.create(0);
      IExpr expr = If.create(Bool.create(0), x, x);
      assertEquals(x, expr);
   }

   @Test
   public void testSort1() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = If.create(Bool.create(0), x, y);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testSort2() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = If.create(Bool.create(0), x, y);
      assertEquals(RealSort.create(), expr.sort());
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      If.create(Bool.create(0), x, y).satDelta(new SortModel());
   }

   @Test
   public void testSatDelta1() {
      Fun c = Bool.create(0);
      Fun A = Bool.create(1);
      Fun B = Bool.create(2);

      MapModel model = new MapModel();
      model.put(c, BoolVal.FALSE);
      model.put(A, BoolVal.TRUE);
      model.put(B, BoolVal.FALSE);

      assertEquals(new SatDelta(1, 0), If.create(c, A, B).satDelta(model));
   }

   @Test
   public void testSatDelta2() {
      Fun c = Bool.create(0);
      Fun A = Bool.create(1);
      Fun B = Bool.create(2);

      MapModel model = new MapModel();
      model.put(c, BoolVal.TRUE);
      model.put(A, BoolVal.TRUE);
      model.put(B, BoolVal.FALSE);

      assertEquals(new SatDelta(0, 1), If.create(c, A, B).satDelta(model));
   }

   @Test
   public void testToSmt() {
      Fun c = Bool.create(0);
      Fun A = Bool.create(1);
      Fun B = Bool.create(2);
      assertTrue(If.create(c, A, B).toSmt().contains("ite"));
   }
}
