package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecAShRTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroAShROne() {
      IExpr expr = BitVecAShR.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroAShRTwo() {
      IExpr expr = BitVecAShR.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testOneAShROne() {
      IExpr expr = BitVecAShR.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testTwoAShROne() {
      IExpr expr = BitVecAShR.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testNegOneAShROne() {
      IExpr expr = BitVecAShR.create(BitVecVal.create("#b1111"), BitVecVal.create("#b0001"));
      assertEquals(BitVecVal.create("#b1111"), expr);
   }

   @Test
   public void testEightAShRThree() {
      IExpr expr = BitVecAShR.create(BitVecVal.create(8, 32), BitVecVal.create(3, 32));
      assertEquals(BitVecVal.create(1, 32), expr);
   }

   @Test
   public void testEightAShRFour() {
      IExpr expr = BitVecAShR.create(BitVecVal.create(8, 32), BitVecVal.create(4, 32));
      assertEquals(BitVecVal.create(0, 32), expr);
   }

   @Test
   public void testXAShRZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecAShR.create(x, BitVecVal.create(0, 4));
      assertEquals(x, expr);
   }

   @Test
   public void testZeroAShRX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecAShR.create(BitVecVal.create(0, 4), x);
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testXAShRY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecAShR.create(x, y);
      assertEquals(BitVecAShR.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecAShR.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecAShR.create(x, y);
      assertTrue(expr.toSmt().contains("bvashr"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecAShR.create(x, y).satDelta(new SortModel());
   }
}
