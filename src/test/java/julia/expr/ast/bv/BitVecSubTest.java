package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecSubTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroSubOne() {
      IExpr expr = BitVecSub.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create("#b1111"), expr);
   }

   @Test
   public void testZeroSubTwo() {
      IExpr expr = BitVecSub.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create("#b1110"), expr);
   }

   @Test
   public void testOneSubOne() {
      IExpr expr = BitVecSub.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testTwoSubOne() {
      IExpr expr = BitVecSub.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testEightSubThree() {
      IExpr expr = BitVecSub.create(BitVecVal.create(8, 4), BitVecVal.create(3, 4));
      assertEquals(BitVecVal.create(5, 4), expr);
   }

   @Test
   public void testEightSubFour() {
      IExpr expr = BitVecSub.create(BitVecVal.create(8, 4), BitVecVal.create(4, 4));
      assertEquals(BitVecVal.create(4, 4), expr);
   }

   @Test
   public void testLargeValueSubLargeValue() {
      IExpr c = BitVecVal.create(32768, 32);
      IExpr expr = BitVecSub.create(c, c);
      assertEquals(BitVecVal.create(0, 32), expr);
   }

   @Test
   public void testXSubZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecSub.create(x, BitVecVal.create(0, 4));
      assertEquals(x, expr);
   }

   @Test
   public void testXSubY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSub.create(x, y);
      assertEquals(BitVecSub.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSub.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecSub.create(x, y);
      assertTrue(expr.toSmt().contains("bvsub"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecSub.create(x, y).satDelta(new SortModel());
   }
}
