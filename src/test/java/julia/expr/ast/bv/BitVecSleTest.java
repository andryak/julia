package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecSleTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroSleOne() {
      IExpr expr = BitVecSle.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testZeroSleTwo() {
      IExpr expr = BitVecSle.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testOneSleOne() {
      IExpr expr = BitVecSle.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTwoSleOne() {
      IExpr expr = BitVecSle.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testNegOneSleOne() {
      IExpr expr = BitVecSle.create(BitVecVal.create("#b1111"), BitVecVal.create("#b0001"));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testNegOneSleNegOne() {
      IExpr expr = BitVecSle.create(BitVecVal.create("#b1111"), BitVecVal.create("#b1111"));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testNegTwoSleNegOne() {
      IExpr expr = BitVecSle.create(BitVecVal.create("#b1110"), BitVecVal.create("#b1111"));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXSleY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSle.create(x, y);
      assertEquals(BitVecSle.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSle.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSle.create(x, y);
      assertTrue(expr.toSmt().contains("bvsle"));
   }

   @Test
   public void testSatDelta1() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      assertEquals(new SatDelta(0, 1), BitVecSle.create(x, y).satDelta(new SortModel()));
   }

   @Test
   public void testSatDelta2() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecSle.create(BitVecVal.create(1, 4), x);
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }
}
