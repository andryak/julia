package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecSgeTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroSgeOne() {
      IExpr expr = BitVecSge.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testZeroSgeTwo() {
      IExpr expr = BitVecSge.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testOneSgeOne() {
      IExpr expr = BitVecSge.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTwoSgeOne() {
      IExpr expr = BitVecSge.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testNegOneSgeOne() {
      IExpr expr = BitVecSge.create(BitVecVal.create("#b1111"), BitVecVal.create("#b0001"));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testNegOneSgeNegOne() {
      IExpr expr = BitVecSge.create(BitVecVal.create("#b1111"), BitVecVal.create("#b1111"));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testNegTwoSgeNegOne() {
      IExpr expr = BitVecSge.create(BitVecVal.create("#b1110"), BitVecVal.create("#b1111"));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXSgeY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSge.create(x, y);
      assertEquals(BitVecSge.class, expr.getClass());
      assertEquals(BoolVal.TRUE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSge.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecSge.create(x, y);
      assertTrue(expr.toSmt().contains("bvsge"));
   }

   @Test
   public void testSatDelta1() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      assertEquals(new SatDelta(0, 1), BitVecSge.create(x, y).satDelta(new SortModel()));
   }

   @Test
   public void testSatDelta2() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecSge.create(x, BitVecVal.create(1, 4));
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }
}
