package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecUDivTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroUDivOne() {
      IExpr expr = BitVecUDiv.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroUDivTwo() {
      IExpr expr = BitVecUDiv.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testOneUDivOne() {
      IExpr expr = BitVecUDiv.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testTwoUDivOne() {
      IExpr expr = BitVecUDiv.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testEightUDivThree() {
      IExpr expr = BitVecUDiv.create(BitVecVal.create(8, 32), BitVecVal.create(3, 32));
      assertEquals(BitVecVal.create(2, 32), expr);
   }

   @Test
   public void testEightUDivFour() {
      IExpr expr = BitVecUDiv.create(BitVecVal.create(8, 4), BitVecVal.create(4, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testLargeValueUDivLargeValue() {
      IExpr c1 = BitVecVal.create(32810, 32);
      IExpr c2 = BitVecVal.create(32768, 32);
      IExpr expr = BitVecUDiv.create(c1, c2);
      assertEquals(BitVecVal.create(1, 32), expr);
   }

   @Test
   public void testXUDivY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecUDiv.create(x, y);
      assertEquals(BitVecUDiv.class, expr.getClass());
   }

   @Test
   public void testXUDivOne() {
      Fun x = BitVec.create(0, 4);
      assertEquals(x, BitVecUDiv.create(x, BitVecVal.create(1, 4)));
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecUDiv.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecUDiv.create(x, y);
      assertTrue(expr.toSmt().contains("bvudiv"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecUDiv.create(x, y).satDelta(new SortModel());
   }
}
