package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecXOrTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroXOrOne() {
      IExpr expr = BitVecXOr.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testZeroXOrTwo() {
      IExpr expr = BitVecXOr.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testOneXOrOne() {
      IExpr expr = BitVecXOr.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testTwoXOrOne() {
      IExpr expr = BitVecXOr.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(3, 4), expr);
   }

   @Test
   public void testEightXOrThree() {
      IExpr expr = BitVecXOr.create(BitVecVal.create(8, 4), BitVecVal.create(3, 4));
      assertEquals(BitVecVal.create(11, 4), expr);
   }

   @Test
   public void testEightXOrFour() {
      IExpr expr = BitVecXOr.create(BitVecVal.create(8, 4), BitVecVal.create(4, 4));
      assertEquals(BitVecVal.create(12, 4), expr);
   }

   @Test
   public void testLargeValueXOrLargeValue() {
      IExpr c = BitVecVal.create(32768, 32);
      IExpr expr = BitVecXOr.create(c, c);
      assertEquals(BitVecVal.create(0, 32), expr);
   }

   @Test
   public void testXXOrZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecXOr.create(x, BitVecVal.create(0, 4));
      assertEquals(x, expr);
   }

   @Test
   public void testZeroXOrX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecXOr.create(BitVecVal.create(0, 4), x);
      assertEquals(x, expr);
   }

   @Test
   public void testXXOrY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecXOr.create(x, y);
      assertEquals(BitVecXOr.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecXOr.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecXOr.create(x, y);
      assertTrue(expr.toSmt().contains("bvxor"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecXOr.create(x, y).satDelta(new SortModel());
   }

   @Test
   public void testEval() {
      Fun x = BitVec.create(0, 2);
      Fun y = BitVec.create(1, 2);
      IExpr expr = BitVecXOr.create(x, y);
      assertEquals(BitVecVal.create("#b00"), expr.eval(new SortModel()));
   }
}
