package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecShLTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroShLOne() {
      IExpr expr = BitVecShL.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroShLTwo() {
      IExpr expr = BitVecShL.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testOneShLOne() {
      IExpr expr = BitVecShL.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testTwoShLOne() {
      IExpr expr = BitVecShL.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(4, 4), expr);
   }

   @Test
   public void testEightShLThree() {
      IExpr expr = BitVecShL.create(BitVecVal.create(8, 32), BitVecVal.create(3, 32));
      assertEquals(BitVecVal.create(64, 32), expr);
   }

   @Test
   public void testEightShLFour() {
      IExpr expr = BitVecShL.create(BitVecVal.create(8, 32), BitVecVal.create(4, 32));
      assertEquals(BitVecVal.create(128, 32), expr);
   }

   @Test
   public void testXShLZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecShL.create(x, BitVecVal.create(0, 4));
      assertEquals(x, expr);
   }

   @Test
   public void testZeroShLX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecShL.create(BitVecVal.create(0, 4), x);
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testXShLY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecShL.create(x, y);
      assertEquals(BitVecShL.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecShL.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecShL.create(x, y);
      assertTrue(expr.toSmt().contains("bvshl"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecShL.create(x, y).satDelta(new SortModel());
   }
}
