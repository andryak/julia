package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.uf.Fun;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetIndex1() {
      Fun x = BitVec.create(0, 4);
      assertEquals(0, x.getIndex());
   }

   @Test
   public void testGetIndex2() {
      Fun x = BitVec.create(1, 4);
      assertEquals(1, x.getIndex());
   }

   @Test
   public void testGetIndex3() {
      Fun x = BitVec.create(2, 4);
      assertEquals(2, x.getIndex());
   }
}
