package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecNotTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNotZero() {
      IExpr expr = BitVecNot.create(BitVecVal.create("#b0000"));
      assertEquals(BitVecVal.create("#b1111"), expr);
   }

   @Test
   public void testNotOne() {
      IExpr expr = BitVecNot.create(BitVecVal.create("#b0001"));
      assertEquals(BitVecVal.create("#b1110"), expr);
   }

   @Test
   public void testNotTwo() {
      IExpr expr = BitVecNot.create(BitVecVal.create("#b0010"));
      assertEquals(BitVecVal.create("#b1101"), expr);
   }

   @Test
   public void testNotOfLargeValue() {
      IExpr expr = BitVecNot.create(BitVecVal.create("#b1010101010101010"));
      assertEquals(BitVecVal.create("#b0101010101010101"), expr);
   }

   @Test
   public void testNotX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecNot.create(x);
      assertEquals(BitVecNot.class, expr.getClass());
   }

   @Test
   public void testNotOfNot() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecNot.create(BitVecNot.create(x));
      assertEquals(x, expr);
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecNot.create(x);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecNot.create(x);
      assertTrue(expr.toSmt().contains("bvnot"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      BitVecNot.create(x).satDelta(new SortModel());
   }
}
