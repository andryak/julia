package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecConcatTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroConcatOne() {
      IExpr expr = BitVecConcat.create(BitVecVal.create("#b0"), BitVecVal.create("#b1"));
      assertEquals(BitVecVal.create("#b01"), expr);
   }

   @Test
   public void testZeroConcatTwo() {
      IExpr expr = BitVecConcat.create(BitVecVal.create("#b00"), BitVecVal.create("#b10"));
      assertEquals(BitVecVal.create("#b0010"), expr);
   }

   @Test
   public void testOneConcatOne() {
      IExpr expr = BitVecConcat.create(BitVecVal.create("#b1"), BitVecVal.create("#b1"));
      assertEquals(BitVecVal.create("#b11"), expr);
   }

   @Test
   public void testTwoConcatOne() {
      IExpr expr = BitVecConcat.create(BitVecVal.create("#b10"), BitVecVal.create("#b01"));
      assertEquals(BitVecVal.create("#b1001"), expr);
   }

   @Test
   public void testSmallSizeBitVecConcatLargeSizeBitVec() {
      IExpr expr = BitVecConcat.create(BitVecVal.create("#b1"), BitVecVal.create("#b001"));
      assertEquals(BitVecVal.create("#b1001"), expr);
   }

   @Test
   public void testLargeSizeBitVecConcatSmallSizeBitVec() {
      IExpr expr = BitVecConcat.create(BitVecVal.create("#b100"), BitVecVal.create("#b1"));
      assertEquals(BitVecVal.create("#b1001"), expr);
   }

   @Test
   public void testXConcatY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecConcat.create(x, y);
      assertEquals(BitVecConcat.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecConcat.create(x, y);
      assertEquals(BitVecSort.create(8), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecConcat.create(x, y);
      assertTrue(expr.toSmt().contains("concat"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecConcat.create(x, y).satDelta(new SortModel());
   }
}
