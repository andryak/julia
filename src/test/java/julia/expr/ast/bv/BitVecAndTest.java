package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecAndTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroAndOne() {
      IExpr expr = BitVecAnd.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroAndTwo() {
      IExpr expr = BitVecAnd.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testOneAndOne() {
      IExpr expr = BitVecAnd.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testTwoAndOne() {
      IExpr expr = BitVecAnd.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testEightAndThree() {
      IExpr expr = BitVecAnd.create(BitVecVal.create(8, 4), BitVecVal.create(3, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testEightAndFour() {
      IExpr expr = BitVecAnd.create(BitVecVal.create(8, 4), BitVecVal.create(4, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testLargeValueAndLargeValue() {
      IExpr c = BitVecVal.create(32768, 32);
      IExpr expr = BitVecAnd.create(c, c);
      assertEquals(c, expr);
   }

   @Test
   public void testXAndZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecAnd.create(x, BitVecVal.create(0, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroAndX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecAnd.create(BitVecVal.create(0, 4), x);
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testXAndY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecAnd.create(x, y);
      assertEquals(BitVecAnd.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecAnd.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecAnd.create(x, y);
      assertTrue(expr.toSmt().contains("bvand"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecAnd.create(x, y).satDelta(new SortModel());
   }
}
