package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecExtractTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testConstExtract1() {
      IExpr expr = BitVecExtract.create(BitVecVal.create("#b11110000"), 3, 0);
      assertEquals(BitVecVal.create("#b0000"), expr);
   }

   @Test
   public void testConstExtract2() {
      IExpr expr = BitVecExtract.create(BitVecVal.create("#b11110000"), 7, 4);
      assertEquals(BitVecVal.create("#b1111"), expr);
   }

   @Test
   public void testConstExtract3() {
      IExpr expr = BitVecExtract.create(BitVecVal.create("#b11110000"), 5, 2);
      assertEquals(BitVecVal.create("#b1100"), expr);
   }

   @Test
   public void testExtractAllBits() {
      Fun x = BitVec.create(0, 32);
      IExpr expr = BitVecExtract.create(x, 31, 0);
      assertEquals(x, expr);
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecExtract.create(x, 2, 0);
      assertEquals(BitVecSort.create(3), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecExtract.create(x, 2, 0);
      assertTrue(expr.toSmt().contains("extract"));
      assertTrue(expr.toSmt().contains("2"));
      assertTrue(expr.toSmt().contains("0"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      BitVecExtract.create(x, 2, 0).satDelta(new SortModel());
   }
}
