package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecSignExtTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testOneSignExtByThree() {
      IExpr expr = BitVecSignExt.create(BitVecVal.create("#b01"), 3);
      assertEquals(BitVecVal.create("#b00001"), expr);
   }

   @Test
   public void testNegOneSignExtByThree() {
      IExpr expr = BitVecSignExt.create(BitVecVal.create("#b11"), 3);
      assertEquals(BitVecVal.create("#b11111"), expr);
   }

   @Test
   public void testXSignExtByZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecSignExt.create(x, 0);
      assertEquals(x, expr);
   }

   @Test
   public void testXSignExtByThree() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecSignExt.create(x, 3);
      assertEquals(BitVecSignExt.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecSignExt.create(x, 3);
      assertEquals(BitVecSort.create(7), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      IExpr expr = BitVecSignExt.create(x, 3);
      assertTrue(expr.toSmt().contains("sign_ext"));
      assertTrue(expr.toSmt().contains("3"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      BitVecSignExt.create(x, 3).satDelta(new SortModel());
   }

   @Test
   public void testEval() {
      Fun x = BitVec.create(0, 2);
      IExpr expr = BitVecSignExt.create(x, 2);
      assertEquals(BitVecVal.create("#b0000"), expr.eval(new SortModel()));
   }
}
