package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecMulTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroMulOne() {
      IExpr expr = BitVecMul.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroMulTwo() {
      IExpr expr = BitVecMul.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testOneMulOne() {
      IExpr expr = BitVecMul.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testTwoMulOne() {
      IExpr expr = BitVecMul.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testEightMulThree() {
      IExpr expr = BitVecMul.create(BitVecVal.create(8, 32), BitVecVal.create(3, 32));
      assertEquals(BitVecVal.create(24, 32), expr);
   }

   @Test
   public void testEightMulFour() {
      IExpr expr = BitVecMul.create(BitVecVal.create(8, 32), BitVecVal.create(4, 32));
      assertEquals(BitVecVal.create(32, 32), expr);
   }

   @Test
   public void testLargeValueMulLargeValue() {
      IExpr c = BitVecVal.create(1024, 32);
      IExpr expr = BitVecMul.create(c, c);
      assertEquals(BitVecVal.create(1048576, 32), expr);
   }

   @Test
   public void testXMulZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecMul.create(x, BitVecVal.create(0, 4));
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testZeroMulX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecMul.create(BitVecVal.create(0, 4), x);
      assertEquals(BitVecVal.create(0, 4), expr);
   }

   @Test
   public void testXMulY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecMul.create(x, y);
      assertEquals(BitVecMul.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecMul.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecMul.create(x, y);
      assertTrue(expr.toSmt().contains("bvmul"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecMul.create(x, y).satDelta(new SortModel());
   }
}
