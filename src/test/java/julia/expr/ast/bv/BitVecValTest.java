package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import julia.expr.utils.BigRational;
import julia.expr.utils.BitVector;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecValTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testSomeBitVecsAreSingletons() {
      BitVecVal c0 = BitVecVal.create(0, 4);
      BitVecVal c1 = BitVecVal.create(0, 4);
      assertSame(c0, c1);
   }

   @Test
   public void testSomeBitVecsAreNotSingletons1() {
      // Size is not power of two.
      BitVecVal c0 = BitVecVal.create(0, 5);
      BitVecVal c1 = BitVecVal.create(0, 5);
      assertNotSame(c0, c1);
   }

   @Test
   public void testSomeBitVecsAreNotSingletons2() {
      // Size is too large.
      BitVecVal c0 = BitVecVal.create(0, 1024);
      BitVecVal c1 = BitVecVal.create(0, 1024);
      assertNotSame(c0, c1);
   }

   @Test
   public void testSomeBitVecsAreNotSingletons3() {
      // Value is too large.
      BitVecVal c0 = BitVecVal.create(Long.MAX_VALUE, 64);
      BitVecVal c1 = BitVecVal.create(Long.MAX_VALUE, 64);
      assertNotSame(c0, c1);
   }

   @Test
   public void testOneIsNotTwo() {
      BitVecVal c0 = BitVecVal.create(1, 4);
      BitVecVal c1 = BitVecVal.create(2, 4);
      assertNotEquals(c0, c1);
   }

   @Test
   public void testSomeLargeValuesAreDifferent() {
      BitVecVal c0 = BitVecVal.create(32768, 32);
      BitVecVal c1 = BitVecVal.create(65536, 32);
      assertNotEquals(c0, c1);
   }

   @Test
   public void testGetValueOfZero() {
      assertEquals(BitVector.valueOf("#b0000"), BitVecVal.create(0, 4).getValue());
   }

   @Test
   public void testGetValueOfOne() {
      assertEquals(BitVector.valueOf("#b0001"), BitVecVal.create(1, 4).getValue());
   }

   @Test
   public void testGetValueOfTwo() {
      assertEquals(BitVector.valueOf("#b0010"), BitVecVal.create(2, 4).getValue());
   }

   @Test
   public void testGetValueOfLargeValue() {
      BitVecVal c = BitVecVal.create(32768, 16);
      assertEquals(BitVector.valueOf("#b1000000000000000"), c.getValue());
   }

   @Test
   public void testSort() {
      assertEquals(BitVecSort.create(4), BitVecVal.create(0, 4).sort());
   }

   @Test
   public void testZeroToString() {
      assertEquals("#b0000", BitVecVal.create(0, 4).toString());
   }

   @Test
   public void testOneToString() {
      assertEquals("#b0001", BitVecVal.create(1, 4).toString());
   }

   @Test
   public void testTwoToString() {
      assertEquals("#b0010", BitVecVal.create(2, 4).toString());
   }

   @Test
   public void testEval() {
      BitVecVal c = BitVecVal.create(0, 4);
      assertEquals(c, c.eval());
   }

   @Test
   public void testRenameVars() {
      BitVecVal c = BitVecVal.create(0, 4);
      assertEquals(c, c.renameVars(null));
   }

   @Test
   public void testZeroToSmt() {
      assertEquals("#b0000", BitVecVal.create(0, 4).toSmt());
   }

   @Test
   public void testOneToSmt() {
      assertEquals("#b0001", BitVecVal.create(1, 4).toSmt());
   }

   @Test
   public void testTwoToSmt() {
      assertEquals("#b0010", BitVecVal.create(2, 4).toSmt());
   }

   @Test
   public void isZero() {
      assertTrue(BitVecVal.create("#b0000").isZero());
      assertFalse(BitVecVal.create("#b0001").isZero());
      assertFalse(BitVecVal.create("#b1111").isZero());
   }

   @Test
   public void isOne() {
      assertFalse(BitVecVal.create("#b0000").isOne());
      assertTrue(BitVecVal.create("#b0001").isOne());
      assertFalse(BitVecVal.create("#b1111").isOne());
   }

   @Test
   public void isNegOne() {
      assertFalse(BitVecVal.create("#b0000").isNegOne());
      assertFalse(BitVecVal.create("#b0001").isNegOne());
      assertTrue(BitVecVal.create("#b1111").isNegOne());
   }

   @Test
   public void delta1() {
      BitVecVal c0 = BitVecVal.create(0, 4);
      BitVecVal c1 = BitVecVal.create(0, 4);
      assertEquals(BigRational.ZERO, c0.delta(c1));
   }

   @Test
   public void delta2() {
      BitVecVal c0 = BitVecVal.create(1, 4);
      BitVecVal c1 = BitVecVal.create(0, 4);
      assertEquals(BigRational.ONE, c0.delta(c1));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      BitVecVal.create(0, 4).satDelta(new SortModel());
   }
}
