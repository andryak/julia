package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecAddTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroAddOne() {
      IExpr expr = BitVecAdd.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testZeroAddTwo() {
      IExpr expr = BitVecAdd.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testOneAddOne() {
      IExpr expr = BitVecAdd.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testTwoAddOne() {
      IExpr expr = BitVecAdd.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(3, 4), expr);
   }

   @Test
   public void testEightAddThree() {
      IExpr expr = BitVecAdd.create(BitVecVal.create(8, 4), BitVecVal.create(3, 4));
      assertEquals(BitVecVal.create(11, 4), expr);
   }

   @Test
   public void testEightAddFour() {
      IExpr expr = BitVecAdd.create(BitVecVal.create(8, 4), BitVecVal.create(4, 4));
      assertEquals(BitVecVal.create(12, 4), expr);
   }

   @Test
   public void testLargeValueAddLargeValue() {
      IExpr c0 = BitVecVal.create(32768, 32);
      IExpr c1 = BitVecVal.create(32768, 32);
      IExpr expr = BitVecAdd.create(c0, c1);
      assertEquals(BitVecVal.create(65536, 32), expr);
   }

   @Test
   public void testXAddZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecAdd.create(x, BitVecVal.create(0, 4));
      assertEquals(x, expr);
   }

   @Test
   public void testZeroAddX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecAdd.create(BitVecVal.create(0, 4), x);
      assertEquals(x, expr);
   }

   @Test
   public void testXAddY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecAdd.create(x, y);
      assertEquals(BitVecAdd.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecAdd.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecAdd.create(x, y);
      assertTrue(expr.toSmt().contains("bvadd"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecAdd.create(x, y).satDelta(new SortModel());
   }
}
