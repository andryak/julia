package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecOrTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroOrOne() {
      IExpr expr = BitVecOr.create(BitVecVal.create(0, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testZeroOrTwo() {
      IExpr expr = BitVecOr.create(BitVecVal.create(0, 4), BitVecVal.create(2, 4));
      assertEquals(BitVecVal.create(2, 4), expr);
   }

   @Test
   public void testOneOrOne() {
      IExpr expr = BitVecOr.create(BitVecVal.create(1, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(1, 4), expr);
   }

   @Test
   public void testTwoOrOne() {
      IExpr expr = BitVecOr.create(BitVecVal.create(2, 4), BitVecVal.create(1, 4));
      assertEquals(BitVecVal.create(3, 4), expr);
   }

   @Test
   public void testEightOrThree() {
      IExpr expr = BitVecOr.create(BitVecVal.create(8, 4), BitVecVal.create(3, 4));
      assertEquals(BitVecVal.create(11, 4), expr);
   }

   @Test
   public void testEightOrFour() {
      IExpr expr = BitVecOr.create(BitVecVal.create(8, 4), BitVecVal.create(4, 4));
      assertEquals(BitVecVal.create(12, 4), expr);
   }

   @Test
   public void testLargeValueOrLargeValue() {
      IExpr c = BitVecVal.create(32768, 32);
      IExpr expr = BitVecOr.create(c, c);
      assertEquals(c, expr);
   }

   @Test
   public void testXOrZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecOr.create(x, BitVecVal.create(0, 4));
      assertEquals(x, expr);
   }

   @Test
   public void testZeroOrX() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecOr.create(BitVecVal.create(0, 4), x);
      assertEquals(x, expr);
   }

   @Test
   public void testXOrY() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecOr.create(x, y);
      assertEquals(BitVecOr.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      IExpr expr = BitVecOr.create(x, y);
      assertEquals(BitVecSort.create(4), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      Fun y = BitVec.create(1, 32);
      IExpr expr = BitVecOr.create(x, y);
      assertTrue(expr.toSmt().contains("bvor"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      Fun y = BitVec.create(1, 4);
      BitVecOr.create(x, y).satDelta(new SortModel());
   }
}
