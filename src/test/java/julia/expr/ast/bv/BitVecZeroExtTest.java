package julia.expr.ast.bv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.BitVecSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BitVecZeroExtTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testOneZeroExtByThree() {
      IExpr expr = BitVecZeroExt.create(BitVecVal.create("#b01"), 3);
      assertEquals(BitVecVal.create("#b00001"), expr);
   }

   @Test
   public void testNegOneZeroExtByThree() {
      IExpr expr = BitVecZeroExt.create(BitVecVal.create("#b11"), 3);
      assertEquals(BitVecVal.create("#b00011"), expr);
   }

   @Test
   public void testXZeroExtByZero() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecZeroExt.create(x, 0);
      assertEquals(x, expr);
   }

   @Test
   public void testXZeroExtByThree() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecZeroExt.create(x, 3);
      assertEquals(BitVecZeroExt.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = BitVec.create(0, 4);
      IExpr expr = BitVecZeroExt.create(x, 3);
      assertEquals(BitVecSort.create(7), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = BitVec.create(0, 32);
      IExpr expr = BitVecZeroExt.create(x, 3);
      assertTrue(expr.toSmt().contains("zero_ext"));
      assertTrue(expr.toSmt().contains("3"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = BitVec.create(0, 4);
      BitVecZeroExt.create(x, 3).satDelta(new SortModel());
   }

   @Test
   public void testEval() {
      Fun x = BitVec.create(0, 2);
      IExpr expr = BitVecZeroExt.create(x, 2);
      assertEquals(BitVecVal.create("#b0000"), expr.eval(new SortModel()));
   }
}
