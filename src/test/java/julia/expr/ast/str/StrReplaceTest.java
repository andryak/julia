package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.StrSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrReplaceTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrReplaceConstStrings() {
      IExpr expr = StrReplace.create(
            StrVal.create("test"),
            StrVal.create("st"),
            StrVal.create("nt"));
      assertEquals(StrVal.create("tent"), expr);
   }

   @Test
   public void testExprEqualsPattern() {
      Fun s = Str.create(0);
      IExpr expr = StrReplace.create(StrVal.create("test"), StrVal.create("test"), s);
      assertEquals(s, expr);
   }

   @Test
   public void testPatternEqualsReplacement() {
      Fun s = Str.create(0);
      IExpr expr = StrReplace.create(s, StrVal.create("test"), StrVal.create("test"));
      assertEquals(s, expr);
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      Fun p = Str.create(1);
      Fun r = Str.create(1);
      assertEquals(StrSort.create(), StrReplace.create(s, p, r).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      Fun p = Str.create(1);
      Fun r = Str.create(2);
      IExpr expr = StrReplace.create(s, p, r);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(StrVal.create("test"), expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      Fun p = Str.create(1);
      Fun r = Str.create(2);
      IExpr expr = StrReplace.create(s, p, r);

      VarMap map = new VarMap();
      map.put(s, 1);
      map.put(p, 2);
      map.put(r, 0);

      assertEquals(StrReplace.create(p, r, s), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      Fun p = Str.create(1);
      Fun r = Str.create(2);
      assertTrue(StrReplace.create(s, p, r).toSmt().contains("str.replace"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      Fun p = Str.create(1);
      Fun r = Str.create(2);
      StrReplace.create(s, p, r).satDelta(new SortModel());
   }
}
