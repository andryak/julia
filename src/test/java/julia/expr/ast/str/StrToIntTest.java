package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrToIntTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrToIntOfOne() {
      assertEquals(IntVal.ONE, StrToInt.create(StrVal.create("1")));
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      assertEquals(IntSort.create(), StrToInt.create(s).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      IExpr expr = StrToInt.create(s);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("42"));

      assertEquals(IntVal.create(42), expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      IExpr expr = StrToInt.create(s);

      VarMap map = new VarMap();
      map.put(s, 1);

      assertEquals(StrToInt.create(Str.create(1)), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      assertTrue(StrToInt.create(s).toSmt().contains("str.to.int"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      StrToInt.create(s).satDelta(new SortModel());
   }
}
