package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrLenTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrLenOfEmptyString() {
      assertEquals(IntVal.ZERO, StrLen.create(StrVal.EMPTY));
   }

   @Test
   public void testStrLenOfNonEmptyString() {
      assertEquals(IntVal.create(4), StrLen.create(StrVal.create("test")));
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      assertEquals(IntSort.create(), StrLen.create(s).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      IExpr expr = StrLen.create(s);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(IntVal.create(4), expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      IExpr expr = StrLen.create(s);

      VarMap map = new VarMap();
      map.put(s, 1);

      assertEquals(StrLen.create(Str.create(1)), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      assertTrue(StrLen.create(s).toSmt().contains("str.len"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      StrLen.create(s).satDelta(new SortModel());
   }
}
