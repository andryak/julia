package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrContainsTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrContainsWithConstStrings1() {
      IExpr expr = StrContains.create(StrVal.create("te"), StrVal.create("st"));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testStrContainsWithConstStrings2() {
      IExpr expr = StrContains.create(StrVal.create("xxyy"), StrVal.create("xy"));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testStrContainsWithEmptyString() {
      Fun s = Str.create(0);
      IExpr expr = StrContains.create(s, StrVal.EMPTY);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrContains.create(s, t);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrContains.create(s, t);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrContains.create(s, t);

      VarMap map = new VarMap();
      map.put(s, 1);
      map.put(t, 0);

      assertEquals(StrContains.create(t, s), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      assertTrue(StrContains.create(s, t).toSmt().contains("str.contains"));
   }

   @Test
   public void testSatDelta1() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      assertEquals(new SatDelta(0, 1), StrContains.create(s, t).satDelta(new SortModel()));
   }

   @Test
   public void testSatDelta2() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);

      MapModel model = new MapModel();
      model.put(s, StrVal.create("xx"));
      model.put(t, StrVal.create("yyyy"));

      assertEquals(new SatDelta(4, 0), StrContains.create(s, t).satDelta(model));
   }

   @Test
   public void testSatDelta3() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);

      MapModel model = new MapModel();
      model.put(s, StrVal.create("xxxx"));
      model.put(t, StrVal.create("yy"));

      assertEquals(new SatDelta(2, 0), StrContains.create(s, t).satDelta(model));
   }

   @Test
   public void testSatDelta4() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);

      MapModel model = new MapModel();
      model.put(s, StrVal.create("xyyx"));
      model.put(t, StrVal.create("z"));

      assertEquals(new SatDelta(1, 0), StrContains.create(s, t).satDelta(model));
   }
}
