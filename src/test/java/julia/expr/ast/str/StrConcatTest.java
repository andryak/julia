package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.StrSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrConcatTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrConcatOfConstStrings() {
      IExpr expr = StrConcat.create(StrVal.create("te"), StrVal.create("st"));
      assertEquals(StrVal.create("test"), expr);
   }

   @Test
   public void testStrConcatOfEmptyString1() {
      Fun s = Str.create(0);
      IExpr expr = StrConcat.create(s, StrVal.EMPTY);
      assertEquals(s, expr);
   }

   @Test
   public void testStrConcatOfEmptyString2() {
      Fun s = Str.create(0);
      IExpr expr = StrConcat.create(StrVal.EMPTY, s);
      assertEquals(s, expr);
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrConcat.create(s, t);
      assertEquals(StrSort.create(), expr.sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrConcat.create(s, t);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("pun"));

      // The mighty kobold!
      assertEquals(StrVal.create("punpun"), expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrConcat.create(s, t);

      VarMap map = new VarMap();
      map.put(s, 1);
      map.put(t, 0);

      assertEquals(StrConcat.create(t, s), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      assertTrue(StrConcat.create(s, t).toSmt().contains("str.++"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      StrConcat.create(s, t).satDelta(new SortModel());
   }
}
