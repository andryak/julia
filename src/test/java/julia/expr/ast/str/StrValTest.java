package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import julia.expr.ast.Expr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.StrSort;
import julia.expr.utils.BigRational;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrValTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testEmptyStringIsSingleton() {
      StrVal c0 = StrVal.EMPTY;
      StrVal c1 = StrVal.create("");
      assertSame(c0, c1);
   }

   @Test
   public void testSomeStringsAreSingletons() {
      StrVal c0 = StrVal.create("A");
      StrVal c1 = StrVal.create("A");
      assertSame(c0, c1);
   }

   @Test
   public void testSomeStringsAreNotCached() {
      StrVal c0 = StrVal.create("A");
      for (int i = 0; i < 4096; ++i) {
         StrVal.create("B" + i);
      }
      StrVal c1 = StrVal.create("A");
      assertNotSame(c0, c1);
   }

   @Test
   public void testGetValueOfEmptyString() {
      assertEquals("", StrVal.EMPTY.getValue());
   }

   @Test
   public void testGetValueOfNonEmptyString() {
      assertEquals("test", StrVal.create("test").getValue());
   }

   @Test
   public void testSort() {
      assertEquals(StrSort.create(), StrVal.EMPTY.sort());
   }

   @Test
   public void testEmptyStringToString() {
      assertEquals("\"\"", StrVal.EMPTY.toString());
   }

   @Test
   public void testNonEmptyStringToString() {
      assertEquals("\"test\"", StrVal.create("test").toString());
   }

   @Test
   public void testEval() {
      assertEquals(StrVal.EMPTY, StrVal.EMPTY.eval());
   }

   @Test
   public void testRenameVars() {
      assertEquals(StrVal.EMPTY, StrVal.EMPTY.renameVars(null));
   }

   @Test
   public void testEmptyStringToSmt() {
      assertEquals("\"\"", StrVal.EMPTY.toSmt());
   }

   @Test
   public void testNonEmptyStringToSmt() {
      assertEquals("\"test\"", StrVal.create("test").toSmt());
   }

   @Test
   public void testStringWithQuotesToSmt() {
      assertEquals("\"\"\"\"", StrVal.create("\"").toSmt());
   }

   @Test
   public void testDelta() {
      StrVal c0 = StrVal.create("test");
      StrVal c1 = StrVal.create("roast");
      assertEquals(BigRational.create(3), c0.delta(c1));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      StrVal.EMPTY.satDelta(new SortModel());
   }
}
