package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.StrSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntToStrTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testIntToStrOfOne() {
      assertEquals(StrVal.create("1"), IntToStr.create(IntVal.ONE));
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      assertEquals(StrSort.create(), IntToStr.create(x).sort());
   }

   @Test
   public void testEval() {
      Fun x = Int.create(0);
      IExpr expr = IntToStr.create(x);

      SortModel model = new SortModel();
      model.setIntInter(IntVal.create(42));

      assertEquals(StrVal.create("42"), expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun x = Int.create(0);
      IExpr expr = IntToStr.create(x);

      VarMap map = new VarMap();
      map.put(x, 1);

      assertEquals(IntToStr.create(Int.create(1)), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      assertTrue(IntToStr.create(x).toSmt().contains("int.to.str"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      IntToStr.create(x).satDelta(new SortModel());
   }
}
