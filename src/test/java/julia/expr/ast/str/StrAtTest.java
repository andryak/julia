package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.StrSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrAtTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrAtZeroOfNonEmptyString() {
      assertEquals(StrVal.create("t"), StrAt.create(StrVal.create("test"), 0));
   }

   @Test
   public void testStrAtOneOfNonEmptyString() {
      assertEquals(StrVal.create("e"), StrAt.create(StrVal.create("test"), 1));
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      assertEquals(StrSort.create(), StrAt.create(s, 0).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      IExpr expr = StrAt.create(s, 0);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(StrVal.create("t"), expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      IExpr expr = StrAt.create(s, 0);

      VarMap map = new VarMap();
      map.put(s, 1);

      assertEquals(StrAt.create(Str.create(1), 0), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      assertTrue(StrAt.create(s, 0).toSmt().contains("str.at"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      StrAt.create(s, 0).satDelta(new SortModel());
   }
}
