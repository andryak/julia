package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.StrSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class SubStrTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testSubStringOfEmptyString() {
      assertEquals(StrVal.EMPTY, SubStr.create(StrVal.EMPTY, IntVal.ZERO, IntVal.ZERO));
   }

   @Test
   public void testSubStringOfNonEmptyString1() {
      assertEquals(StrVal.EMPTY, SubStr.create(StrVal.create("test"), IntVal.TWO, IntVal.ZERO));
   }

   @Test
   public void testSubStringOfNonEmptyString2() {
      IExpr expr = SubStr.create(StrVal.create("test"), IntVal.TWO, IntVal.ONE);
      assertEquals(StrVal.create("s"), expr);
   }

   @Test
   public void testSubStringOfNonEmptyString3() {
      IExpr expr = SubStr.create(StrVal.create("test"), IntVal.TWO, IntVal.TWO);
      assertEquals(StrVal.create("st"), expr);
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      assertEquals(StrSort.create(), SubStr.create(s, IntVal.ZERO, IntVal.TWO).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(StrVal.create("te"), SubStr.create(s, IntVal.ZERO, IntVal.TWO).eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);

      VarMap map = new VarMap();
      map.put(s, 1);

      IExpr expected = SubStr.create(Str.create(1), IntVal.ZERO, IntVal.TWO);
      IExpr actual = SubStr.create(s, IntVal.ZERO, IntVal.TWO).renameVars(map);
      assertEquals(expected, actual);
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      assertTrue(SubStr.create(s, IntVal.ZERO, IntVal.TWO).toSmt().contains("str.substr"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      SubStr.create(s, IntVal.ZERO, IntVal.TWO).satDelta(new SortModel());
   }
}
