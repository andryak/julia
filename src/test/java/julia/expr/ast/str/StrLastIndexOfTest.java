package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import julia.utils.VarMap;
import julia.utils.Z3StrUtils;
import julia.utils.Z3StrUtils.Z3StrDecl;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrLastIndexOfTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrLastIndexOfEmptyString() {
      assertEquals(
            IntVal.create(4),
            StrLastIndexOf.create(StrVal.create("test"), StrVal.EMPTY));
   }

   @Test
   public void testStrLastIndexOfNonEmptyString1() {
      IExpr expr = StrLastIndexOf.create(StrVal.create("test"), StrVal.create("es"));
      assertEquals(IntVal.create(1), expr);
   }

   @Test
   public void testStrLastIndexOfNonEmptyString2() {
      IExpr expr = StrLastIndexOf.create(StrVal.create("test"), StrVal.create("t"));
      assertEquals(IntVal.create(3), expr);
   }

   @Test
   public void testStrLastIndexOfNonEmptyString3() {
      IExpr expr = StrLastIndexOf.create(StrVal.create("test"), StrVal.create("et"));
      assertEquals(IntVal.create(-1), expr);
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      assertEquals(IntSort.create(), StrLastIndexOf.create(s, t).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrLastIndexOf.create(s, t);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(IntVal.ZERO, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrLastIndexOf.create(s, t);

      VarMap map = new VarMap();
      map.put(s, 1);
      map.put(t, 0);

      assertEquals(StrLastIndexOf.create(t, s), expr.renameVars(map));
   }

   @Test(expected = IllegalStateException.class)
   public void testToSmt() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      StrLastIndexOf.create(s, t).toSmt();
   }

   @Test
   public void testToZ3StrDecl() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      Z3StrDecl decl = Z3StrUtils.toZ3StrDecl(StrLastIndexOf.create(s, t));

      assertEquals(2, decl.getVarDecls().size());
      assertTrue(decl.getQuery().contains("LastIndexof"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      StrLastIndexOf.create(s, t).satDelta(new SortModel());
   }
}
