package julia.expr.ast.str;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrIndexOfTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testStrIndexOfEmptyString() {
      assertEquals(IntVal.ZERO, StrIndexOf.create(StrVal.create("test"), StrVal.EMPTY));
   }

   @Test
   public void testStrIndexOfNonEmptyString1() {
      IExpr expr = StrIndexOf.create(StrVal.create("test"), StrVal.create("es"));
      assertEquals(IntVal.create(1), expr);
   }

   @Test
   public void testStrIndexOfNonEmptyString2() {
      IExpr expr = StrIndexOf.create(StrVal.create("test"), StrVal.create("et"));
      assertEquals(IntVal.create(-1), expr);
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      assertEquals(IntSort.create(), StrIndexOf.create(s, t).sort());
   }

   @Test
   public void testEval() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrIndexOf.create(s, t);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(IntVal.ZERO, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      IExpr expr = StrIndexOf.create(s, t);

      VarMap map = new VarMap();
      map.put(s, 1);
      map.put(t, 0);

      assertEquals(StrIndexOf.create(t, s), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      assertTrue(StrIndexOf.create(s, t).toSmt().contains("str.indexof"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun s = Str.create(0);
      Fun t = Str.create(1);
      StrIndexOf.create(s, t).satDelta(new SortModel());
   }
}
