package julia.expr.ast.re;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.str.Str;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MatchTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testConstStringMatchesConstRegex() {
      IExpr s = StrVal.create("010101");
      IExpr re = ReStar.create(StrToRe.create(StrVal.create("01")));
      assertEquals(BoolVal.TRUE, Match.create(s, re).eval());
   }

   @Test
   public void testSort() {
      Fun s = Str.create(0);
      IExpr re = StrToRe.create(StrVal.create("test"));
      assertEquals(BoolSort.create(), Match.create(s, re).sort());
   }

   @Test
   public void testEval1() {
      Fun s = Str.create(0);
      IExpr re = StrToRe.create(StrVal.create("test"));
      IExpr expr = Match.create(s, re);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("test"));

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun s = Str.create(0);
      IExpr re = StrToRe.create(StrVal.create("test"));
      IExpr expr = Match.create(s, re);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("toast"));

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testEval3() {
      Fun s = Str.create(0);
      IExpr a = StrToRe.create(StrVal.create("a"));
      IExpr b = StrToRe.create(StrVal.create("b"));
      IExpr re = ReConcat.create(a, b);
      IExpr expr = Match.create(s, re);

      SortModel model = new SortModel();
      model.setStringInter(StrVal.create("ab"));

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testEval4() {
      Fun s = Str.create(0);
      IExpr a = StrToRe.create(StrVal.create("a"));
      IExpr b = StrToRe.create(StrVal.create("b"));
      IExpr re = ReUnion.create(a, b);
      IExpr expr = Match.create(s, re);

      SortModel model1 = new SortModel();
      SortModel model2 = new SortModel();
      model1.setStringInter(StrVal.create("a"));
      model2.setStringInter(StrVal.create("b"));

      assertEquals(BoolVal.TRUE, expr.eval(model1));
      assertEquals(BoolVal.TRUE, expr.eval(model2));
   }

   @Test
   public void testRenameVars() {
      Fun s = Str.create(0);
      IExpr re = StrToRe.create(StrVal.create("test"));
      IExpr expr = Match.create(s, re);

      VarMap map = new VarMap();
      map.put(s, 1);

      assertEquals(Match.create(Str.create(1), re), expr.renameVars(map));
   }

   @Test
   public void testToSmt() {
      Fun s = Str.create(0);
      IExpr expr = Match.create(s, StrToRe.create(StrVal.create("test")));
      assertTrue(expr.toSmt().contains("str.in.re"));
   }

   @Test
   public void testSatDelta1() {
      Fun s = Str.create(0);
      IExpr re = StrToRe.create(StrVal.create("test"));
      SatDelta delta = Match.create(s, re).satDelta(new SortModel());
      assertTrue(delta.dir().compareTo(BigRational.ZERO) > 0);
   }

   @Test
   public void testSatDelta2() {
      Fun s = Str.create(0);
      IExpr re = StrToRe.create(StrVal.create(""));
      SatDelta delta = Match.create(s, re).satDelta(new SortModel());
      assertTrue(delta.dir().compareTo(BigRational.ZERO) == 0);
   }
}
