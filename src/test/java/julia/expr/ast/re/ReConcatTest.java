package julia.expr.ast.re;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.str.StrVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.ReSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ReConcatTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testSort() {
      IExpr re1 = StrToRe.create(StrVal.create("hello"));
      IExpr re2 = StrToRe.create(StrVal.create("world"));
      assertEquals(ReSort.create(), ReConcat.create(re1, re2).sort());
   }

   @Test
   public void testEval() {
      IExpr re1 = StrToRe.create(StrVal.create("hello"));
      IExpr re2 = StrToRe.create(StrVal.create("world"));
      IExpr expr = ReConcat.create(re1, re2);
      assertEquals(expr, expr.eval());
   }

   @Test
   public void testRenameVars() {
      IExpr re1 = StrToRe.create(StrVal.create("hello"));
      IExpr re2 = StrToRe.create(StrVal.create("world"));
      IExpr expr = ReConcat.create(re1, re2);
      assertEquals(expr, expr.renameVars(new VarMap()));
   }

   @Test
   public void testToSmt() {
      IExpr re1 = StrToRe.create(StrVal.create("hello"));
      IExpr re2 = StrToRe.create(StrVal.create("world"));
      IExpr expr = ReConcat.create(re1, re2);
      assertTrue(expr.toSmt().contains("re.++"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      IExpr re1 = StrToRe.create(StrVal.create("hello"));
      IExpr re2 = StrToRe.create(StrVal.create("world"));
      ReConcat.create(re1, re2).satDelta(new SortModel());
   }
}
