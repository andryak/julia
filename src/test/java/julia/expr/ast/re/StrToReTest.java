package julia.expr.ast.re;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.str.StrVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.ReSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrToReTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testSort() {
      assertEquals(ReSort.create(), StrToRe.create(StrVal.create("test")).sort());
   }

   @Test
   public void testEval() {
      IExpr expr = StrToRe.create(StrVal.create("test"));
      assertEquals(expr, expr.eval());
   }

   @Test
   public void testRenameVars() {
      IExpr expr = StrToRe.create(StrVal.create("test"));
      assertEquals(expr, expr.renameVars(new VarMap()));
   }

   @Test
   public void testToSmt() {
      assertTrue(StrToRe.create(StrVal.create("test")).toSmt().contains("str.to.re"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      StrToRe.create(StrVal.create("test")).satDelta(new SortModel());
   }
}
