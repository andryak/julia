package julia.expr.ast.array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.ArraySort;
import julia.expr.utils.ConstArray;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArrayValTest {
   private static final ConstArray CARRAY;
   static {
      CARRAY = new ConstArray(IntVal.ZERO);
   }

   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetValueOfConstArray() {
      ArrayVal carray = ArrayVal.create(CARRAY, ArraySort.INT_INT);
      assertEquals(CARRAY, carray.getValue());
   }

   @Test
   public void testSort() {
      ArrayVal carray = ArrayVal.create(CARRAY, ArraySort.INT_INT);
      assertEquals(ArraySort.INT_INT, carray.sort());
   }

   @Test
   public void testToString() {
      ArrayVal carray = ArrayVal.create(CARRAY, ArraySort.INT_INT);
      assertTrue(carray.toString().contains("0"));
   }

   @Test
   public void testEval() {
      ArrayVal carray = ArrayVal.create(CARRAY, ArraySort.INT_INT);
      assertEquals(carray, carray.eval(null));
   }

   @Test
   public void testRenameVars() {
      ArrayVal carray = ArrayVal.create(CARRAY, ArraySort.INT_INT);
      assertEquals(carray, carray.renameVars(null));
   }

   @Test(expected = IllegalStateException.class)
   public void testToSmt() {
      ArrayVal.create(CARRAY, ArraySort.INT_INT).toSmt();
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      ArrayVal.create(CARRAY, ArraySort.INT_INT).satDelta(new SortModel());
   }
}
