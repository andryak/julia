package julia.expr.ast.array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.ArraySort;
import julia.expr.utils.BigRational;
import julia.expr.utils.ConstArray;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArraySelectTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testCreateConstSelect() {
      ArrayVal carray = ArrayVal.create(new ConstArray(IntVal.TWO), ArraySort.INT_INT);
      assertEquals(IntVal.TWO, ArraySelect.create(carray, IntVal.ZERO));
   }

   @Test
   public void testCreateNonConstSelect1() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      assertEquals(ArraySelect.class, ArraySelect.create(array, IntVal.ZERO).getClass());
   }

   @Test
   public void testCreateNonConstSelect2() {
      ArrayVal carray = ArrayVal.create(new ConstArray(IntVal.TWO), ArraySort.INT_INT);
      assertEquals(ArraySelect.class, ArraySelect.create(carray, Int.create(0)).getClass());
   }

   @Test
   public void testToSmt() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      IExpr expr = ArraySelect.create(array, IntVal.ZERO);
      assertTrue(expr.toSmt().contains("select"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefinedForIntArray() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      ArraySelect.create(array, IntVal.ZERO).satDelta(new SortModel());
   }

   @Test
   public void testSatDeltaIsDefinedForBoolArray() {
      Fun array = Array.create(0, ArraySort.INT_BOOL);
      SatDelta delta = ArraySelect.create(array, IntVal.ZERO).satDelta(new SortModel());
      assertEquals(new SatDelta(BigRational.POS_INFINITY, BigRational.ZERO), delta);
   }
}
