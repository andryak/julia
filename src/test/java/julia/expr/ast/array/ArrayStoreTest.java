package julia.expr.ast.array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.HashMap;
import java.util.Map;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.ArraySort;
import julia.expr.utils.ConstArray;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArrayStoreTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testCreateConstStore() {
      IConst _default = IntVal.create(42);

      Map<IConst, IConst> map = new HashMap<>();
      map.put(IntVal.ZERO, IntVal.ONE);

      ArrayVal carray1 = ArrayVal.create(new ConstArray(_default), ArraySort.INT_INT);
      ArrayVal carray2 = ArrayVal.create(new ConstArray(map, _default), ArraySort.INT_INT);

      assertEquals(carray2, ArrayStore.create(carray1, IntVal.ZERO, IntVal.ONE));
   }

   @Test
   public void testCreateNonConstStore1() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      IExpr expr = ArrayStore.create(array, IntVal.ZERO, IntVal.ONE);
      assertEquals(ArrayStore.class, expr.getClass());
   }

   @Test
   public void testCreateNonConstStore2() {
      ArrayVal carray = ArrayVal.create(new ConstArray(IntVal.ZERO), ArraySort.INT_INT);
      IExpr expr = ArrayStore.create(carray, Int.create(0), IntVal.ONE);
      assertEquals(ArrayStore.class, expr.getClass());
   }

   @Test
   public void testCreateNonConstStore3() {
      ArrayVal carray = ArrayVal.create(new ConstArray(IntVal.ZERO), ArraySort.INT_INT);
      IExpr expr = ArrayStore.create(carray, IntVal.ZERO, Int.create(0));
      assertEquals(ArrayStore.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      IExpr expr = ArrayStore.create(array, IntVal.ZERO, IntVal.ONE);
      assertEquals(ArraySort.INT_INT, expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      IExpr expr = ArrayStore.create(array, IntVal.ZERO, IntVal.ONE);
      assertTrue(expr.toSmt().contains("store"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      ArrayStore.create(array, IntVal.ZERO, IntVal.ONE).satDelta(new SortModel());
   }
}
