package julia.expr.ast.array;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.uf.Fun;
import julia.expr.sort.ArraySort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArrayTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetIndex1() {
      Fun array = Array.create(0, ArraySort.INT_INT);
      assertEquals(0, array.getIndex());
   }

   @Test
   public void testGetIndex2() {
      Fun array = Array.create(1, ArraySort.INT_INT);
      assertEquals(1, array.getIndex());
   }

   @Test
   public void testGetIndex3() {
      Fun array = Array.create(2, ArraySort.INT_INT);
      assertEquals(2, array.getIndex());
   }
}
