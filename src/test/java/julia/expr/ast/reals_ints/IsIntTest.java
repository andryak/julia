package julia.expr.ast.reals_ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IsIntTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testIsIntOfConst1() {
      assertEquals(BoolVal.TRUE, IsInt.create(RealVal.create(1)));
   }

   @Test
   public void testIsIntOfConst2() {
      assertEquals(BoolVal.FALSE, IsInt.create(RealVal.create("1/2")));
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      IExpr expr = IsInt.create(x);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      IExpr expr = IsInt.create(x);
      assertTrue(expr.toSmt().contains("is_int"));
   }

   @Test
   public void testSatDelta1() {
      IExpr expr = IsInt.create(Real.create(0));

      SortModel model = new SortModel();
      model.setRealInter(RealVal.create("3/2"));

      SatDelta expected = new SatDelta(BigRational.create("1/2"), BigRational.ZERO);
      assertEquals(expected, expr.satDelta(model));
   }

   @Test
   public void testSatDelta2() {
      IExpr expr = IsInt.create(Real.create(0));
      assertEquals(new SatDelta(0, 1), expr.satDelta(new SortModel()));
   }
}
