package julia.expr.ast.reals_ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ToIntTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testToIntOfConst1() {
      assertEquals(IntVal.ZERO, ToInt.create(RealVal.create("1/2")));
   }

   @Test
   public void testToIntOfConst2() {
      assertEquals(IntVal.ONE, ToInt.create(RealVal.create("3/2")));
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      IExpr expr = ToInt.create(x);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      IExpr expr = ToInt.create(x);
      assertTrue(expr.toSmt().contains("to_int"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDelta2() {
      IExpr expr = ToInt.create(Real.create(0));
      expr.satDelta(new SortModel());
   }
}
