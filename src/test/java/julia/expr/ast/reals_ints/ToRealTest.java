package julia.expr.ast.reals_ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.RealSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ToRealTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testToRealOfConst1() {
      assertEquals(RealVal.ZERO, ToReal.create(IntVal.create(0)));
   }

   @Test
   public void testToRealOfConst2() {
      assertEquals(RealVal.ONE, ToReal.create(IntVal.create(1)));
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      IExpr expr = ToReal.create(x);
      assertEquals(RealSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      IExpr expr = ToReal.create(x);
      assertTrue(expr.toSmt().contains("to_real"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDelta2() {
      IExpr expr = ToReal.create(Int.create(0));
      expr.satDelta(new SortModel());
   }
}
