package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AndTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNoParamsAnd() {
      assertEquals(BoolVal.TRUE, And.create());
   }

   @Test
   public void testOneParamAnd() {
      Fun x = Bool.create(0);
      IExpr expr = And.create(x);
      assertEquals(x, expr);
   }

   @Test
   public void testFalseAndFalse() {
      IExpr expr = And.create(BoolVal.FALSE, BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTrueAndFalse() {
      IExpr expr = And.create(BoolVal.TRUE, BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testFalseAndTrue() {
      IExpr expr = And.create(BoolVal.FALSE, BoolVal.TRUE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTrueAndTrue() {
      IExpr expr = And.create(BoolVal.TRUE, BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXAndY() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = And.create(x, y);
      assertEquals(And.class, expr.getClass());
   }

   @Test
   public void testXAndX() {
      Fun x = Bool.create(0);
      IExpr expr = And.create(x, x);
      assertEquals(x, expr);
   }

   @Test
   public void testAndOfAnds() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr param = And.create(x, y);
      IExpr expr = And.create(param, param);
      assertEquals(And.create(x, x, y, y), expr);
   }

   @Test
   public void testSort() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = And.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = And.create(x, y);
      assertTrue(expr.toSmt().contains("and"));
   }

   @Test
   public void testEval1() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = And.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.TRUE);

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = And.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.FALSE);

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = And.create(x, y);

      VarMap map = new VarMap();
      map.put(x, 1);
      map.put(y, 0);

      assertEquals(And.create(y, x), expr.renameVars(map));
   }

   @Test
   public void testSatDelta() {
      Fun x = Int.create(0);
      IExpr expr1 = IntGt.create(x, IntVal.ZERO);
      IExpr expr2 = IntGt.create(x, IntVal.ONE);
      IExpr expr3 = IntGt.create(x, IntVal.TWO);
      IExpr expr = And.create(expr1, expr2, expr3);
      assertEquals(new SatDelta(6, 0), expr.satDelta(new SortModel()));
   }
}
