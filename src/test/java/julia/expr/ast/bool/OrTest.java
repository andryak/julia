package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class OrTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNoParamsOr() {
      assertEquals(BoolVal.FALSE, Or.create());
   }

   @Test
   public void testOneParamOr() {
      Fun x = Bool.create(0);
      IExpr expr = Or.create(x);
      assertEquals(x, expr);
   }

   @Test
   public void testFalseOrFalse() {
      IExpr expr = Or.create(BoolVal.FALSE, BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTrueOrFalse() {
      IExpr expr = Or.create(BoolVal.TRUE, BoolVal.FALSE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testFalseOrTrue() {
      IExpr expr = Or.create(BoolVal.FALSE, BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTrueOrTrue() {
      IExpr expr = Or.create(BoolVal.TRUE, BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXOrY() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Or.create(x, y);
      assertEquals(Or.class, expr.getClass());
   }

   @Test
   public void testXOrX() {
      Fun x = Bool.create(0);
      IExpr expr = Or.create(x, x);
      assertEquals(x, expr);
   }

   @Test
   public void testOrOfOrs() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr param = Or.create(x, y);
      IExpr expr = Or.create(param, param);
      assertEquals(Or.create(x, x, y, y), expr);
   }

   @Test
   public void testSort() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Or.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Or.create(x, y);
      assertTrue(expr.toSmt().contains("or"));
   }

   @Test
   public void testEval1() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Or.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.FALSE);
      model.put(y, BoolVal.FALSE);

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Or.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.FALSE);

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Or.create(x, y);

      VarMap map = new VarMap();
      map.put(x, 1);
      map.put(y, 0);

      assertEquals(Or.create(y, x), expr.renameVars(map));
   }

   @Test
   public void testSatDelta() {
      Fun x = Int.create(0);
      IExpr expr1 = IntGt.create(x, IntVal.ZERO);
      IExpr expr2 = IntGt.create(x, IntVal.ONE);
      IExpr expr3 = IntGt.create(x, IntVal.TWO);
      IExpr expr = Or.create(expr1, expr2, expr3);
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }
}
