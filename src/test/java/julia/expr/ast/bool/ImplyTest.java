package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ImplyTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testFalseImpliesFalse() {
      IExpr expr = Imply.create(BoolVal.FALSE, BoolVal.FALSE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testFalseImpliesTrue() {
      IExpr expr = Imply.create(BoolVal.FALSE, BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTrueImpliesFalse() {
      IExpr expr = Imply.create(BoolVal.TRUE, BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTrueImpliesTrue() {
      IExpr expr = Imply.create(BoolVal.TRUE, BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testFalseImpliesX() {
      IExpr expr = Imply.create(BoolVal.FALSE, Bool.create(0));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXImpliesTrue() {
      IExpr expr = Imply.create(Bool.create(0), BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXImpliesY() {
      IExpr expr = Imply.create(Bool.create(0), Bool.create(1));
      assertEquals(Or.class, expr.getClass());
   }

   @Test
   public void testSort() {
      IExpr expr = Imply.create(Bool.create(0), Bool.create(1));
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      IExpr expr = Imply.create(Bool.create(0), Bool.create(1));
      assertTrue(expr.toSmt().contains("or"));
      assertTrue(expr.toSmt().contains("not"));
   }

   @Test
   public void testEval1() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);
      IExpr expr = Imply.create(A, B);

      MapModel model = new MapModel();
      model.put(A, BoolVal.TRUE);
      model.put(B, BoolVal.FALSE);

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);
      IExpr expr = Imply.create(A, B);

      MapModel model = new MapModel();
      model.put(A, BoolVal.TRUE);
      model.put(B, BoolVal.TRUE);

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);
      IExpr expr = Imply.create(A, B);

      VarMap map = new VarMap();
      map.put(A, 1);
      map.put(B, 0);

      assertEquals(Imply.create(B, A), expr.renameVars(map));
   }

   @Test
   public void testSatDelta() {
      Fun A = Bool.create(0);
      Fun B = Bool.create(1);
      assertEquals(new SatDelta(0, 1), Imply.create(A, B).satDelta(new SortModel()));
   }
}
