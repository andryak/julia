package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.usort.USort;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.sort.DeclareSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DistinctTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testFalseDistinctFalse() {
      IExpr expr = Distinct.create(BoolVal.FALSE, BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTrueDistinctFalse() {
      IExpr expr = Distinct.create(BoolVal.TRUE, BoolVal.FALSE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testZeroDistinctZero() {
      IExpr expr = Distinct.create(IntVal.ZERO, IntVal.ZERO);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testZeroDistinctOne() {
      IExpr expr = Distinct.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXDistinctY() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Distinct.create(x, y);
      assertEquals(Distinct.class, expr.getClass());
   }

   @Test(expected = IllegalArgumentException.class)
   public void testCreateWithChildrenOfDifferentSorts() {
      Fun x = Bool.create(0);
      Fun y = Int.create(1);
      Distinct.create(x, y);
   }

   @Test
   public void testXDistinctX() {
      Fun x = Bool.create(0);
      IExpr expr = Distinct.create(x, x);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testSort() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Distinct.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Distinct.create(x, y);
      assertTrue(expr.toSmt().contains("distinct"));
   }

   @Test
   public void testEval1() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Distinct.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.TRUE);

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Distinct.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.FALSE);

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Distinct.create(x, y);

      VarMap map = new VarMap();
      map.put(x, 1);
      map.put(y, 0);

      assertEquals(Distinct.create(y, x), expr.renameVars(map));
   }

   @Test
   public void testSatDeltaIsZero() {
      IExpr expr = Distinct.create(Int.create(0), IntVal.TWO);
      assertEquals(new SatDelta(0, 2), expr.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaWithIntegers() {
      IExpr expr = Distinct.create(Int.create(0), Int.create(1));
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaWithReals() {
      IExpr expr = Distinct.create(Real.create(0), Real.create(1));
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaWithUSorts() {
      final DeclareSort A = DeclareSort.A;
      IExpr expr = Distinct.create(USort.create(0, A), USort.create(1, A));
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }
}
