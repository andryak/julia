package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class NotTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNotFalse() {
      IExpr expr = Not.create(BoolVal.FALSE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testNotTrue() {
      IExpr expr = Not.create(BoolVal.TRUE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testNotX() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(A);
      assertEquals(Not.class, expr.getClass());
   }

   @Test
   public void testNotNotX() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(Not.create(A));
      assertEquals(A, expr);
   }

   @Test
   public void testSort() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(A);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(A);
      assertTrue(expr.toSmt().contains("not"));
   }

   @Test
   public void testEval1() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(A);

      MapModel model = new MapModel();
      model.put(A, BoolVal.TRUE);

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(A);

      MapModel model = new MapModel();
      model.put(A, BoolVal.FALSE);

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun A = Bool.create(0);
      IExpr expr = Not.create(A);

      VarMap map = new VarMap();
      map.put(A, 1);

      assertEquals(Not.create(Bool.create(1)), expr.renameVars(map));
   }

   @Test
   public void testSatDelta() {
      Fun A = Bool.create(0);
      assertEquals(new SatDelta(0, 1), Not.create(A).satDelta(new SortModel()));
   }
}
