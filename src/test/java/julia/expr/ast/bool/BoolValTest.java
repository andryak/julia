package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BoolValTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testIsFalseOfFalse() {
      assertTrue(BoolVal.FALSE.isFalse());
   }

   @Test
   public void testIsTrueOfTrue() {
      assertTrue(BoolVal.TRUE.isTrue());
   }

   @Test
   public void testFalseIsSingleton() {
      assertSame(BoolVal.FALSE, BoolVal.create(false));
   }

   @Test
   public void testTrueIsSingleton() {
      assertSame(BoolVal.TRUE, BoolVal.create(true));
   }

   @Test
   public void testTrueEqualsTrue() {
      assertEquals(BoolVal.TRUE, BoolVal.TRUE);
   }

   @Test
   public void testTrueIsNotFalse() {
      assertNotEquals(BoolVal.TRUE, BoolVal.FALSE);
   }

   @Test
   public void testGetValueOfTrue() {
      assertEquals(true, BoolVal.TRUE.getValue());
   }

   @Test
   public void testGetValueOfFalse() {
      assertEquals(false, BoolVal.FALSE.getValue());
   }

   @Test
   public void testSort() {
      assertSame(BoolSort.create(), BoolVal.TRUE.sort());
   }

   @Test
   public void testFalseToString() {
      assertEquals("false", BoolVal.FALSE.toString());
   }

   @Test
   public void testTrueToString() {
      assertEquals("true", BoolVal.TRUE.toString());
   }

   @Test
   public void testEval() {
      assertEquals(BoolVal.TRUE, BoolVal.TRUE.eval());
   }

   @Test
   public void testRenameVars() {
      assertEquals(BoolVal.TRUE, BoolVal.TRUE.renameVars(null));
   }

   @Test
   public void testFalseToSmt() {
      assertEquals("false", BoolVal.FALSE.toSmt());
   }

   @Test
   public void testTrueToSmt() {
      assertEquals("true", BoolVal.TRUE.toSmt());
   }

   @Test
   public void testSatDeltaOfTrue() {
      SatDelta delta = new SatDelta(BigRational.ZERO, BigRational.POS_INFINITY);
      assertEquals(delta, BoolVal.TRUE.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaOfFalse() {
      SatDelta delta = new SatDelta(BigRational.POS_INFINITY, BigRational.ZERO);
      assertEquals(delta, BoolVal.FALSE.satDelta(new SortModel()));
   }
}
