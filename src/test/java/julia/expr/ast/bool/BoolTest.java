package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.uf.Fun;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BoolTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetIndex1() {
      Fun A = Bool.create(0);
      assertEquals(0, A.getIndex());
   }

   @Test
   public void testGetIndex2() {
      Fun B = Bool.create(1);
      assertEquals(1, B.getIndex());
   }

   @Test
   public void testGetIndex3() {
      Fun C = Bool.create(2);
      assertEquals(2, C.getIndex());
   }
}
