package julia.expr.ast.bool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.usort.USort;
import julia.expr.ast.usort.USortVal;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.sort.DeclareSort;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class EqualTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testFalseEqualFalse() {
      IExpr expr = Equal.create(BoolVal.FALSE, BoolVal.FALSE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTrueEqualFalse() {
      IExpr expr = Equal.create(BoolVal.TRUE, BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testZeroEqualZero() {
      IExpr expr = Equal.create(IntVal.ZERO, IntVal.ZERO);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testZeroEqualOne() {
      IExpr expr = Equal.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXEqualY() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Equal.create(x, y);
      assertEquals(Equal.class, expr.getClass());
   }

   @Test(expected = IllegalArgumentException.class)
   public void testCreateWithChildrenOfDifferentSorts() {
      Fun x = Bool.create(0);
      Fun y = Int.create(1);
      Equal.create(x, y);
   }

   @Test
   public void testXEqualX() {
      Fun x = Bool.create(0);
      IExpr expr = Equal.create(x, x);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testSort() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Equal.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Equal.create(x, y);
      assertTrue(expr.toSmt().contains("="));
   }

   @Test
   public void testEval1() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Equal.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.TRUE);

      assertEquals(BoolVal.TRUE, expr.eval(model));
   }

   @Test
   public void testEval2() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Equal.create(x, y);

      MapModel model = new MapModel();
      model.put(x, BoolVal.TRUE);
      model.put(y, BoolVal.FALSE);

      assertEquals(BoolVal.FALSE, expr.eval(model));
   }

   @Test
   public void testRenameVars() {
      Fun x = Bool.create(0);
      Fun y = Bool.create(1);
      IExpr expr = Equal.create(x, y);

      VarMap map = new VarMap();
      map.put(x, 1);
      map.put(y, 0);

      assertEquals(Equal.create(y, x), expr.renameVars(map));
   }

   @Test
   public void testSatDeltaIsZero() {
      IExpr expr = Equal.create(Int.create(0), Int.create(1));
      assertEquals(new SatDelta(0, 1), expr.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaWithIntegers() {
      IExpr expr = Equal.create(Int.create(0), IntVal.TWO);
      assertEquals(new SatDelta(2, 0), expr.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaWithReals() {
      IExpr expr = Equal.create(Real.create(0), RealVal.TWO);
      assertEquals(new SatDelta(2, 0), expr.satDelta(new SortModel()));
   }

   @Test
   public void testSatDeltaWithUSorts() {
      final DeclareSort A = DeclareSort.A;
      IExpr expr = Equal.create(USort.create(0, A), USortVal.create(1, A));
      assertEquals(new SatDelta(1, 0), expr.satDelta(new SortModel()));
   }
}
