package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.RealSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealMulTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNoParamsMul() {
      assertEquals(RealVal.ONE, RealMul.create());
   }

   @Test
   public void testOneParamMul() {
      Fun x = Real.create(0);
      IExpr expr = RealMul.create(x);
      assertEquals(x, expr);
   }

   @Test
   public void testZeroTimesZero() {
      IExpr expr = RealMul.create(RealVal.ZERO, RealVal.ZERO);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testOneTimesZero() {
      IExpr expr = RealMul.create(RealVal.ONE, RealVal.ZERO);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testZeroTimesOne() {
      IExpr expr = RealMul.create(RealVal.ZERO, RealVal.ONE);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testOneTimesTwo() {
      IExpr expr = RealMul.create(RealVal.ONE, RealVal.TWO);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testTwoTimesOne() {
      IExpr expr = RealMul.create(RealVal.TWO, RealVal.ONE);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testTwoTimesTwo() {
      IExpr expr = RealMul.create(RealVal.TWO, RealVal.TWO);
      assertEquals(RealVal.create(4), expr);
   }

   @Test
   public void testLargeValueTimesLargeValue() {
      IExpr c = RealVal.create(32768);
      IExpr expr = RealMul.create(c, c);
      assertEquals(RealVal.create("1073741824"), expr);
   }

   @Test
   public void testXTimesY() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealMul.create(x, y);
      assertEquals(RealMul.class, expr.getClass());
   }

   @Test
   public void testMulOfMuls() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr param = RealMul.create(x, y);
      IExpr expr = RealMul.create(param, param);
      assertEquals(RealMul.create(x, x, y, y), expr);
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealMul.create(x, y);
      assertEquals(RealSort.create(), expr.sort());
   }

   @Test
   public void testCoeff() {
      Fun x = Real.create(0);
      RealMul expr = (RealMul) RealMul.create(x, RealVal.TWO);
      assertEquals(RealVal.TWO, expr.coeff());
   }

   @Test
   public void testTerms() {
      Fun x = Real.create(0);
      RealMul expr = (RealMul) RealMul.create(x, RealVal.TWO);
      assertEquals(1, expr.terms().size());
      assertTrue(expr.terms().contains(x));
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealMul.create(x, y);
      assertTrue(expr.toSmt().contains("*"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      RealMul.create(x, y).satDelta(new SortModel());
   }
}
