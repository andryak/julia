package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.RealSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealDivTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroDivOne() {
      IExpr expr = RealDiv.create(RealVal.ZERO, RealVal.ONE);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testZeroDivTwo() {
      IExpr expr = RealDiv.create(RealVal.ZERO, RealVal.TWO);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testOneDivOne() {
      IExpr expr = RealDiv.create(RealVal.ONE, RealVal.ONE);
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testTwoDivOne() {
      IExpr expr = RealDiv.create(RealVal.TWO, RealVal.ONE);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testEightDivThree() {
      IExpr expr = RealDiv.create(RealVal.create(8), RealVal.create(3));
      assertEquals(RealVal.create("8/3"), expr);
   }

   @Test
   public void testEightDivFour() {
      IExpr expr = RealDiv.create(RealVal.create(8), RealVal.create(4));
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testXDivOne() {
      Fun x = Real.create(0);
      IExpr expr = RealDiv.create(x, RealVal.ONE);
      assertEquals(x, expr);
   }

   @Test
   public void testZeroDivX() {
      Fun x = Real.create(0);
      IExpr expr = RealDiv.create(RealVal.ZERO, x);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testXDivY() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealDiv.create(x, y);
      assertEquals(RealDiv.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealDiv.create(x, y);
      assertEquals(RealSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealDiv.create(x, y);
      assertTrue(expr.toSmt().contains("/"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      RealDiv.create(x, y).satDelta(new SortModel());
   }
}
