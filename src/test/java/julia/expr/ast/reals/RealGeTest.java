package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealGeTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroGeOne() {
      IExpr expr = RealGe.create(RealVal.ZERO, RealVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testZeroGeTwo() {
      IExpr expr = RealGe.create(RealVal.ZERO, RealVal.TWO);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testOneGeOne() {
      IExpr expr = RealGe.create(RealVal.ONE, RealVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTwoGeOne() {
      IExpr expr = RealGe.create(RealVal.TWO, RealVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testEightGeThree() {
      IExpr expr = RealGe.create(RealVal.create(8), RealVal.create(3));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testThreeGeEight() {
      IExpr expr = RealGe.create(RealVal.create(3), RealVal.create(8));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testLargeValueGeLargeValue1() {
      IExpr c0 = RealVal.create(32810);
      IExpr c1 = RealVal.create(32768);
      IExpr expr = RealGe.create(c0, c1);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testLargeValueGeLargeValue2() {
      IExpr c0 = RealVal.create(32768);
      IExpr c1 = RealVal.create(32810);
      IExpr expr = RealGe.create(c0, c1);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXGeX() {
      Fun x = Real.create(0);
      IExpr expr = RealGe.create(x, x);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXGeY() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealGe.create(x, y);
      assertEquals(RealGe.class, expr.getClass());
      assertEquals(BoolVal.TRUE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealGe.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealGe.create(x, y);
      assertTrue(expr.toSmt().contains(">="));
   }

   @Test
   public void testSatDelta() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      assertEquals(new SatDelta(0, 1), RealGe.create(x, y).satDelta(new SortModel()));
   }
}
