package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.RealSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealAddTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNoParamsAdd() {
      assertEquals(RealVal.ZERO, RealAdd.create());
   }

   @Test
   public void testOneParamAdd() {
      Fun x = Real.create(0);
      IExpr expr = RealAdd.create(x);
      assertEquals(x, expr);
   }

   @Test
   public void testZeroPlusZero() {
      IExpr expr = RealAdd.create(RealVal.ZERO, RealVal.ZERO);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testOnePlusZero() {
      IExpr expr = RealAdd.create(RealVal.ONE, RealVal.ZERO);
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testZeroPlusOne() {
      IExpr expr = RealAdd.create(RealVal.ZERO, RealVal.ONE);
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testZeroPlusTwo() {
      IExpr expr = RealAdd.create(RealVal.ZERO, RealVal.TWO);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testTwoPlusZero() {
      IExpr expr = RealAdd.create(RealVal.TWO, RealVal.ZERO);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testOnePlusOne() {
      IExpr expr = RealAdd.create(RealVal.ONE, RealVal.ONE);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testLargeValuePlusLargeValue() {
      IExpr c = RealVal.create(32768);
      IExpr expr = RealAdd.create(c, c);
      assertEquals(RealVal.create(65536), expr);
   }

   @Test
   public void testXPlusY() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealAdd.create(x, y);
      assertEquals(RealAdd.class, expr.getClass());
   }

   @Test
   public void testXMinusX() {
      Fun x = Real.create(0);
      IExpr expr = RealAdd.create(x, RealNeg.create(x));
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testTwoXMinusX() {
      Fun x = Real.create(0);
      IExpr expr = RealAdd.create(RealMul.create(x, RealVal.TWO), RealNeg.create(x));
      assertEquals(x, expr);
   }

   @Test
   public void testAddOfAdds() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr param = RealAdd.create(x, y);
      IExpr expr = RealAdd.create(param, param);
      assertEquals(RealAdd.create(x, x, y, y), expr);
   }

   @Test
   public void testAddOfMuls() {
      Fun x = Real.create(0);
      IExpr param = RealMul.create(x, x);
      IExpr expr = RealAdd.create(param, param);
      assertEquals(RealMul.create(param, RealVal.TWO), expr);
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealAdd.create(x, y);
      assertEquals(RealSort.create(), expr.sort());
   }

   @Test
   public void testCoeff() {
      Fun x = Real.create(0);
      RealAdd expr = (RealAdd) RealAdd.create(x, RealVal.TWO);
      assertEquals(RealVal.TWO, expr.coeff());
   }

   @Test
   public void testTerms() {
      Fun x = Real.create(0);
      RealAdd expr = (RealAdd) RealAdd.create(x, RealVal.TWO);
      assertEquals(1, expr.terms().size());
      assertTrue(expr.terms().contains(x));
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealAdd.create(x, y);
      assertTrue(expr.toSmt().contains("+"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      RealAdd.create(x, y).satDelta(new SortModel());
   }
}
