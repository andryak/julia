package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealNegTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNegZero() {
      IExpr expr = RealNeg.create(RealVal.ZERO);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testNegOne() {
      IExpr expr = RealNeg.create(RealVal.ONE);
      assertEquals(RealVal.create(-1), expr);
   }

   @Test
   public void testNegTwo() {
      IExpr expr = RealNeg.create(RealVal.TWO);
      assertEquals(RealVal.create(-2), expr);
   }

   @Test
   public void testNegNegOne() {
      IExpr expr = RealNeg.create(RealVal.create(-1));
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testNegNegTwo() {
      IExpr expr = RealNeg.create(RealVal.create(-2));
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testNegOfLargeValue() {
      IExpr expr = RealNeg.create(RealVal.create(32768));
      assertEquals(RealVal.create(-32768), expr);
   }
}
