package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealLeTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroLeOne() {
      IExpr expr = RealLe.create(RealVal.ZERO, RealVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testZeroLeTwo() {
      IExpr expr = RealLe.create(RealVal.ZERO, RealVal.TWO);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testOneLeOne() {
      IExpr expr = RealLe.create(RealVal.ONE, RealVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTwoLeOne() {
      IExpr expr = RealLe.create(RealVal.TWO, RealVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testEightLeThree() {
      IExpr expr = RealLe.create(RealVal.create(8), RealVal.create(3));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testThreeLeEight() {
      IExpr expr = RealLe.create(RealVal.create(3), RealVal.create(8));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testLargeValueLeLargeValue1() {
      IExpr c0 = RealVal.create(32810);
      IExpr c1 = RealVal.create(32768);
      IExpr expr = RealLe.create(c0, c1);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testLargeValueLeLargeValue2() {
      IExpr c0 = RealVal.create(32768);
      IExpr c1 = RealVal.create(32810);
      IExpr expr = RealLe.create(c0, c1);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXLeX() {
      Fun x = Real.create(0);
      IExpr expr = RealLe.create(x, x);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXLeY() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealLe.create(x, y);
      assertEquals(RealLe.class, expr.getClass());
      assertEquals(BoolVal.TRUE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealLe.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      IExpr expr = RealLe.create(x, y);
      assertTrue(expr.toSmt().contains("<="));
   }

   @Test
   public void testSatDelta() {
      Fun x = Real.create(0);
      Fun y = Real.create(1);
      assertEquals(new SatDelta(0, 1), RealLe.create(x, y).satDelta(new SortModel()));
   }
}
