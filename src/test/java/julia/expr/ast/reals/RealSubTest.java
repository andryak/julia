package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealSubTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroSubOne() {
      IExpr expr = RealSub.create(RealVal.ZERO, RealVal.ONE);
      assertEquals(RealVal.create(-1), expr);
   }

   @Test
   public void testZeroSubTwo() {
      IExpr expr = RealSub.create(RealVal.ZERO, RealVal.TWO);
      assertEquals(RealVal.create(-2), expr);
   }

   @Test
   public void testOneSubOne() {
      IExpr expr = RealSub.create(RealVal.ONE, RealVal.ONE);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testTwoSubOne() {
      IExpr expr = RealSub.create(RealVal.TWO, RealVal.ONE);
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testEightSubThree() {
      IExpr expr = RealSub.create(RealVal.create(8), RealVal.create(3));
      assertEquals(RealVal.create(5), expr);
   }

   @Test
   public void testLargeValueSubLargeValue() {
      IExpr c0 = RealVal.create(32810);
      IExpr c1 = RealVal.create(32768);
      IExpr expr = RealSub.create(c0, c1);
      assertEquals(RealVal.create(42), expr);
   }

   @Test
   public void testXSubZero() {
      Fun x = Real.create(0);
      IExpr expr = RealSub.create(x, RealVal.ZERO);
      assertEquals(x, expr);
   }
}
