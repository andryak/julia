package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.RealSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealAbsTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testAbsZero() {
      IExpr expr = RealAbs.create(RealVal.ZERO);
      assertEquals(RealVal.ZERO, expr);
   }

   @Test
   public void testAbsOne() {
      IExpr expr = RealAbs.create(RealVal.ONE);
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testAbsTwo() {
      IExpr expr = RealAbs.create(RealVal.TWO);
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testAbsNegOne() {
      IExpr expr = RealAbs.create(RealVal.create(-1));
      assertEquals(RealVal.ONE, expr);
   }

   @Test
   public void testAbsNegTwo() {
      IExpr expr = RealAbs.create(RealVal.create(-2));
      assertEquals(RealVal.TWO, expr);
   }

   @Test
   public void testAbsOfLargeValue() {
      IExpr expr = RealAbs.create(RealVal.create(32768));
      assertEquals(RealVal.create(32768), expr);
   }

   @Test
   public void testAbsX() {
      Fun x = Real.create(0);
      IExpr expr = RealAbs.create(x);
      assertEquals(RealAbs.class, expr.getClass());
   }

   @Test
   public void testAbsOfAbs() {
      Fun x = Real.create(0);
      IExpr expr = RealAbs.create(RealAbs.create(x));
      assertEquals(RealAbs.create(x), expr);
   }

   @Test
   public void testSort() {
      Fun x = Real.create(0);
      IExpr expr = RealAbs.create(x);
      assertEquals(RealSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Real.create(0);
      IExpr expr = RealAbs.create(x);
      assertTrue(expr.toSmt().contains("abs"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Real.create(0);
      RealAbs.create(x).satDelta(new SortModel());
   }
}
