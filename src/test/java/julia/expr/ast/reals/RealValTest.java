package julia.expr.ast.reals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import julia.expr.ast.Expr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RealValTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroIsSingleton() {
      RealVal c0 = RealVal.ZERO;
      RealVal c1 = RealVal.create("0");
      RealVal c2 = RealVal.create(0);
      RealVal c3 = RealVal.create(0);
      assertSame(c0, c1);
      assertSame(c1, c2);
      assertSame(c2, c3);
   }

   @Test
   public void testOneIsSingleton() {
      RealVal c0 = RealVal.ONE;
      RealVal c1 = RealVal.create("1");
      RealVal c2 = RealVal.create(1);
      RealVal c3 = RealVal.create(1);
      assertSame(c0, c1);
      assertSame(c1, c2);
      assertSame(c2, c3);
   }

   @Test
   public void testTwoIsSingleton() {
      RealVal c0 = RealVal.TWO;
      RealVal c1 = RealVal.create("2");
      RealVal c2 = RealVal.create(2);
      RealVal c3 = RealVal.create(2);
      assertSame(c0, c1);
      assertSame(c1, c2);
      assertSame(c2, c3);
   }

   @Test
   public void testNegOneIsSingleton() {
      RealVal c0 = RealVal.create(-1);
      RealVal c1 = RealVal.create(-1);
      assertSame(c0, c1);
   }

   @Test
   public void testNegTwoIsSingleton() {
      RealVal c0 = RealVal.create(-2);
      RealVal c1 = RealVal.create(-2);
      assertSame(c0, c1);
   }

   @Test
   public void testOneIsNotTwo() {
      RealVal c0 = RealVal.ONE;
      RealVal c1 = RealVal.TWO;
      assertNotEquals(c0, c1);
   }

   @Test
   public void testLargeValuesAreNotSingletons1() {
      RealVal c0 = RealVal.create("18446744073709551616");
      RealVal c1 = RealVal.create("18446744073709551616");
      assertNotSame(c0, c1);
      assertEquals(c0, c1);
   }

   @Test
   public void testLargeValuesAreNotSingletons2() {
      RealVal c0 = RealVal.create(65536);
      RealVal c1 = RealVal.create(65536);
      assertNotSame(c0, c1);
      assertEquals(c0, c1);
   }

   @Test
   public void testLargeValuesAreNotSingletons3() {
      RealVal c0 = RealVal.create(-65536);
      RealVal c1 = RealVal.create(-65536);
      assertNotSame(c0, c1);
      assertEquals(c0, c1);
   }

   @Test
   public void testSomeLargeValuesAreDifferent() {
      RealVal c0 = RealVal.create(32768);
      RealVal c1 = RealVal.create(65536);
      assertNotEquals(c0, c1);
   }

   @Test
   public void testGetValueOfZero() {
      assertEquals(BigRational.create(0), RealVal.ZERO.getValue());
   }

   @Test
   public void testGetValueOfOne() {
      assertEquals(BigRational.create(1), RealVal.ONE.getValue());
   }

   @Test
   public void testGetValueOfTwo() {
      assertEquals(BigRational.create(2), RealVal.TWO.getValue());
   }

   @Test
   public void testGetValueOfLargeValue() {
      assertEquals(BigRational.create(32768), RealVal.create(32768).getValue());
   }

   @Test
   public void testSort() {
      assertEquals(RealSort.create(), RealVal.ZERO.sort());
   }

   @Test
   public void testZeroToString() {
      assertEquals("0", RealVal.ZERO.toString());
   }

   @Test
   public void testOneToString() {
      assertEquals("1", RealVal.ONE.toString());
   }

   @Test
   public void testTwoToString() {
      assertEquals("2", RealVal.TWO.toString());
   }

   @Test
   public void testNegTwoToString() {
      assertEquals("-2", RealVal.create(-2).toString());
   }

   @Test
   public void testEval() {
      assertEquals(RealVal.ZERO, RealVal.ZERO.eval());
   }

   @Test
   public void testRenameVars() {
      assertEquals(RealVal.ZERO, RealVal.ZERO.renameVars(null));
   }

   @Test
   public void testZeroToSmt() {
      assertEquals("0", RealVal.ZERO.toSmt());
   }

   @Test
   public void testOneToSmt() {
      assertEquals("1", RealVal.ONE.toSmt());
   }

   @Test
   public void testTwoToSmt() {
      assertEquals("2", RealVal.TWO.toSmt());
   }

   @Test
   public void testNegTwoToSmt() {
      assertEquals("(- 2)", RealVal.create(-2).toSmt());
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      RealVal.ZERO.satDelta(new SortModel());
   }
}
