package julia.expr.ast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecZeroExt;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.uf.Fun;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExprTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testRenameXToYInX() {
      Fun x = Int.create(0);
      VarMap map = new VarMap();
      map.put(x, 1);
      IExpr expr = x.renameVars(map);
      assertEquals(Int.create(1), expr);
   }

   @Test
   public void testRenameXToYInXAddY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntAdd.create(x, y);

      VarMap map = new VarMap();
      map.put(x, 1);

      assertEquals(IntAdd.create(y, y), expr.renameVars(map));
   }

   @Test
   public void testRenameXToYAndYToXInXAddY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntAdd.create(x, y);

      VarMap map = new VarMap();
      map.put(x, 1);
      map.put(y, 0);

      assertEquals(expr, expr.renameVars(map));
   }

   @Test
   public void testCompress() {
      Fun x = Int.create(42);
      Fun y = Int.create(666);
      IExpr expr = IntAdd.create(x, y);
      assertEquals(IntAdd.create(Int.create(0), Int.create(1)), expr.compress());
   }

   @Test
   public void testXEqualsX() {
      Fun x = Int.create(0);
      assertEquals(x, x);
   }

   @Test
   public void testXNotEqualsNull() {
      Fun x = Int.create(0);
      assertNotEquals(x, null);
   }

   @Test
   public void testXNotEqualsY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      assertNotEquals(x, y);
   }

   @Test
   public void testXNotEqualsAdd() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      assertNotEquals(x, IntAdd.create(x, y));
   }

   @Test
   public void testToString1() {
      Fun x = Int.create(0);
      assertEquals("(v0 : Int)", x.toString());
   }

   @Test
   public void testToString2() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntAdd.create(x, y);

      assertTrue(expr.toString().contains("IntAdd"));
      assertTrue(expr.toString().contains("(v0 : Int)"));
      assertTrue(expr.toString().contains("(v1 : Int)"));
   }

   @Test
   public void testToString3() {
      Fun bv = BitVec.create(0, 4);
      IExpr expr = BitVecZeroExt.create(bv, 2);
      assertEquals("(BitVecZeroExt (v0 : BitVec(4)) 2)", expr.toString());
   }
}
