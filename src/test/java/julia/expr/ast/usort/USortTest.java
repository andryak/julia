package julia.expr.ast.usort;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.uf.Fun;
import julia.expr.sort.DeclareSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class USortTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetIndex1() {
      Fun x = USort.create(0, DeclareSort.A);
      assertEquals(0, x.getIndex());
   }

   @Test
   public void testGetIndex2() {
      Fun x = USort.create(1, DeclareSort.A);
      assertEquals(1, x.getIndex());
   }

   @Test
   public void testGetIndex3() {
      Fun x = USort.create(2, DeclareSort.A);
      assertEquals(2, x.getIndex());
   }

   @Test
   public void testAlternativeConstructor() {
      Fun a0 = USort.create(0, DeclareSort.A);
      Fun a1 = USort.create(0, "A");
      assertEquals(a0, a1);
   }

   @Test
   public void testSortIsA() {
      assertEquals(DeclareSort.A, USort.create(0, DeclareSort.A).sort());
   }

   @Test
   public void testSortIsB() {
      assertEquals(DeclareSort.B, USort.create(0, DeclareSort.B).sort());
   }
}
