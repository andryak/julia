package julia.expr.ast.usort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import java.math.BigInteger;
import julia.expr.ast.Expr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.DeclareSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class USortValTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testInstancesAreCached() {
      USortVal a1 = USortVal.create(0, DeclareSort.A);
      USortVal a2 = USortVal.create(0, DeclareSort.A);
      assertSame(a1, a2);
   }

   @Test
   public void testInstanceCachingIsLimited() {
      USortVal a1 = USortVal.create(0, DeclareSort.A);
      for (int i = 1; i <= 4096; ++i) {
         USortVal.create(i, DeclareSort.A);
      }
      USortVal a2 = USortVal.create(0, DeclareSort.A);
      assertNotSame(a1, a2);
   }

   @Test
   public void testGetValue() {
      assertEquals(BigInteger.ZERO, USortVal.create(0, DeclareSort.A).getValue());
      assertEquals(BigInteger.ZERO, USortVal.create(0, DeclareSort.B).getValue());
   }

   @Test
   public void testSort() {
      assertSame(DeclareSort.A, USortVal.create(0, DeclareSort.A).sort());
      assertSame(DeclareSort.B, USortVal.create(0, DeclareSort.B).sort());
   }

   @Test
   public void testToString1() {
      assertEquals("(const-0 : A)", USortVal.create(0, DeclareSort.A).toString());
   }

   @Test
   public void testToString2() {
      assertEquals("(const-1 : B)", USortVal.create(1, DeclareSort.B).toString());
   }

   @Test
   public void testEval() {
      USortVal value = USortVal.create(0, DeclareSort.A);
      assertEquals(value, value.eval());
   }

   @Test
   public void testRenameVars() {
      USortVal value = USortVal.create(0, DeclareSort.A);
      assertEquals(value, value.renameVars(null));
   }

   @Test(expected = IllegalStateException.class)
   public void testToSmt() {
      USortVal.create(0, DeclareSort.A).toSmt();
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      USortVal.create(0, DeclareSort.A).satDelta(new SortModel());
   }
}
