package julia.expr.ast.uf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.MapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.FunSort;
import julia.expr.sort.IntSort;
import julia.expr.utils.FunInter;
import julia.utils.VarMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class FunTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetIndex1() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      assertEquals(0, f.getIndex());
   }

   @Test
   public void testGetIndex2() {
      Fun f = Fun.create(1, FunSort.INT_INT);
      assertEquals(1, f.getIndex());
   }

   @Test
   public void testGetIndex3() {
      Fun f = Fun.create(2, FunSort.INT_INT);
      assertEquals(2, f.getIndex());
   }

   @Test
   public void testSort1() {
      Fun f = Fun.create(0, BoolSort.create());
      assertEquals(BoolSort.create(), f.sort());
   }

   @Test
   public void testSort2() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      assertEquals(FunSort.INT_INT, f.sort());
   }

   @Test
   public void testRenameVars1() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      VarMap map = new VarMap();
      map.put(f, 1);
      Fun expr = (Fun) f.renameVars(map);
      assertEquals(1, expr.getIndex());
   }

   @Test
   public void testRenameVars2() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      Fun expr = (Fun) f.renameVars(new VarMap());
      assertEquals(0, expr.getIndex());
   }

   @Test
   public void testEval1() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      FunVal expr = (FunVal) f.eval(new SortModel());
      assertEquals(FunVal.create(new FunInter(IntVal.ZERO), FunSort.INT_INT), expr);
   }

   @Test
   public void testEval2() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      IExpr expr = f.eval(new MapModel());
      assertEquals(f, expr);
   }

   @Test
   public void testToString1() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      assertEquals("(v0 : (Int) -> Int)", f.toString());
   }

   @Test
   public void testToString2() {
      Fun f = Fun.create(0, BoolSort.create());
      assertEquals("(v0 : Bool)", f.toString());
   }

   @Test
   public void testSignature() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      List<ExprSort> sig = f.signature();
      assertEquals(2, sig.size());
      assertEquals(IntSort.create(), sig.get(0));
      assertEquals(IntSort.create(), sig.get(1));
   }

   @Test
   public void testToSmtDecl() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      assertTrue(f.toSmtDecl().matches("\\(declare-fun [^ ]+ \\(Int\\) Int\\)"));
   }

   @Test
   public void testToSmt() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      assertTrue(f.toSmt().contains(FunSort.INT_INT.prefix()));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun f = Fun.create(0, FunSort.INT_INT);
      f.satDelta(new SortModel());
   }

   @Test
   public void testSatDelta() {
      Fun f = Fun.create(0, BoolSort.create());
      assertEquals(new SatDelta(1, 0), f.satDelta(new SortModel()));
   }
}
