package julia.expr.ast.uf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.FunSort;
import julia.expr.utils.BigRational;
import julia.expr.utils.FunInter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class FunAppTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testCreateConstSelect() {
      FunVal cfun = FunVal.create(new FunInter(IntVal.TWO), FunSort.INT_INT);
      assertEquals(IntVal.TWO, FunApp.create(cfun, IntVal.ZERO));
   }

   @Test
   public void testCreateNonConstSelect1() {
      Fun fun = Fun.create(0, FunSort.INT_INT);
      assertEquals(FunApp.class, FunApp.create(fun, IntVal.ZERO).getClass());
   }

   @Test
   public void testCreateNonConstSelect2() {
      FunVal cfun = FunVal.create(new FunInter(IntVal.TWO), FunSort.INT_INT);
      assertEquals(FunApp.class, FunApp.create(cfun, Int.create(0)).getClass());
   }

   @Test
   public void testToSmt() {
      Fun fun = Fun.create(0, FunSort.INT_INT);
      IExpr expr = FunApp.create(fun, IntVal.ZERO);
      assertTrue(expr.toSmt().contains("0"));
      assertTrue(expr.toSmt().contains(fun.toSmt()));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefinedForIntFun() {
      Fun fun = Fun.create(0, FunSort.INT_INT);
      FunApp.create(fun, IntVal.ZERO).satDelta(new SortModel());
   }

   @Test
   public void testSatDeltaIsDefinedForBoolFun() {
      Fun fun = Fun.create(0, FunSort.INT_BOOL);
      SatDelta delta = FunApp.create(fun, IntVal.ZERO).satDelta(new SortModel());
      assertEquals(new SatDelta(BigRational.POS_INFINITY, BigRational.ZERO), delta);
   }
}
