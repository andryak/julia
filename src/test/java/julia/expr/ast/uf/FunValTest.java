package julia.expr.ast.uf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.FunSort;
import julia.expr.utils.FunInter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class FunValTest {
   private static final FunInter CFUN;
   static {
      CFUN = new FunInter(IntVal.ZERO);
   }

   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testGetValueOfConstFun() {
      FunVal cfun = FunVal.create(CFUN, FunSort.INT_INT);
      assertEquals(CFUN, cfun.getValue());
   }

   @Test
   public void testSort() {
      FunVal cfun = FunVal.create(CFUN, FunSort.INT_INT);
      assertEquals(FunSort.INT_INT, cfun.sort());
   }

   @Test
   public void testToString() {
      FunVal cfun = FunVal.create(CFUN, FunSort.INT_INT);
      assertTrue(cfun.toString().contains("0"));
   }

   @Test
   public void testEval() {
      FunVal cfun = FunVal.create(CFUN, FunSort.INT_INT);
      assertEquals(cfun, cfun.eval(null));
   }

   @Test
   public void testRenameVars() {
      FunVal cfun = FunVal.create(CFUN, FunSort.INT_INT);
      assertEquals(cfun, cfun.renameVars(null));
   }

   @Test(expected = IllegalStateException.class)
   public void testToSmt() {
      FunVal.create(CFUN, FunSort.INT_INT).toSmt();
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      FunVal.create(CFUN, FunSort.INT_INT).satDelta(new SortModel());
   }
}
