package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntNegTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNegZero() {
      IExpr expr = IntNeg.create(IntVal.ZERO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testNegOne() {
      IExpr expr = IntNeg.create(IntVal.ONE);
      assertEquals(IntVal.create(-1), expr);
   }

   @Test
   public void testNegTwo() {
      IExpr expr = IntNeg.create(IntVal.TWO);
      assertEquals(IntVal.create(-2), expr);
   }

   @Test
   public void testNegNegOne() {
      IExpr expr = IntNeg.create(IntVal.create(-1));
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testNegNegTwo() {
      IExpr expr = IntNeg.create(IntVal.create(-2));
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testNegOfLargeValue() {
      IExpr expr = IntNeg.create(IntVal.create(32768));
      assertEquals(IntVal.create(-32768), expr);
   }
}
