package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntDivTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroDivOne() {
      IExpr expr = IntDiv.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testZeroDivTwo() {
      IExpr expr = IntDiv.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testOneDivOne() {
      IExpr expr = IntDiv.create(IntVal.ONE, IntVal.ONE);
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testTwoDivOne() {
      IExpr expr = IntDiv.create(IntVal.TWO, IntVal.ONE);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testEightDivThree() {
      IExpr expr = IntDiv.create(IntVal.create(8), IntVal.create(3));
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testEightDivFour() {
      IExpr expr = IntDiv.create(IntVal.create(8), IntVal.create(4));
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testLargeValueDivLargeValue() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntDiv.create(c0, c1);
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testXDivOne() {
      Fun x = Int.create(0);
      IExpr expr = IntDiv.create(x, IntVal.ONE);
      assertEquals(x, expr);
   }

   @Test
   public void testZeroDivX() {
      Fun x = Int.create(0);
      IExpr expr = IntDiv.create(IntVal.ZERO, x);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testXDivY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntDiv.create(x, y);
      assertEquals(IntDiv.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntDiv.create(x, y);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntDiv.create(x, y);
      assertTrue(expr.toSmt().contains("div"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IntDiv.create(x, y).satDelta(new SortModel());
   }
}
