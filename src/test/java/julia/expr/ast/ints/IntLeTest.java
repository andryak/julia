package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntLeTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroLeOne() {
      IExpr expr = IntLe.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testZeroLeTwo() {
      IExpr expr = IntLe.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testOneLeOne() {
      IExpr expr = IntLe.create(IntVal.ONE, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTwoLeOne() {
      IExpr expr = IntLe.create(IntVal.TWO, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testEightLeThree() {
      IExpr expr = IntLe.create(IntVal.create(8), IntVal.create(3));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testThreeLeEight() {
      IExpr expr = IntLe.create(IntVal.create(3), IntVal.create(8));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testLargeValueLeLargeValue1() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntLe.create(c0, c1);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testLargeValueLeLargeValue2() {
      IExpr c0 = IntVal.create(32768);
      IExpr c1 = IntVal.create(32810);
      IExpr expr = IntLe.create(c0, c1);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXLeX() {
      Fun x = Int.create(0);
      IExpr expr = IntLe.create(x, x);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXLeY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntLe.create(x, y);
      assertEquals(IntLe.class, expr.getClass());
      assertEquals(BoolVal.TRUE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntLe.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntLe.create(x, y);
      assertTrue(expr.toSmt().contains("<="));
   }

   @Test
   public void testSatDelta() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      assertEquals(new SatDelta(0, 1), IntLe.create(x, y).satDelta(new SortModel()));
   }
}
