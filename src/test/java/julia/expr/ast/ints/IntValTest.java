package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import java.math.BigInteger;
import julia.expr.ast.Expr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntValTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroIsSingleton() {
      IntVal c0 = IntVal.ZERO;
      IntVal c1 = IntVal.create("0");
      IntVal c2 = IntVal.create(0);
      IntVal c3 = IntVal.create(0);
      assertSame(c0, c1);
      assertSame(c1, c2);
      assertSame(c2, c3);
   }

   @Test
   public void testOneIsSingleton() {
      IntVal c0 = IntVal.ONE;
      IntVal c1 = IntVal.create("1");
      IntVal c2 = IntVal.create(1);
      IntVal c3 = IntVal.create(1);
      assertSame(c0, c1);
      assertSame(c1, c2);
      assertSame(c2, c3);
   }

   @Test
   public void testTwoIsSingleton() {
      IntVal c0 = IntVal.TWO;
      IntVal c1 = IntVal.create("2");
      IntVal c2 = IntVal.create(2);
      IntVal c3 = IntVal.create(2);
      assertSame(c0, c1);
      assertSame(c1, c2);
      assertSame(c2, c3);
   }

   @Test
   public void testNegOneIsSingleton() {
      IntVal c0 = IntVal.create(-1);
      IntVal c1 = IntVal.create(-1);
      assertSame(c0, c1);
   }

   @Test
   public void testNegTwoIsSingleton() {
      IntVal c0 = IntVal.create(-2);
      IntVal c1 = IntVal.create(-2);
      assertSame(c0, c1);
   }

   @Test
   public void testOneIsNotTwo() {
      IntVal c0 = IntVal.ONE;
      IntVal c1 = IntVal.TWO;
      assertNotEquals(c0, c1);
   }

   @Test
   public void testLargeValuesAreNotSingletons1() {
      IntVal c0 = IntVal.create("18446744073709551616");
      IntVal c1 = IntVal.create("18446744073709551616");
      assertNotSame(c0, c1);
      assertEquals(c0, c1);
   }

   @Test
   public void testLargeValuesAreNotSingletons2() {
      IntVal c0 = IntVal.create(65536);
      IntVal c1 = IntVal.create(65536);
      assertNotSame(c0, c1);
      assertEquals(c0, c1);
   }

   @Test
   public void testLargeValuesAreNotSingletons3() {
      IntVal c0 = IntVal.create(-65536);
      IntVal c1 = IntVal.create(-65536);
      assertNotSame(c0, c1);
      assertEquals(c0, c1);
   }

   @Test
   public void testSomeLargeValuesAreDifferent() {
      IntVal c0 = IntVal.create(32768);
      IntVal c1 = IntVal.create(65536);
      assertNotEquals(c0, c1);
   }

   @Test
   public void testGetValueOfZero() {
      assertEquals(BigInteger.valueOf(0), IntVal.ZERO.getValue());
   }

   @Test
   public void testGetValueOfOne() {
      assertEquals(BigInteger.valueOf(1), IntVal.ONE.getValue());
   }

   @Test
   public void testGetValueOfTwo() {
      assertEquals(BigInteger.valueOf(2), IntVal.TWO.getValue());
   }

   @Test
   public void testGetValueOfLargeValue() {
      assertEquals(BigInteger.valueOf(32768), IntVal.create(32768).getValue());
   }

   @Test
   public void testSort() {
      assertEquals(IntSort.create(), IntVal.ZERO.sort());
   }

   @Test
   public void testZeroToString() {
      assertEquals("0", IntVal.ZERO.toString());
   }

   @Test
   public void testOneToString() {
      assertEquals("1", IntVal.ONE.toString());
   }

   @Test
   public void testTwoToString() {
      assertEquals("2", IntVal.TWO.toString());
   }

   @Test
   public void testNegTwoToString() {
      assertEquals("-2", IntVal.create(-2).toString());
   }

   @Test
   public void testEval() {
      assertSame(IntVal.ZERO, IntVal.ZERO.eval());
   }

   @Test
   public void testRenameVars() {
      assertEquals(IntVal.ZERO, IntVal.ZERO.renameVars(null));
   }

   @Test
   public void testZeroToSmt() {
      assertEquals("0", IntVal.ZERO.toSmt());
   }

   @Test
   public void testOneToSmt() {
      assertEquals("1", IntVal.ONE.toSmt());
   }

   @Test
   public void testTwoToSmt() {
      assertEquals("2", IntVal.TWO.toSmt());
   }

   @Test
   public void testNegTwoToSmt() {
      assertEquals("(- 2)", IntVal.create(-2).toSmt());
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      IntVal.ZERO.satDelta(new SortModel());
   }
}
