package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntGtTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroGtOne() {
      IExpr expr = IntGt.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testZeroGtTwo() {
      IExpr expr = IntGt.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testOneGtOne() {
      IExpr expr = IntGt.create(IntVal.ONE, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTwoGtOne() {
      IExpr expr = IntGt.create(IntVal.TWO, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testEightGtThree() {
      IExpr expr = IntGt.create(IntVal.create(8), IntVal.create(3));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testThreeGtEight() {
      IExpr expr = IntGt.create(IntVal.create(3), IntVal.create(8));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testLargeValueGtLargeValue1() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntGt.create(c0, c1);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testLargeValueGtLargeValue2() {
      IExpr c0 = IntVal.create(32768);
      IExpr c1 = IntVal.create(32810);
      IExpr expr = IntGt.create(c0, c1);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXGtX() {
      Fun x = Int.create(0);
      IExpr expr = IntGt.create(x, x);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXGtY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntGt.create(x, y);
      assertEquals(IntGt.class, expr.getClass());
      assertEquals(BoolVal.FALSE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntGt.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntGt.create(x, y);
      assertTrue(expr.toSmt().contains(">"));
   }

   @Test
   public void testSatDelta() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      assertEquals(new SatDelta(1, 0), IntGt.create(x, y).satDelta(new SortModel()));
   }
}
