package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntGeTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroGeOne() {
      IExpr expr = IntGe.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testZeroGeTwo() {
      IExpr expr = IntGe.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testOneGeOne() {
      IExpr expr = IntGe.create(IntVal.ONE, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testTwoGeOne() {
      IExpr expr = IntGe.create(IntVal.TWO, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testEightGeThree() {
      IExpr expr = IntGe.create(IntVal.create(8), IntVal.create(3));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testThreeGeEight() {
      IExpr expr = IntGe.create(IntVal.create(3), IntVal.create(8));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testLargeValueGeLargeValue1() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntGe.create(c0, c1);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testLargeValueGeLargeValue2() {
      IExpr c0 = IntVal.create(32768);
      IExpr c1 = IntVal.create(32810);
      IExpr expr = IntGe.create(c0, c1);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXGeX() {
      Fun x = Int.create(0);
      IExpr expr = IntGe.create(x, x);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXGeY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntGe.create(x, y);
      assertEquals(IntGe.class, expr.getClass());
      assertEquals(BoolVal.TRUE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntGe.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntGe.create(x, y);
      assertTrue(expr.toSmt().contains(">="));
   }

   @Test
   public void testSatDelta() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      assertEquals(new SatDelta(0, 1), IntGe.create(x, y).satDelta(new SortModel()));
   }
}
