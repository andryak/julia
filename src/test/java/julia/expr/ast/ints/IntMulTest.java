package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntMulTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNoParamsMul() {
      assertEquals(IntVal.ONE, IntMul.create());
   }

   @Test
   public void testOneParamMul() {
      Fun x = Int.create(0);
      IExpr expr = IntMul.create(x);
      assertEquals(x, expr);
   }

   @Test
   public void testZeroTimesZero() {
      IExpr expr = IntMul.create(IntVal.ZERO, IntVal.ZERO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testOneTimesZero() {
      IExpr expr = IntMul.create(IntVal.ONE, IntVal.ZERO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testZeroTimesOne() {
      IExpr expr = IntMul.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testOneTimesTwo() {
      IExpr expr = IntMul.create(IntVal.ONE, IntVal.TWO);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testTwoTimesOne() {
      IExpr expr = IntMul.create(IntVal.TWO, IntVal.ONE);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testTwoTimesTwo() {
      IExpr expr = IntMul.create(IntVal.TWO, IntVal.TWO);
      assertEquals(IntVal.create(4), expr);
   }

   @Test
   public void testLargeValueTimesLargeValue() {
      IExpr c = IntVal.create(32768);
      IExpr expr = IntMul.create(c, c);
      assertEquals(IntVal.create("1073741824"), expr);
   }

   @Test
   public void testXTimesY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntMul.create(x, y);
      assertEquals(IntMul.class, expr.getClass());
   }

   @Test
   public void testMulOfMuls() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr param = IntMul.create(x, y);
      IExpr expr = IntMul.create(param, param);
      assertEquals(IntMul.create(x, x, y, y), expr);
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntMul.create(x, y);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testCoeff() {
      Fun x = Int.create(0);
      IntMul expr = (IntMul) IntMul.create(x, IntVal.TWO);
      assertEquals(IntVal.TWO, expr.coeff());
   }

   @Test
   public void testTerms() {
      Fun x = Int.create(0);
      IntMul expr = (IntMul) IntMul.create(x, IntVal.TWO);
      assertEquals(1, expr.terms().size());
      assertTrue(expr.terms().contains(x));
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntMul.create(x, y);
      assertTrue(expr.toSmt().contains("*"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IntMul.create(x, y).satDelta(new SortModel());
   }
}
