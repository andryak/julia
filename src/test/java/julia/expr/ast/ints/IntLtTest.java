package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntLtTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroLtOne() {
      IExpr expr = IntLt.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testZeroLtTwo() {
      IExpr expr = IntLt.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testOneLtOne() {
      IExpr expr = IntLt.create(IntVal.ONE, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testTwoLtOne() {
      IExpr expr = IntLt.create(IntVal.TWO, IntVal.ONE);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testEightLtThree() {
      IExpr expr = IntLt.create(IntVal.create(8), IntVal.create(3));
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testThreeLtEight() {
      IExpr expr = IntLt.create(IntVal.create(3), IntVal.create(8));
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testLargeValueLtLargeValue1() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntLt.create(c0, c1);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testLargeValueLtLargeValue2() {
      IExpr c0 = IntVal.create(32768);
      IExpr c1 = IntVal.create(32810);
      IExpr expr = IntLt.create(c0, c1);
      assertEquals(BoolVal.TRUE, expr);
   }

   @Test
   public void testXLtX() {
      Fun x = Int.create(0);
      IExpr expr = IntLt.create(x, x);
      assertEquals(BoolVal.FALSE, expr);
   }

   @Test
   public void testXLtY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntLt.create(x, y);
      assertEquals(IntLt.class, expr.getClass());
      assertEquals(BoolVal.FALSE, expr.eval(new SortModel()));
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntLt.create(x, y);
      assertEquals(BoolSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntLt.create(x, y);
      assertTrue(expr.toSmt().contains("<"));
   }

   @Test
   public void testSatDelta() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      assertEquals(new SatDelta(1, 0), IntLt.create(x, y).satDelta(new SortModel()));
   }
}
