package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntAddTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testNoParamsAdd() {
      assertEquals(IntVal.ZERO, IntAdd.create());
   }

   @Test
   public void testOneParamAdd() {
      Fun x = Int.create(0);
      IExpr expr = IntAdd.create(x);
      assertEquals(x, expr);
   }

   @Test
   public void testZeroPlusZero() {
      IExpr expr = IntAdd.create(IntVal.ZERO, IntVal.ZERO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testOnePlusZero() {
      IExpr expr = IntAdd.create(IntVal.ONE, IntVal.ZERO);
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testZeroPlusOne() {
      IExpr expr = IntAdd.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testZeroPlusTwo() {
      IExpr expr = IntAdd.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testTwoPlusZero() {
      IExpr expr = IntAdd.create(IntVal.TWO, IntVal.ZERO);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testOnePlusOne() {
      IExpr expr = IntAdd.create(IntVal.ONE, IntVal.ONE);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testLargeValuePlusLargeValue() {
      IExpr c = IntVal.create(32768);
      IExpr expr = IntAdd.create(c, c);
      assertEquals(IntVal.create(65536), expr);
   }

   @Test
   public void testXPlusY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntAdd.create(x, y);
      assertEquals(IntAdd.class, expr.getClass());
   }

   @Test
   public void testXMinusX() {
      Fun x = Int.create(0);
      IExpr expr = IntAdd.create(x, IntNeg.create(x));
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testTwoXMinusX() {
      Fun x = Int.create(0);
      IExpr expr = IntAdd.create(IntMul.create(x, IntVal.TWO), IntNeg.create(x));
      assertEquals(x, expr);
   }

   @Test
   public void testAddOfAdds() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr param = IntAdd.create(x, y);
      IExpr expr = IntAdd.create(param, param);
      assertEquals(IntAdd.create(x, x, y, y), expr);
   }

   @Test
   public void testAddOfMuls() {
      Fun x = Int.create(0);
      IExpr param = IntMul.create(x, x);
      IExpr expr = IntAdd.create(param, param);
      assertEquals(IntMul.create(param, IntVal.TWO), expr);
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntAdd.create(x, y);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testCoeff() {
      Fun x = Int.create(0);
      IntAdd expr = (IntAdd) IntAdd.create(x, IntVal.TWO);
      assertEquals(IntVal.TWO, expr.coeff());
   }

   @Test
   public void testTerms() {
      Fun x = Int.create(0);
      IntAdd expr = (IntAdd) IntAdd.create(x, IntVal.TWO);
      assertEquals(1, expr.terms().size());
      assertTrue(expr.terms().contains(x));
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntAdd.create(x, y);
      assertTrue(expr.toSmt().contains("+"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IntAdd.create(x, y).satDelta(new SortModel());
   }
}
