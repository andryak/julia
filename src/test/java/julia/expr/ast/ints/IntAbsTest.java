package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntAbsTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testAbsZero() {
      IExpr expr = IntAbs.create(IntVal.ZERO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testAbsOne() {
      IExpr expr = IntAbs.create(IntVal.ONE);
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testAbsTwo() {
      IExpr expr = IntAbs.create(IntVal.TWO);
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testAbsNegOne() {
      IExpr expr = IntAbs.create(IntVal.create(-1));
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testAbsNegTwo() {
      IExpr expr = IntAbs.create(IntVal.create(-2));
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testAbsOfLargeValue() {
      IExpr expr = IntAbs.create(IntVal.create(32768));
      assertEquals(IntVal.create(32768), expr);
   }

   @Test
   public void testAbsX() {
      Fun x = Int.create(0);
      IExpr expr = IntAbs.create(x);
      assertEquals(IntAbs.class, expr.getClass());
   }

   @Test
   public void testAbsOfAbs() {
      Fun x = Int.create(0);
      IExpr expr = IntAbs.create(IntAbs.create(x));
      assertEquals(IntAbs.create(x), expr);
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      IExpr expr = IntAbs.create(x);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      IExpr expr = IntAbs.create(x);
      assertTrue(expr.toSmt().contains("abs"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      IntAbs.create(x).satDelta(new SortModel());
   }
}
