package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.SortModel;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntModTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroModOne() {
      IExpr expr = IntMod.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testZeroModTwo() {
      IExpr expr = IntMod.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testOneModOne() {
      IExpr expr = IntMod.create(IntVal.ONE, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testTwoModOne() {
      IExpr expr = IntMod.create(IntVal.TWO, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testEightModThree() {
      IExpr expr = IntMod.create(IntVal.create(8), IntVal.create(3));
      assertEquals(IntVal.TWO, expr);
   }

   @Test
   public void testLargeValueModLargeValue() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntMod.create(c0, c1);
      assertEquals(IntVal.create(42), expr);
   }

   @Test
   public void testXModOne() {
      Fun x = Int.create(0);
      IExpr expr = IntMod.create(x, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testZeroModX() {
      Fun x = Int.create(0);
      IExpr expr = IntMod.create(IntVal.ZERO, x);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testXModY() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntMod.create(x, y);
      assertEquals(IntMod.class, expr.getClass());
   }

   @Test
   public void testSort() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntMod.create(x, y);
      assertEquals(IntSort.create(), expr.sort());
   }

   @Test
   public void testToSmt() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IExpr expr = IntMod.create(x, y);
      assertTrue(expr.toSmt().contains("mod"));
   }

   @Test(expected = UndefinedSatDeltaError.class)
   public void testSatDeltaIsUndefined() {
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      IntMod.create(x, y).satDelta(new SortModel());
   }
}
