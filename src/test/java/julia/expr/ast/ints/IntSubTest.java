package julia.expr.ast.ints;

import static org.junit.Assert.assertEquals;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.uf.Fun;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntSubTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testZeroSubOne() {
      IExpr expr = IntSub.create(IntVal.ZERO, IntVal.ONE);
      assertEquals(IntVal.create(-1), expr);
   }

   @Test
   public void testZeroSubTwo() {
      IExpr expr = IntSub.create(IntVal.ZERO, IntVal.TWO);
      assertEquals(IntVal.create(-2), expr);
   }

   @Test
   public void testOneSubOne() {
      IExpr expr = IntSub.create(IntVal.ONE, IntVal.ONE);
      assertEquals(IntVal.ZERO, expr);
   }

   @Test
   public void testTwoSubOne() {
      IExpr expr = IntSub.create(IntVal.TWO, IntVal.ONE);
      assertEquals(IntVal.ONE, expr);
   }

   @Test
   public void testEightSubThree() {
      IExpr expr = IntSub.create(IntVal.create(8), IntVal.create(3));
      assertEquals(IntVal.create(5), expr);
   }

   @Test
   public void testLargeValueSubLargeValue() {
      IExpr c0 = IntVal.create(32810);
      IExpr c1 = IntVal.create(32768);
      IExpr expr = IntSub.create(c0, c1);
      assertEquals(IntVal.create(42), expr);
   }

   @Test
   public void testXSubZero() {
      Fun x = Int.create(0);
      IExpr expr = IntSub.create(x, IntVal.ZERO);
      assertEquals(x, expr);
   }
}
