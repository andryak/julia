package julia.expr.ast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import julia.expr.ast.array.Array;
import julia.expr.ast.array.ArraySelect;
import julia.expr.ast.array.ArrayStore;
import julia.expr.ast.array.ArrayVal;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Distinct;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Not;
import julia.expr.ast.bool.Or;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecAShR;
import julia.expr.ast.bv.BitVecAdd;
import julia.expr.ast.bv.BitVecAnd;
import julia.expr.ast.bv.BitVecConcat;
import julia.expr.ast.bv.BitVecExtract;
import julia.expr.ast.bv.BitVecLShR;
import julia.expr.ast.bv.BitVecMul;
import julia.expr.ast.bv.BitVecNot;
import julia.expr.ast.bv.BitVecOr;
import julia.expr.ast.bv.BitVecSRem;
import julia.expr.ast.bv.BitVecSge;
import julia.expr.ast.bv.BitVecSgt;
import julia.expr.ast.bv.BitVecShL;
import julia.expr.ast.bv.BitVecSignExt;
import julia.expr.ast.bv.BitVecSle;
import julia.expr.ast.bv.BitVecSlt;
import julia.expr.ast.bv.BitVecSub;
import julia.expr.ast.bv.BitVecUDiv;
import julia.expr.ast.bv.BitVecURem;
import julia.expr.ast.bv.BitVecUge;
import julia.expr.ast.bv.BitVecUgt;
import julia.expr.ast.bv.BitVecUle;
import julia.expr.ast.bv.BitVecUlt;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.bv.BitVecXOr;
import julia.expr.ast.bv.BitVecZeroExt;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntAbs;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.ints.IntDiv;
import julia.expr.ast.ints.IntGe;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLe;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntMod;
import julia.expr.ast.ints.IntMul;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.mixed.If;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealAbs;
import julia.expr.ast.reals.RealAdd;
import julia.expr.ast.reals.RealDiv;
import julia.expr.ast.reals.RealGe;
import julia.expr.ast.reals.RealGt;
import julia.expr.ast.reals.RealLe;
import julia.expr.ast.reals.RealLt;
import julia.expr.ast.reals.RealMul;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.reals_ints.IsInt;
import julia.expr.ast.reals_ints.ToInt;
import julia.expr.ast.reals_ints.ToReal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.uf.FunApp;
import julia.expr.ast.uf.FunVal;
import julia.expr.sort.FunSort;
import julia.expr.sort.IntSort;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExprPackageIntegrationTest {
   @BeforeClass
   public static void setup() {
      Expr.setCheckParams(true);
   }

   @AfterClass
   public static void teardown() {
      Expr.setCheckParams(false);
   }

   @Test
   public void testOpCodesAreUnique() throws Exception {
      List<Integer> opCodes = new ArrayList<>();
      opCodes.add(ArraySelect.OPCODE);
      opCodes.add(ArrayStore.OPCODE);
      opCodes.add(ArrayVal.OPCODE);
      opCodes.add(And.OPCODE);
      opCodes.add(BoolVal.OPCODE);
      opCodes.add(Distinct.OPCODE);
      opCodes.add(Equal.OPCODE);
      opCodes.add(Not.OPCODE);
      opCodes.add(Or.OPCODE);
      opCodes.add(BitVecAdd.OPCODE);
      opCodes.add(BitVecAnd.OPCODE);
      opCodes.add(BitVecAShR.OPCODE);
      opCodes.add(BitVecConcat.OPCODE);
      opCodes.add(BitVecExtract.OPCODE);
      opCodes.add(BitVecLShR.OPCODE);
      opCodes.add(BitVecMul.OPCODE);
      opCodes.add(BitVecNot.OPCODE);
      opCodes.add(BitVecOr.OPCODE);
      opCodes.add(BitVecSge.OPCODE);
      opCodes.add(BitVecSgt.OPCODE);
      opCodes.add(BitVecShL.OPCODE);
      opCodes.add(BitVecSignExt.OPCODE);
      opCodes.add(BitVecSle.OPCODE);
      opCodes.add(BitVecSlt.OPCODE);
      opCodes.add(BitVecSRem.OPCODE);
      opCodes.add(BitVecSub.OPCODE);
      opCodes.add(BitVecUDiv.OPCODE);
      opCodes.add(BitVecUge.OPCODE);
      opCodes.add(BitVecUgt.OPCODE);
      opCodes.add(BitVecUle.OPCODE);
      opCodes.add(BitVecUlt.OPCODE);
      opCodes.add(BitVecURem.OPCODE);
      opCodes.add(BitVecVal.OPCODE);
      opCodes.add(BitVecXOr.OPCODE);
      opCodes.add(BitVecZeroExt.OPCODE);
      opCodes.add(IntAbs.OPCODE);
      opCodes.add(IntAdd.OPCODE);
      opCodes.add(IntDiv.OPCODE);
      opCodes.add(IntGe.OPCODE);
      opCodes.add(IntGt.OPCODE);
      opCodes.add(IntLe.OPCODE);
      opCodes.add(IntLt.OPCODE);
      opCodes.add(IntMod.OPCODE);
      opCodes.add(IntMul.OPCODE);
      opCodes.add(IntVal.OPCODE);
      opCodes.add(If.OPCODE);
      opCodes.add(RealAbs.OPCODE);
      opCodes.add(RealAdd.OPCODE);
      opCodes.add(RealDiv.OPCODE);
      opCodes.add(RealGe.OPCODE);
      opCodes.add(RealGt.OPCODE);
      opCodes.add(RealLe.OPCODE);
      opCodes.add(RealLt.OPCODE);
      opCodes.add(RealMul.OPCODE);
      opCodes.add(RealVal.OPCODE);
      opCodes.add(IsInt.OPCODE);
      opCodes.add(ToInt.OPCODE);
      opCodes.add(ToReal.OPCODE);
      opCodes.add(Fun.OPCODE);
      opCodes.add(FunApp.OPCODE);
      opCodes.add(FunVal.OPCODE);
      Set<Integer> opCodeSet = new HashSet<>(opCodes);
      assertEquals(opCodes.size(), opCodeSet.size());
   }

   @Test
   public void testHashClashRatio() {
      final double TOLERANCE = 0.015; // 1.5%.

      Fun A = Bool.create(0);
      Fun B = Bool.create(1);
      Fun x = Int.create(0);
      Fun y = Int.create(1);
      Fun p = Real.create(0);
      Fun q = Real.create(1);
      Fun bv0 = BitVec.create(0, 1);
      Fun bv1 = BitVec.create(1, 1);
      Fun array = Array.create(0, IntSort.create(), IntSort.create());
      Fun f = Fun.create(0, FunSort.create(Arrays.asList(IntSort.create()), IntSort.create()));

      List<Integer> hashes = new ArrayList<>();

      // Variables.
      hashes.add(A.hashCode());
      hashes.add(B.hashCode());
      hashes.add(x.hashCode());
      hashes.add(y.hashCode());
      hashes.add(p.hashCode());
      hashes.add(q.hashCode());
      hashes.add(bv0.hashCode());
      hashes.add(bv1.hashCode());

      // Constants.
      hashes.add(BoolVal.TRUE.hashCode());
      hashes.add(BoolVal.FALSE.hashCode());
      hashes.add(IntVal.ZERO.hashCode());
      hashes.add(IntVal.ONE.hashCode());
      hashes.add(IntVal.TWO.hashCode());
      hashes.add(RealVal.ZERO.hashCode());
      hashes.add(RealVal.ONE.hashCode());
      hashes.add(RealVal.TWO.hashCode());
      hashes.add(BitVecVal.create(0, 1).hashCode());
      hashes.add(BitVecVal.create(1, 1).hashCode());

      // Other nodes.
      hashes.add(ArraySelect.create(array, IntVal.ZERO).hashCode());
      hashes.add(ArrayStore.create(array, IntVal.ZERO, IntVal.ZERO).hashCode());
      hashes.add(And.create(A, B).hashCode());
      hashes.add(Distinct.create(x, y).hashCode());
      hashes.add(Equal.create(x, y).hashCode());
      hashes.add(Not.create(A).hashCode());
      hashes.add(Or.create(A, B).hashCode());
      hashes.add(BitVecAdd.create(bv0, bv1).hashCode());
      hashes.add(BitVecAnd.create(bv0, bv1).hashCode());
      hashes.add(BitVecAShR.create(bv0, bv1).hashCode());
      hashes.add(BitVecConcat.create(bv0, bv1).hashCode());
      hashes.add(BitVecExtract.create(bv0, 0, 0).hashCode());
      hashes.add(BitVecLShR.create(bv0, bv1).hashCode());
      hashes.add(BitVecMul.create(bv0, bv1).hashCode());
      hashes.add(BitVecNot.create(bv0).hashCode());
      hashes.add(BitVecOr.create(bv0, bv1).hashCode());
      hashes.add(BitVecSge.create(bv0, bv1).hashCode());
      hashes.add(BitVecSgt.create(bv0, bv1).hashCode());
      hashes.add(BitVecShL.create(bv0, bv1).hashCode());
      hashes.add(BitVecSignExt.create(bv0, 1).hashCode());
      hashes.add(BitVecSle.create(bv0, bv1).hashCode());
      hashes.add(BitVecSlt.create(bv0, bv1).hashCode());
      hashes.add(BitVecSRem.create(bv0, bv1).hashCode());
      hashes.add(BitVecSub.create(bv0, bv1).hashCode());
      hashes.add(BitVecUDiv.create(bv0, bv1).hashCode());
      hashes.add(BitVecUge.create(bv0, bv1).hashCode());
      hashes.add(BitVecUgt.create(bv0, bv1).hashCode());
      hashes.add(BitVecUle.create(bv0, bv1).hashCode());
      hashes.add(BitVecUlt.create(bv0, bv1).hashCode());
      hashes.add(BitVecURem.create(bv0, bv1).hashCode());
      hashes.add(BitVecXOr.create(bv0, bv1).hashCode());
      hashes.add(BitVecZeroExt.create(bv0, 1).hashCode());
      hashes.add(IntAbs.create(x).hashCode());
      hashes.add(IntAdd.create(x, y).hashCode());
      hashes.add(IntDiv.create(x, y).hashCode());
      hashes.add(IntGe.create(x, y).hashCode());
      hashes.add(IntGt.create(x, y).hashCode());
      hashes.add(IntLe.create(x, y).hashCode());
      hashes.add(IntLt.create(x, y).hashCode());
      hashes.add(IntMod.create(x, y).hashCode());
      hashes.add(IntMul.create(x, y).hashCode());
      hashes.add(If.create(A, x, y).hashCode());
      hashes.add(RealAbs.create(p).hashCode());
      hashes.add(RealAdd.create(p, q).hashCode());
      hashes.add(RealDiv.create(p, q).hashCode());
      hashes.add(RealGe.create(p, q).hashCode());
      hashes.add(RealGt.create(p, q).hashCode());
      hashes.add(RealLe.create(p, q).hashCode());
      hashes.add(RealLt.create(p, q).hashCode());
      hashes.add(RealMul.create(p, q).hashCode());
      hashes.add(IsInt.create(p).hashCode());
      hashes.add(ToInt.create(p).hashCode());
      hashes.add(ToReal.create(x).hashCode());
      hashes.add(FunApp.create(f, Arrays.asList(IntVal.ZERO)).hashCode());

      Set<Integer> hashSet = new HashSet<>(hashes);
      int allHashes = hashes.size();
      int uniqueHashes = hashSet.size();
      int repeatedHashes = (allHashes - uniqueHashes);
      double repeatedHashesRatio = (((double) repeatedHashes) / allHashes);

      String errMsg = String.format(
            "Too many hash collisions. Found %d repeated hashes over %d (%.2f).",
            repeatedHashes,
            allHashes,
            repeatedHashesRatio);

      assertTrue(errMsg, repeatedHashesRatio < TOLERANCE);
   }
}
