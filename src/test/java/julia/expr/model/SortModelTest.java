package julia.expr.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.ast.array.Array;
import julia.expr.ast.array.ArrayVal;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.uf.FunVal;
import julia.expr.ast.usort.USort;
import julia.expr.ast.usort.USortVal;
import julia.expr.model.SortModel;
import julia.expr.sort.ArraySort;
import julia.expr.sort.DeclareSort;
import julia.expr.sort.FunSort;
import julia.expr.utils.ConstArray;
import julia.expr.utils.FunInter;
import org.junit.Test;

public class SortModelTest {
   @Test
   public void testAllBoolVarsAreMappedToTrue() {
      SortModel model = new SortModel();
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(0)));
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(1)));
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(2)));
   }

   @Test
   public void testAllIntVarsAreMappedToZero() {
      SortModel model = new SortModel();
      assertEquals(IntVal.ZERO, model.getValueFor(Int.create(0)));
      assertEquals(IntVal.ZERO, model.getValueFor(Int.create(1)));
      assertEquals(IntVal.ZERO, model.getValueFor(Int.create(2)));
   }

   @Test
   public void testAllRealVarsAreMappedToZero() {
      SortModel model = new SortModel();
      assertEquals(RealVal.ZERO, model.getValueFor(Real.create(0)));
      assertEquals(RealVal.ZERO, model.getValueFor(Real.create(1)));
      assertEquals(RealVal.ZERO, model.getValueFor(Real.create(2)));
   }

   @Test
   public void testAllBitVecVarsAreMappedToZero() {
      SortModel model = new SortModel();
      assertEquals(BitVecVal.create(0, 4), model.getValueFor(BitVec.create(0, 4)));
      assertEquals(BitVecVal.create(0, 4), model.getValueFor(BitVec.create(1, 4)));
      assertEquals(BitVecVal.create(0, 4), model.getValueFor(BitVec.create(2, 4)));
   }

   @Test
   public void testBitVecVarsAreMappedToEqualSizedConsts() {
      SortModel model = new SortModel();
      assertEquals(BitVecVal.create(0, 32), model.getValueFor(BitVec.create(0, 32)));
      assertEquals(BitVecVal.create(0, 64), model.getValueFor(BitVec.create(1, 64)));
   }

   @Test
   public void testUSortVarsAreMappedToConsistentConsts() {
      final DeclareSort A = DeclareSort.A;
      final DeclareSort B = DeclareSort.B;
      SortModel model = new SortModel();
      assertEquals(USortVal.create(0, A), model.getValueFor(USort.create(0, A)));
      assertEquals(USortVal.create(0, B), model.getValueFor(USort.create(1, B)));
   }

   @Test
   public void testAllBoolVarsAreMappedToFalse() {
      SortModel model = new SortModel();
      model.setBoolInter(BoolVal.FALSE);
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(0)));
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(1)));
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(2)));
   }

   @Test
   public void testAllIntVarsAreMappedToTwo() {
      SortModel model = new SortModel();
      model.setIntInter(IntVal.TWO);
      assertEquals(IntVal.TWO, model.getValueFor(Int.create(0)));
      assertEquals(IntVal.TWO, model.getValueFor(Int.create(1)));
      assertEquals(IntVal.TWO, model.getValueFor(Int.create(2)));
   }

   @Test
   public void testAllRealVarsAreMappedToTwo() {
      SortModel model = new SortModel();
      model.setRealInter(RealVal.TWO);
      assertEquals(RealVal.TWO, model.getValueFor(Real.create(0)));
      assertEquals(RealVal.TWO, model.getValueFor(Real.create(1)));
      assertEquals(RealVal.TWO, model.getValueFor(Real.create(2)));
   }

   @Test
   public void testAllArrayVarsAreMappedToConsistentValues() {
      final ArrayVal CARRAY = ArrayVal.create(new ConstArray(IntVal.TWO), ArraySort.INT_INT);
      SortModel model = new SortModel();
      model.setIntInter(IntVal.TWO);
      assertEquals(CARRAY, model.getValueFor(Array.create(0, ArraySort.INT_INT)));
      assertEquals(CARRAY, model.getValueFor(Array.create(1, ArraySort.INT_INT)));
      assertEquals(CARRAY, model.getValueFor(Array.create(2, ArraySort.INT_INT)));
   }

   @Test
   public void testAllFunVarsAreMappedToConsistentValues() {
      final FunVal CFUN = FunVal.create(new FunInter(IntVal.TWO), FunSort.INT_INT);
      SortModel model = new SortModel();
      model.setIntInter(IntVal.TWO);
      assertEquals(CFUN, model.getValueFor(Fun.create(0, FunSort.INT_INT)));
      assertEquals(CFUN, model.getValueFor(Fun.create(1, FunSort.INT_INT)));
      assertEquals(CFUN, model.getValueFor(Fun.create(2, FunSort.INT_INT)));
   }

   @Test
   public void testToString() {
      SortModel model = new SortModel();
      assertTrue(model.toString().contains("0"));
   }
}
