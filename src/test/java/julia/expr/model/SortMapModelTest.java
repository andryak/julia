package julia.expr.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import julia.expr.ast.array.Array;
import julia.expr.ast.array.ArrayVal;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.uf.FunVal;
import julia.expr.model.MapModel;
import julia.expr.model.SortMapModel;
import julia.expr.sort.ArraySort;
import julia.expr.sort.FunSort;
import julia.expr.utils.ConstArray;
import julia.expr.utils.FunInter;
import org.junit.Test;

public class SortMapModelTest {
   private SortMapModel sampleModel() {
      final ArrayVal CARRAY = ArrayVal.create(new ConstArray(IntVal.TWO), ArraySort.INT_INT);
      final FunVal CFUN = FunVal.create(new FunInter(IntVal.TWO), FunSort.INT_INT);

      MapModel mapModel = new MapModel();
      mapModel.put(Bool.create(0), BoolVal.TRUE);
      mapModel.put(Int.create(0), IntVal.TWO);
      mapModel.put(Real.create(0), RealVal.TWO);
      mapModel.put(BitVec.create(0, 4), BitVecVal.create("#b0010"));
      mapModel.put(Array.create(0, ArraySort.INT_INT), CARRAY);
      mapModel.put(Fun.create(0, FunSort.INT_INT), CFUN);

      return new SortMapModel(mapModel);
   }

   @Test
   public void testGetValueForOfMissingBoolVar() {
      SortMapModel model = sampleModel();
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(1)));
   }

   @Test
   public void testGetValueForOfMissingIntVar() {
      SortMapModel model = sampleModel();
      assertEquals(IntVal.ZERO, model.getValueFor(Int.create(1)));
   }

   @Test
   public void testGetValueForOfMissingRealVar() {
      SortMapModel model = sampleModel();
      assertEquals(RealVal.ZERO, model.getValueFor(Real.create(1)));
   }

   @Test
   public void testGetValueForOfMissingBitVecVar() {
      SortMapModel model = sampleModel();
      assertEquals(BitVecVal.create(0, 4), model.getValueFor(BitVec.create(1, 4)));
   }

   @Test
   public void testGetValueForOfMissingArrayVar() {
      final ArrayVal CARRAY = ArrayVal.create(new ConstArray(IntVal.ZERO), ArraySort.INT_INT);
      SortMapModel model = sampleModel();
      assertEquals(CARRAY, model.getValueFor(Array.create(1, ArraySort.INT_INT)));
   }

   @Test
   public void testGetValueForOfMissingFunVar() {
      final FunVal CFUN = FunVal.create(new FunInter(IntVal.ZERO), FunSort.INT_INT);
      SortMapModel model = sampleModel();
      assertEquals(CFUN, model.getValueFor(Fun.create(1, FunSort.INT_INT)));
   }

   // ===

   @Test
   public void testGetValueForOfPresentBoolVar() {
      SortMapModel model = sampleModel();
      assertEquals(BoolVal.TRUE, model.getValueFor(Bool.create(0)));
   }

   @Test
   public void testGetValueForOfPresentIntVar() {
      SortMapModel model = sampleModel();
      assertEquals(IntVal.TWO, model.getValueFor(Int.create(0)));
   }

   @Test
   public void testGetValueForOfPresentRealVar() {
      SortMapModel model = sampleModel();
      assertEquals(RealVal.TWO, model.getValueFor(Real.create(0)));
   }

   @Test
   public void testGetValueForOfPresentBitVecVar() {
      SortMapModel model = sampleModel();
      assertEquals(BitVecVal.create("#b0010"), model.getValueFor(BitVec.create(0, 4)));
   }

   @Test
   public void testGetValueForOfPresentArrayVar() {
      final ArrayVal CARRAY = ArrayVal.create(new ConstArray(IntVal.TWO), ArraySort.INT_INT);
      SortMapModel model = sampleModel();
      assertEquals(CARRAY, model.getValueFor(Array.create(0, ArraySort.INT_INT)));
   }

   @Test
   public void testGetValueForOfPresentFunVar() {
      final FunVal CFUN = FunVal.create(new FunInter(IntVal.TWO), FunSort.INT_INT);
      SortMapModel model = sampleModel();
      assertEquals(CFUN, model.getValueFor(Fun.create(0, FunSort.INT_INT)));
   }

   @Test
   public void testToString() {
      assertNotNull(sampleModel().toString());
   }
}
