package julia.expr.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import java.util.HashMap;
import java.util.Map;
import julia.expr.ast.IConst;
import julia.expr.ast.array.Array;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.ints.Int;
import julia.expr.ast.reals.Real;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.sort.ArraySort;
import julia.expr.sort.FunSort;
import org.junit.Test;

public class MapModelTest {
   @Test
   public void testEmptyModelHasNoBoolVals() {
      MapModel model = new MapModel();
      assertNull(model.getValueFor(Bool.create(0)));
   }

   @Test
   public void testEmptyModelHasNoIntVals() {
      MapModel model = new MapModel();
      assertNull(model.getValueFor(Int.create(0)));
   }

   @Test
   public void testEmptyModelHasNoRealVals() {
      MapModel model = new MapModel();
      assertNull(model.getValueFor(Real.create(0)));
   }

   @Test
   public void testEmptyModelHasNoBitVecVals() {
      MapModel model = new MapModel();
      assertNull(model.getValueFor(BitVec.create(0, 4)));
   }

   @Test
   public void testEmptyModelHasNoArrayVals() {
      MapModel model = new MapModel();
      assertNull(model.getValueFor(Array.create(0, ArraySort.INT_INT)));
   }

   @Test
   public void testEmptyModelHasNoFunVals() {
      MapModel model = new MapModel();
      assertNull(model.getValueFor(Fun.create(0, FunSort.INT_INT)));
   }

   @Test
   public void testMapModelIsDiscrete() {
      MapModel model = new MapModel();
      model.put(Bool.create(0), BoolVal.TRUE);
      assertEquals(BoolVal.TRUE, model.getValueFor(Bool.create(0)));
      assertNull(model.getValueFor(Bool.create(1)));
   }

   @Test
   public void testCreateMapModelFromMap() {
      Map<Fun, IConst> map = new HashMap<>();
      map.put(Bool.create(0), BoolVal.TRUE);
      map.put(Bool.create(1), BoolVal.FALSE);
      MapModel model = new MapModel(map);
      assertEquals(BoolVal.TRUE, model.getValueFor(Bool.create(0)));
      assertEquals(BoolVal.FALSE, model.getValueFor(Bool.create(1)));
      assertNull(model.getValueFor(Bool.create(2)));
   }
}
