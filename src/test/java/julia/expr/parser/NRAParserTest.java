package julia.expr.parser;

import static org.junit.Assert.assertEquals;
import julia.expr.parser.exc.ParserException;
import julia.expr.parser.sexpr.NRAParser;
import julia.expr.parser.sexpr.SexprParser;
import julia.utils.Exprs;
import org.junit.Test;

public class NRAParserTest {
   private final static SexprParser parser = new NRAParser();

   @Test
   public void testParse1() throws ParserException {
      String sexpr = "1 1 lt v 0 c 0";
      String infix = "r0 < 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse2() throws ParserException {
      String sexpr = "1 1 le v 0 c 0";
      String infix = "r0 <= 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse3() throws ParserException {
      String sexpr = "1 1 gt v 0 c 0";
      String infix = "r0 > 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse4() throws ParserException {
      String sexpr = "1 1 ge v 0 c 0";
      String infix = "r0 >= 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse5() throws ParserException {
      String sexpr = "1 1 eq v 0 c 0";
      String infix = "r0 = 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse6() throws ParserException {
      String sexpr = "1 1 ne v 0 c 0";
      String infix = "r0 != 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse7() throws ParserException {
      String sexpr = "1 1 ne * 2 v 0 v 0 c 0";
      String infix = "r0 * r0 != 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse8() throws ParserException {
      String sexpr = "1 1 ne * 2 + 2 v 1 c 5 + 2 v 0 c 10 + 2 * 2 v 1 c 5 * 2 v 0 c 10";
      String infix = "(r1 + 5.0) * (r0 + 10.0) != (r1 * 5.0) + (r0 * 10.0)";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse9() throws ParserException {
      String sexpr = "1 1 ne * 3 v 0 v 1 v 2 v 3";
      String infix = "r0 * r1 * r2 != r3";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }

   @Test
   public void testParse10() throws ParserException {
      String sexpr = "2 2 lt v 0 c 0 lt v 0 c 1 1 gt v 0 c 0";
      String infix = "(r0 < 0.0 or r0 < 1.0) and r0 > 0.0";
      assertEquals(Exprs.create(infix), parser.parse(sexpr));
   }
}
