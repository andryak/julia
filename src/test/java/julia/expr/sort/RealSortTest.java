package julia.expr.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import julia.expr.sort.BoolSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import org.junit.Test;

public class RealSortTest {
   @Test
   public void testEquals() {
      assertEquals(RealSort.create(), RealSort.create());
      assertNotEquals(RealSort.create(), BoolSort.create());
      assertNotEquals(RealSort.create(), IntSort.create());
   }
}
