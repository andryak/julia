package julia.expr.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import julia.expr.sort.BoolSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import org.junit.Test;

public class BoolSortTest {
   @Test
   public void testEquals() {
      assertEquals(BoolSort.create(), BoolSort.create());
      assertNotEquals(BoolSort.create(), IntSort.create());
      assertNotEquals(BoolSort.create(), RealSort.create());
   }
}
