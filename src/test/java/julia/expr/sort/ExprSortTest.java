package julia.expr.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import org.junit.Test;

public class ExprSortTest {
   @Test
   public void testOpCodesAreUnique() {
      List<Integer> opCodes = new ArrayList<>();

      opCodes.add(BoolSort.OPCODE);
      opCodes.add(IntSort.OPCODE);
      opCodes.add(RealSort.OPCODE);
      opCodes.add(BitVecSort.OPCODE);
      opCodes.add(ArraySort.OPCODE);

      Set<Integer> opCodeSet = new HashSet<>();
      opCodeSet.addAll(opCodes);

      int allOpCodes = opCodes.size();
      int uniqueOpCodes = opCodeSet.size();

      assertTrue(allOpCodes == uniqueOpCodes); // All op-codes must be unique.
   }

   @Test
   public void testSingleAbstractSortInstance() {
      assertTrue(BitVecSort.create(32) == BitVecSort.create(32));
      assertTrue(ArraySort.create() == ArraySort.create());
   }

   @Test
   public void testBitVecSortEquals() {
      for (int i = 1; i <= 128; ++i) {
         assertEquals(BitVecSort.create(i), BitVecSort.create(i));
      }
   }

   @Test
   public void testArraySortEquals() {
      assertEquals(ArraySort.create(), ArraySort.create());

      ArraySort s1 = ArraySort.create(BoolSort.create(), IntSort.create());
      ArraySort s2 = ArraySort.create(BoolSort.create(), IntSort.create());
      assertEquals(s1, s2);
   }
}
