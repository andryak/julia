package julia.expr.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import julia.expr.sort.BoolSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import org.junit.Test;

public class IntSortTest {
   @Test
   public void testEquals() {
      assertEquals(IntSort.create(), IntSort.create());
      assertNotEquals(IntSort.create(), BoolSort.create());
      assertNotEquals(IntSort.create(), RealSort.create());
   }
}
