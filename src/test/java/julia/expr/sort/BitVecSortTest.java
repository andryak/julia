package julia.expr.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.math.BigInteger;
import julia.expr.sort.BitVecSort;
import org.junit.Test;

public class BitVecSortTest {
   @Test
   public void testEquals() {
      for (int i = 1; i <= 128; ++i) {
         assertEquals(BitVecSort.create(i), BitVecSort.create(i));
      }
      assertFalse(BitVecSort.create(32).equals(null));
   }

   @Test
   public void testGetSize() {
      for (int i = 1; i <= 128; ++i) {
         assertEquals(BigInteger.valueOf(i), BitVecSort.create(i).getSize());
      }
   }

   @Test
   public void testToString() {
      for (int i = 1; i <= 128; ++i) {
         assertTrue(BitVecSort.create(i).toString().contains(String.valueOf(i)));
      }
   }

   @Test(expected = IllegalArgumentException.class)
   public void testInvalidBitVecSort1() {
      BitVecSort.create(0);
   }

   @Test(expected = IllegalArgumentException.class)
   public void testInvalidBitVecSort2() {
      BitVecSort.create(-1);
   }
}
