package julia.expr.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import org.junit.Test;

public class ArraySortTest {
   @Test
   public void testEqualsToNull() {
      assertFalse(ArraySort.create().equals(null));
   }

   @Test
   public void testEqualsSameDomainAndRange() {
      assertEquals(ArraySort.create(), ArraySort.create());
   }

   @Test
   public void testEqualsSameDomainDifferentRange() {
      ArraySort s1 = ArraySort.create(BoolSort.create(), IntSort.create());
      ArraySort s2 = ArraySort.create(BoolSort.create(), BoolSort.create());
      assertNotEquals(s1, s2);
   }

   @Test
   public void testEqualsDifferentDomainSameRange() {
      ArraySort s1 = ArraySort.create(BoolSort.create(), IntSort.create());
      ArraySort s2 = ArraySort.create(IntSort.create(), IntSort.create());
      assertNotEquals(s1, s2);
   }

   @Test
   public void testEqualsDifferentDomainDifferentRange() {
      ArraySort s1 = ArraySort.create(BoolSort.create(), IntSort.create());
      ArraySort s2 = ArraySort.create(IntSort.create(), BoolSort.create());
      assertNotEquals(s1, s2);
   }

   @Test
   public void testToString() {
      final ExprSort BOOL_SORT = BoolSort.create();
      final ExprSort INT_SORT = IntSort.create();
      final ExprSort ARRAY_SORT = ArraySort.create(BOOL_SORT, INT_SORT);

      assertTrue(ARRAY_SORT.toString().contains(BOOL_SORT.toString()));
      assertTrue(ARRAY_SORT.toString().contains(INT_SORT.toString()));
   }
}
