package julia.expr.slicer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.HashSet;
import java.util.Set;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.BoolVal;
import julia.utils.Exprs;
import org.junit.Test;

public class ExprSlicerTest {
   private Set<IExpr> conjuncts(String... exprs) {
      Set<IExpr> result = new HashSet<>();
      for (final String expr : exprs) {
         result.add(Exprs.create(expr));
      }
      return result;
   }

   @Test
   public void testSliceExprWithNoVars() {
      IExpr expr = Exprs.create("0 + 1 > 0");
      Set<IExpr> conjuncts = conjuncts("x0 < x2", "x1 < x3", "x4 < 42", "x0 < x1");
      assertEquals(expr, ExprSlicer.slice(conjuncts, expr));
   }

   @Test
   public void testSliceExprWithoutCommonVars() {
      IExpr expr = Exprs.create("x5 + x6 > 0");
      Set<IExpr> conjuncts = conjuncts("x0 < x2", "x1 < x3", "x4 < 42", "x0 < x1");
      assertEquals(expr, ExprSlicer.slice(conjuncts, expr));
   }

   @Test
   public void testEmptySlice() {
      IExpr expr = Exprs.create("x5 + x6 > 0");
      Set<IExpr> conjuncts = conjuncts();
      assertEquals(expr, ExprSlicer.slice(conjuncts, expr));
   }

   @Test
   public void testSliceExprWithSomeCommonVars() {
      IExpr expr = Exprs.create("x0 + x1 > 0");
      Set<IExpr> conjuncts = conjuncts("x0 < x2", "x1 < x3", "x4 < 42", "x0 < x1");
      Set<IExpr> slice = Exprs.getConjuncts(ExprSlicer.slice(conjuncts, expr));
      assertTrue(slice.contains(Exprs.create("x0 < x2")));
      assertTrue(slice.contains(Exprs.create("x1 < x3")));
      assertTrue(slice.contains(Exprs.create("x0 < x1")));
      assertTrue(slice.contains(expr));
   }

   @Test
   public void testAlternativeSliceMethod() {
      IExpr expr = Exprs.create("x0 + x1 > 0");
      Set<IExpr> conjuncts = conjuncts("x0 < x2", "x1 < x3", "x4 < 42", "x0 < x1");
      IExpr prefix = And.create(conjuncts.toArray(new Expr[0]));
      Set<IExpr> slice = Exprs.getConjuncts(ExprSlicer.slice(prefix, expr));
      assertTrue(slice.contains(Exprs.create("x0 < x2")));
      assertTrue(slice.contains(Exprs.create("x1 < x3")));
      assertTrue(slice.contains(Exprs.create("x0 < x1")));
      assertTrue(slice.contains(expr));
   }

   @Test
   public void testSelfSlice() {
      Set<IExpr> conjuncts = conjuncts("x1 > 1", "x0 > 0", "x2 > 0", "x0 + x1 < 3", "2 < 3");
      Set<IExpr> slice = ExprSlicer.selfSlice(conjuncts);

      assertEquals(2, slice.size());
      assertTrue(slice.contains(Exprs.create("x2 > 0")));
   }

   @Test
   public void testInconsistentSelfSlice() {
      Set<IExpr> conjuncts = conjuncts("x1 > 1", "x0 > 0", "0 < 0", "x0 + x1 < 3");
      Set<IExpr> slice = ExprSlicer.selfSlice(conjuncts);
      assertEquals(1, slice.size());
      assertTrue(slice.contains(BoolVal.FALSE));
   }

   @Test
   public void testEmptySelfSlice() {
      Set<IExpr> conjuncts = new HashSet<>();
      Set<IExpr> slice = ExprSlicer.selfSlice(conjuncts);
      assertEquals(0, slice.size());
   }

   @Test
   public void testTrivialSelfSlice() {
      IExpr expr = Exprs.create("x1 > x2");
      Set<IExpr> slice = ExprSlicer.selfSlice(expr);
      assertEquals(1, slice.size());
      assertTrue(slice.contains(expr));
   }
}
