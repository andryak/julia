package julia.expr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import java.math.BigInteger;
import org.junit.Test;

public class BigRationalTest {
   @Test
   public void testInfinityIsInfinite() {
      assertTrue(BigRational.POS_INFINITY.isInfinite());
      assertTrue(BigRational.NEG_INFINITY.isInfinite());
   }

   @Test
   public void testOneIsFinite() {
      assertTrue(BigRational.ONE.isFinite());
      assertTrue(BigRational.ONE.isFinite());
   }

   @Test(expected = IllegalArgumentException.class)
   public void testZeroOverZeroIsUndefined() {
      BigRational.create(0, 0);
   }

   @Test(expected = IllegalArgumentException.class)
   public void testOneOverZeroIsUndefined() {
      BigRational.create(1, 0);
   }

   @Test
   public void testCreateFromBigInteger() {
      BigRational v = BigRational.create(BigInteger.TEN);
      assertEquals(BigInteger.TEN, v.p());
      assertEquals(BigInteger.ONE, v.q());
   }

   @Test
   public void testTwoForthsIsReduced() {
      BigRational v = BigRational.create(2, 4);
      assertEquals(BigInteger.valueOf(1), v.p());
      assertEquals(BigInteger.valueOf(2), v.q());
   }

   @Test
   public void testZeroIsReduced() {
      BigRational v = BigRational.create(0, 4);
      assertEquals(BigInteger.valueOf(0), v.p());
      assertEquals(BigInteger.valueOf(1), v.q());
   }

   @Test
   public void testSignIsUp1() {
      BigRational v = BigRational.create(-1, 2);
      assertEquals(BigInteger.valueOf(-1), v.p());
      assertEquals(BigInteger.valueOf(2), v.q());
   }

   @Test
   public void testSignIsUp2() {
      BigRational v = BigRational.create(1, -2);
      assertEquals(BigInteger.valueOf(-1), v.p());
      assertEquals(BigInteger.valueOf(2), v.q());
   }

   @Test
   public void testSignIsUp3() {
      BigRational v = BigRational.create(-1, -2);
      assertEquals(BigInteger.valueOf(1), v.p());
      assertEquals(BigInteger.valueOf(2), v.q());
   }

   @Test
   public void testNegativeSignum() {
      BigRational v = BigRational.create(-1, 2);
      assertEquals(-1, v.signum());
   }

   @Test
   public void testZeroSignum() {
      BigRational v = BigRational.create(0);
      assertEquals(0, v.signum());
   }

   @Test
   public void testPositiveSignum() {
      BigRational v = BigRational.create(1, 2);
      assertEquals(1, v.signum());
   }

   @Test
   public void testHalfAddHalf() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1, 2);
      assertEquals(BigRational.create(1), v1.add(v2));
   }

   @Test
   public void testOneAddOne() {
      BigRational v1 = BigRational.create(1);
      BigRational v2 = BigRational.create(1);
      assertEquals(BigRational.create(2), v1.add(v2));
   }

   @Test
   public void testInfinityAddInfinitySameSign1() {
      BigRational inf = BigRational.POS_INFINITY;
      assertEquals(inf, inf.add(inf));
   }

   @Test
   public void testInfinityAddInfinitySameSign2() {
      BigRational inf = BigRational.NEG_INFINITY;
      assertEquals(inf, inf.add(inf));
   }

   @Test
   public void testInfinityAddOne() {
      BigRational v1 = BigRational.POS_INFINITY;
      BigRational v2 = BigRational.ONE;
      assertEquals(v1, v1.add(v2));
   }

   @Test
   public void testOneAddInfinity() {
      BigRational v1 = BigRational.ONE;
      BigRational v2 = BigRational.POS_INFINITY;
      assertEquals(v2, v1.add(v2));
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityAddInfinityDifferentSigns1() {
      BigRational.POS_INFINITY.add(BigRational.NEG_INFINITY);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityAddInfinityDifferentSigns2() {
      BigRational.NEG_INFINITY.add(BigRational.POS_INFINITY);
   }

   @Test
   public void testHalfMinusHalf() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1, 2);
      assertEquals(BigRational.create(0), v1.subtract(v2));
   }

   @Test
   public void testHalfTimesHalf() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1, 2);
      assertEquals(BigRational.create(1, 4), v1.multiply(v2));
   }

   @Test(expected = ArithmeticException.class)
   public void testZeroTimesInfinity1() {
      BigRational v1 = BigRational.ZERO;
      BigRational v2 = BigRational.POS_INFINITY;
      v1.multiply(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testZeroTimesInfinity2() {
      BigRational v1 = BigRational.ZERO;
      BigRational v2 = BigRational.NEG_INFINITY;
      v1.multiply(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityTimesZero1() {
      BigRational v1 = BigRational.POS_INFINITY;
      BigRational v2 = BigRational.ZERO;
      v1.multiply(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityTimesZero2() {
      BigRational v1 = BigRational.NEG_INFINITY;
      BigRational v2 = BigRational.ZERO;
      v1.multiply(v2);
   }

   @Test
   public void testHalfDivideHalf() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1, 2);
      assertEquals(BigRational.create(1), v1.divide(v2));
   }

   @Test
   public void testOneDivideTwo() {
      BigRational v1 = BigRational.create(1);
      BigRational v2 = BigRational.create(2);
      assertEquals(BigRational.create(1, 2), v1.divide(v2));
   }

   @Test(expected = ArithmeticException.class)
   public void testOneDivideZero() {
      BigRational v1 = BigRational.create(1);
      BigRational v2 = BigRational.ZERO;
      v1.divide(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityDivideInfinity1() {
      BigRational v1 = BigRational.POS_INFINITY;
      BigRational v2 = BigRational.POS_INFINITY;
      v1.divide(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityDivideInfinity2() {
      BigRational v1 = BigRational.POS_INFINITY;
      BigRational v2 = BigRational.NEG_INFINITY;

      v1.divide(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityDivideInfinity3() {
      BigRational v1 = BigRational.NEG_INFINITY;
      BigRational v2 = BigRational.POS_INFINITY;
      v1.divide(v2);
   }

   @Test(expected = ArithmeticException.class)
   public void testInfinityDivideInfinity4() {
      BigRational v1 = BigRational.NEG_INFINITY;
      BigRational v2 = BigRational.NEG_INFINITY;
      v1.divide(v2);
   }

   @Test
   public void testHashCode() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1, 2);
      assertEquals(v1.hashCode(), v2.hashCode());
   }

   @Test
   public void testToString1() {
      BigRational v = BigRational.create(1, 2);
      assertEquals("1/2", v.toString());
   }

   @Test
   public void testToString2() {
      BigRational v = BigRational.create(42);
      assertEquals("42", v.toString());
   }

   @Test
   public void testToString3() {
      assertEquals("+inf", BigRational.POS_INFINITY.toString());
   }

   @Test
   public void testToString4() {
      assertEquals("-inf", BigRational.NEG_INFINITY.toString());
   }

   @Test
   public void testBigRationalEqualsSelf() {
      BigRational v = BigRational.create(1, 2);
      assertEquals(v, v);
   }

   @Test
   public void testBigRationalNotEqualsNull() {
      BigRational v = BigRational.create(1, 2);
      assertNotEquals(v, null);
   }

   @Test
   public void testBigRationalNotEqualsDouble() {
      BigRational v = BigRational.create(1, 2);
      assertNotEquals(v, new Double(0));
   }

   @Test
   public void testAbsOfZero() {
      BigRational v = BigRational.create(0);
      assertEquals(BigRational.create(0), v.abs());
   }

   @Test
   public void testAbsOfHalf() {
      BigRational v = BigRational.create(1, 2);
      assertEquals(BigRational.create(1, 2), v.abs());
   }

   @Test
   public void testAbsOfNegHalf() {
      BigRational v = BigRational.create(-1, 2);
      assertEquals(BigRational.create(1, 2), v.abs());
   }

   @Test
   public void testReciprocalOfHalf() {
      BigRational v = BigRational.create(1, 2);
      assertEquals(BigRational.create(2), v.reciprocal());
   }

   @Test(expected = ArithmeticException.class)
   public void testReciprocalOfZero() {
      BigRational.ZERO.reciprocal();
   }

   @Test
   public void testToInt() {
      BigRational v = BigRational.create(3, 2);
      assertEquals(BigInteger.valueOf(1), v.toInt());
   }

   @Test
   public void testIsInt1() {
      BigRational v = BigRational.create(3, 2);
      assertFalse(v.isInt());
   }

   @Test
   public void testIsInt2() {
      BigRational v = BigRational.create(3);
      assertTrue(v.isInt());
   }

   @Test
   public void testMin1() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1);
      assertEquals(v1, v1.min(v2));
   }

   @Test
   public void testMin2() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1);
      assertEquals(v1, v2.min(v1));
   }

   @Test
   public void testMax1() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1);
      assertEquals(v2, v1.max(v2));
   }

   @Test
   public void testMax2() {
      BigRational v1 = BigRational.create(1, 2);
      BigRational v2 = BigRational.create(1);
      assertEquals(v2, v2.max(v1));
   }

   @Test
   public void testBigRationalFromString1() {
      BigRational v = BigRational.create("1/2");
      assertEquals(BigRational.create(1, 2), v);
   }

   @Test
   public void testBigRationalFromString2() {
      BigRational v = BigRational.create("-1/2");
      assertEquals(BigRational.create(-1, 2), v);
   }

   @Test
   public void testBigRationalFromString3() {
      BigRational v = BigRational.create("1.25");
      assertEquals(BigRational.create(5, 4), v);
   }

   @Test
   public void testBigRationalFromString4() {
      BigRational v = BigRational.create("-1.25");
      assertEquals(BigRational.create(-5, 4), v);
   }

   @Test
   public void testBigRationalFromString5() {
      BigRational v = BigRational.create("42");
      assertEquals(BigRational.create(42), v);
   }

   @Test
   public void testBigRationalFromString6() {
      BigRational v = BigRational.create("-42");
      assertEquals(BigRational.create(-42), v);
   }

   @Test
   public void testCompareInfiniteAndFiniteValues1() {
      BigRational v1 = BigRational.POS_INFINITY;
      BigRational v2 = BigRational.create(42);
      assertEquals(1, v1.compareTo(v2));
   }

   @Test
   public void testCompareInfiniteAndFiniteValues2() {
      BigRational v1 = BigRational.NEG_INFINITY;
      BigRational v2 = BigRational.create(42);
      assertEquals(-1, v1.compareTo(v2));
   }

   @Test
   public void testCompareFiniteAndInfiniteValues1() {
      BigRational v1 = BigRational.create(42);
      BigRational v2 = BigRational.POS_INFINITY;
      assertEquals(-1, v1.compareTo(v2));
   }

   @Test
   public void testCompareFiniteAndInfiniteValues2() {
      BigRational v1 = BigRational.create(42);
      BigRational v2 = BigRational.NEG_INFINITY;
      assertEquals(1, v1.compareTo(v2));
   }

   @Test
   public void testCompareInfiniteValuesSameSign() {
      BigRational v1 = BigRational.NEG_INFINITY;
      BigRational v2 = BigRational.POS_INFINITY;
      assertEquals(0, v1.compareTo(v1));
      assertEquals(0, v2.compareTo(v2));
   }

   @Test
   public void testCompareInfiniteValuesDifferentSigns() {
      BigRational v1 = BigRational.NEG_INFINITY;
      BigRational v2 = BigRational.POS_INFINITY;
      assertEquals(-1, v1.compareTo(v2));
      assertEquals(1, v2.compareTo(v1));
   }

   @Test(expected = ArithmeticException.class)
   public void testInfiniteToInt1() {
      BigRational.POS_INFINITY.toInt();
   }

   @Test(expected = ArithmeticException.class)
   public void testInfiniteToInt2() {
      BigRational.NEG_INFINITY.toInt();
   }

   @Test
   public void testThreeOverTwoToInt() {
      assertEquals(BigInteger.valueOf(1), BigRational.create(3, 2).toInt());
   }

   @Test(expected = ArithmeticException.class)
   public void testInfiniteToDouble1() {
      BigRational.POS_INFINITY.doubleValue();
   }

   @Test(expected = ArithmeticException.class)
   public void testInfiniteToDouble2() {
      BigRational.NEG_INFINITY.doubleValue();
   }

   @Test
   public void testThreeOverTwoToDouble() {
      assertEquals(1.5, BigRational.create(3, 2).doubleValue(), 0.001);
   }

   @Test(expected = IllegalArgumentException.class)
   public void testBigRationalFromInvalidString() {
      BigRational.create("test");
   }
}
