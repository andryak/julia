package julia.expr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import julia.expr.utils.BitVector;
import org.junit.Test;

public class BitVectorTest {
   @Test
   public void testGetSizeOfRegularBitVector() {
      BitVector bv = new BitVector(0, 4);
      assertEquals(BigInteger.valueOf(4), bv.getSize());
   }

   @Test
   public void testGetSizeOfUndersizedBitVector() {
      BitVector bv = new BitVector(32, 4);
      assertEquals(BigInteger.valueOf(4), bv.getSize());
   }

   @Test
   public void testGetValueOfRegularBitVector() {
      BitVector bv = new BitVector(2, 4);
      assertEquals(BigInteger.valueOf(2), bv.getValue());
   }

   @Test
   public void testGetValueOfUndersizedBitVector1() {
      BitVector bv = new BitVector(4, 2);
      assertEquals(BigInteger.valueOf(0), bv.getValue());
   }

   @Test
   public void testGetValueOfUndersizedBitVector2() {
      BitVector bv = new BitVector(5, 2);
      assertEquals(BigInteger.valueOf(1), bv.getValue());
   }

   @Test
   public void testGetValueOfUndersizedBitVector3() {
      BitVector bv = new BitVector(6, 2);
      assertEquals(BigInteger.valueOf(2), bv.getValue());
   }

   @Test
   public void testBitVectorEqualsSelf() {
      BitVector bv = new BitVector(0, 32);
      assertEquals(bv, bv);
   }

   @Test
   public void testBitVectorNotEqualsNull() {
      assertNotEquals(new BitVector(0, 32), null);
   }

   @Test
   public void testBitVectorNotEqualsInteger() {
      BitVector bv = new BitVector(0, 32);
      assertNotEquals(bv, 1);
   }

   @Test
   public void testBitVectorEqualsSameBitVector() {
      BitVector bv1 = new BitVector(0, 32);
      BitVector bv2 = new BitVector(0, 32);
      assertEquals(bv1, bv2);
   }

   @Test
   public void testBitVectorNotEqualsDifferentSizeBitVector() {
      BitVector bv1 = new BitVector(0, 32);
      BitVector bv2 = new BitVector(0, 64);
      assertNotEquals(bv1, bv2);
   }

   @Test
   public void testBitVectorNotEqualsDifferentValueBitVector() {
      BitVector bv1 = new BitVector(0, 32);
      BitVector bv2 = new BitVector(1, 32);
      assertNotEquals(bv1, bv2);
   }

   @Test
   public void testBitVectorNotEqualsDifferentBitVector() {
      BitVector bv1 = new BitVector(0, 32);
      BitVector bv2 = new BitVector(1, 64);
      assertNotEquals(bv1, bv2);
   }

   @Test
   public void testConcat() {
      BitVector bv1 = new BitVector(0, 4);
      BitVector bv2 = new BitVector(2, 4);
      BitVector bv3 = new BitVector(4, 4);
      assertEquals(BitVector.valueOf("#b00000010"), bv1.concat(bv2));
      assertEquals(BitVector.valueOf("#b00000100"), bv1.concat(bv3));
      assertEquals(BitVector.valueOf("#b00100100"), bv2.concat(bv3));
   }

   @Test
   public void testExtract() {
      BitVector bv = BitVector.valueOf("#b101010");
      assertEquals(new BitVector(0, 1), bv.extract(0, 0));
      assertEquals(new BitVector(2, 2), bv.extract(1, 0));
      assertEquals(new BitVector(1, 2), bv.extract(2, 1));
      assertEquals(new BitVector(5, 3), bv.extract(3, 1));
      assertEquals(new BitVector(21, 5), bv.extract(5, 1));
      assertEquals(new BitVector(42, 6), bv.extract(5, 0));
   }

   @Test
   public void testNot() {
      BitVector bv1 = BitVector.valueOf("#b11001");
      BitVector bv2 = BitVector.valueOf("#b00110");
      assertEquals(BitVector.valueOf("#b00110"), bv1.not());
      assertEquals(BitVector.valueOf("#b11001"), bv2.not());
   }

   @Test
   public void testDoubleNot() {
      BitVector bv = BitVector.valueOf("#b11001");
      assertEquals(bv, bv.not().not());
   }

   @Test
   public void testAnd() {
      BitVector bv1 = BitVector.valueOf("#b11001");
      BitVector bv2 = BitVector.valueOf("#b01111");
      assertEquals(BitVector.valueOf("#b01001"), bv1.and(bv2));
   }

   @Test
   public void testOr() {
      BitVector bv1 = BitVector.valueOf("#b11001");
      BitVector bv2 = BitVector.valueOf("#b01101");
      assertEquals(BitVector.valueOf("#b11101"), bv1.or(bv2));
   }

   @Test
   public void testXOr() {
      BitVector bv1 = BitVector.valueOf("#b11001");
      BitVector bv2 = BitVector.valueOf("#b01101");
      assertEquals(BitVector.valueOf("#b10100"), bv1.xor(bv2));
   }

   @Test
   public void testNeg() {
      List<BitVector> bitvecs = new ArrayList<>();
      bitvecs.add(BitVector.valueOf("#b00000"));
      bitvecs.add(BitVector.valueOf("#b11111"));
      bitvecs.add(BitVector.valueOf("#b10001"));
      bitvecs.add(BitVector.valueOf("#b11001"));
      bitvecs.add(BitVector.valueOf("#b001"));

      for (final BitVector bv : bitvecs) {
         BitVector zero = new BitVector(BigInteger.ZERO, bv.getSize());
         assertEquals(zero.sub(bv), bv.neg());
      }
   }

   @Test
   public void testAddWithoutOverflow() {
      BitVector bv1 = BitVector.valueOf("#b01001");
      BitVector bv2 = BitVector.valueOf("#b01101");
      assertEquals(BitVector.valueOf("#b10110"), bv1.add(bv2));
   }

   @Test
   public void testAddWithOverflow() {
      BitVector bv1 = BitVector.valueOf("#b11001");
      BitVector bv2 = BitVector.valueOf("#b01101");
      assertEquals(BitVector.valueOf("#b00110"), bv1.add(bv2));
   }

   @Test
   public void testMulWithoutOverflow() {
      BitVector bv1 = BitVector.valueOf("#b00010");
      BitVector bv2 = BitVector.valueOf("#b00011");
      assertEquals(BitVector.valueOf("#b00110"), bv1.mul(bv2));
   }

   @Test
   public void testMulWithOverflow() {
      BitVector bv1 = BitVector.valueOf("#b111");
      BitVector bv2 = BitVector.valueOf("#b010");
      assertEquals(BitVector.valueOf("#b110"), bv1.mul(bv2));
   }

   @Test
   public void testUDiv() {
      BitVector bv1 = BitVector.valueOf("#b100");
      BitVector bv2 = BitVector.valueOf("#b010");
      assertEquals(BitVector.valueOf("#b010"), bv1.udiv(bv2));
   }

   @Test
   public void testSub() {
      BitVector bv1 = BitVector.valueOf("#b100");
      BitVector bv2 = BitVector.valueOf("#b010");
      assertEquals(BitVector.valueOf("#b010"), bv1.sub(bv2));
      assertEquals(BitVector.valueOf("#b110"), bv2.sub(bv1));
   }

   @Test
   public void testURem() {
      BitVector bv1 = BitVector.valueOf("#b100");
      BitVector bv2 = BitVector.valueOf("#b010");
      assertEquals(BitVector.valueOf("#b000"), bv1.urem(bv2));
   }

   @Test
   public void testSRem() {
      BitVector bv1 = BitVector.valueOf("#b100");
      BitVector bv2 = BitVector.valueOf("#b010");
      assertEquals(BitVector.valueOf("#b000"), bv1.srem(bv2));
   }

   @Test
   public void testLeftShift() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertEquals(BitVector.valueOf("#b1000"), bv1.shl(bv2));
      assertEquals(BitVector.valueOf("#b0110"), bv2.shl(bv1));
   }

   @Test
   public void testLogicalRightShift() {
      BitVector bv1 = BitVector.valueOf("#b1100");
      BitVector bv2 = BitVector.valueOf("#b0001");
      assertEquals(BitVector.valueOf("#b0110"), bv1.lshr(bv2));
   }

   @Test
   public void testArithmeticRightShift() {
      BitVector bv1 = BitVector.valueOf("#b1100");
      BitVector bv2 = BitVector.valueOf("#b0001");
      assertEquals(BitVector.valueOf("#b1110"), bv1.ashr(bv2));
   }

   @Test
   public void testUle() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertTrue(bv1.ule(bv2));
      assertFalse(bv2.ule(bv1));
      assertTrue(bv2.ule(bv2));
   }

   @Test
   public void testUlt() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertTrue(bv1.ult(bv2));
      assertFalse(bv2.ult(bv1));
      assertFalse(bv2.ult(bv2));
   }

   @Test
   public void testUge() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertFalse(bv1.uge(bv2));
      assertTrue(bv2.uge(bv1));
      assertTrue(bv2.uge(bv2));
   }

   @Test
   public void testUgt() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertFalse(bv1.ugt(bv2));
      assertTrue(bv2.ugt(bv1));
      assertFalse(bv2.ugt(bv2));
   }

   @Test
   public void testSleOfPositiveBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertTrue(bv1.sle(bv2));
      assertFalse(bv2.sle(bv1));
      assertTrue(bv2.sle(bv2));
   }

   @Test
   public void testSltOfPositiveBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertTrue(bv1.slt(bv2));
      assertFalse(bv2.slt(bv1));
      assertFalse(bv2.slt(bv2));
   }

   @Test
   public void testSgeOfPositiveBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertFalse(bv1.sge(bv2));
      assertTrue(bv2.sge(bv1));
      assertTrue(bv2.sge(bv2));
   }

   @Test
   public void testSgtOfPositiveBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b0001");
      BitVector bv2 = BitVector.valueOf("#b0011");
      assertFalse(bv1.sgt(bv2));
      assertTrue(bv2.sgt(bv1));
      assertFalse(bv2.sgt(bv2));
   }

   @Test
   public void testSleOfNegativeBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b1001");
      BitVector bv2 = BitVector.valueOf("#b1011");
      assertTrue(bv1.sle(bv2));
      assertFalse(bv2.sle(bv1));
      assertTrue(bv2.sle(bv2));
   }

   @Test
   public void testSltOfNegativeBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b1001");
      BitVector bv2 = BitVector.valueOf("#b1011");
      assertTrue(bv1.slt(bv2));
      assertFalse(bv2.slt(bv1));
      assertFalse(bv2.slt(bv2));
   }

   @Test
   public void testSgeOfNegativeBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b1001");
      BitVector bv2 = BitVector.valueOf("#b1011");
      assertFalse(bv1.sge(bv2));
      assertTrue(bv2.sge(bv1));
      assertTrue(bv2.sge(bv2));
   }

   @Test
   public void testSgtOfNegativeBitVectors() {
      BitVector bv1 = BitVector.valueOf("#b1001");
      BitVector bv2 = BitVector.valueOf("#b1011");
      assertFalse(bv1.sgt(bv2));
      assertTrue(bv2.sgt(bv1));
      assertFalse(bv2.sgt(bv2));
   }

   @Test
   public void testSignumOfPositiveBitVector() {
      BitVector bv = BitVector.valueOf("#b0001");
      assertEquals(1, bv.signum());
   }

   @Test
   public void testSignumOfNegativeBitVector() {
      BitVector bv = BitVector.valueOf("#b1000");
      assertEquals(-1, bv.signum());
   }

   @Test
   public void testSignumOfNullBitVector() {
      BitVector bv = BitVector.valueOf("#b0000");
      assertEquals(0, bv.signum());
   }

   @Test
   public void testSignOfPositiveBitVector() {
      BitVector bv = BitVector.valueOf("#b0001");
      assertTrue(bv.sign());
   }

   @Test
   public void testSignOfNegativeBitVector() {
      BitVector bv = BitVector.valueOf("#b1011");
      assertFalse(bv.sign());
   }

   @Test
   public void testToSignedInteger() {
      BitVector bv1 = BitVector.valueOf("#b000");
      BitVector bv2 = BitVector.valueOf("#b001");
      BitVector bv3 = BitVector.valueOf("#b010");
      BitVector bv4 = BitVector.valueOf("#b011");
      BitVector bv5 = BitVector.valueOf("#b100");
      BitVector bv6 = BitVector.valueOf("#b101");
      BitVector bv7 = BitVector.valueOf("#b110");
      BitVector bv8 = BitVector.valueOf("#b111");
      assertEquals(BigInteger.ZERO, bv1.toSignedInteger());
      assertEquals(BigInteger.ONE, bv2.toSignedInteger());
      assertEquals(BigInteger.valueOf(2), bv3.toSignedInteger());
      assertEquals(BigInteger.valueOf(3), bv4.toSignedInteger());
      assertEquals(BigInteger.valueOf(-4), bv5.toSignedInteger());
      assertEquals(BigInteger.valueOf(-3), bv6.toSignedInteger());
      assertEquals(BigInteger.valueOf(-2), bv7.toSignedInteger());
      assertEquals(BigInteger.valueOf(-1), bv8.toSignedInteger());
   }

   @Test
   public void testZeroExt() {
      BitVector bv1 = BitVector.valueOf("#b000");
      BitVector bv2 = BitVector.valueOf("#b001");
      BitVector bv3 = BitVector.valueOf("#b010");
      BitVector bv4 = BitVector.valueOf("#b011");
      BitVector bv5 = BitVector.valueOf("#b100");
      BitVector bv6 = BitVector.valueOf("#b101");
      BitVector bv7 = BitVector.valueOf("#b110");
      BitVector bv8 = BitVector.valueOf("#b111");
      assertEquals(BitVector.valueOf("#b000000"), bv1.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000001"), bv2.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000010"), bv3.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000011"), bv4.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000100"), bv5.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000101"), bv6.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000110"), bv7.zeroExt(3));
      assertEquals(BitVector.valueOf("#b000111"), bv8.zeroExt(3));
   }

   @Test
   public void testSignExt() {
      BitVector bv1 = BitVector.valueOf("#b000");
      BitVector bv2 = BitVector.valueOf("#b001");
      BitVector bv3 = BitVector.valueOf("#b010");
      BitVector bv4 = BitVector.valueOf("#b011");
      BitVector bv5 = BitVector.valueOf("#b100");
      BitVector bv6 = BitVector.valueOf("#b101");
      BitVector bv7 = BitVector.valueOf("#b110");
      BitVector bv8 = BitVector.valueOf("#b111");
      assertEquals(BitVector.valueOf("#b000000"), bv1.signExt(3));
      assertEquals(BitVector.valueOf("#b000001"), bv2.signExt(3));
      assertEquals(BitVector.valueOf("#b000010"), bv3.signExt(3));
      assertEquals(BitVector.valueOf("#b000011"), bv4.signExt(3));
      assertEquals(BitVector.valueOf("#b111100"), bv5.signExt(3));
      assertEquals(BitVector.valueOf("#b111101"), bv6.signExt(3));
      assertEquals(BitVector.valueOf("#b111110"), bv7.signExt(3));
      assertEquals(BitVector.valueOf("#b111111"), bv8.signExt(3));
   }

   @Test
   public void testValueOfBinaryString() {
      BitVector bv1 = BitVector.valueOf("0b0");
      BitVector bv2 = BitVector.valueOf("0b1");
      BitVector bv3 = BitVector.valueOf("0b00");
      BitVector bv4 = BitVector.valueOf("0b01");
      BitVector bv5 = BitVector.valueOf("0b10");
      BitVector bv6 = BitVector.valueOf("0b11");
      BitVector bv7 = BitVector.valueOf("#b0");
      BitVector bv8 = BitVector.valueOf("#b1");
      BitVector bv9 = BitVector.valueOf("#b00");
      BitVector bv10 = BitVector.valueOf("#b01");
      BitVector bv11 = BitVector.valueOf("#b10");
      BitVector bv12 = BitVector.valueOf("#b11");
      assertEquals(new BitVector(0, 1), bv1);
      assertEquals(new BitVector(1, 1), bv2);
      assertEquals(new BitVector(0, 2), bv3);
      assertEquals(new BitVector(1, 2), bv4);
      assertEquals(new BitVector(2, 2), bv5);
      assertEquals(new BitVector(3, 2), bv6);
      assertEquals(new BitVector(0, 1), bv7);
      assertEquals(new BitVector(1, 1), bv8);
      assertEquals(new BitVector(0, 2), bv9);
      assertEquals(new BitVector(1, 2), bv10);
      assertEquals(new BitVector(2, 2), bv11);
      assertEquals(new BitVector(3, 2), bv12);
   }

   @Test
   public void testValueOfHexString() {
      BitVector bv1 = BitVector.valueOf("0x0");
      BitVector bv2 = BitVector.valueOf("0xd34d");
      BitVector bv3 = BitVector.valueOf("0xdead");
      BitVector bv4 = BitVector.valueOf("0xD34D");
      BitVector bv5 = BitVector.valueOf("0xDEAD");
      BitVector bv6 = BitVector.valueOf("#x0");
      BitVector bv7 = BitVector.valueOf("#xd34d");
      BitVector bv8 = BitVector.valueOf("#xdead");
      BitVector bv9 = BitVector.valueOf("#xD34D");
      BitVector bv10 = BitVector.valueOf("#xDEAD");
      assertEquals(new BitVector(0, 4), bv1);
      assertEquals(new BitVector(0xd34d, 16), bv2);
      assertEquals(new BitVector(0xdead, 16), bv3);
      assertEquals(new BitVector(0xd34d, 16), bv4);
      assertEquals(new BitVector(0xdead, 16), bv5);
      assertEquals(new BitVector(0, 4), bv6);
      assertEquals(new BitVector(0xd34d, 16), bv7);
      assertEquals(new BitVector(0xdead, 16), bv8);
      assertEquals(new BitVector(0xd34d, 16), bv9);
      assertEquals(new BitVector(0xdead, 16), bv10);
   }

   @Test
   public void testHashCode() {
      BitVector bv1 = BitVector.valueOf("#x0000");
      BitVector bv2 = BitVector.valueOf("#x0000");
      assertEquals(bv1.hashCode(), bv2.hashCode());
   }

   @Test
   public void testToString() {
      BitVector bv = BitVector.valueOf("#b1001");
      assertEquals("#b1001", bv.toString());
   }
}
