/** 
 * This ANTLR grammar enables the parsing of models produced by the Z3 SMT solver. 
 * The parser can be generated running ANTLR v4 with the flag -visitor and must be placed 
 * in the package julia.solver.z3.parser.
 */

grammar Z3Model;

model
   : '(model' (assignment)* ')'
   ;

assignment
   : '(' 'define-fun' USORT_VAR_ID (index=INT) '()' declareSort uSortVal ')' # USortDef
   | '(' 'define-fun' BOOL_VAR_ID (index=INT) '()' boolSort boolVal ')' # BoolDef
   | '(' 'define-fun' BOOL_RES_VAR_ID (index=INT) '()' boolSort boolVal ')' # BoolResDef
   | '(' 'define-fun' INT_VAR_ID (index=INT) '()' intSort intVal ')' # IntDef
   | '(' 'define-fun' REAL_VAR_ID (index=INT) '()' realSort realVal ')' # RealDef
   | '(' 'define-fun' BIT_VEC_VAR_ID (index=INT) '()' bitVecSort bitVecVal ')' # BitVecDef
   | '(' 'define-fun' ARR_VAR_ID (index=INT) '()' arraySort '(' '_' 'as-array' (ref=GEN_VAR_ID) ')' ')' # ArrayDecl
   | '(' 'define-fun' (arrRef=GEN_VAR_ID) '(' '(' (varRef=GEN_VAR_ID) (indexSort=anySort) ')' ')' (valueSort=anySort) arrayVal ')' # ArrayDef
   | '(' 'define-fun' FUN_VAR_ID (index=INT) '(' ('(' GEN_VAR_ID anySort ')')+ ')' (range=anySort) funVal ')' # FunDef
   | '(' 'define-fun' STR_VAR_ID (index=INT) '()' strSort strVal ')' # StrDef
   ;


// # ========================
// # | Variable identifiers |
// # ========================

USORT_VAR_ID
   : 'usort'
   ;

BOOL_VAR_ID
   : 'bool'
   ;
   
BOOL_RES_VAR_ID
   : '_c'
   ;
   
INT_VAR_ID
   : 'int'
   ;
   
REAL_VAR_ID
   : 'real'
   ;
   
STR_VAR_ID
  : 'string'
  ;
   
BIT_VEC_VAR_ID
   : 'bitvec'
   ;
   
ARR_VAR_ID
   : 'array'
   ;

FUN_VAR_ID
   : 'fun'
   ;
  
GEN_VAR_ID
   : [a-z]+ '!' INT
   ;


// # =========
// # | Sorts |
// # =========

declareSort
   : DECLARE_SORT
   ;

boolSort
   : 'Bool'
   ;

intSort
   : 'Int'
   ;

realSort
   : 'Real'
   ;
   
strSort
  : 'String'
  ;

bitVecSort
   : '(' '_' 'BitVec' (size=INT) ')'
   ;

concreteSort
   : declareSort
   | boolSort
   | intSort
   | realSort
   | strSort
   | bitVecSort
   ;

arraySort
   : '(' 'Array' (indexSort=concreteSort) (valueSort=concreteSort) ')' # ConcreteArraySort
   | '(' 'Array' (indexSort=arraySort) (valueSort=arraySort) ')' # AbstractArraySort
   ;
   
anySort
   : declareSort
   | boolSort
   | intSort
   | realSort
   | strSort
   | bitVecSort
   | arraySort
   ;


// # ==========
// # | Values |
// # ==========

uSortVal
   : (sortName=DECLARE_SORT) '!val!' (index=INT)
   ;

boolVal
   : 'true'
   | 'false'
   ;
   
intVal
   : INT # Int
   | '(' '-' intVal ')' # IntNeg
   ;

realVal
   : REAL # Real
   | '(' '/' (p=REAL) (q=REAL) ')' # RealRational
   | '(' '-' realVal ')' # RealNeg
   ;

strVal
  : STR
  ;

bitVecVal
   : HEX
   | BIN
   ;

concreteVal
   : uSortVal
   | boolVal
   | intVal
   | realVal
   | bitVecVal
   ;

arrayVal
   : concreteVal # ConstArray
   | '(' 'ite' '(' '=' GEN_VAR_ID (index=concreteVal) ')' (value=concreteVal) arrayVal ')' # NonConstArray
   ;
      
arg
   : '(' '=' GEN_VAR_ID concreteVal ')'
   ;

argsList
   : arg # SingleFunArg
   | '(' 'and' (arg)+ ')' # ManyFunArgs
   ;
   
funVal
   : concreteVal # ConstFun
   | '(' 'ite' (args=argsList) (value=concreteVal) funVal ')' # NonConstFun
   ;   
   
   
// # ================
// # | Basic tokens |
// # ================
   
DECLARE_SORT
   : [A-Z]+
   ;
   
INT
   : INT_DIGIT+
   ;

REAL
   : [0-9]+[.][0-9]+
   ;

STR
   : '"' (STRING_CHR | '""')* '"'
   ;

BIN
   : '#b' BIN_DIGIT+
   ;
   
HEX
   : '#x' HEX_DIGIT+
   ;

fragment INT_DIGIT 
   : [0-9] 
   ;
   
fragment STRING_CHR
   : ~[\r\n"]
   ;

fragment BIN_DIGIT 
   : [01] 
   ;
   
fragment HEX_DIGIT 
   : [0-9a-fA-F] 
   ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;
   

// # =================
// # | Ignored lines |
// # =================
   
LINE_COMMENT
   : ';;' ~[\r\n]* -> skip
   ;
   
DECLARE_FUN
   : '(declare-fun' ~[\r\n]* -> skip
   ;
   
FORALL
   : '(forall' ~[\r\n]* -> skip
   ;
