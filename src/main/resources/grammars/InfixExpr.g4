/** 
 * This ANTLR grammar enables the parsing of strings representing expressions in infix-notation.
 * The parser can be generated running ANTLR v4 with the flag -visitor and must be placed 
 * in the package julia.expr.parser.infix.
 */

grammar InfixExpr;

expr
   : typedExpr EOF
   ;

typedExpr
   : boolExpr # BoolExpression
   | intExpr # IntExpression
   | realExpr # RealExpression
   | bvExpr # BitVecExpression
   ;

boolExpr
   : '(' boolExpr ')' # ParBoolExpr
   | boolExpr andSymbol boolExpr # BoolAnd
   | boolExpr orSymbol boolExpr # BoolOr
   | notSymbol boolExpr # BoolNot
   | boolLiteral # SingleBoolLiteral
   ;
   
boolLiteral
   : realIneq # RealInequality
   | intIneq # IntInequality
   | bvIneq # BitVecInequality
   | boolVar # BoolVariable
   | boolConst # BoolConstant
   ;

andSymbol
   : 'and'
   | '&'
   | '&&'
   | '^'
   ;
   
orSymbol
   : 'or'
   | '|'
   | '||'
   ;
   
notSymbol
   : 'not'
   | '!'
   ;

intIneq
   : (lhs=intExpr) '<' (rhs=intExpr) # IntLt
   | (lhs=intExpr) '>' (rhs=intExpr) # IntGt
   | (lhs=intExpr) '<=' (rhs=intExpr) # IntLe
   | (lhs=intExpr) '>=' (rhs=intExpr) # IntGe
   | (lhs=intExpr) '=' (rhs=intExpr) # IntEq
   | (lhs=intExpr) '!=' (rhs=intExpr) # IntDistinct
   ;

realIneq
   : (lhs=realExpr) '<' (rhs=realExpr) # RealLt
   | (lhs=realExpr) '>' (rhs=realExpr) # RealGt
   | (lhs=realExpr) '<=' (rhs=realExpr) # RealLe
   | (lhs=realExpr) '>=' (rhs=realExpr) # RealGe
   | (lhs=realExpr) '=' (rhs=realExpr) # RealEq
   | (lhs=realExpr) '!=' (rhs=realExpr) # RealDistinct
   ;

bvIneq
   : (lhs=bvExpr) '<' (rhs=bvExpr) # BitVecUlt
   | (lhs=bvExpr) '>' (rhs=bvExpr) # BitVecUgt
   | (lhs=bvExpr) '<=' (rhs=bvExpr) # BitVecUle
   | (lhs=bvExpr) '>=' (rhs=bvExpr) # BitVecUge
   | (lhs=bvExpr) '<s' (rhs=bvExpr) # BitVecSlt
   | (lhs=bvExpr) '>s' (rhs=bvExpr) # BitVecSgt
   | (lhs=bvExpr) '<=s' (rhs=bvExpr) # BitVecSle
   | (lhs=bvExpr) '>=s' (rhs=bvExpr) # BitVecSge
   ;

boolConst
   : 'true'
   | 'false' 
   ;
   
boolVar
   : 'b' (index=INT)
   ;  

intExpr
   : '(' intExpr ')' # ParIntExpr
   | 'abs(' intExpr ')' # IntAbs 
   | '-' intExpr # IntNeg
   | intExpr modSymbol intExpr # IntMod
   | intExpr divSymbol intExpr # IntDiv
   | intExpr '*' intExpr # IntMul
   | intExpr '+' intExpr # IntAdd
   | intExpr '-' intExpr # IntSub
   | intLiteral # SingleIntLiteral
   ;
   
intLiteral
   : intVar # IntVariable
   | INT # IntConstant
   ;
   
divSymbol
   : 'div'
   | '/'
   ;
   
modSymbol
   : 'mod'
   | '%'
   ;
   
intVar
   : 'x' (index=INT)
   ;

realExpr
   : '(' realExpr ')' # ParRealExpr
   | 'abs(' realExpr ')' # RealAbs 
   | '-' realExpr # RealNeg
   | realExpr modSymbol realExpr # RealMod
   | realExpr divSymbol realExpr # RealDiv
   | realExpr '*' realExpr # RealMul
   | realExpr '+' realExpr # RealAdd
   | realExpr '-' realExpr # RealSub
   | realLiteral # SingleRealLiteral
   ;

realLiteral
   : realVar # RealVariable
   | REAL # RealConstant
   ;

realVar
   : 'r' (index=INT)
   ;

bvExpr
   : '(' bvExpr ')' # ParBitVecExpr
   | bvExpr '+' bvExpr # BitVecAdd
   | bvExpr '&' bvExpr # BitVecAnd
   | bvExpr '>>' bvExpr # BitVecAShR
   | bvExpr '.' bvExpr # BitVecConcat
   | bvExpr '>>>' bvExpr # BitVecLShR   
   | bvExpr '*' bvExpr # BitVecMul
   | bvExpr '|' bvExpr # BitVecOr
   | bvExpr '%s' bvExpr # BitVecSRem
   | bvExpr '-' bvExpr # BitVecSub
   | bvExpr '/' bvExpr # BitVecUDiv
   | bvExpr '%' bvExpr # BitVecURem
   | bvExpr '^' bvExpr # BitVecXOr
   | bvLiteral # SingleBitVecLiteral
   ;

bvLiteral
   : bvVar # BitVecVariable
   | HEX # HexBitVecConstant
   | BIN # BinBitVecConstant
   ;

bvVar
   : 'bv' (index=INT) '(' (size=INT) ')'
   ;

INT
   : INT_DIGIT+
   ;

REAL
   : [0-9]+[.][0-9]+
   ;

BIN
   : '#b' BIN_DIGIT+
   ;
   
HEX
   : '#x' HEX_DIGIT+
   ;

fragment INT_DIGIT 
   : [0-9] 
   ;

fragment BIN_DIGIT 
   : [01] 
   ;
   
fragment HEX_DIGIT 
   : [0-9a-fA-F] 
   ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;
