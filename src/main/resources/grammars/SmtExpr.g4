/** 
 * This ANTLR grammar enables the parsing of strings produced by the KLEE symbolic executor 
 * representing expressions in SMT-LIB v2 format.
 * The parser can be generated running ANTLR v4 with the flag -visitor and must be placed 
 * in the package julia.expr.parser.smt.
 */

grammar SmtExpr;

cmd
   : (option)* (declaration)* (assertion)* EOF
   ;

option
   : '(' 'set-logic' 'QF_AUFBV' ')'
   | '(' 'set-option' ':produce-models' ('true' | 'false') ')'
   ;

declaration
   : '(' 'declare-fun' (name=ID) '()' (sort=exprSort) ')'
   ;

exprSort
   : 'Bool' # BoolSort
   | '(' '_' 'BitVec' (size=INT) ')' # BitVecSort
   | '(' 'Array' (indexSort=exprSort) (valueSort=exprSort) ')' # ArraySort
   ;

assertion
   : '(' 'assert' expr ')'
   ;

expr
   : '(' '=' (lhs=expr) (rhs=expr) ')' # EqualExpr
   | '(' 'and' (lhs=expr) (rhs=expr) ')' # BoolAnd
   | '(' 'or' (lhs=expr) (rhs=expr) ')' # BoolOr
   | '(' 'let' '(' (binding)+ ')' (targetExpr=expr) ')' # LetExpr
   | '(' 'ite'  (condExpr=expr) (thenExpr=expr) (elseExpr=expr) ')' # IteExpr
   | '(' 'bvadd' (lhs=expr) (rhs=expr) ')' # BitVecAdd
   | '(' 'bvand' (lhs=expr) (rhs=expr) ')' # BitVecAnd
   | '(' 'bvashr' (lhs=expr) (rhs=expr) ')' # BitVecAShR
   | '(' 'bvlshr' (lhs=expr) (rhs=expr) ')' # BitVecLShR
   | '(' 'bvmul' (lhs=expr) (rhs=expr) ')' # BitVecMul
   | '(' 'bvor' (lhs=expr) (rhs=expr) ')' # BitVecOr
   | '(' 'bvshl' (lhs=expr) (rhs=expr) ')' # BitVecShL
   | '(' 'bvslt' (lhs=expr) (rhs=expr) ')' # BitVecSlt
   | '(' 'bvule' (lhs=expr) (rhs=expr) ')' # BitVecUle
   | '(' 'bvult' (lhs=expr) (rhs=expr) ')' # BitVecUlt
   | '(' 'bvsub' (lhs=expr) (rhs=expr) ')' # BitVecSub
   | '(' 'bvudiv' (lhs=expr) (rhs=expr) ')' # BitVecUDiv
   | '(' 'bvurem' (lhs=expr) (rhs=expr) ')' # BitVecURem
   | '(' 'bvsrem' (lhs=expr) (rhs=expr) ')' # BitVecSRem
   | '(' 'bvxor' (lhs=expr) (rhs=expr) ')' # BitVecXOr 
   | '(' 'concat' (lhs=expr) (rhs=expr) ')' # BitVecConcat
   | '(' '(' '_' 'extract' (i=INT) (j=INT) ')' expr ')' # BitVecExtract
   | '(' '(' '_' 'sign_extend' (n=INT) ')' expr ')' # BitVecSignExt
   | '(' '(' '_' 'zero_extend' (n=INT) ')' expr ')' # BitVecZeroExt   
   | '(' 'select' (array=expr) (index=expr) ')' # ArraySelect
   | '(' 'store' (array=expr) (index=expr) (value=expr) ')' # ArrayStore
   | 'true' # BoolValTrue
   | 'false' # BoolValFalse 
   | '(' '_' (value=BIT_VEC_DEC) (size=INT) ')' # BitVecValDec
   | BIT_VEC_BIN # BitVecValBin
   | BIT_VEC_HEX # BitVecValHex
   | LET_BINDER # BoundVariable
   | ID # Variable
   ;

binding
   : '(' (binder=LET_BINDER) (bindedExpr=expr) ')'
   ;

BIT_VEC_BIN
   : '#' 'b' [01]+
   ;

BIT_VEC_DEC
   : 'bv' [0-9]+
   ;

BIT_VEC_HEX
   : '#' 'x' [a-fA-F0-9]+
   ;

INT
   : [0-9]+
   ;

LET_BINDER
   : '?' ID
   ;

ID
   : [a-zA-Z][a-zA-Z0-9_-]*
   ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;
