/** 
 * This ANTLR grammar enables the parsing of expressions in Z3-Str format.
 * The parser can be generated running ANTLR v4 with the flag -visitor and must be placed 
 * in the package julia.expr.parser.z3_str.
 */

grammar Z3StrExpr;

cmd
   : (declaration)* (assertion)* EOF
   ;

declaration
   : '(' 'declare-variable' (name=ID) (sort=exprSort) ')'
   | '(' 'declare-fun' (name=ID) '()' (sort=exprSort) ')'
   ;

assertion
   : '(' 'assert' expr ')'
   ;

expr
   : '(' '=' (lhs=expr) (rhs=expr) ')' # Equal
   | '(' 'and' (expr)+ ')' # And
   | '(' 'or' (expr)+ ')' # Or
   | '(' 'not' expr ')' # Not
   | '(' 'ite'  (condExpr=expr) (thenExpr=expr) (elseExpr=expr) ')' # If
   | '(' 'implies' (lhs=expr) (rhs=expr) ')' # Imply
   | '(' 'Concat' (lhs=expr) (rhs=expr) ')' # Concat
   | '(' 'Contains' (lhs=expr) (rhs=expr) ')' # Contains
   | '(' 'EndsWith' (lhs=expr) (rhs=expr) ')' # EndsWith
   | '(' 'Indexof' (lhs=expr) (rhs=expr) ')' # Indexof
   | '(' 'Length' expr ')' # Length
   | '(' 'Replace' (target=expr) (pattern=expr) (repl=expr) ')' # Replace
   | '(' 'StartsWith' (lhs=expr) (rhs=expr) ')' # StartsWith
   | '(' 'Substring' (target=expr) (from=expr) (to=expr) ')' # Substring
   | '(' 'LastIndexof' (lhs=expr) (rhs=expr) ')' # LastIndexof
   | '(' 'RegexConcat' (lhs=expr) (rhs=expr) ')' # RegexConcat
   | '(' 'RegexIn' (lhs=expr) (rhs=expr) ')' # RegexIn
   | '(' 'RegexStar' expr ')' # RegexStar
   | '(' 'RegexUnion' (lhs=expr) (rhs=expr) ')' # RegexUnion
   | '(' 'Str2Reg' expr ')' # Str2Reg 
   | '(' '+' (lhs=expr) (rhs=expr) ')' # Add
   | '(' '-' expr ')' # Neg
   | '(' '-' (lhs=expr) (rhs=expr) ')' # Sub   
   | '(' 'div' (lhs=expr) (rhs=expr) ')' # Div
   | '(' '>' (lhs=expr) (rhs=expr) ')' # Gt
   | '(' '<' (lhs=expr) (rhs=expr) ')' # Lt
   | '(' '>=' (lhs=expr) (rhs=expr) ')' # Ge   
   | '(' '<=' (lhs=expr) (rhs=expr) ')' # Le
   | BOOL # BoolVal
   | STR # StrVal
   | INT # IntVal
   | ID # Variable
   ;
   
exprSort
   : 'Bool' # BoolSort
   | 'Int' # IntSort
   | 'String' # StrSort
   ;

BOOL
   : 'true'
   | 'false'
   ;

INT
   : [0-9]+
   ;
   
ID
   : [a-zA-Z][a-zA-Z0-9_-]*
   ;

STR
   : '"' (STRING_CHR | '\\t' | '\\"' | '\\\\')* '"'
   ;

fragment STRING_CHR
   : ~[\r\n\t"\\]
   ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;
