package julia;

/**
 * General information about this project.
 * <p>
 * Julia is a support caching framework for SMT solvers.
 * 
 * Julia supports:
 * <ul>
 * <li>the core theory,</li>
 * <li>the theory of integers,</li>
 * <li>the theory of reals,</li>
 * <li>the theory of strings,</li>
 * <li>the mixed theory of integers and reals,</li>
 * <li>the theory of fixed-size bit-vectors,</li>
 * <li>the theory of arrays,</li>
 * <li>the theory of uninterpreted functions.</li>
 * </ul>
 *
 * @author Andrea Aquino <andrex.aquino@gmail.com>
 */
public final class Info {
   /**
    * The name of this project.
    */
   public static final String FULLNAME = "Java (version of) Utopia Lacking (an) Interesting Acronym";

   /**
    * The acronym of this project.
    */
   public static final String ACRONYM = "Julia";

   /**
    * The current version of this project.
    */
   public static final String VERSION = "2.1.0";

   /**
    * The license under which this project is distributed.
    */
   public static final String LICENSE = "GNU GPL v.3.0 or greater";

   /**
    * Do not instantiate me!
    */
   private Info() {}
}