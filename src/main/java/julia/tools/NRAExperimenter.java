package julia.tools;

import java.io.IOException;
import julia.expr.ast.IExpr;
import julia.expr.parser.exc.ParserException;
import julia.expr.parser.sexpr.NRAParser;
import julia.expr.parser.sexpr.SexprParser;
import julia.solver.Solver;

/**
 * Experimenter tool to analyze data sets in the NRA benchmark.
 */
public class NRAExperimenter extends DatasetExperimenter {
   /**
    * Creates a new experimenter for the NRA benchmark.
    * 
    * @param path
    *           the path to a data set file in the benchmark.
    * @param solver
    *           the solver to use to solve the expressions in the data set.
    */
   public NRAExperimenter(String path, Solver solver) throws IOException {
      super(path, solver);
   }

   @Override
   public IExpr parse(String s) throws ParserException {
      SexprParser parser = new NRAParser();
      return parser.parse(s);
   }
}
