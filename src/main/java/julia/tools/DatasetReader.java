package julia.tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Utility class to read expressions from a data set file.
 * <p>
 * A data set file is a text file containing expressions separated by empty lines.
 */
public class DatasetReader {
   private BufferedReader reader;

   /**
    * Creates a new reader to extract expressions from the given file.
    * 
    * @param path
    *           the path to the data set file.
    * @throws IOException
    *            if the given data set file does not exist.
    */
   public DatasetReader(String path) throws FileNotFoundException {
      this.reader = new BufferedReader(new FileReader(path));
   }

   /**
    * Returns true if the underlying data set file of this reader contains an expression.
    * 
    * @return true if the underlying data set file of this reader contains an expression.
    */
   public boolean hasNext() throws IOException {
      return this.reader.ready();
   }

   /**
    * Returns a new expression from the underlying data set file of this reader.
    * 
    * @return a new expression from the underlying data set file of this reader.
    */
   public String next() throws IOException {
      StringBuilder builder = new StringBuilder();
      while (true) {
         String line = this.reader.readLine();
         if (line == null || line.isEmpty()) {
            break;
         }
         builder.append(line);
         builder.append("\n");
      }
      return builder.toString();
   }
}
