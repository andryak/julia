package julia.tools;

import java.io.IOException;
import julia.expr.ast.IExpr;
import julia.expr.parser.exc.ParserException;
import julia.expr.parser.z3_str.Z3StrExprBuilder;
import julia.expr.parser.z3_str.Z3StrExprLexer;
import julia.expr.parser.z3_str.Z3StrExprParser;
import julia.solver.Solver;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Experimenter tool to analyze data sets in the STR benchmark.
 */
public class STRExperimenter extends DatasetExperimenter {
   /**
    * Creates a new experimenter for the STR benchmark.
    * 
    * @param path
    *           the path to a data set file in the benchmark.
    * @param solver
    *           the solver to use to solve the expressions in the data set.
    */
   public STRExperimenter(String path, Solver solver) throws IOException {
      super(path, solver);
   }

   @Override
   public IExpr parse(String s) throws ParserException {
      // Parse the input expression using the dedicated parser.
      ANTLRInputStream input = new ANTLRInputStream(s);
      Z3StrExprLexer lexer = new Z3StrExprLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      Z3StrExprParser parser = new Z3StrExprParser(tokens);
      ParseTree tree = parser.cmd();

      // In case of syntax errors throw an exception.
      if (parser.getNumberOfSyntaxErrors() > 0) {
         throw new ParserException(String.format("Cannot parse expression %s.", s));
      }

      Z3StrExprBuilder builder = new Z3StrExprBuilder();
      return (IExpr) builder.visit(tree);
   }
}
