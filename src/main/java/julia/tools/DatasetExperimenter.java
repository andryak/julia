package julia.tools;

import java.io.IOException;
import julia.expr.ast.IExpr;
import julia.expr.parser.exc.ParserException;
import julia.solver.Solver;

/**
 * Experimenter class to analyze a data set in a benchmark.
 */
public abstract class DatasetExperimenter {
   private final DatasetReader reader;
   private final Solver solver;

   /**
    * Creates a new analyzer based on the given solver for the given data set.
    * 
    * @param path
    *           the path to a data set file in a benchmark.
    * @param solver
    *           the solver to use to solve the expressions in the data set.
    */
   public DatasetExperimenter(String path, Solver solver) throws IOException {
      this.reader = new DatasetReader(path);
      this.solver = solver;
   }

   /**
    * Calls the solver of this experimenter on all expressions in its data set.
    */
   public void analyze() throws IOException, ParserException {
      while (this.reader.hasNext()) {
         IExpr expr = this.parse(this.reader.next());
         this.solver.check(expr);
      }
   }

   /**
    * Returns an expression representing the given string.
    * 
    * @param expr
    *           a string representing an expression from a data set.
    * @return an expression representing the given string.
    */
   public abstract IExpr parse(String expr) throws ParserException;
}
