package julia.tools;

import java.io.IOException;
import julia.expr.ast.IExpr;
import julia.expr.parser.exc.ParserException;
import julia.expr.parser.smt.SmtExprBuilder;
import julia.expr.parser.smt.SmtExprLexer;
import julia.expr.parser.smt.SmtExprParser;
import julia.solver.Solver;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Experimenter tool to analyze data sets in the ABV benchmark.
 */
public class ABVExperimenter extends DatasetExperimenter {
   /**
    * Creates a new experimenter for the ABV benchmark.
    * 
    * @param path
    *           the path to a data set file in the benchmark.
    * @param solver
    *           the solver to use to solve the expressions in the data set.
    */
   public ABVExperimenter(String path, Solver solver) throws IOException {
      super(path, solver);
   }

   @Override
   public IExpr parse(String s) throws ParserException {
      // Parse the input expression using the dedicated parser.
      ANTLRInputStream input = new ANTLRInputStream(s);
      SmtExprLexer lexer = new SmtExprLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      SmtExprParser parser = new SmtExprParser(tokens);
      ParseTree tree = parser.cmd();

      // In case of syntax errors throw an exception.
      if (parser.getNumberOfSyntaxErrors() > 0) {
         throw new ParserException(String.format("Cannot parse expression %s.", s));
      }

      SmtExprBuilder builder = new SmtExprBuilder();
      return (IExpr) builder.visit(tree);
   }
}
