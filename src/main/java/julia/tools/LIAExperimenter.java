package julia.tools;

import java.io.IOException;
import julia.expr.ast.IExpr;
import julia.expr.parser.exc.ParserException;
import julia.expr.parser.sexpr.LIAParser;
import julia.expr.parser.sexpr.SexprParser;
import julia.solver.Solver;

/**
 * Experimenter tool to analyze data sets in the LIA benchmark.
 */
public class LIAExperimenter extends DatasetExperimenter {
   /**
    * Creates a new experimenter for the LIA benchmark.
    * 
    * @param path
    *           the path to a data set file in the benchmark.
    * @param solver
    *           the solver to use to solve the expressions in the data set.
    */
   public LIAExperimenter(String path, Solver solver) throws IOException {
      super(path, solver);
   }

   @Override
   public IExpr parse(String s) throws ParserException {
      SexprParser parser = new LIAParser();
      return parser.parse(s);
   }
}
