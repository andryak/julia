package julia.expr.ast.re;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.str.StrVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ReSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import julia.utils.StringUtils;
import com.google.common.base.Preconditions;

/**
 * Node representing a string matching a regular expression.
 */
public final class Match extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(49);

   /**
    * Sets the policy for the calculation of sat-delta for this operator.
    */
   final static boolean COSTLY_SATDELTA = false;

   /**
    * Creates a new node representing a string matching a regular expression.
    * 
    * @param s
    *           the string to match to the regular expression.
    * @param re
    *           the regular expression.
    */
   private Match(IExpr s, IExpr re) {
      super(Arrays.asList(s, re));
   }

   /**
    * Returns a new node representing a string matching a regular expression.
    * 
    * @param s
    *           the string to match to the regular expression.
    * @param re
    *           the regular expression.
    * @return a new node representing a string matching a regular expression.
    */
   public static IExpr create(IExpr s, IExpr re) {
      if (Expr.CHECK_PARAMS) {
         checkParams(s, re);
      }
      return new Match(s, re);
   }

   public static void checkParams(IExpr s, IExpr re) {
      Preconditions.checkArgument(
            s.sort() instanceof StrSort,
            "Match first child must have string sort, got %s.",
            s.sort());

      Preconditions.checkArgument(
            re.sort() instanceof ReSort,
            "Match second child must have regex sort, got %s.",
            re.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public IExpr eval(Model model) {
      // No need to evaluate the regular expression, since it is constant by default.
      IExpr s = this.getExpr(0).eval(model);
      IExpr re = this.getExpr(1);

      StrVal sval = Exprs.cast(s, StrVal.class);
      if (sval != null) {
         IRe _re = (IRe) re;
         String pattern = _re.toJavaRegex();
         return Pattern.matches(pattern, sval.getValue()) ? BoolVal.TRUE : BoolVal.FALSE;
      }
      return Match.create(s, re);
   }

   @Override
   public String toSmt() {
      String s = this.getExpr(0).toSmt();
      String re = this.getExpr(1).toSmt();
      return String.format("(str.in.re %s %s)", s, re);
   }

   @Override
   public SatDelta satDelta(Model model) {
      if (!COSTLY_SATDELTA) {
         // Inexpensive computation.
         IExpr expr = this.eval(model);
         if (expr.equals(BoolVal.TRUE)) {
            return new SatDelta(0, 1);
         } else if (expr.equals(BoolVal.FALSE)) {
            return new SatDelta(1, 0);
         }
         throw new UndefinedSatDeltaError(this, model);
      }

      // No need to evaluate the regular expression, since it is constant by default.
      IExpr s = this.getExpr(0).eval(model);
      IExpr re = this.getExpr(1);

      StrVal sval = Exprs.cast(s, StrVal.class);
      if (sval != null) {
         IRe _re = (IRe) re;
         String pattern = _re.toJavaRegex();
         if (Pattern.matches(pattern, sval.getValue())) {
            return new SatDelta(0, 1);
         } else {
            int delta = StringUtils.regexDelta(sval.getValue(), pattern);
            return new SatDelta(delta + 1, 0);
         }
      }
      throw new UndefinedSatDeltaError(this, model);
   }
}
