package julia.expr.ast.re;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ReSort;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the Kleene star of a regular expression.
 */
public class ReStar extends Expr implements IRe {
   public final static int OPCODE = PrimeDispatcher.getPrime(51);

   /**
    * Creates a new node representing the Kleene star of a regular expression.
    * 
    * @param expr
    *           a regular expression.
    */
   private ReStar(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the Kleene star of a regular expression.
    * 
    * @param expr
    *           a regular expression.
    * @return a new node representing the Kleene star of a regular expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }
      return new ReStar(expr);
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort() instanceof ReSort,
            "ReStar child must have regex sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ReSort sort() {
      return ReSort.create();
   }

   @Override
   public IExpr eval(Model model) {
      return this;
   }

   @Override
   public String toSmt() {
      return String.format("(re.* %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public String toJavaRegex() {
      IRe expr = (IRe) this.getExpr(0);
      return "(" + expr.toJavaRegex() + ")*";
   }
}
