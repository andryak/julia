package julia.expr.ast.re;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.str.StrVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ReSort;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the coercion of a string expression to a regular expression.
 */
public class StrToRe extends Expr implements IRe {
   public final static int OPCODE = PrimeDispatcher.getPrime(53);

   /**
    * Creates a new node representing the coercion of the given string expression to a
    * regular expression.
    * 
    * @param expr
    *           a string expression.
    */
   private StrToRe(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the coercion of the given string expression to a
    * regular expression.
    * 
    * @param expr
    *           a string expression.
    * @return a new node representing the coercion of the given string expression to a
    *         regular expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }
      return new StrToRe(expr);
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr instanceof StrVal,
            "StrToRe child must be a string constant, got %s.",
            expr.getClass().getSimpleName());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ReSort sort() {
      return ReSort.create();
   }

   @Override
   public IExpr eval(Model model) {
      return this;
   }

   @Override
   public String toSmt() {
      return String.format("(str.to.re %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public String toJavaRegex() {
      StrVal v = (StrVal) this.getExpr(0);
      return Pattern.quote(v.getValue());
   }
}
