package julia.expr.ast.re;

import julia.expr.ast.IExpr;

/**
 * Interface of nodes representing regular expressions.
 * <p>
 * All nodes representing regular expressions <b>must implement</b> this interface.
 */
public interface IRe extends IExpr {
   public String toJavaRegex();
}
