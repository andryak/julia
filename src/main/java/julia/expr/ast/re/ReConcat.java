package julia.expr.ast.re;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ReSort;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the concatenation of two regular expressions.
 */
public final class ReConcat extends Expr implements IRe {
   public final static int OPCODE = PrimeDispatcher.getPrime(50);

   /**
    * Creates a new node representing the concatenation of two regular expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private ReConcat(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing the concatenation of two regular expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the concatenation of two regular expressions.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }
      return new ReConcat(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof ReSort,
            "ReConcat first child must have regex sort, got %s.",
            lhs.sort());

      Preconditions.checkArgument(
            rhs.sort() instanceof ReSort,
            "ReConcat second child must have regex sort, got %s.",
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ReSort sort() {
      return ReSort.create();
   }

   @Override
   public IExpr eval(Model model) {
      return this;
   }

   @Override
   public String toSmt() {
      String lhs = this.getExpr(0).toSmt();
      String rhs = this.getExpr(1).toSmt();
      return String.format("(re.++ %s %s)", lhs, rhs);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public String toJavaRegex() {
      IRe lhs = (IRe) this.getExpr(0);
      IRe rhs = (IRe) this.getExpr(1);
      return "(" + lhs.toJavaRegex() + ")(" + rhs.toJavaRegex() + ")";
   }
}
