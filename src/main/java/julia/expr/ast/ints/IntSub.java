package julia.expr.ast.ints;

import julia.expr.ast.IExpr;

/**
 * Fake class to produce the arithmetic subtraction of the given expressions.
 */
public final class IntSub {
   /**
    * Do not instantiate me!
    */
   private IntSub() {}

   /**
    * Returns the arithmetic subtraction of the given integer expressions.
    * <p>
    * If exprs is empty this method returns {@code IntVal.ZERO}. Otherwise it returns
    * exprs[0] - exprs[1] - ... - exprs[n].
    * 
    * @param exprs
    *           a list of expressions.
    * @return the arithmetic subtraction of the given integer expressions.
    */
   public static IExpr create(IExpr... exprs) {
      if (exprs.length <= 0) {
         return IntVal.ZERO;
      }

      IExpr result = exprs[0];
      for (int i = 1; i < exprs.length; ++i) {
         result = IntAdd.create(result, IntMul.create(IntVal.create(-1), exprs[i]));
      }
      return result;
   }
}
