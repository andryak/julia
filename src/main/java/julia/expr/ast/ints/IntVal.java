package julia.expr.ast.ints;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing an integer constant.
 */
public final class IntVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(18);

   private final static int POSITIVE_INSTANCES = 1024;
   private final static IntVal[] instances;

   public final static IntVal ZERO;
   public final static IntVal ONE;
   public final static IntVal TWO;

   static {
      final int INSTANCES = (2 * POSITIVE_INSTANCES);

      instances = new IntVal[INSTANCES];
      for (int i = 0; i < INSTANCES; ++i) {
         instances[i] = new IntVal(BigInteger.valueOf(i - POSITIVE_INSTANCES));
      }

      ZERO = instances[POSITIVE_INSTANCES];
      ONE = instances[POSITIVE_INSTANCES + 1];
      TWO = instances[POSITIVE_INSTANCES + 2];
   }

   /**
    * Creates a new integer constant with the given value.
    * 
    * @param value
    *           the value of the integer constant.
    */
   private IntVal(BigInteger value) {
      super(new ArrayList<>(), Arrays.asList(value));
   }

   /**
    * Returns a new integer constant with the given value.
    * 
    * @param value
    *           the value of the integer constant.
    * @return a new integer constant with the given value.
    */
   public static IntVal create(String value) {
      return create(new BigInteger(value));
   }

   /**
    * Returns a new integer constant with the given value.
    * 
    * @param value
    *           the value of the integer constant.
    * @return a new integer constant with the given value.
    */
   public static IntVal create(long value) {
      return create(BigInteger.valueOf(value));
   }

   /**
    * Returns a new integer constant with the given value.
    * 
    * @param value
    *           the value of the integer constant.
    * @return a new integer constant with the given value.
    */

   public static IntVal create(BigInteger value) {
      try {
         // Throws if value does not fit in 32 bits.
         int v = value.intValueExact();
         if (v >= -POSITIVE_INSTANCES && v < POSITIVE_INSTANCES) {
            return instances[v + POSITIVE_INSTANCES];
         }
      } catch (ArithmeticException e) {
         // Values larger than 32 bits are not cached.
      }
      return new IntVal(value);
   }

   @Override
   protected IntVal make(List<IExpr> exprs, List<Object> args) {
      return create((BigInteger) args.get(0));
   }

   @Override
   public BigInteger getValue() {
      return (BigInteger) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public IntSort sort() {
      return IntSort.create();
   }

   @Override
   public String toString() {
      return this.getValue().toString();
   }

   // Added for efficiency.
   @Override
   public IntVal eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public IntVal renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      BigInteger value = this.getValue();
      if (value.signum() < 0) {
         return "(- " + value.abs().toString() + ")";
      }
      return value.toString();
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public BigRational delta(IConst other) {
      BigInteger value = (BigInteger) other.getValue();
      return BigRational.create(this.getValue().subtract(value).abs());
   }
}
