package julia.expr.ast.ints;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing two integer expressions being one less than or equal to the other
 * one.
 */
public final class IntLe extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(14);

   /**
    * Creates a new node representing the two given integer expressions being one less
    * than or equal to the other one.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private IntLe(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing the two given integer expressions being one less
    * than or equal to the other one.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the two given integer expressions being one less
    *         than or equal to the other one.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      IntVal lval = Exprs.cast(lhs, IntVal.class);
      IntVal rval = Exprs.cast(rhs, IntVal.class);
      if (lval != null && rval != null) {
         return BoolVal.create(lval.getValue().compareTo(rval.getValue()) <= 0);
      } else if (lhs.equals(rhs)) {
         return BoolVal.TRUE;
      }
      return new IntLe(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof IntSort,
            "IntLe first child must have integer sort, got %s",
            lhs.sort());

      Preconditions.checkArgument(
            rhs.sort() instanceof IntSort,
            "IntLe second child must have integer sort, got %s",
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public String toSmt() {
      return String.format("(<= %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr lexpr = this.getExpr(0).eval(model);
      IExpr rexpr = this.getExpr(1).eval(model);

      IntVal lval = Exprs.cast(lexpr, IntVal.class);
      IntVal rval = Exprs.cast(rexpr, IntVal.class);
      if (lval != null && rval != null) {
         BigInteger lhs = lval.getValue();
         BigInteger rhs = rval.getValue();

         BigRational dir;
         BigRational inv;

         if (lhs.compareTo(rhs) <= 0) {
            dir = BigRational.ZERO;
            inv = BigRational.create(rhs.subtract(lhs).add(BigInteger.ONE));
         } else {
            dir = BigRational.create(lhs.subtract(rhs));
            inv = BigRational.ZERO;
         }
         return new SatDelta(dir, inv);
      }
      throw new UndefinedSatDeltaError(this, model);
   }

   @Override
   public ExprSort sort() {
      return BoolSort.create();
   }
}
