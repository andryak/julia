package julia.expr.ast.ints;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the product of two IntExpr.
 */
public final class IntMul extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(17);

   /**
    * Creates a new node representing the product of the given integer expressions.
    * 
    * @param exprs
    *           the expressions to multiply.
    */
   private IntMul(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns the coefficient of this product.
    * 
    * @return the coefficient of this product.
    */
   public IntVal coeff() {
      return (IntVal) this.getExpr(0);
   }

   /**
    * Returns the non-constant terms of this product.
    * 
    * @return the non-constant terms of this product.
    */
   public List<IExpr> terms() {
      return this.getExprs().subList(1, this.getExprs().size());
   }

   /**
    * Returns a new node representing the product of the given integer expressions.
    * 
    * @param exprs
    *           the array of expressions to multiply.
    * @return a new node representing the product of the given integer expressions.
    */
   public static IExpr create(IExpr... exprs) {
      return create(Arrays.asList(exprs));
   }

   /**
    * Returns a new node representing the product of the given integer expressions.
    * 
    * @param exprs
    *           the list of expressions to multiply.
    * @return a new node representing the product of the given integer expressions.
    */
   public static IExpr create(List<IExpr> exprs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(exprs);
      }

      Queue<IExpr> queue = new LinkedList<>(exprs);
      List<IExpr> newArgsList = new ArrayList<>();
      BigInteger coeff = BigInteger.ONE;
      while (!queue.isEmpty()) {
         IExpr expr = queue.poll();
         if (expr instanceof IntVal) {
            if (expr == IntVal.ZERO) {
               return IntVal.ZERO;
            }
            coeff = coeff.multiply(((IntVal) expr).getValue());
         } else if (expr instanceof IntMul) {
            queue.addAll(expr.getExprs());
         } else {
            newArgsList.add(expr);
         }
      }

      if (newArgsList.size() == 0) {
         return IntVal.create(coeff);
      } else if (coeff.equals(BigInteger.ONE) && newArgsList.size() == 1) {
         return newArgsList.get(0);
      }

      // Sort children and ensure that the first is the coefficient.
      newArgsList.sort((e1, e2) -> Integer.compare(e1.hashCode(), e2.hashCode()));
      newArgsList.add(0, IntVal.create(coeff));
      return new IntMul(newArgsList);
   }

   public static void checkParams(List<IExpr> exprs) {
      Preconditions.checkArgument(
            exprs.stream().allMatch(e -> e.sort() == IntSort.create()),
            "IntMul children must have integer sort, got [%s].",
            Joiner.on(",").join(exprs.stream().map(e -> e.sort()).iterator()));
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(* %s)",
            Joiner.on(" ").join(this.getExprs().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
