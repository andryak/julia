package julia.expr.ast.ints;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the remainder of the division of two integer expressions.
 */
public final class IntMod extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(16);

   /**
    * Creates a new node representing the remainder of the division of the two given
    * integer expressions.
    * 
    * @param dividend
    *           the dividend.
    * @param divisor
    *           the divisor.
    */
   private IntMod(IExpr dividend, IExpr divisor) {
      super(Arrays.asList(dividend, divisor));
   }

   /**
    * Returns a new node representing the remainder of the division of the two given
    * integer expressions.
    * 
    * @param dividend
    *           the dividend.
    * @param divisor
    *           the divisor.
    * @return a new node representing the remainder of the division of the two given
    *         integer expressions.
    */
   public static IExpr create(IExpr dividend, IExpr divisor) {
      if (Expr.CHECK_PARAMS) {
         checkParams(dividend, divisor);
      }

      IntVal p = Exprs.cast(dividend, IntVal.class);
      IntVal q = Exprs.cast(divisor, IntVal.class);
      if (p != null & q != null) {
         return IntVal.create(p.getValue().mod(q.getValue()));
      } else if (p == IntVal.ZERO) {
         return IntVal.ZERO;
      } else if (q == IntVal.ONE) {
         return IntVal.ZERO;
      } else {
         return new IntMod(dividend, divisor);
      }
   }

   public static void checkParams(IExpr dividend, IExpr divisor) {
      Preconditions.checkArgument(
            dividend.sort().equals(IntSort.create()),
            "IntMod first child must have integer sort, got %s.",
            dividend.sort());

      Preconditions.checkArgument(
            divisor.sort().equals(IntSort.create()),
            "IntMod second child must have integer sort, got %s.",
            divisor.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(mod %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
