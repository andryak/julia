package julia.expr.ast.ints;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the sum of two integer expressions.
 */
public final class IntAdd extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(10);

   /**
    * Creates a new node representing the sum of the given integer expressions.
    * 
    * @param exprs
    *           the expressions to sum.
    */
   private IntAdd(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns the coefficient of this sum.
    * 
    * @return the coefficient of this sum.
    */
   public IntVal coeff() {
      return (IntVal) this.getExpr(0);
   }

   /**
    * Returns the non-constant terms of this sum.
    * 
    * @return the non-constant terms of this sum.
    */
   public List<IExpr> terms() {
      return this.getExprs().subList(1, this.getExprs().size());
   }

   /**
    * Returns a new node representing the sum of the given integer expressions.
    * 
    * @param exprs
    *           the array of expressions to sum.
    */
   public static IExpr create(IExpr... exprs) {
      return create(Arrays.asList(exprs));
   }

   /**
    * Returns a new node representing the sum of the given integer expressions.
    * 
    * @param exprs
    *           the list of expressions to sum.
    */
   public static IExpr create(List<IExpr> exprs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(exprs);
      }

      Queue<IExpr> queue = new LinkedList<>(exprs);
      Map<List<IExpr>, BigInteger> termMap = new HashMap<>();
      BigInteger coeff = BigInteger.ZERO;
      while (!queue.isEmpty()) {
         IExpr expr = queue.poll();
         if (expr instanceof IntVal) {
            coeff = coeff.add(((IntVal) expr).getValue());
         } else if (expr instanceof IntAdd) {
            queue.addAll(expr.getExprs());
         } else if (expr instanceof IntMul) {
            IntMul e = (IntMul) expr;
            BigInteger ecoeff = e.coeff().getValue();
            List<IExpr> eterms = e.terms();
            termMap.put(eterms, termMap.getOrDefault(eterms, BigInteger.ZERO).add(ecoeff));
         } else {
            List<IExpr> eterms = Arrays.asList(expr);
            termMap.put(
                  eterms,
                  termMap.getOrDefault(eterms, BigInteger.ZERO).add(BigInteger.ONE));
         }
      }

      // Build the list of non-constant arguments.
      List<IExpr> newArgsList = new ArrayList<>();
      for (Entry<List<IExpr>, BigInteger> entry : termMap.entrySet()) {
         if (entry.getValue().equals(BigInteger.ZERO)) {
            continue;
         } else if (entry.getValue().equals(BigInteger.ONE)) {
            newArgsList.add(IntMul.create(entry.getKey()));
         } else {
            List<IExpr> mulExprs = new ArrayList<>();
            mulExprs.add(IntVal.create(entry.getValue()));
            mulExprs.addAll(entry.getKey());
            newArgsList.add(IntMul.create(mulExprs));
         }
      }

      if (newArgsList.size() == 0) {
         return IntVal.create(coeff);
      } else if (coeff.equals(BigInteger.ZERO) && newArgsList.size() == 1) {
         return newArgsList.get(0);
      }

      // Sort children and ensure that the first is the coefficient.
      newArgsList.sort((e1, e2) -> Integer.compare(e1.hashCode(), e2.hashCode()));
      newArgsList.add(0, IntVal.create(coeff));
      return new IntAdd(newArgsList);
   }

   public static void checkParams(List<IExpr> exprs) {
      Preconditions.checkArgument(
            exprs.stream().allMatch(e -> e.sort() == IntSort.create()),
            "IntAdd children must have integer sort, got [%s].",
            Joiner.on(",").join(exprs.stream().map(e -> e.sort()).iterator()));
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(+ %s)",
            Joiner.on(" ").join(this.getExprs().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
