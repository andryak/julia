package julia.expr.ast.ints;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the absolute value of an integer expression.
 */
public final class IntAbs extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(9);

   /**
    * Creates a new node representing the absolute value of the given integer expression.
    * 
    * @param expr
    *           the expression to get the absolute value of.
    */
   private IntAbs(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the absolute value of the given integer expression.
    * 
    * @param expr
    *           the expression to get the absolute value of.
    * @return a new node representing the absolute value of the given integer expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      IntVal c = Exprs.cast(expr, IntVal.class);
      if (c != null) {
         return IntVal.create(c.getValue().abs());
      } else if (expr instanceof IntAbs) {
         return expr;
      } else {
         return new IntAbs(expr);
      }
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort().equals(IntSort.create()),
            "IntAbs child must have integer sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(abs %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
