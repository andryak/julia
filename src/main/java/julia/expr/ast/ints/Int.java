package julia.expr.ast.ints;

import julia.expr.ast.uf.Fun;
import julia.expr.sort.IntSort;
import julia.utils.Exprs;

/**
 * Fake class to create integer variables using the {@code Int.create(...)} syntax.
 */
public final class Int {
   /**
    * Do not instantiate me!
    */
   private Int() {}

   /**
    * Returns a new variable of integer sort.
    * 
    * @param index
    *           the index of the variable.
    * @return a new variable of integer sort.
    */
   public static Fun create(int index) {
      return Exprs.mkConst(index, IntSort.create());
   }
}