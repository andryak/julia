package julia.expr.ast.ints;

import julia.expr.ast.IExpr;

/**
 * Fake class to produce the arithmetic negation of an integer expression.
 */
public final class IntNeg {
   /**
    * Do not instantiate me!
    */
   private IntNeg() {}

   /**
    * Returns the arithmetic negation of the given integer expression.
    * 
    * @param expr
    *           the expression to negate.
    * @return the arithmetic negation of the given integer expression.
    */
   public static IExpr create(IExpr expr) {
      return IntMul.create(IntVal.create(-1), expr);
   }
}
