package julia.expr.ast;

import java.util.List;
import java.util.Set;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.uf.Fun;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.utils.VarMap;

/**
 * Interface of nodes representing expressions.
 */
public interface IExpr {
   /**
    * Returns a unique identifier of this node class.
    * <p>
    * The identifier is related to the node class, not to the node. Thus, the children of
    * this node do not affect the identifier.
    * 
    * @return a unique identifier of this node class.
    */
   public int opcode();

   /**
    * Returns the sort of the expression represented by this node.
    * 
    * @return the sort of the expression represented by this node.
    */
   public ExprSort sort();

   /**
    * Returns a copy of this expression with renamed variables.
    * <p>
    * The variables are renamed according to the given substitution map.
    * 
    * @param map
    *           a map from variables to their new indexes.
    * @return a copy of this expression with renamed variables.
    * 
    * @see VarMap
    */
   public IExpr renameVars(VarMap map);

   /**
    * Evaluates this expression on the given model.
    * 
    * @param model
    *           a model mapping the variables in this expression to concrete values.
    * @return the expression obtained evaluating this expression on the given model.
    */
   public IExpr eval(Model model);

   /**
    * Evaluates this expression on the empty model.
    * <p>
    * <b>Notice:</b> this is only safe if this expression contains no variables.
    * 
    * @return the expression obtained evaluating this expression on the empty model.
    */
   public IExpr eval();

   /**
    * Returns a hash code representing this expression tree.
    * <p>
    * Structurally equal trees have the same hash codes.
    * 
    * @return a hash code representing this expression tree.
    */
   @Override
   public int hashCode();

   /**
    * Re-indexes the variables of this expression.
    * <p>
    * This method returns a copy of this expression in which the indexes of the variables
    * of each sort have been re-indexed starting from zero.
    * 
    * @return a copy of this expression in which the variables have been re-indexed.
    */
   public IExpr compress();

   /**
    * Returns the index-th child of this node.
    * 
    * @param index
    *           the index of the child to extract.
    * @return the index-th child of this node.
    * @throws IndexOutOfBoundsException
    *            if the index-th child does not exist.
    */
   public IExpr getExpr(int index);

   /**
    * Returns the children of this node.
    * 
    * @return the children of this node.
    */
   public List<IExpr> getExprs();

   /**
    * Returns the index-th argument of this node.
    * 
    * @param index
    *           the index of the argument to extract.
    * @return the index-th argument of this node.
    * @throws IndexOutOfBoundsException
    *            if the index-th argument does not exist.
    */
   public Object getArg(int index);

   /**
    * Returns the arguments of this node.
    * 
    * @return the arguments of this node.
    */
   public List<Object> getArgs();

   /**
    * Returns true if this expression and the given object are equal.
    * <p>
    * If the given object is not an expression, {@code false} is returned. Otherwise the
    * two expressions are compared for structural equality.
    * 
    * @param other
    *           the object to compare to this expression.
    * @return true if this expression and the given object are equal.
    */
   @Override
   public boolean equals(Object other);

   /**
    * Returns the name of this expression.
    * 
    * @return the name of this expression.
    */
   public String getName();

   /**
    * Returns a string representing this expression.
    * 
    * @return a string representing this expression.
    */
   @Override
   public String toString();

   /**
    * Returns the set of variables in this expression tree.
    * <p>
    * A variable is an uninterpreted function.
    * 
    * @return the set of variables in this expression tree.
    * 
    * @see Fun
    */
   public Set<Fun> getVars();

   /**
    * Returns a SMT-LIB v2 string representing this expression.
    * 
    * @return a SMT-LIB v2 string representing this expression.
    */
   public String toSmt();

   /**
    * Returns the sat-delta value of this expression w.r.t the given model.
    * 
    * @return the sat-delta value of this expression w.r.t the given model.
    */
   public SatDelta satDelta(Model model);
}
