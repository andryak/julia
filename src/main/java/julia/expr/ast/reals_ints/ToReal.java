package julia.expr.ast.reals_ints;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the coercion of a integer expression to a real value.
 */
public final class ToReal extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(65);

   /**
    * Creates a new node representing the coercion of the given integer expression to a
    * real value.
    * 
    * @param expr
    *           the expression to coerce to a real.
    */
   private ToReal(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the coercion of the given integer expression to a
    * real value.
    * 
    * @param expr
    *           the expression to coerce to a real.
    * @return a new node representing the coercion of the given integer expression to a
    *         real value.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      IntVal v = Exprs.cast(expr, IntVal.class);
      if (v != null) {
         return RealVal.create(BigRational.create(v.getValue()));
      } else {
         return new ToReal(expr);
      }
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort().equals(IntSort.create()),
            "ToReal child must be an integer, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return RealSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(to_real %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
