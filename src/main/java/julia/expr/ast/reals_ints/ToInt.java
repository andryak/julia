package julia.expr.ast.reals_ints;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the largest integer smaller than a real expression.
 */
public final class ToInt extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(64);

   /**
    * Creates a new node representing the largest integer smaller than the given real
    * expression.
    * 
    * @param expr
    *           the real expression to transform into integer.
    */
   private ToInt(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the largest integer smaller than the given real
    * expression.
    * 
    * @param expr
    *           the real expression to transform into integer.
    * @return a new node representing the largest integer smaller than the given real
    *         expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      RealVal v = Exprs.cast(expr, RealVal.class);
      if (v != null) {
         return IntVal.create(v.getValue().toInt());
      }
      return new ToInt(expr);
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort().equals(RealSort.create()),
            "ToInt child must be a real, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(to_int %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
