package julia.expr.ast.reals_ints;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the function that maps to true all and only the reals in the image of
 * ToReal.
 * 
 * @see ToReal
 */
public final class IsInt extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(63);

   /**
    * Creates a new node representing the function that maps to true all and only the
    * reals in the image of ToReal applied to the given expression.
    * 
    * @param expr
    *           the expression argument to the IsInt function.
    */
   private IsInt(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the function that maps to true all and only the
    * reals in the image of ToReal applied to the given expression.
    * 
    * @param expr
    *           the expression argument to the IsInt function.
    * @return a new node representing the function that maps to true all and only the
    *         reals in the image of ToReal applied to the given expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      RealVal v = Exprs.cast(expr, RealVal.class);
      if (v != null) {
         return BoolVal.create(v.getValue().isInt());
      } else {
         return new IsInt(expr);
      }
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort().equals(RealSort.create()),
            "IsInt child must be a real, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(is_int %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr expr = this.getExpr(0).eval(model);
      RealVal v = Exprs.cast(expr, RealVal.class);
      if (v != null) {
         BigRational value = v.getValue().abs();

         BigRational dir;
         BigRational inv;

         if (value.isInt()) {
            dir = BigRational.ZERO;
            inv = BigRational.ONE;
         } else {
            dir = value.subtract(BigRational.create(value.toInt()));
            inv = BigRational.ZERO;
         }
         return new SatDelta(dir, inv);
      }
      throw new UndefinedSatDeltaError(this, model);
   }
}
