package julia.expr.ast.usort;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.DeclareSort;
import julia.utils.LRUCache;
import julia.utils.Pair;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing a constant of uninterpreted sort.
 */
public final class USortVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(79);

   private final static LRUCache<Pair<BigInteger, DeclareSort>, USortVal> instances;
   static {
      instances = new LRUCache<>(1024);
   }

   /**
    * Creates a new node representing a constant of uninterpreted sort.
    * 
    * @param value
    *           the value of the constant.
    * @param sort
    *           the sort of the constant.
    */
   private USortVal(BigInteger value, DeclareSort sort) {
      super(new ArrayList<>(), Arrays.asList(value, sort));
   }

   /**
    * Returns a new node representing a constant of uninterpreted sort.
    * 
    * @param value
    *           the value of the constant.
    * @param sort
    *           the sort of the constant.
    * @return a new node representing a constant of uninterpreted sort.
    */
   public static USortVal create(long value, DeclareSort sort) {
      return create(BigInteger.valueOf(value), sort);
   }

   /**
    * Returns a new node representing a constant of uninterpreted sort.
    * 
    * @param value
    *           the value of the constant.
    * @param sort
    *           the sort of the constant.
    * @return a new node representing a constant of uninterpreted sort.
    */
   public static USortVal create(BigInteger value, DeclareSort sort) {
      Pair<BigInteger, DeclareSort> params = new Pair<>(value, sort);
      USortVal cvalue = instances.get(params);
      if (cvalue != null) {
         return cvalue;
      }
      USortVal v = new USortVal(value, sort);
      instances.put(params, v);
      return v;
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create((BigInteger) args.get(0), (DeclareSort) args.get(1));
   }

   @Override
   public BigInteger getValue() {
      return (BigInteger) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public DeclareSort sort() {
      return (DeclareSort) this.getArg(1);
   }

   @Override
   public String toString() {
      return String.format("(const-%d : %s)", this.getValue(), this.sort());
   }

   // Added for efficiency.
   @Override
   public USortVal eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public USortVal renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      throw new IllegalStateException("FunVal is not part of the SMT-LIB v2 standard.");
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
