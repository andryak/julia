package julia.expr.ast.usort;

import julia.expr.ast.uf.Fun;
import julia.expr.sort.DeclareSort;
import julia.utils.Exprs;

/**
 * Fake class to create variables of uninterpreted sort using the
 * {@code USort.create(...)} syntax.
 */
public final class USort {
   /**
    * Do not instantiate me!
    */
   private USort() {}

   public static Fun create(int index, String name) {
      return create(index, DeclareSort.create(name));
   }

   /**
    * Returns a new variable of uninterpreted sort.
    * 
    * @param index
    *           the index of the variable.
    * @param sort
    *           the sort of the variable.
    * @return a new variable of uninterpreted sort.
    */
   public static Fun create(int index, DeclareSort sort) {
      return Exprs.mkConst(index, sort);
   }
}