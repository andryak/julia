package julia.expr.ast.bv;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the sum of two bit-vector expressions.
 */
public final class BitVecAdd extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(22);

   /**
    * Creates a new node representing the sum of two bit-vector expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private BitVecAdd(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing the sum of two bit-vector expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the sum of two bit-vector expressions.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      BitVecVal lval = Exprs.cast(lhs, BitVecVal.class);
      BitVecVal rval = Exprs.cast(rhs, BitVecVal.class);
      if (lval != null && rval != null) {
         // BitVecVal(a) + BitVecVal(b) = BitVecVal(a+b)
         return BitVecVal.create(lval.getValue().add(rval.getValue()));
      } else if (lval != null && lval.isZero()) {
         // BitVecVal(0) + e = e
         return rhs;
      } else if (rval != null && rval.isZero()) {
         // e + BitVecVal(0) = e
         return lhs;
      }
      return new BitVecAdd(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof BitVecSort,
            "BitVecAdd first child must have bit-vector sort, got %s.",
            lhs.sort());

      Preconditions.checkArgument(
            lhs.sort().equals(rhs.sort()),
            "BitVecAdd children must have the same sort, got %s and %s.",
            lhs.sort(),
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getExpr(0).sort();
   }

   @Override
   public String toSmt() {
      return String.format("(bvadd %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
