package julia.expr.ast.bv;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the logical right shift of a bit-vector expression by another
 * bit-vector expression.
 */
public final class BitVecLShR extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(27);

   /**
    * Creates a new node representing the logical right shift of a bit-vector expression
    * by another bit-vector expression.
    * 
    * @param lhs
    *           the bit-vector being shifted.
    * @param rhs
    *           the expression representing the number of bits by which lhs is shifted.
    */
   private BitVecLShR(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Creates a new node representing the logical right shift of a bit-vector expression
    * by another bit-vector expression.
    * 
    * @param lhs
    *           the bit-vector being shifted.
    * @param rhs
    *           the expression representing the number of bits by which lhs is shifted.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      BitVecVal lval = Exprs.cast(lhs, BitVecVal.class);
      BitVecVal rval = Exprs.cast(rhs, BitVecVal.class);
      if (lval != null && rval != null) {
         // BitVecVal(a) lshr BitVecVal(b) = BitVecVal(a lshr b)
         return BitVecVal.create(lval.getValue().lshr(rval.getValue()));
      } else if (lval != null && lval.isZero()) {
         // BitVecVal(0) lshr BitVecVal(b) = BitVecVal(0)
         return lhs;
      } else if (rval != null && rval.isZero()) {
         // BitVecVal(a) lshr BitVecVal(0) = BitVecVal(a)
         return lhs;
      }
      return new BitVecLShR(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof BitVecSort,
            "BitVecLShR first child must have bit-vector sort, got %s.",
            lhs.sort());

      Preconditions.checkArgument(
            lhs.sort().equals(rhs.sort()),
            "BitVecLShR children must have the same sort, got %s and %s.",
            lhs.sort(),
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getExpr(0).sort();
   }

   @Override
   public String toSmt() {
      return String.format("(bvlshr %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
