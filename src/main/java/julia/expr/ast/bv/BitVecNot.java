package julia.expr.ast.bv;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the bitwise not of a bit-vector expression.
 */
public final class BitVecNot extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(29);

   /**
    * Creates a new node representing the bitwise not of a bit-vector expression.
    * 
    * @param expr
    *           the expression to invert.
    */
   private BitVecNot(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Creates a new node representing the bitwise not of a bit-vector expression.
    * 
    * @param expr
    *           the expression to invert.
    * @return a new node representing the bitwise not of a bit-vector expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      BitVecVal val = Exprs.cast(expr, BitVecVal.class);
      if (val != null) {
         // (~ BitVecVal(a)) = BitVecVal(~a)
         return BitVecVal.create(val.getValue().not());
      } else if (expr instanceof BitVecNot) {
         // (~ (~ e)) = e
         return expr.getExpr(0);
      } else {
         return new BitVecNot(expr);
      }
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort() instanceof BitVecSort,
            "BitVecNot child must have bit-vector sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getExpr(0).sort();
   }

   @Override
   public String toSmt() {
      return String.format("(bvnot %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
