package julia.expr.ast.bv;

import java.math.BigInteger;
import julia.expr.ast.uf.Fun;
import julia.expr.sort.BitVecSort;
import julia.utils.Exprs;

/**
 * Fake class to create bit-vector variables using the {@code BitVec.create(...)} syntax.
 */
public final class BitVec {
   /**
    * Do not instantiate me!
    */
   private BitVec() {}

   /**
    * Returns a new variable of bit-vector sort.
    * 
    * @param index
    *           the index of the variable.
    * @param size
    *           the length of the bit-vector represented by the variable.
    * @return a new variable of bit-vector sort.
    */
   public static Fun create(int index, long size) {
      return create(index, BigInteger.valueOf(size));
   }

   /**
    * Returns a new variable of bit-vector sort.
    * 
    * @param index
    *           the index of the variable.
    * @param size
    *           the length of the bit-vector represented by the variable.
    * @return a new variable of bit-vector sort.
    */
   public static Fun create(int index, BigInteger size) {
      return Exprs.mkConst(index, BitVecSort.create(size));
   }
}