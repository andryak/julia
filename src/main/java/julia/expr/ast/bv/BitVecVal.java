package julia.expr.ast.bv;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.utils.BigRational;
import julia.expr.utils.BitVector;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing a bit-vector constant.
 */
public final class BitVecVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(45);

   private final static int INSTANCES = 1024;
   private final static BitVecVal[][] instances;

   static {
      instances = new BitVecVal[7][INSTANCES];
      for (int sizeLog = 0; sizeLog < 7; ++sizeLog) {
         for (int v = 0; v < INSTANCES; ++v) {
            instances[sizeLog][v] = new BitVecVal(new BitVector(v, 1 << sizeLog));
         }
      }
   }

   /**
    * Creates a new node representing a bit-vector constant.
    * 
    * @param value
    *           the value of the bit-vector constant.
    */
   private BitVecVal(BitVector value) {
      super(new ArrayList<>(), Arrays.asList(value));
   }

   /**
    * Returns true if this node represents a bit-vector constant of zero value.
    * 
    * @return true if this node represents a bit-vector constant of zero value.
    */
   public boolean isZero() {
      return this.getValue().signum() == 0;
   }

   /**
    * Returns true if this node represents the bit-vector constant 1.
    * 
    * @return true if this node represents the bit-vector constant 1.
    */
   public boolean isOne() {
      BigInteger uval = this.getValue().toUnsignedInteger();
      return uval.equals(BigInteger.ONE);
   }

   /**
    * Returns true if this node represents the bit-vector constant -1.
    * 
    * @return true if this node represents the bit-vector constant -1.
    */
   public boolean isNegOne() {
      BigInteger sval = this.getValue().toSignedInteger();
      return sval.equals(BigInteger.ONE.negate());
   }

   /**
    * Returns a new node representing a bit-vector constant.
    * 
    * @param value
    *           the value of the bit-vector constant.
    * @return a new node representing a bit-vector constant.
    */
   public static BitVecVal create(String value) {
      return create(BitVector.valueOf(value));
   }

   /**
    * Returns a new node representing a bit-vector constant.
    * 
    * @param value
    *           the value of the bit-vector constant.
    * @param size
    *           the size of the bit-vector constant.
    * @return a new node representing a bit-vector constant.
    */
   public static BitVecVal create(long value, long size) {
      return create(new BitVector(value, size));
   }

   /**
    * Returns a new node representing a bit-vector constant.
    * 
    * @param value
    *           the value of the bit-vector constant.
    * @param size
    *           the size of the bit-vector constant.
    * @return a new node representing a bit-vector constant.
    */
   public static BitVecVal create(BigInteger value, BigInteger size) {
      return create(new BitVector(value, size));
   }

   /**
    * Returns a new node representing a bit-vector constant.
    * 
    * @param value
    *           the value of the bit-vector constant.
    * @return a new node representing a bit-vector constant.
    */
   public static BitVecVal create(BitVector value) {
      try {
         // Throws if value or size do not fit in 32 bits.
         int v = value.toUnsignedInteger().intValueExact();
         int size = value.getSize().intValueExact();

         // If the size is a power of two less than or equal to 64,
         if (size > 0 && (size & (size - 1)) == 0 && size <= 64 && v < INSTANCES) {
            // Get the base two logarithm of size.
            int sizeLog;
            for (sizeLog = -1; size > 0; size >>>= 1, ++sizeLog) {}

            return instances[sizeLog][v];
         }
      } catch (ArithmeticException e) {
         // Bit-vectors with values or sizes larger than 32 bits are not cached.
      }
      return new BitVecVal(value);
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create((BitVector) args.get(0));
   }

   @Override
   public BitVector getValue() {
      return (BitVector) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BitVecSort sort() {
      return BitVecSort.create(this.getValue().getSize());
   }

   @Override
   public String toString() {
      return this.getValue().toString();
   }

   // Added for efficiency.
   @Override
   public BitVecVal eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public BitVecVal renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      BitVector value = this.getValue();
      String result = "#b";
      for (int i = value.getSize().intValue() - 1; i >= 0; --i) {
         result += (value.getValue().testBit(i)) ? ("1") : ("0");
      }
      return result;
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public BigRational delta(IConst other) {
      BitVector value = (BitVector) other.getValue();
      return BigRational.create(this.getValue().sub(value).toUnsignedInteger());
   }
}
