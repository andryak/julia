package julia.expr.ast.bv;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the sign extension of a bit-vector expression.
 */
public final class BitVecSignExt extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(34);

   /**
    * Creates a new node representing the sign extension of the given bit-vector
    * expression by the given number of bits.
    * 
    * @param expr
    *           the expression to extend.
    * @param n
    *           the number of sign bits to add to expr.
    */
   private BitVecSignExt(IExpr expr, long n) {
      super(Arrays.asList(expr), Arrays.asList(n));
   }

   /**
    * Returns the number of sign bits added to the child expression.
    * 
    * @return the number of sign bits added to the child expression.
    */
   public long getN() {
      return (Long) this.getArg(0);
   }

   /**
    * Returns a new node representing the sign extension of the given bit-vector
    * expression by the given number of bits.
    * 
    * @param expr
    *           the expression to extend.
    * @param n
    *           the number of sign bits to add to expr.
    * @return a new node representing the sign extension of the given bit-vector
    *         expression by the given number of bits.
    */
   public static IExpr create(IExpr expr, long n) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, n);
      }

      BitVecVal val = Exprs.cast(expr, BitVecVal.class);
      if (val != null) {
         // (sign_ext BitVecVal(a) n) = BitVecVal((sign_ext a n))
         return BitVecVal.create(val.getValue().signExt(n));
      } else if (n == 0) {
         return expr;
      } else {
         return new BitVecSignExt(expr, n);
      }
   }

   public static void checkParams(IExpr expr, long n) {
      Preconditions.checkArgument(
            expr.sort() instanceof BitVecSort,
            "BitVecSignExt child must have bit-vector sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            n >= 0,
            "BitVecSignExt argument must be a non-negative integer, got %s.",
            n);
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), (Long) args.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BitVecSort sort() {
      BigInteger size = ((BitVecSort) this.getExpr(0).sort()).getSize();
      BigInteger n = BigInteger.valueOf(this.getN());
      return BitVecSort.create(size.add(n));
   }

   @Override
   public String toSmt() {
      return String.format("((_ sign_extend %d) %s)", this.getN(), this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
