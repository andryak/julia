package julia.expr.ast.bv;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the extraction of a portion of a bit-vector expression.
 */
public final class BitVecExtract extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(26);

   /**
    * Creates a new node representing the extraction of a portion of a bit-vector
    * expression.
    * 
    * @param expr
    *           the base expression.
    * @param high
    *           the index of the leftmost bit to extract.
    * @param low
    *           the index of the rightmost bit to extract.
    */
   private BitVecExtract(IExpr expr, long high, long low) {
      super(Arrays.asList(expr), Arrays.asList(high, low));
   }

   /**
    * Returns the index of the leftmost bit to extract
    * 
    * @return the index of the leftmost bit to extract
    */
   public long getHigh() {
      return (Long) this.getArg(0);
   }

   /**
    * Returns the index of the rightmost bit to extract.
    * 
    * @return the index of the rightmost bit to extract.
    */
   public long getLow() {
      return (Long) this.getArg(1);
   }

   /**
    * Returns a new node representing the extraction of a portion of a bit-vector
    * expression.
    * 
    * @param expr
    *           the base expression.
    * @param high
    *           index of the leftmost bit to extract.
    * @param low
    *           index of the rightmost bit to extract.
    * @return a new node representing the extraction of a portion of a bit-vector
    *         expression.
    */
   public static IExpr create(IExpr expr, long high, long low) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, high, low);
      }

      BitVecVal v = Exprs.cast(expr, BitVecVal.class);
      if (v != null) {
         return BitVecVal.create(v.getValue().extract(high, low));
      }

      // If the extraction would return the whole expression, return it directly.
      BitVecSort exprSort = (BitVecSort) expr.sort();
      BigInteger lastIndex = exprSort.getSize().subtract(BigInteger.ONE);
      if (BigInteger.valueOf(high).equals(lastIndex) && low == 0) {
         return expr;
      }

      return new BitVecExtract(expr, high, low);
   }

   public static void checkParams(IExpr expr, long high, long low) {
      Preconditions.checkArgument(
            expr.sort() instanceof BitVecSort,
            "BitVecExtract child must have bit-vector sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            ((BitVecSort) expr.sort()).getSize().compareTo(BigInteger.valueOf(high)) >= 0,
            "BitVecExtract child size must be greater than the first argument, got %s.",
            ((BitVecSort) expr.sort()).getSize());

      Preconditions.checkArgument(
            high >= low,
            "BitVecExtract first argument must be non negative, got %s.",
            high);

      Preconditions.checkArgument(
            low >= 0,
            "BitVecExtract second argument must be non negative, got %s.",
            low);
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), (Long) args.get(0), (Long) args.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BitVecSort sort() {
      return BitVecSort.create(this.getHigh() - this.getLow() + 1);
   }

   @Override
   public String toSmt() {
      long high = this.getHigh();
      long low = this.getLow();
      String expr = this.getExpr(0).toSmt();
      return String.format("((_ extract %d %d) %s)", high, low, expr);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
