package julia.expr.ast.bv;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the remainder of the unsigned division of two bit-vector expressions.
 */
public final class BitVecURem extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(44);

   /**
    * Creates a new node representing the remainder of the unsigned division of the two
    * given bit-vector expressions.
    * 
    * @param lhs
    *           the dividend.
    * @param rhs
    *           the divisor.
    */
   private BitVecURem(IExpr dividend, IExpr divisor) {
      super(Arrays.asList(dividend, divisor));
   }

   /**
    * Returns a new node representing the remainder of the unsigned division of the two
    * given bit-vector expressions.
    * 
    * @param lhs
    *           the dividend.
    * @param rhs
    *           the divisor.
    * @return a new node representing the remainder of the unsigned division of the two
    *         given bit-vector expressions.
    */
   public static IExpr create(IExpr dividend, IExpr divisor) {
      if (Expr.CHECK_PARAMS) {
         checkParams(dividend, divisor);
      }

      BitVecVal p = Exprs.cast(dividend, BitVecVal.class);
      BitVecVal q = Exprs.cast(divisor, BitVecVal.class);
      if (p != null && q != null) {
         // BitVecVal(a) urem BitVecVal(b) = BitVecVal(a urem b)
         return BitVecVal.create(p.getValue().urem(q.getValue()));
      } else if (q != null && q.isOne()) {
         // BitVecVal(a) urem 1 = 0
         return BitVecVal.create(BigInteger.ZERO, q.sort().getSize());
      }
      return new BitVecURem(dividend, divisor);
   }

   public static void checkParams(IExpr dividend, IExpr divisor) {
      Preconditions.checkArgument(
            dividend.sort() instanceof BitVecSort,
            "BitVecURem first child must have bit-vector sort, got %s.",
            dividend.sort());

      Preconditions.checkArgument(
            dividend.sort().equals(divisor.sort()),
            "BitVecURem children must have the same sort, got %s and %s.",
            dividend.sort(),
            divisor.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getExpr(0).sort();
   }

   @Override
   public String toSmt() {
      return String.format("(bvurem %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
