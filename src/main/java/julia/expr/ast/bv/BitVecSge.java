package julia.expr.ast.bv;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing two bit-vector expressions being one greater than or equal to the
 * other one (in signed arithmetics).
 */
public final class BitVecSge extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(31);

   /**
    * Creates a new node representing the two given bit-vector expressions being one
    * greater than or equal to the other one (in signed arithmetics).
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private BitVecSge(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing the two given bit-vector expressions being one
    * greater than or equal to the other one (in signed arithmetics).
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the two given bit-vector expressions being one
    *         greater than or equal to the other one (in signed arithmetics).
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      BitVecVal lval = Exprs.cast(lhs, BitVecVal.class);
      BitVecVal rval = Exprs.cast(rhs, BitVecVal.class);
      if (lval != null && rval != null) {
         return BoolVal.create(lval.getValue().sge(rval.getValue()));
      }
      return new BitVecSge(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof BitVecSort,
            "BitVecSge first child must have bit-vector sort, got %s",
            lhs.sort());

      Preconditions.checkArgument(
            lhs.sort().equals(rhs.sort()),
            "BitVecSge children must have the same sort, got %s and %s",
            lhs.sort(),
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public String toSmt() {
      return String.format("(bvsge %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr lexpr = this.getExpr(0).eval(model);
      IExpr rexpr = this.getExpr(1).eval(model);

      BitVecVal lval = Exprs.cast(lexpr, BitVecVal.class);
      BitVecVal rval = Exprs.cast(rexpr, BitVecVal.class);
      if (lval != null && rval != null) {
         BigInteger lhs = lval.getValue().toSignedInteger();
         BigInteger rhs = rval.getValue().toSignedInteger();

         BigRational dir;
         BigRational inv;

         if (lhs.compareTo(rhs) >= 0) {
            dir = BigRational.ZERO;
            inv = BigRational.create(lhs.subtract(rhs).add(BigInteger.ONE));
         } else {
            dir = BigRational.create(rhs.subtract(lhs));
            inv = BigRational.ZERO;
         }
         return new SatDelta(dir, inv);
      }
      throw new UndefinedSatDeltaError(this, model);
   }

   @Override
   public ExprSort sort() {
      return BoolSort.create();
   }
}
