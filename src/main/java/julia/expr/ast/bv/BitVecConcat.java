package julia.expr.ast.bv;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the concatenation of two bit-vector expressions.
 */
public final class BitVecConcat extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(25);

   /**
    * Creates a new node representing the concatenation of two bit-vector expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private BitVecConcat(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns the left child of this expression.
    * 
    * @return the left child of this expression.
    */
   public IExpr getLhs() {
      return this.getExpr(0);
   }

   /**
    * Returns the right child of this expression.
    * 
    * @return the right child of this expression.
    */
   public IExpr getRhs() {
      return this.getExpr(1);
   }

   /**
    * Returns a new node representing the concatenation of two bit-vector expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the concatenation of two bit-vector expressions.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      BitVecVal lval = Exprs.cast(lhs, BitVecVal.class);
      BitVecVal rval = Exprs.cast(rhs, BitVecVal.class);
      if (lval != null && rval != null) {
         // BitVecVal(a) concat BitVecVal(b) = BitVecVal(a concat b)
         return BitVecVal.create(lval.getValue().concat(rval.getValue()));
      }
      return new BitVecConcat(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof BitVecSort,
            "BitVecConcat first child must have bit-vector sort, got %s.",
            lhs.sort());

      Preconditions.checkArgument(
            rhs.sort() instanceof BitVecSort,
            "BitVecConcat second child must have bit-vector sort, got %s.",
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BitVecSort sort() {
      BitVecSort lsort = (BitVecSort) this.getLhs().sort();
      BitVecSort rsort = (BitVecSort) this.getRhs().sort();
      return BitVecSort.create(lsort.getSize().add(rsort.getSize()));
   }

   @Override
   public String toSmt() {
      return String.format("(concat %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
