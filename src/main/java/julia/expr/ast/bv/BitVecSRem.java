package julia.expr.ast.bv;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the remainder of the signed division of two bit-vector expressions.
 */
public final class BitVecSRem extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(37);

   /**
    * Creates a new node representing the remainder of the signed division of the two
    * given bit-vector expressions.
    * 
    * @param dividend
    *           the dividend.
    * @param divisor
    *           the divisor.
    */
   private BitVecSRem(IExpr dividend, IExpr divisor) {
      super(Arrays.asList(dividend, divisor));
   }

   /**
    * Returns a new node representing the remainder of the signed division of the two
    * given bit-vector expressions.
    * 
    * @param dividend
    *           the dividend.
    * @param divisor
    *           the divisor.
    * @return a new node representing the remainder of the signed division of the two
    *         given bit-vector expressions.
    */
   public static IExpr create(IExpr dividend, IExpr divisor) {
      if (Expr.CHECK_PARAMS) {
         checkParams(dividend, divisor);
      }

      BitVecVal p = Exprs.cast(dividend, BitVecVal.class);
      BitVecVal q = Exprs.cast(divisor, BitVecVal.class);
      if (p != null && q != null) {
         // BitVecVal(a) srem BitVecVal(b) = BitVecVal(a srem b)
         return BitVecVal.create(p.getValue().srem(q.getValue()));
      }
      return new BitVecSRem(dividend, divisor);
   }

   public static void checkParams(IExpr dividend, IExpr divisor) {
      Preconditions.checkArgument(
            dividend.sort() instanceof BitVecSort,
            "BitVecSRem first child must have bit-vector sort, got %s.",
            dividend.sort());

      Preconditions.checkArgument(
            dividend.sort().equals(divisor.sort()),
            "BitVecSRem children must have the same sort, got %s and %s.",
            dividend.sort(),
            divisor.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getExpr(0).sort();
   }

   @Override
   public String toSmt() {
      return String.format("(bvsrem %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
