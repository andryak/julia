package julia.expr.ast;

import julia.expr.utils.BigRational;

/**
 * The interface of nodes in an expression tree which represent sorted constants.
 * <p>
 * A sorted constant is an interpreted nullary function.
 */
public interface IConst extends IExpr {
   /**
    * Returns the value of this constant.
    * 
    * @return the value of this constant.
    */
   public Object getValue();

   /**
    * Returns the distance of this constant from another constant.
    * <p>
    * The default implementation of this method returns zero if this constant equals the
    * given constant, one otherwise.
    * 
    * @param other
    *           the other constant.
    * @return the distance of this constant from another constant.
    */
   default BigRational delta(IConst other) {
      if (this.equals(other)) {
         return BigRational.ZERO;
      }
      return BigRational.ONE;
   }
}
