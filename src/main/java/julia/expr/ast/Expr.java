package julia.expr.ast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import julia.expr.ast.uf.Fun;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.utils.Counter;
import julia.utils.Hashing;
import julia.utils.VarMap;
import com.google.common.base.Joiner;

/**
 * Node in an expression tree.
 * <p>
 * <b>Notice:</b> all subclasses of this class must be immutable.
 */
public abstract class Expr implements IExpr {
   /**
    * Flag determining whether node parameters are checked at build time or not.
    */
   protected static boolean CHECK_PARAMS = false;

   /**
    * Sets or unsets the CHECK_PARAMS flag.
    * 
    * @param value
    *           the new value of the flag.
    * 
    * @see CHECK_PARAMS.
    */
   public static void setCheckParams(boolean value) {
      CHECK_PARAMS = value;
   }

   /**
    * The children of this node.
    */
   private final List<IExpr> exprs;

   /**
    * Extra arguments for this node.
    */
   private final List<Object> args;

   /**
    * Hash code representing this node and its children.
    */
   private final int hash;

   /**
    * Creates a new node with the given children.
    * 
    * @param exprs
    *           the children of the node being created.
    */
   protected Expr(List<IExpr> exprs) {
      this(exprs, new ArrayList<>());
   }

   /**
    * Creates a new node with the given children and extra arguments.
    * 
    * @param exprs
    *           the children of the node being created.
    * @param args
    *           the extra arguments of the node being created.
    */
   protected Expr(List<IExpr> exprs, List<Object> args) {
      this.exprs = exprs;
      this.args = args;
      this.hash = Hashing.combine(this.opcode(), exprs, args);
   }

   /**
    * Returns a copy of this node with different children and arguments.
    * 
    * @return a copy of this node with different children and arguments.
    */
   protected abstract IExpr make(List<IExpr> exprs, List<Object> args);

   /**
    * Returns a copy of this expression with renamed variables.
    * <p>
    * The variables are renamed according to the given substitution map.
    * 
    * @param map
    *           a map from variables to their new indexes.
    * @return a copy of this expression with renamed variables.
    * 
    * @see VarMap
    */
   public IExpr renameVars(VarMap map) {
      List<IExpr> exprs = new ArrayList<>();
      for (final IExpr expr : this.getExprs()) {
         exprs.add(expr.renameVars(map));
      }
      return this.make(exprs, this.getArgs());
   }

   /**
    * Evaluates this expression on the given model.
    * 
    * @param model
    *           a model mapping the variables in this expression to concrete values.
    * @return the expression obtained evaluating this expression on the given model.
    */
   public IExpr eval(Model model) {
      List<IExpr> exprs = new ArrayList<>();
      for (final IExpr expr : this.getExprs()) {
         exprs.add(expr.eval(model));
      }
      return this.make(exprs, this.getArgs());
   }

   /**
    * Evaluates this expression on the empty model.
    * <p>
    * <b>Notice:</b> this is only safe if this expression contains no variables.
    * 
    * @return the expression obtained evaluating this expression on the empty model.
    */
   public IExpr eval() {
      return this.eval(null);
   }

   /**
    * Returns a hash code representing this expression tree.
    * <p>
    * Structurally equal trees have the same hash codes.
    * 
    * @return a hash code representing this expression tree.
    */
   @Override
   public int hashCode() {
      return this.hash;
   }

   /**
    * Re-indexes the variables of this expression.
    * <p>
    * This method returns a copy of this expression in which the indexes of the variables
    * of each sort have been re-indexed starting from zero.
    * 
    * @return a copy of this expression in which the variables have been re-indexed.
    */
   public IExpr compress() {
      Counter<ExprSort> counter = new Counter<>();
      Map<Fun, Integer> map = new HashMap<>();
      for (final Fun v : this.getVars()) {
         int i = counter.increment(v.sort()).intValue();
         map.put(v, i - 1);
      }
      return this.renameVars(new VarMap(map));
   }

   /**
    * Returns the index-th child of this node.
    * 
    * @param index
    *           the index of the child to extract.
    * @return the index-th child of this node.
    * @throws IndexOutOfBoundsException
    *            if the index-th child does not exist.
    */
   public IExpr getExpr(int index) {
      return this.getExprs().get(index);
   }

   /**
    * Returns the children of this node.
    * 
    * @return the children of this node.
    */
   public List<IExpr> getExprs() {
      return this.exprs;
   }

   /**
    * Returns the index-th argument of this node.
    * 
    * @param index
    *           the index of the argument to extract.
    * @return the index-th argument of this node.
    * @throws IndexOutOfBoundsException
    *            if the index-th argument does not exist.
    */
   public Object getArg(int index) {
      return this.getArgs().get(index);
   }

   /**
    * Returns the arguments of this node.
    * 
    * @return the arguments of this node.
    */
   public List<Object> getArgs() {
      return this.args;
   }

   /**
    * Returns true if this expression and the given object are equal.
    * <p>
    * If the given object is not an expression, {@code false} is returned. Otherwise the
    * two expressions are compared for structural equality.
    * 
    * @param other
    *           the object to compare to this expression.
    * @return true if this expression and the given object are equal.
    */
   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (this.getClass() != other.getClass()) {
         return false;
      } else if (this.hashCode() != other.hashCode()) {
         return false;
      } else {
         IExpr o = (IExpr) other;
         boolean sameExprs = this.getExprs().equals(o.getExprs());
         boolean sameArgs = this.getArgs().equals(o.getArgs());
         return (sameExprs && sameArgs);
      }
   }

   /**
    * Returns the name of this expression.
    * <p>
    * The default implementation of this method returns the name of the node class.
    * 
    * @return the name of this expression.
    */
   public String getName() {
      return this.getClass().getSimpleName();
   }

   /**
    * Returns a string representing this expression.
    * <p>
    * The string is a human readable representation of this node, which contains its name,
    * its children and its arguments.
    * 
    * @return a string representing this expression.
    */
   @Override
   public String toString() {
      String name = this.getName();
      List<IExpr> exprs = this.getExprs();
      List<Object> args = this.getArgs();
      if (exprs.size() <= 0 && args.size() <= 0) {
         // If this node has no children nor arguments return its name.
         // Overriding this method for nodes which would follow in this case is preferred.
         return name;
      } else if (exprs.size() <= 0) {
         return String.format("(%s %s)", name, Joiner.on(" ").join(args));
      } else if (args.size() <= 0) {
         return String.format("(%s %s)", name, Joiner.on(" ").join(exprs));
      } else {
         String sexprs = Joiner.on(" ").join(exprs);
         String sargs = Joiner.on(" ").join(args);
         return String.format("(%s %s %s)", name, sexprs, sargs);
      }
   }

   /**
    * Returns the set of variables in this expression tree.
    * <p>
    * A variable is an uninterpreted function.
    * 
    * @return the set of variables in this expression tree.
    * 
    * @see Fun
    */
   public Set<Fun> getVars() {
      Set<Fun> result = new HashSet<>();
      Queue<IExpr> queue = new LinkedList<>();
      queue.add(this);
      while (!queue.isEmpty()) {
         IExpr current = queue.poll();
         if (current instanceof Fun) {
            result.add((Fun) current);
         } else {
            queue.addAll(current.getExprs());
         }
      }
      return result;
   }
}
