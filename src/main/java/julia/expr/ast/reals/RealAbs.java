package julia.expr.ast.reals;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.RealSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the absolute value of a real expression.
 */
public final class RealAbs extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(54);

   /**
    * Creates a new node representing the absolute value of the given real expression.
    * 
    * @param expr
    *           the expression to get the absolute value of.
    */
   private RealAbs(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the absolute value of the given real expression.
    * 
    * @param expr
    *           the expression to get the absolute value of.
    * @return a new node representing the absolute value of the given real expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      RealVal c = Exprs.cast(expr, RealVal.class);
      if (c != null) {
         return RealVal.create(c.getValue().abs());
      } else if (expr instanceof RealAbs) {
         return expr;
      } else {
         return new RealAbs(expr);
      }
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort().equals(RealSort.create()),
            "RealAbs child must have real sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return RealSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(abs %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
