package julia.expr.ast.reals;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing two real expressions being one greater than or equal to the other
 * one.
 */
public final class RealGe extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(57);

   /**
    * Creates a new node representing the two given real expressions being one greater
    * than or equal to the other one.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private RealGe(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing the two given real expressions being one greater
    * than or equal to the other one.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the two given real expressions being one greater
    *         than or equal to the other one.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      RealVal lval = Exprs.cast(lhs, RealVal.class);
      RealVal rval = Exprs.cast(rhs, RealVal.class);
      if (lval != null && rval != null) {
         return BoolVal.create(lval.getValue().compareTo(rval.getValue()) >= 0);
      } else if (lhs.equals(rhs)) {
         return BoolVal.TRUE;
      }
      return new RealGe(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof RealSort,
            "RealGe first child must have real sort, got %s",
            lhs.sort());

      Preconditions.checkArgument(
            rhs.sort() instanceof RealSort,
            "RealGe second child must have real sort, got %s",
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public String toSmt() {
      return String.format("(>= %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr lexpr = this.getExpr(0).eval(model);
      IExpr rexpr = this.getExpr(1).eval(model);

      RealVal lval = Exprs.cast(lexpr, RealVal.class);
      RealVal rval = Exprs.cast(rexpr, RealVal.class);
      if (lval != null && rval != null) {
         BigRational lhs = lval.getValue();
         BigRational rhs = rval.getValue();

         BigRational dir;
         BigRational inv;

         if (lhs.compareTo(rhs) >= 0) {
            dir = BigRational.ZERO;
            inv = lhs.subtract(rhs).add(BigRational.ONE);
         } else {
            dir = rhs.subtract(lhs);
            inv = BigRational.ZERO;
         }
         return new SatDelta(dir, inv);
      }
      throw new UndefinedSatDeltaError(this, model);
   }

   @Override
   public ExprSort sort() {
      return BoolSort.create();
   }
}
