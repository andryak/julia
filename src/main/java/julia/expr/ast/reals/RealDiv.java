package julia.expr.ast.reals;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.RealSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the division of two real expressions.
 */
public final class RealDiv extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(56);

   /**
    * Creates a new node representing the division of the two given real expressions.
    * 
    * @param dividend
    *           the dividend.
    * @param divisor
    *           the divisor.
    */
   private RealDiv(IExpr dividend, IExpr divisor) {
      super(Arrays.asList(dividend, divisor));
   }

   /**
    * Returns a new node representing the division of the two given real expressions.
    * 
    * @param dividend
    *           the dividend.
    * @param divisor
    *           the divisor.
    * @return a new node representing the division of the two given real expressions.
    */
   public static IExpr create(IExpr dividend, IExpr divisor) {
      if (Expr.CHECK_PARAMS) {
         checkParams(dividend, divisor);
      }

      RealVal p = Exprs.cast(dividend, RealVal.class);
      RealVal q = Exprs.cast(divisor, RealVal.class);
      if (p != null & q != null) {
         return RealVal.create(p.getValue().divide(q.getValue()));
      } else if (p == RealVal.ZERO) {
         return RealVal.ZERO;
      } else if (q == RealVal.ONE) {
         return dividend;
      } else if (dividend.equals(divisor)) {
         return RealVal.ONE;
      } else {
         return new RealDiv(dividend, divisor);
      }
   }

   public static void checkParams(IExpr dividend, IExpr divisor) {
      Preconditions.checkArgument(
            dividend.sort().equals(RealSort.create()),
            "RealDiv first child must have real sort, got %s.",
            dividend.sort());

      Preconditions.checkArgument(
            divisor.sort().equals(RealSort.create()),
            "RealDiv second child must have real sort, got %s.",
            divisor.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return RealSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(/ %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
