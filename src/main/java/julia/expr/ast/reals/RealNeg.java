package julia.expr.ast.reals;

import julia.expr.ast.IExpr;

/**
 * Fake class to produce the arithmetic negation of a real expression.
 */
public final class RealNeg {
   /**
    * Do not instantiate me!
    */
   private RealNeg() {}

   /**
    * Returns the arithmetic negation of the given real expression.
    * 
    * @param expr
    *           the expression to negate.
    * @return the arithmetic negation of the given real expression.
    */
   public static IExpr create(IExpr expr) {
      return RealMul.create(RealVal.create("-1"), expr);
   }
}
