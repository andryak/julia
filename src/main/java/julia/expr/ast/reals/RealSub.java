package julia.expr.ast.reals;

import julia.expr.ast.IExpr;

/**
 * Fake class to produce the arithmetic subtraction of the given expressions.
 */
public final class RealSub {
   /**
    * Do not instantiate me!
    */
   private RealSub() {}

   /**
    * Returns the arithmetic subtraction of the given real expressions.
    * <p>
    * If exprs is empty this method returns {@code RealVal.ZERO}. Otherwise it returns
    * exprs[0] - exprs[1] - ... - exprs[n].
    * 
    * @param exprs
    *           a list of expressions.
    * @return the arithmetic subtraction of the given real expressions.
    */
   public static IExpr create(IExpr... exprs) {
      if (exprs.length <= 0) {
         return RealVal.ZERO;
      }

      IExpr result = exprs[0];
      for (int i = 1; i < exprs.length; ++i) {
         result = RealAdd.create(result, RealMul.create(RealVal.create(-1), exprs[i]));
      }
      return result;
   }
}
