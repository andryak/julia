package julia.expr.ast.reals;

import julia.expr.ast.uf.Fun;
import julia.expr.sort.RealSort;
import julia.utils.Exprs;

/**
 * Fake class to create real variables using the {@code Real.create(...)} syntax.
 */
public final class Real {
   /**
    * Do not instantiate me!
    */
   private Real() {}

   /**
    * Returns a new variable of real sort.
    * 
    * @param index
    *           the index of the variable.
    * @return a new variable of real sort.
    */
   public static Fun create(int index) {
      return Exprs.mkConst(index, RealSort.create());
   }
}
