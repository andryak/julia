package julia.expr.ast.reals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the sum of real expressions.
 */
public final class RealAdd extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(55);

   /**
    * Creates a new node representing the sum of the given real expressions.
    * 
    * @param exprs
    *           the list of expressions to sum.
    */
   private RealAdd(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns the coefficient of this sum.
    * 
    * @return the coefficient of this sum.
    */
   public RealVal coeff() {
      return (RealVal) this.getExpr(0);
   }

   /**
    * Returns the non-constant terms of this sum.
    * 
    * @return the non-constant terms of this sum.
    */
   public List<IExpr> terms() {
      return this.getExprs().subList(1, this.getExprs().size());
   }

   /**
    * Returns a new node representing the sum of the given real expressions.
    * 
    * @param exprs
    *           the array of expressions to sum.
    * @return a new node representing the sum of the given real expressions.
    */
   public static IExpr create(IExpr... exprs) {
      return create(Arrays.asList(exprs));
   }

   /**
    * Returns a new node representing the sum of the given real expressions.
    * 
    * @param exprs
    *           the list of expressions to sum.
    * @return a new node representing the sum of the given real expressions.
    */
   public static IExpr create(List<IExpr> exprs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(exprs);
      }

      Queue<IExpr> queue = new LinkedList<>(exprs);
      Map<List<IExpr>, BigRational> termMap = new HashMap<>();
      BigRational coeff = BigRational.ZERO;
      while (!queue.isEmpty()) {
         IExpr expr = queue.poll();
         if (expr instanceof RealVal) {
            coeff = coeff.add(((RealVal) expr).getValue());
         } else if (expr instanceof RealAdd) {
            queue.addAll(expr.getExprs());
         } else if (expr instanceof RealMul) {
            RealMul e = (RealMul) expr;
            BigRational ecoeff = e.coeff().getValue();
            List<IExpr> eterms = e.terms();
            termMap.put(eterms, termMap.getOrDefault(eterms, BigRational.ZERO).add(ecoeff));
         } else {
            List<IExpr> eterms = Arrays.asList(expr);
            termMap.put(
                  eterms,
                  termMap.getOrDefault(eterms, BigRational.ZERO).add(BigRational.ONE));
         }
      }

      // Build the list of non-constant arguments.
      List<IExpr> newArgsList = new ArrayList<>();
      for (Entry<List<IExpr>, BigRational> entry : termMap.entrySet()) {
         if (entry.getValue().equals(BigRational.ZERO)) {
            continue;
         } else if (entry.getValue().equals(BigRational.ONE)) {
            newArgsList.add(RealMul.create(entry.getKey()));
         } else {
            List<IExpr> mulExprs = new ArrayList<>();
            mulExprs.add(RealVal.create(entry.getValue()));
            mulExprs.addAll(entry.getKey());
            newArgsList.add(RealMul.create(mulExprs));
         }
      }

      if (newArgsList.size() == 0) {
         return RealVal.create(coeff);
      } else if (coeff.equals(BigRational.ZERO) && newArgsList.size() == 1) {
         return newArgsList.get(0);
      }

      // Sort children and ensure that the first is the coefficient.
      newArgsList.sort((e1, e2) -> Integer.compare(e1.hashCode(), e2.hashCode()));
      newArgsList.add(0, RealVal.create(coeff));
      return new RealAdd(newArgsList);
   }

   public static void checkParams(List<IExpr> exprs) {
      Preconditions.checkArgument(
            exprs.stream().allMatch(e -> e.sort() == RealSort.create()),
            "RealAdd children must have real sort, got [%s].",
            Joiner.on(",").join(exprs.stream().map(e -> e.sort()).iterator()));
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return RealSort.create();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(+ %s)",
            Joiner.on(" ").join(this.getExprs().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
