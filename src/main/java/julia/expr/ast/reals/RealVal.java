package julia.expr.ast.reals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing a real constant.
 */
public final class RealVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(62);

   private final static int POSITIVE_INSTANCES = 1024;
   private final static RealVal[] instances;

   public final static RealVal ZERO;
   public final static RealVal ONE;
   public final static RealVal TWO;

   static {
      final int INSTANCES = (2 * POSITIVE_INSTANCES);

      instances = new RealVal[INSTANCES];
      for (int i = 0; i < INSTANCES; ++i) {
         instances[i] = new RealVal(BigRational.create(i - POSITIVE_INSTANCES, 1));
      }

      ZERO = instances[POSITIVE_INSTANCES];
      ONE = instances[POSITIVE_INSTANCES + 1];
      TWO = instances[POSITIVE_INSTANCES + 2];
   }

   /**
    * Creates a new real constant with the given value.
    * 
    * @param value
    *           the value of the new real constant.
    */
   private RealVal(BigRational value) {
      super(new ArrayList<>(), Arrays.asList(value));
   }

   /**
    * Returns a new real constant with the given value.
    * 
    * @param value
    *           the value of the new real constant.
    * @return a new real constant with the given value.
    */
   public static RealVal create(String value) {
      return create(BigRational.create(value));
   }

   /**
    * Returns a new real constant with the given value.
    * 
    * @param value
    *           the value of the new real constant.
    * @return a new real constant with the given value.
    */
   public static RealVal create(long value) {
      return create(BigRational.create(value));
   }

   /**
    * Returns a new real constant with the given value.
    * 
    * @param value
    *           the value of the new real constant.
    * @return a new real constant with the given value.
    */
   public static RealVal create(BigRational value) {
      // Reals which are not exact integers are not cached.
      if (!value.q().equals(BigInteger.ONE)) {
         return new RealVal(value);
      }
      try {
         // Throws if p does not fit in 32 bits.
         int v = value.p().intValueExact();
         if (v >= -POSITIVE_INSTANCES && v < POSITIVE_INSTANCES) {
            return instances[v + POSITIVE_INSTANCES];
         }
      } catch (ArithmeticException e) {
         // Values larger than 32 bits are not cached.
      }
      return new RealVal(value);
   }

   @Override
   protected RealVal make(List<IExpr> exprs, List<Object> args) {
      return create((BigRational) args.get(0));
   }

   @Override
   public BigRational getValue() {
      return (BigRational) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public RealSort sort() {
      return RealSort.create();
   }

   @Override
   public String toString() {
      return this.getValue().toString();
   }

   // Added for efficiency.
   @Override
   public RealVal eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public RealVal renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      BigInteger p = this.getValue().p().abs();
      BigInteger q = this.getValue().q().abs();

      String result = p.toString();
      if (!q.equals(BigInteger.ONE)) {
         result = "(/ " + result + " " + q.toString() + ")";
      }

      if (this.getValue().signum() < 0) {
         result = "(- " + result + ")";
      }

      return result;
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public BigRational delta(IConst other) {
      BigRational value = (BigRational) other.getValue();
      return this.getValue().subtract(value).abs();
   }
}
