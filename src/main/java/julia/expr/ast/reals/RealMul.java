package julia.expr.ast.reals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.sort.RealSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the product of real expressions.
 */
public final class RealMul extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(61);

   /**
    * Creates a new node representing the product of the given real expressions.
    * 
    * @param exprs
    *           the list of expressions to multiply.
    */
   private RealMul(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns the coefficient of this product.
    * 
    * @return the coefficient of this product.
    */
   public RealVal coeff() {
      return (RealVal) this.getExpr(0);
   }

   /**
    * Returns the non-constant terms of this product.
    * 
    * @return the non-constant terms of this product.
    */
   public List<IExpr> terms() {
      return this.getExprs().subList(1, this.getExprs().size());
   }

   /**
    * Returns a new node representing the product of the given real expressions.
    * 
    * @param exprs
    *           the array of expressions to multiply.
    * @return a new node representing the product of the given real expressions.
    */
   public static IExpr create(IExpr... exprs) {
      return create(Arrays.asList(exprs));
   }

   /**
    * Returns a new node representing the product of the given real expressions.
    * 
    * @param exprs
    *           the list of expressions to multiply.
    * @return a new node representing the product of the given real expressions.
    */
   public static IExpr create(List<IExpr> exprs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(exprs);
      }

      Queue<IExpr> queue = new LinkedList<>(exprs);
      List<IExpr> newArgsList = new ArrayList<>();
      BigRational coeff = BigRational.ONE;
      while (!queue.isEmpty()) {
         IExpr expr = queue.poll();
         if (expr instanceof RealVal) {
            if (expr == RealVal.ZERO) {
               return RealVal.ZERO;
            }
            coeff = coeff.multiply(((RealVal) expr).getValue());
         } else if (expr instanceof RealMul) {
            queue.addAll(expr.getExprs());
         } else {
            newArgsList.add(expr);
         }
      }

      if (newArgsList.size() == 0) {
         return RealVal.create(coeff);
      } else if (coeff.equals(BigRational.ONE) && newArgsList.size() == 1) {
         return newArgsList.get(0);
      }

      // Sort children and ensure that the first is the coefficient.
      newArgsList.sort((e1, e2) -> Integer.compare(e1.hashCode(), e2.hashCode()));
      newArgsList.add(0, RealVal.create(coeff));
      return new RealMul(newArgsList);
   }

   public static void checkParams(List<IExpr> exprs) {
      Preconditions.checkArgument(
            exprs.stream().allMatch(e -> e.sort() == RealSort.create()),
            "RealMul children must have real sort, got [%s].",
            Joiner.on(",").join(exprs.stream().map(e -> e.sort()).iterator()));
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return RealSort.create();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(* %s)",
            Joiner.on(" ").join(this.getExprs().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
