package julia.expr.ast.uf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.FunSort;
import julia.expr.utils.FunInter;
import julia.utils.LRUCache;
import julia.utils.Pair;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing an interpreted function.
 */
public final class FunVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(21);

   private final static LRUCache<Pair<FunInter, FunSort>, FunVal> instances;
   static {
      instances = new LRUCache<>(1024);
   }

   /**
    * Creates a new node representing an interpreted function.
    * 
    * @param map
    *           the map describing the graph of this function.
    * @param elseValue
    *           the value assigned to all values not in the map.
    * @param sort
    *           the sort of this function.
    */
   private FunVal(FunInter inter, FunSort sort) {
      super(new ArrayList<>(), Arrays.asList(inter, sort));
   }

   /**
    * Returns a new node representing an interpreted function.
    * 
    * @param map
    *           the map describing the graph of this function.
    * @param elseValue
    *           the value assigned to all values not in the map.
    * @param sort
    *           the sort of this function.
    * @return a new node representing an interpreted function.
    */
   public static FunVal create(FunInter inter, FunSort sort) {
      Pair<FunInter, FunSort> params = new Pair<>(inter, sort);
      FunVal value = instances.get(params);
      if (value != null) {
         return value;
      }
      FunVal v = new FunVal(inter, sort);
      instances.put(params, v);
      return v;
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create((FunInter) args.get(0), (FunSort) args.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public FunSort sort() {
      return (FunSort) this.getArg(1);
   }

   // Added for efficiency.
   @Override
   public IExpr eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public IExpr renameVars(VarMap map) {
      return this;
   }

   @Override
   public FunInter getValue() {
      return (FunInter) this.getArg(0);
   }

   @Override
   public String toString() {
      return this.getValue().toString();
   }

   @Override
   public String toSmt() {
      throw new IllegalStateException("FunVal is not part of the SMT-LIB v2 standard.");
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
