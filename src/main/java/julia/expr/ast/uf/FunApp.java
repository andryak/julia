package julia.expr.ast.uf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.FunSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the application of a function to some arguments.
 */
public final class FunApp extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(20);

   /**
    * Creates a new node representing the application of a function to some arguments.
    * <p>
    * The first expression in the given list is the function, the others are its
    * arguments.
    * 
    * @param exprs
    *           a list of expressions.
    * @return a new node representing the application of a function to some arguments.
    */
   private FunApp(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns the child of this node representing the function.
    * 
    * @return the child of this node representing the function
    */
   public IExpr f() {
      return this.getExpr(0);
   }

   /**
    * Returns the children of this node representing the function arguments.
    * 
    * @return the children of this node representing the function arguments.
    */
   public List<IExpr> args() {
      return this.getExprs().subList(1, this.getExprs().size());
   }

   /**
    * Creates a new node representing the application of a function to some arguments.
    * 
    * @param f
    *           the function.
    * @param fargs
    *           the arguments.
    * @return a new node representing the application of a function to some arguments.
    */
   public static IExpr create(IExpr f, IExpr... fargs) {
      return create(f, Arrays.asList(fargs));
   }

   /**
    * Creates a new node representing the application of a function to some arguments.
    * 
    * @param f
    *           the function.
    * @param fargs
    *           the arguments.
    * @return a new node representing the application of a function to some arguments.
    */
   public static IExpr create(IExpr f, List<IExpr> fargs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(f, fargs);
      }

      // If the function and all parameters are constants perform the application.
      FunVal v = Exprs.cast(f, FunVal.class);
      if (v != null && fargs.stream().allMatch(e -> e instanceof IConst)) {
         List<IConst> cfargs = fargs
               .stream()
               .map(e -> (IConst) e)
               .collect(Collectors.toList());
         return v.getValue().get(cfargs);
      }

      // Otherwise return a wrapper node.
      List<IExpr> exprs = new ArrayList<>();
      exprs.add(f);
      exprs.addAll(fargs);
      return new FunApp(exprs);
   }

   public static void checkParams(IExpr f, List<IExpr> fargs) {
      Preconditions.checkArgument(
            f.sort() instanceof FunSort,
            "FunApp first child must have function sort, got %s.",
            f);

      FunSort fsort = (FunSort) f.sort();
      List<ExprSort> fargSorts = fargs
            .stream()
            .map(e -> e.sort())
            .collect(Collectors.toList());

      Preconditions
            .checkArgument(
                  fargSorts.equals(fsort.domain()),
                  "FunApp children from the second on must match the domain of the first child, got %s instead of %s.",
                  fargSorts,
                  fsort.domain());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.subList(1, exprs.size()));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return ((FunSort) this.f().sort()).range();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(%s %s)",
            this.f().toSmt(),
            Joiner.on(" ").join(this.args().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      if (!(this.sort() instanceof BoolSort)) {
         throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
      }
      IExpr expr = this.eval(model);
      if (expr instanceof BoolVal) {
         return expr.satDelta(model);
      }
      throw new UndefinedSatDeltaError(this, model);
   }
}