package julia.expr.ast.uf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.FunSort;
import julia.expr.utils.BigRational;
import julia.utils.LRUCache;
import julia.utils.Pair;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;
import com.google.common.base.Joiner;

/**
 * Node representing an uninterpreted function.
 * <p>
 * Uninterpreted functions are variables in SMT instances.
 */
public final class Fun extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(19);

   private final static LRUCache<Pair<Integer, FunSort>, Fun> instances;
   static {
      instances = new LRUCache<>(1024);
   }

   /**
    * Creates a new node representing an uninterpreted function with the given index and
    * sort.
    * 
    * @param index
    *           the index of the function.
    * @param sort
    *           the sort of the function.
    */
   private Fun(int index, FunSort sort) {
      super(new ArrayList<>(), Arrays.asList(index, sort));
   }

   /**
    * Returns the index of this uninterpreted function.
    * 
    * @return the index of this uninterpreted function.
    */
   public int getIndex() {
      return (int) this.getArg(0);
   }

   /**
    * Returns the domain of this function.
    * <p>
    * This is the list of the sorts of the arguments of this function.
    * 
    * @return the domain of this function.
    */
   public List<ExprSort> domain() {
      return ((FunSort) this.getArg(1)).domain();
   }

   /**
    * Returns the range of this function.
    * <p>
    * This is the sort of the value returned by this function.
    * 
    * @return the range of this function.
    */
   public ExprSort range() {
      return ((FunSort) this.getArg(1)).range();
   }

   /**
    * Returns the signature of this function.
    * <p>
    * This is the list of the sorts of the arguments of the function and the sort it
    * returns.
    * 
    * @return the signature of this function.
    */
   public List<ExprSort> signature() {
      List<ExprSort> sorts = new ArrayList<>();
      sorts.addAll(this.domain());
      sorts.add(this.range());
      return sorts;
   }

   /**
    * Returns a SMT-LIB v2 compliant declaration of this function.
    * 
    * @return a SMT-LIB v2 compliant declaration of this function.
    */
   public String toSmtDecl() {
      return String.format(
            "(declare-fun %s (%s) %s)",
            this.toSmt(),
            Joiner.on(" ").join(this.domain().stream().map(e -> e.toSmt()).iterator()),
            this.range().toSmt());
   }

   /**
    * Returns a new node representing a new function with the given arity in which the
    * sort of all parameters and the range equals the given sort.
    * 
    * @param index
    *           the index of the function.
    * @param arity
    *           the arity of the function.
    * @param sort
    *           the sort of all parameters and of the range of the function.
    * @return a new node representing a new function with the given arity in which the
    *         sort of all parameters and the range equals the given sort.
    */
   public static Fun createHomogenous(int index, int arity, ExprSort sort) {
      List<ExprSort> domain = new ArrayList<>();
      for (int i = 0; i < arity; ++i) {
         domain.add(sort);
      }
      return create(index, FunSort.create(domain, sort));
   }

   /**
    * Returns a new node representing an uninterpreted nullary function with the given
    * index and range.
    * 
    * @param index
    *           the index of the function.
    * @param range
    *           the range of the nullary function.
    * @return a new node representing an uninterpreted nullary function with the given
    *         index and range.
    */
   public static Fun create(int index, ExprSort range) {
      return create(index, FunSort.create(new ArrayList<>(), range));
   }

   /**
    * Returns a new node representing an uninterpreted function with the given index and
    * sort.
    * 
    * @param index
    *           the index of the function.
    * @param sort
    *           the sort of the function.
    * @return a new node representing an uninterpreted function with the given index and
    *         sort.
    */
   public static Fun create(int index, FunSort sort) {
      Pair<Integer, FunSort> params = new Pair<>(index, sort);
      Fun value = instances.get(params);
      if (value != null) {
         return value;
      }
      Fun f = new Fun(index, sort);
      instances.put(params, f);
      return f;
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create((int) args.get(0), (FunSort) args.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      FunSort fsort = (FunSort) this.getArg(1);
      if (fsort.arity() == 0) {
         return fsort.range();
      }
      return fsort;
   }

   @Override
   public String toString() {
      if (this.domain().size() > 0) {
         return String.format(
               "(v%d : (%s) -> %s)",
               this.getIndex(),
               Joiner.on(",").join(this.domain()),
               this.range());
      } else {
         return String.format("(v%d : %s)", this.getIndex(), this.range());
      }
   }

   @Override
   public IExpr eval(Model model) {
      IExpr value = model.getValueFor(this);
      return (value != null) ? value : this;
   }

   @Override
   public IExpr renameVars(VarMap map) {
      Integer index = map.getIndexFor(this);
      return (index != null) ? Fun.create(index, (FunSort) this.getArg(1)) : this;
   }

   @Override
   public String toSmt() {
      return this.sort().prefix() + this.getIndex();
   }

   @Override
   public SatDelta satDelta(Model model) {
      if (!(this.sort() instanceof BoolSort)) {
         throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
      }
      BoolVal value = (BoolVal) model.getValueFor(this);
      if (value == null) {
         throw new UndefinedSatDeltaError(this, model);
      }
      BigRational dir = BigRational.ZERO;
      BigRational inv = BigRational.ONE;
      if (value.isFalse()) {
         dir = BigRational.ONE;
         inv = BigRational.ZERO;
      }
      return new SatDelta(dir, inv);
   }
}
