package julia.expr.ast.mixed;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing an "if then else".
 */
public final class If extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(48);

   /**
    * Creates a new node representing an "if then else".
    * 
    * @param c
    *           the condition.
    * @param t
    *           the "then" branch.
    * @param e
    *           the "else" branch.
    */
   private If(IExpr c, IExpr t, IExpr e) {
      super(Arrays.asList(c, t, e));
   }

   /**
    * Returns the child of this node representing the condition of the "if then else".
    * 
    * @return the child of this node representing the condition of the "if then else".
    */
   public IExpr getCondition() {
      return this.getExpr(0);
   }

   /**
    * Returns the child of this node representing the then branch of the "if then else".
    * 
    * @return the child of this node representing the then branch of the "if then else".
    */
   public IExpr getThen() {
      return this.getExpr(1);
   }

   /**
    * Returns the child of this node representing the else branch of the "if then else".
    * 
    * @return the child of this node representing the else branch of the "if then else".
    */
   public IExpr getElse() {
      return this.getExpr(2);
   }

   /**
    * Returns a new node representing an "if then else".
    * 
    * @param c
    *           the condition.
    * @param t
    *           the "then" branch.
    * @param e
    *           the "else" branch.
    * @return a new node representing an "if then else".
    */
   public static IExpr create(IExpr c, IExpr t, IExpr e) {
      if (Expr.CHECK_PARAMS) {
         checkParams(c, t, e);
      }

      BoolVal cval = Exprs.cast(c, BoolVal.class);
      if (cval != null) {
         return cval.isTrue() ? t : e;
      } else if (t.equals(e)) {
         return t;
      } else {
         return new If(c, t, e);
      }
   }

   public static void checkParams(IExpr c, IExpr t, IExpr e) {
      Preconditions.checkArgument(
            c.sort() instanceof BoolSort,
            "If first child must have boolean sort, got %s",
            c.sort());

      Preconditions.checkArgument(
            t.sort().equals(e.sort()),
            "If second and third child must have the same sort, got %s and %s",
            t.sort(),
            e.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1), exprs.get(2));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getThen().sort();
   }

   @Override
   public String toSmt() {
      String c = this.getCondition().toSmt();
      String t = this.getThen().toSmt();
      String e = this.getElse().toSmt();
      return String.format("(ite %s %s %s)", c, t, e);
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr cexpr = this.getCondition().eval(model);
      BoolVal c = Exprs.cast(cexpr, BoolVal.class);
      if (c != null) {
         // The sat-delta value of either branch might be undefined.
         // This is the case for if returning non-boolean expressions.
         if (c.isTrue()) {
            return this.getThen().satDelta(model);
         } else {
            return this.getElse().satDelta(model);
         }
      }
      throw new UndefinedSatDeltaError(this, model);
   }
}
