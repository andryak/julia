package julia.expr.ast.bool;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing two expressions being different.
 */
public final class Distinct extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(5);

   /**
    * Creates a new node representing the two given expressions being different.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private Distinct(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns the left child of this node.
    * 
    * @return the left child of this node.
    */
   public IExpr getLhs() {
      return this.getExpr(0);
   }

   /**
    * Returns the right child of this node.
    * 
    * @return the right child of this node.
    */
   public IExpr getRhs() {
      return this.getExpr(1);
   }

   /**
    * Returns a new node representing the two given expressions being different.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      if (lhs instanceof IConst && rhs instanceof IConst) {
         Object lval = ((IConst) lhs).getValue();
         Object rval = ((IConst) rhs).getValue();
         return BoolVal.create(!(lval.equals(rval)));
      }

      BoolVal lval = Exprs.cast(lhs, BoolVal.class);
      if (lval != null) {
         return lval.isFalse() ? rhs : Not.create(rhs);
      }

      BoolVal rval = Exprs.cast(rhs, BoolVal.class);
      if (rval != null) {
         return rval.isFalse() ? lhs : Not.create(lhs);
      }

      if (lhs.equals(rhs)) {
         return BoolVal.FALSE;
      }

      return new Distinct(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort().equals(rhs.sort()),
            "Distinct children must have the same sort, got %s and %s.",
            lhs.sort(),
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      String lhs = this.getExpr(0).toSmt();
      String rhs = this.getExpr(1).toSmt();
      return String.format("(distinct %s %s)", lhs, rhs);
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr lhs = this.getLhs().eval(model);
      IExpr rhs = this.getRhs().eval(model);

      if (!(lhs instanceof IConst) || !(rhs instanceof IConst)) {
         throw new UndefinedSatDeltaError(this, model);
      }

      if (lhs.equals(rhs)) {
         return new SatDelta(1, 0);
      } else {
         IConst lval = (IConst) lhs;
         IConst rval = (IConst) rhs;
         return new SatDelta(BigRational.ZERO, lval.delta(rval));
      }
   }
}
