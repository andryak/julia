package julia.expr.ast.bool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing a boolean constant.
 * <p>
 * The only two boolean constants are {@code BoolVal.TRUE} and {@code BoolVal.FALSE}.
 */
public final class BoolVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(4);

   public final static BoolVal TRUE = new BoolVal(true);
   public final static BoolVal FALSE = new BoolVal(false);

   /**
    * Creates a new boolean constant with the given value.
    * 
    * @param value
    *           the value of the boolean constant.
    */
   private BoolVal(boolean value) {
      super(new ArrayList<>(), Arrays.asList(value));
   }

   /**
    * Returns true if this value is the boolean constant {@code true}.
    * 
    * @return true if this value is the boolean constant {@code true}.
    */
   public boolean isTrue() {
      return this.getValue();
   }

   /**
    * Returns true if this value is the boolean constant {@code false}.
    * 
    * @return true if this value is the boolean constant {@code false}.
    */
   public boolean isFalse() {
      return (!this.getValue());
   }

   /**
    * Creates a new boolean constant with the given value.
    * 
    * @param value
    *           the value of the boolean constant.
    */
   public static BoolVal create(boolean value) {
      return value ? TRUE : FALSE;
   }

   @Override
   protected BoolVal make(List<IExpr> exprs, List<Object> args) {
      return create((boolean) args.get(0));
   }

   @Override
   public Boolean getValue() {
      return (Boolean) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toString() {
      return this.getValue() ? "true" : "false";
   }

   @Override
   public boolean equals(Object other) {
      return (this == other);
   }

   // Added for efficiency.
   @Override
   public BoolVal eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public BoolVal renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      return this.getValue() ? "true" : "false";
   }

   @Override
   public SatDelta satDelta(Model model) {
      BigRational dir = BigRational.ZERO;
      BigRational inv = BigRational.POS_INFINITY;
      if (this.isFalse()) {
         BigRational tmp = dir;
         dir = inv;
         inv = tmp;
      }
      return new SatDelta(dir, inv);
   }
}
