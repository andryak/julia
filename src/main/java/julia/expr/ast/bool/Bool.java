package julia.expr.ast.bool;

import julia.expr.ast.uf.Fun;
import julia.expr.sort.BoolSort;
import julia.utils.Exprs;

/**
 * Fake class to create boolean variables using the {@code Bool.create(...)} syntax.
 */
public final class Bool {
   /**
    * Do not instantiate me!
    */
   private Bool() {}

   /**
    * Returns a new variable of boolean sort.
    * 
    * @param index
    *           the index of the variable.
    * @return a new variable of boolean sort.
    */
   public static Fun create(int index) {
      return Exprs.mkConst(index, BoolSort.create());
   }
}