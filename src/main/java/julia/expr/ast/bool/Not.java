package julia.expr.ast.bool;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the negation of an expression.
 */
public final class Not extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(7);

   /**
    * Creates a new node representing the negation of the given expression.
    * 
    * @param expr
    *           the expression to negate.
    */
   private Not(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns the only child of this node representing the negated expression.
    * 
    * @return the only child of this node representing the negated expression.
    */
   public IExpr getNegatedExpr() {
      return this.getExpr(0);
   }

   /**
    * Returns a new node representing the negation of the given expression.
    * 
    * @param expr
    *           the expression to negate.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      BoolVal v = Exprs.cast(expr, BoolVal.class);
      if (v != null) {
         return v.isTrue() ? BoolVal.FALSE : BoolVal.TRUE;
      } else if (expr instanceof Not) {
         return expr.getExpr(0);
      } else {
         return new Not(expr);
      }
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort().equals(BoolSort.create()),
            "Not child must have boolean sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(not %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      SatDelta delta = this.getNegatedExpr().satDelta(model);
      return new SatDelta(delta.inv(), delta.dir());
   }
}
