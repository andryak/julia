package julia.expr.ast.bool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the conjunction of some expressions.
 */
public final class And extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(3);

   /**
    * Creates a new node representing the conjunction of the given expressions.
    * 
    * @param exprs
    *           a list of boolean expressions.
    */
   private And(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns a new node representing the conjunction of the given expressions.
    * <p>
    * If no expression is provided, BoolVal.TRUE is returned. If only one expression is
    * provided, it is returned as is.
    * <p>
    * This method ignores duplicated expressions, removes all occurrences of the boolean
    * constant true and immediately returns false if the boolean constant false is
    * encountered in the given list of boolean expressions.
    * 
    * @param exprs
    *           an array of boolean expressions.
    * @return the conjunction of the given expressions.
    * 
    * @see BoolVal
    */
   public static IExpr create(IExpr... exprs) {
      return create(Arrays.asList(exprs));
   }

   /**
    * Returns a new node representing the conjunction of the given expressions.
    * <p>
    * If no expression is provided, BoolVal.TRUE is returned. If only one expression is
    * provided, it is returned as is.
    * <p>
    * This method ignores duplicated expressions, removes all occurrences of the boolean
    * constant true and immediately returns false if the boolean constant false is
    * encountered in the given list of boolean expressions.
    * 
    * @param exprs
    *           a list of boolean expressions.
    * @return the conjunction of the given expressions.
    * 
    * @see BoolVal
    */
   public static IExpr create(List<IExpr> exprs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(exprs);
      }

      Set<IExpr> newArgsSet = new HashSet<>();
      Queue<IExpr> queue = new LinkedList<>(exprs);
      while (!queue.isEmpty()) {
         IExpr expr = queue.poll();
         if (expr == BoolVal.FALSE) {
            return BoolVal.FALSE;
         } else if (expr == BoolVal.TRUE) {
            continue;
         } else if (expr instanceof And) {
            queue.addAll(expr.getExprs());
         } else {
            newArgsSet.add(expr);
         }
      }

      if (newArgsSet.size() <= 0) {
         return BoolVal.TRUE;
      } else if (newArgsSet.size() == 1) {
         return newArgsSet.iterator().next();
      }

      List<IExpr> newArgsList = new ArrayList<>(newArgsSet);
      newArgsList.sort((e1, e2) -> Integer.compare(e1.hashCode(), e2.hashCode()));

      return new And(newArgsList);
   }

   public static void checkParams(List<IExpr> exprs) {
      Preconditions.checkArgument(
            exprs.stream().allMatch(e -> e.sort() == BoolSort.create()),
            "And children must have boolean sort, got [%s].",
            Joiner.on(",").join(exprs.stream().map(e -> e.sort()).iterator()));
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(and %s)",
            Joiner.on(" ").join(this.getExprs().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      BigRational dir = BigRational.ZERO;
      BigRational inv = BigRational.POS_INFINITY;
      for (final IExpr expr : this.getExprs()) {
         SatDelta delta = expr.satDelta(model);
         dir = dir.add(delta.dir());
         inv = inv.min(delta.inv());
      }
      return new SatDelta(dir, inv);
   }
}
