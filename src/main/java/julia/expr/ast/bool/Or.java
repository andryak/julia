package julia.expr.ast.bool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.utils.BigRational;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Node representing the disjunction of some expressions.
 */
public final class Or extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(8);

   /**
    * Creates a new node representing the disjunction of the given expressions.
    * 
    * @param exprs
    *           a list of boolean expressions.
    */
   private Or(List<IExpr> exprs) {
      super(exprs);
   }

   /**
    * Returns a new node representing the disjunction of the given expressions.
    * <p>
    * If no expression is provided, BoolVal.FALSE is returned. If only one expression is
    * provided, it is returned as is.
    * <p>
    * This method ignores duplicated expressions, removes all occurrences of the boolean
    * constant false and immediately returns true if the boolean constant true is
    * encountered in the given list of boolean expressions.
    * 
    * @param exprs
    *           a list of boolean expressions.
    * @return the disjunction of the given expressions.
    * 
    * @see BoolVal
    */
   public static IExpr create(IExpr... exprs) {
      return create(Arrays.asList(exprs));
   }

   /**
    * Returns a new node representing the disjunction of the given expressions.
    * <p>
    * If no expression is provided, BoolVal.FALSE is returned. If only one expression is
    * provided, it is returned as is.
    * <p>
    * This method ignores duplicated expressions, removes all occurrences of the boolean
    * constant false and immediately returns true if the boolean constant true is
    * encountered in the given list of boolean expressions.
    * 
    * @param exprs
    *           a list of boolean expressions.
    * @return the disjunction of the given expressions.
    * 
    * @see BoolVal
    */
   public static IExpr create(List<IExpr> exprs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(exprs);
      }

      Set<IExpr> newArgsSet = new HashSet<>();
      Queue<IExpr> queue = new LinkedList<>(exprs);
      while (!queue.isEmpty()) {
         IExpr expr = queue.poll();
         if (expr == BoolVal.TRUE) {
            return BoolVal.TRUE;
         } else if (expr == BoolVal.FALSE) {
            continue;
         } else if (expr instanceof Or) {
            queue.addAll(expr.getExprs());
         } else {
            newArgsSet.add(expr);
         }
      }

      if (newArgsSet.size() <= 0) {
         return BoolVal.FALSE;
      } else if (newArgsSet.size() == 1) {
         return newArgsSet.iterator().next();
      }

      List<IExpr> newArgsList = new ArrayList<>(newArgsSet);
      newArgsList.sort((e1, e2) -> Integer.compare(e1.hashCode(), e2.hashCode()));

      return new Or(newArgsList);
   }

   public static void checkParams(List<IExpr> exprs) {
      Preconditions.checkArgument(
            exprs.stream().allMatch(e -> e.sort() == BoolSort.create()),
            "Or children must have boolean sort, got [%s].",
            Joiner.on(",").join(exprs.stream().map(e -> e.sort()).iterator()));
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      return String.format(
            "(or %s)",
            Joiner.on(" ").join(this.getExprs().stream().map(e -> e.toSmt()).iterator()));
   }

   @Override
   public SatDelta satDelta(Model model) {
      BigRational dir = BigRational.POS_INFINITY;
      BigRational inv = BigRational.ZERO;
      for (final IExpr expr : this.getExprs()) {
         SatDelta esd = expr.satDelta(model);
         dir = dir.min(esd.dir());
         inv = inv.add(esd.inv());
      }
      return new SatDelta(dir, inv);
   }
}
