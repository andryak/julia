package julia.expr.ast.bool;

import julia.expr.ast.IExpr;

/**
 * Fake class to produce the implication of the given expressions.
 */
public final class Imply {
   /**
    * Do not instantiate me!
    */
   private Imply() {}

   /**
    * Returns the implication of the given expressions.
    * 
    * @param lhs
    *           the expression implying the other.
    * @param rhs
    *           the expression implied by the other.
    * @return the implication of the given expressions.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      return Or.create(Not.create(lhs), rhs);
   }
}
