package julia.expr.ast.bool;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the equality of two expressions.
 */
public final class Equal extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(6);

   /**
    * Creates a new node representing the equality of the two given expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private Equal(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns the left child of this node.
    * 
    * @return the left child of this node.
    */
   public IExpr getLhs() {
      return this.getExpr(0);
   }

   /**
    * Returns the right child of this node.
    * 
    * @return the right child of this node.
    */
   public IExpr getRhs() {
      return this.getExpr(1);
   }

   /**
    * Creates a new node representing the equality of the two given expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      if (lhs instanceof IConst && rhs instanceof IConst) {
         Object lval = ((IConst) lhs).getValue();
         Object rval = ((IConst) rhs).getValue();
         return BoolVal.create(lval.equals(rval));
      }

      BoolVal lval = Exprs.cast(lhs, BoolVal.class);
      if (lval != null) {
         return lval.isTrue() ? rhs : Not.create(rhs);
      }

      BoolVal rval = Exprs.cast(rhs, BoolVal.class);
      if (rval != null) {
         return rval.isTrue() ? lhs : Not.create(lhs);
      }

      if (lhs.equals(rhs)) {
         return BoolVal.TRUE;
      }

      return new Equal(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort().equals(rhs.sort()),
            "Equal children must have the same sort, got %s and %s.",
            lhs.sort(),
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(= %s %s)", this.getExpr(0).toSmt(), this.getExpr(1).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr lhs = this.getLhs().eval(model);
      IExpr rhs = this.getRhs().eval(model);

      if (!(lhs instanceof IConst) || !(rhs instanceof IConst)) {
         throw new UndefinedSatDeltaError(this, model);
      }

      if (lhs.equals(rhs)) {
         return new SatDelta(0, 1);
      } else {
         IConst lval = (IConst) lhs;
         IConst rval = (IConst) rhs;
         return new SatDelta(lval.delta(rval), BigRational.ZERO);
      }
   }
}
