package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the replacement of the first occurrence of a string with another in a
 * string expression.
 */
public final class StrReplace extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(74);

   /**
    * Creates a new node representing the replacement of the first occurrence of a string
    * with another in a string expression.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private StrReplace(IExpr expr, IExpr target, IExpr replacement) {
      super(Arrays.asList(expr, target, replacement));
   }

   /**
    * Returns a new node representing the replacement of the first occurrence of a string
    * with another in a string expression.
    * 
    * @param expr
    *           the expression in which the replacement will take place.
    * @param target
    *           the target expression to replace.
    * @param replacement
    *           the replacement string.
    * @return a new node representing the replacement of the first occurrence of a string
    *         with another in a string expression.
    */
   public static IExpr create(IExpr expr, IExpr target, IExpr replacement) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, target, replacement);
      }

      StrVal eval = Exprs.cast(expr, StrVal.class);
      StrVal tval = Exprs.cast(target, StrVal.class);
      StrVal rval = Exprs.cast(replacement, StrVal.class);
      if (eval != null && tval != null && rval != null) {
         return StrVal.create(eval.getValue().replaceFirst(tval.getValue(), rval.getValue()));
      } else if (expr.equals(target)) {
         return replacement;
      } else if (target.equals(replacement)) {
         return expr;
      }
      return new StrReplace(expr, target, replacement);
   }

   public static void checkParams(IExpr expr, IExpr target, IExpr replacement) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "StrReplace first child must have string sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            target.sort() instanceof StrSort,
            "StrReplace second child must have string sort, got %s.",
            target.sort());

      Preconditions.checkArgument(
            replacement.sort() instanceof StrSort,
            "StrReplace third child must have string sort, got %s.",
            replacement.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1), exprs.get(2));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public StrSort sort() {
      return StrSort.create();
   }

   @Override
   public String toSmt() {
      String expr = this.getExpr(0).toSmt();
      String target = this.getExpr(1).toSmt();
      String replacement = this.getExpr(2).toSmt();
      return String.format("(str.replace %s %s %s)", expr, target, replacement);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
