package julia.expr.ast.str;

import julia.expr.ast.uf.Fun;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;

/**
 * Fake class to create string variables using the {@code Str.create(...)} syntax.
 */
public final class Str {
   /**
    * Do not instantiate me!
    */
   private Str() {}

   /**
    * Returns a new variable of string sort.
    * 
    * @param index
    *           the index of the variable.
    * @return a new variable of string sort.
    */
   public static Fun create(int index) {
      return Exprs.mkConst(index, StrSort.create());
   }
}
