package julia.expr.ast.str;

import static julia.utils.StringUtils.quote;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.StrSort;
import julia.expr.utils.BigRational;
import julia.utils.LRUCache;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;
import org.apache.commons.lang3.StringUtils;

/**
 * Node representing a string constant.
 */
public final class StrVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(77);

   private final static LRUCache<String, StrVal> instances;
   static {
      instances = new LRUCache<>(1024);
   }

   public final static StrVal EMPTY = new StrVal("");

   /**
    * Creates a new string constant with the given value.
    * 
    * @param value
    *           the value of the new string constant.
    */
   private StrVal(String value) {
      super(new ArrayList<>(), Arrays.asList(value));
   }

   /**
    * Returns true if this string is empty.
    * 
    * @return true if this string is empty.
    */
   public boolean isEmpty() {
      return this.getValue().isEmpty();
   }

   /**
    * Returns a new string constant with the given value.
    * 
    * @param value
    *           the value of the new string constant.
    * @return a new string constant with the given value.
    */
   public static IExpr create(char value) {
      return create(String.valueOf(value));
   }

   /**
    * Returns a new string constant with the given value.
    * 
    * @param value
    *           the value of the new string constant.
    * @return a new string constant with the given value.
    */
   public static StrVal create(String value) {
      if (value.isEmpty()) {
         // The empty string is always cached.
         return EMPTY;
      }

      StrVal instance = instances.get(value);
      if (instance != null) {
         return instance;
      }
      StrVal v = new StrVal(value);
      instances.put(value, v);
      return v;
   }

   @Override
   protected StrVal make(List<IExpr> exprs, List<Object> args) {
      return create((String) args.get(0));
   }

   @Override
   public String getValue() {
      return (String) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public StrSort sort() {
      return StrSort.create();
   }

   @Override
   public String toString() {
      return quote(this.getValue());
   }

   // Added for efficiency.
   @Override
   public StrVal eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public StrVal renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      return quote(this.getValue().replaceAll("\"", "\"\""));
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }

   @Override
   public BigRational delta(IConst other) {
      String value = (String) other.getValue();
      int delta = StringUtils.getLevenshteinDistance(this.getValue(), value);
      return BigRational.create(delta);
   }
}
