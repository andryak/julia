package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.BoolSort;
import julia.expr.sort.StrSort;
import julia.expr.utils.BigRational;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import julia.utils.StringUtils;
import com.google.common.base.Preconditions;

/**
 * Node representing a string expression being the prefix of another string expression.
 */
public final class StrPrefixOf extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(73);

   /**
    * Creates a new node representing a string expression being the prefix another string
    * expression.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private StrPrefixOf(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing a string expression being the prefix another string
    * expression.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing a string expression being the prefix another string
    *         expression.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      StrVal lval = Exprs.cast(lhs, StrVal.class);
      StrVal rval = Exprs.cast(rhs, StrVal.class);
      if (lval != null && rval != null) {
         // StrVal(a) prefixof StrVal(b) = BoolVal(a prefixof b)
         return BoolVal.create(rval.getValue().startsWith(lval.getValue()));
      } else if (lval != null && lval.isEmpty()) {
         // "" prefixof e = true
         return BoolVal.TRUE;
      } else if (lhs.equals(rhs)) {
         // e prefixof e = true
         return BoolVal.TRUE;
      }
      return new StrPrefixOf(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof StrSort,
            "StrPrefixOf first child must have string sort, got %s.",
            lhs.sort());

      Preconditions.checkArgument(
            rhs.sort() instanceof StrSort,
            "StrPrefixOf second child must have string sort, got %s.",
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public BoolSort sort() {
      return BoolSort.create();
   }

   @Override
   public String toSmt() {
      String lhs = this.getExpr(0).toSmt();
      String rhs = this.getExpr(1).toSmt();
      return String.format("(str.prefixof %s %s)", lhs, rhs);
   }

   @Override
   public SatDelta satDelta(Model model) {
      IExpr lexpr = this.getExpr(0).eval(model);
      IExpr rexpr = this.getExpr(1).eval(model);

      StrVal lval = Exprs.cast(lexpr, StrVal.class);
      StrVal rval = Exprs.cast(rexpr, StrVal.class);
      if (lval != null && rval != null) {
         String lstr = lval.getValue();
         String rstr = rval.getValue();

         BigRational dir;
         BigRational inv;

         if (rstr.startsWith(lstr)) {
            dir = BigRational.ZERO;
            inv = BigRational.ONE;
         } else {
            int delta = StringUtils.hammingHead(lstr, rstr);
            delta += Math.max(0, lstr.length() - rstr.length());

            dir = BigRational.create(delta);
            inv = BigRational.ZERO;
         }
         return new SatDelta(dir, inv);
      }
      throw new UndefinedSatDeltaError(this, model);
   }

}
