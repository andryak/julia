package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the extraction of a substring from a string expression.
 */
public final class SubStr extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(78);

   /**
    * Creates a new node representing the extraction of a substring from a string
    * expression.
    * 
    * @param expr
    *           the base expression.
    * @param offset
    *           the index of the first character to extract.
    * @param length
    *           the length of the extracted substring.
    */
   private SubStr(IExpr expr, IExpr offset, IExpr length) {
      super(Arrays.asList(expr, offset, length));
   }

   /**
    * Returns a new node representing the extraction of a substring from a string
    * expression.
    * 
    * @param expr
    *           the base expression.
    * @param offset
    *           the index of the first character to extract.
    * @param length
    *           the length of the extracted substring.
    * @return a new node representing the extraction of a substring from a string
    *         expression.
    */
   public static IExpr create(IExpr expr, IExpr offset, IExpr length) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, offset, length);
      }

      StrVal exprVal = Exprs.cast(expr, StrVal.class);
      IntVal offsetVal = Exprs.cast(offset, IntVal.class);
      IntVal lengthVal = Exprs.cast(length, IntVal.class);
      if (exprVal != null && offsetVal != null && lengthVal != null) {
         int o = offsetVal.getValue().intValue();
         int l = lengthVal.getValue().intValue();
         return StrVal.create(exprVal.getValue().substring(o, o + l));
      } else if (lengthVal != null && lengthVal == IntVal.ZERO) {
         return StrVal.create("");
      }
      return new SubStr(expr, offset, length);
   }

   /**
    * Returns the offset of the substring.
    * 
    * @return the offset of the substring.
    */
   public IExpr getOffset() {
      return this.getExpr(1);
   }

   /**
    * Returns the length of the substring.
    * 
    * @return the length of the substring.
    */
   public IExpr getLength() {
      return this.getExpr(2);
   }

   public static void checkParams(IExpr expr, IExpr offset, IExpr length) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "SubStr first child must have string sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            offset.sort() instanceof IntSort,
            "SubStr second child must have int sort, got %s.",
            offset.sort());

      Preconditions.checkArgument(
            length.sort() instanceof IntSort,
            "SubStr third child must have int sort, got %s.",
            length.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1), exprs.get(2));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public StrSort sort() {
      return StrSort.create();
   }

   @Override
   public String toSmt() {
      String expr = this.getExpr(0).toSmt();
      String offset = this.getOffset().toSmt();
      String length = this.getLength().toSmt();
      return String.format("(str.substr %s %s %s)", expr, offset, length);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
