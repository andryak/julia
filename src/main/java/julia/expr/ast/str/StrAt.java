package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the extraction of a character from a string expression.
 */
public final class StrAt extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(67);

   /**
    * Creates a new node representing the extraction of a character from a string
    * expression.
    * 
    * @param expr
    *           the base expression.
    * @param offset
    *           the index of the character to extract.
    */
   private StrAt(IExpr expr, int offset) {
      super(Arrays.asList(expr), Arrays.asList(offset));
   }

   /**
    * Returns a new node representing the extraction of a character from a string
    * expression.
    * 
    * @param expr
    *           the base expression.
    * @param offset
    *           the index of the character to extract.
    * @return a new node representing the extraction of a character from a string
    *         expression.
    */
   public static IExpr create(IExpr expr, int offset) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, offset);
      }

      StrVal val = Exprs.cast(expr, StrVal.class);
      if (val != null) {
         return StrVal.create(val.getValue().charAt(offset));
      }
      return new StrAt(expr, offset);
   }

   /**
    * Returns the offset of the character to extract.
    * 
    * @return the offset of the character to extract.
    */
   public int getOffset() {
      return (int) this.getArg(0);
   }

   public static void checkParams(IExpr expr, int offset) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "StrAt child must have string sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            offset >= 0,
            "StrAt argument must be non negative, got %s.",
            offset);
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), (int) args.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public StrSort sort() {
      return StrSort.create();
   }

   @Override
   public String toSmt() {
      String expr = this.getExpr(0).toSmt();
      int offset = this.getOffset();
      return String.format("(str.at %s %d)", expr, offset);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
