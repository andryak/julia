package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the length of a string expression.
 */
public final class StrLen extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(72);

   /**
    * Creates a new node representing the length of a string expression.
    * 
    * @param expr
    *           a string expression.
    */
   private StrLen(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the length of a string expression.
    * 
    * @param expr
    *           a string expression.
    * @return a new node representing the length of a string expression.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      StrVal v = Exprs.cast(expr, StrVal.class);
      if (v != null) {
         return IntVal.create(v.getValue().length());
      }
      return new StrLen(expr);
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "StrLen child must have string sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public IntSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(str.len %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
