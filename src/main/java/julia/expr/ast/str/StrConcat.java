package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the concatenation of two string expressions.
 */
public final class StrConcat extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(68);

   /**
    * Creates a new node representing the concatenation of two string expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    */
   private StrConcat(IExpr lhs, IExpr rhs) {
      super(Arrays.asList(lhs, rhs));
   }

   /**
    * Returns a new node representing the concatenation of two string expressions.
    * 
    * @param lhs
    *           the left expression.
    * @param rhs
    *           the right expression.
    * @return a new node representing the concatenation of two string expressions.
    */
   public static IExpr create(IExpr lhs, IExpr rhs) {
      if (Expr.CHECK_PARAMS) {
         checkParams(lhs, rhs);
      }

      StrVal lval = Exprs.cast(lhs, StrVal.class);
      StrVal rval = Exprs.cast(rhs, StrVal.class);
      if (lval != null && rval != null) {
         // StrVal(a) concat StrVal(b) = StrVal(a concat b)
         return StrVal.create(lval.getValue() + rval.getValue());
      } else if (lval != null && lval.isEmpty()) {
         // "" concat e = e
         return rhs;
      } else if (rval != null && rval.isEmpty()) {
         // e concat "" = e
         return lhs;
      }
      return new StrConcat(lhs, rhs);
   }

   public static void checkParams(IExpr lhs, IExpr rhs) {
      Preconditions.checkArgument(
            lhs.sort() instanceof StrSort,
            "StrConcat first child must have string sort, got %s.",
            lhs.sort());

      Preconditions.checkArgument(
            rhs.sort() instanceof StrSort,
            "StrConcat second child must have string sort, got %s.",
            rhs.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public StrSort sort() {
      return StrSort.create();
   }

   @Override
   public String toSmt() {
      String lhs = this.getExpr(0).toSmt();
      String rhs = this.getExpr(1).toSmt();
      return String.format("(str.++ %s %s)", lhs, rhs);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
