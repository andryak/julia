package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the index of the last occurrence of a pattern in a string.
 * <p>
 * The index is -1 if the pattern does not occur in the string.
 */
public final class StrLastIndexOf extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(71);

   /**
    * Creates a new node representing the index of the last occurrence of a pattern in a
    * string.
    * 
    * @param expr
    *           the base expression.
    * @param pattern
    *           the pattern to match to expr.
    */
   private StrLastIndexOf(IExpr expr, IExpr pattern) {
      super(Arrays.asList(expr, pattern));
   }

   /**
    * Returns a new node representing the index of the last occurrence of a pattern in a
    * string.
    * 
    * @param expr
    *           the base expression.
    * @param pattern
    *           the pattern to match to expr.
    * @return a new node representing the index of the last occurrence of a pattern in a
    *         string.
    */
   public static IExpr create(IExpr expr, IExpr pattern) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, pattern);
      }

      StrVal lval = Exprs.cast(expr, StrVal.class);
      StrVal rval = Exprs.cast(pattern, StrVal.class);
      if (lval != null && rval != null) {
         return IntVal.create(lval.getValue().lastIndexOf(rval.getValue()));
      } else if (expr.equals(pattern)) {
         return IntVal.ZERO;
      }
      return new StrLastIndexOf(expr, pattern);
   }

   public static void checkParams(IExpr expr, IExpr pattern) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "StrIndexOf first child must have string sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            pattern.sort() instanceof StrSort,
            "StrIndexOf second child must have string sort, got %s.",
            pattern.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public IntSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      throw new IllegalStateException("StrLastIndexOf is not part of the SMT-LIB v2 standard.");
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
