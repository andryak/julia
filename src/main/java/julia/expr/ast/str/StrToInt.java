package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the conversion of a string expression to an integer.
 */
public final class StrToInt extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(76);

   /**
    * Creates a new node representing the conversion of a string expression to an integer.
    * 
    * @param expr
    *           a string expression.
    */
   private StrToInt(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the conversion of a string expression to an integer.
    * 
    * @param expr
    *           a string expression.
    * @return a new node representing the conversion of a string expression to an integer.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      StrVal v = Exprs.cast(expr, StrVal.class);
      if (v != null) {
         return IntVal.create(v.getValue().toString());
      }
      return new StrToInt(expr);
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "StrToInt child must have string sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public IntSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(str.to.int %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
