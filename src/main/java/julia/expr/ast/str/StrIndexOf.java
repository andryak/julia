package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the index of a pattern in a string starting from an offset.
 * <p>
 * The index is -1 if the pattern does not occur in the string.
 */
public final class StrIndexOf extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(70);

   /**
    * Creates a new node representing the index of a pattern in a string starting from an
    * offset.
    * 
    * @param expr
    *           the base expression.
    * @param pattern
    *           the pattern to match to expr.
    * @param offset
    *           the portion of expr to ignore for the match.
    */
   private StrIndexOf(IExpr expr, IExpr pattern, int offset) {
      super(Arrays.asList(expr, pattern), Arrays.asList(offset));
   }

   /**
    * Returns the portion of the string to ignore for the match.
    * 
    * @return the portion of the string to ignore for the match.
    */
   public int getOffset() {
      return (int) this.getArg(0);
   }

   /**
    * Returns a new node representing the index of a pattern in a string.
    * 
    * @param expr
    *           the base expression.
    * @param pattern
    *           the pattern to match to expr.
    * @return a new node representing the index of a pattern in a string.
    */
   public static IExpr create(IExpr expr, IExpr pattern) {
      return create(expr, pattern, 0);
   }

   /**
    * Returns a new node representing the index of a pattern in a string starting from an
    * offset.
    * 
    * @param expr
    *           the base expression.
    * @param pattern
    *           the pattern to match to expr.
    * @param offset
    *           the portion of expr to ignore for the match.
    * @return a new node representing the index of a pattern in a string starting from an
    *         offset.
    */
   public static IExpr create(IExpr expr, IExpr pattern, int offset) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr, pattern, offset);
      }

      StrVal lval = Exprs.cast(expr, StrVal.class);
      StrVal rval = Exprs.cast(pattern, StrVal.class);
      if (lval != null && rval != null) {
         return IntVal.create(lval.getValue().indexOf(rval.getValue(), offset));
      } else if (expr.equals(pattern)) {
         return IntVal.create((offset == 0) ? 0 : (-1));
      }
      return new StrIndexOf(expr, pattern, offset);
   }

   public static void checkParams(IExpr expr, IExpr pattern, int offset) {
      Preconditions.checkArgument(
            expr.sort() instanceof StrSort,
            "StrIndexOf first child must have string sort, got %s.",
            expr.sort());

      Preconditions.checkArgument(
            pattern.sort() instanceof StrSort,
            "StrIndexOf second child must have string sort, got %s.",
            pattern.sort());

      Preconditions.checkArgument(
            offset >= 0,
            "StrIndexOf argument must be non negative, got %s.",
            offset);
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1), (int) args.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public IntSort sort() {
      return IntSort.create();
   }

   @Override
   public String toSmt() {
      String expr = this.getExpr(0).toSmt();
      String pattern = this.getExpr(1).toSmt();
      int offset = this.getOffset();
      return String.format("(str.indexof %s %s %d)", expr, pattern, offset);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
