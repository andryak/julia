package julia.expr.ast.str;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.ints.IntVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the conversion of an integer expression to a string.
 */
public final class IntToStr extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(66);

   /**
    * Creates a new node representing the conversion of an integer expression to a string.
    * 
    * @param expr
    *           an integer expression.
    */
   private IntToStr(IExpr expr) {
      super(Arrays.asList(expr));
   }

   /**
    * Returns a new node representing the conversion of an integer expression to a string.
    * 
    * @param expr
    *           an integer expression.
    * @return a new node representing the conversion of an integer expression to a string.
    */
   public static IExpr create(IExpr expr) {
      if (Expr.CHECK_PARAMS) {
         checkParams(expr);
      }

      IntVal v = Exprs.cast(expr, IntVal.class);
      if (v != null) {
         return StrVal.create(v.getValue().toString());
      }
      return new IntToStr(expr);
   }

   public static void checkParams(IExpr expr) {
      Preconditions.checkArgument(
            expr.sort() instanceof IntSort,
            "IntToStr child must have int sort, got %s.",
            expr.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public StrSort sort() {
      return StrSort.create();
   }

   @Override
   public String toSmt() {
      return String.format("(int.to.str %s)", this.getExpr(0).toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
