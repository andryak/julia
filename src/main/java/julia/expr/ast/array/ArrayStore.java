package julia.expr.ast.array;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ArraySort;
import julia.expr.sort.ExprSort;
import julia.expr.utils.ConstArray;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing inserting an element into an array.
 */
public final class ArrayStore extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(1);

   /**
    * Creates a new node representing inserting an element into an array.
    * 
    * @param array
    *           the input array.
    * @param index
    *           index at which the specified element is to be inserted.
    * @param value
    *           the element to insert.
    */
   private ArrayStore(IExpr array, IExpr index, IExpr value) {
      super(Arrays.asList(array, index, value));
   }

   /**
    * Returns the child of this node representing the array.
    * 
    * @return the child of this node representing the array.
    */
   public IExpr getArray() {
      return this.getExpr(0);
   }

   /**
    * Returns the child of this node representing the index.
    * 
    * @return the child of this node representing the index.
    */
   public IExpr getIndex() {
      return this.getExpr(1);
   }

   /**
    * Returns the child of this node representing the value.
    * 
    * @return the child of this node representing the value.
    */
   public IExpr getValue() {
      return this.getExpr(2);
   }

   /**
    * Returns a new node representing inserting an element into an array.
    * 
    * @param array
    *           the input array.
    * @param index
    *           index at which the specified element is to be inserted.
    * @param value
    *           the element to insert.
    */
   public static IExpr create(IExpr array, IExpr index, IExpr value) {
      if (Expr.CHECK_PARAMS) {
         checkParams(array, index, value);
      }

      if (array instanceof ArrayVal && index instanceof IConst && value instanceof IConst) {
         // Implements copy-on-write semantic.
         ArrayVal carray = (ArrayVal) array;
         IConst cindex = (IConst) index;
         IConst cvalue = (IConst) value;
         ConstArray cp = carray.getValue().set(cindex, cvalue);
         return ArrayVal.create(cp, carray.sort());
      } else {
         return new ArrayStore(array, index, value);
      }
   }

   public static void checkParams(IExpr array, IExpr index, IExpr value) {
      Preconditions.checkArgument(
            array.sort() instanceof ArraySort,
            "ArrayStore first child must be an array, got %s.",
            array.sort());

      Preconditions
            .checkArgument(
                  index.sort().equals(((ArraySort) array.sort()).domain()),
                  "ArrayStore second child must have the sort of the domain of the first child, got %s.",
                  index.sort());

      Preconditions
            .checkArgument(
                  value.sort().equals(((ArraySort) array.sort()).range()),
                  "ArrayStore third child must have the sort of the range of the first child, got %s.",
                  value.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1), exprs.get(2));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return this.getArray().sort();
   }

   @Override
   public String toSmt() {
      String array = this.getArray().toSmt();
      String index = this.getIndex().toSmt();
      String value = this.getValue().toSmt();
      return String.format("(store %s %s %s)", array, index, value);
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}
