package julia.expr.ast.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ArraySort;
import julia.expr.sort.ExprSort;
import julia.expr.utils.ConstArray;
import julia.utils.PrimeDispatcher;
import julia.utils.VarMap;

/**
 * Node representing a constant array.
 * <p>
 * A constant array is a finite array of constants with constant default value.
 */
public final class ArrayVal extends Expr implements IConst {
   public final static int OPCODE = PrimeDispatcher.getPrime(2);

   /**
    * Creates a new node representing a constant array.
    * 
    * @param value
    *           a constant array.
    * @param sort
    *           the sort of the array.
    */
   private ArrayVal(ConstArray value, ArraySort sort) {
      super(new ArrayList<>(), Arrays.asList(value, sort));
   }

   /**
    * Creates a new node representing a constant array.
    * 
    * @param value
    *           a constant array.
    * @param domain
    *           the domain of the array.
    * @param range
    *           the range of the array.
    */
   public static ArrayVal create(ConstArray value, ExprSort domain, ExprSort range) {
      return create(value, ArraySort.create(domain, range));
   }

   /**
    * Returns a new node representing a constant array.
    * 
    * @param value
    *           a constant array.
    * @param sort
    *           the sort of the array.
    */
   public static ArrayVal create(ConstArray value, ArraySort sort) {
      return new ArrayVal(value, sort);
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create((ConstArray) args.get(0), (ArraySort) args.get(1));
   }

   @Override
   public ConstArray getValue() {
      return (ConstArray) this.getArg(0);
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ArraySort sort() {
      return (ArraySort) this.getArg(1);
   }

   @Override
   public String toString() {
      return this.getValue().toString();
   }

   // Added for efficiency.
   @Override
   public IExpr eval(Model model) {
      return this;
   }

   // Added for efficiency.
   @Override
   public IExpr renameVars(VarMap map) {
      return this;
   }

   @Override
   public String toSmt() {
      throw new IllegalStateException("ArrayVal is not part of the SMT-LIB v2 standard.");
   }

   @Override
   public SatDelta satDelta(Model model) {
      throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
   }
}