package julia.expr.ast.array;

import julia.expr.ast.uf.Fun;
import julia.expr.sort.ArraySort;
import julia.expr.sort.ExprSort;
import julia.utils.Exprs;

/**
 * Fake class to create array variables using the {@code Array.create(...)} syntax.
 */
public final class Array {
   /**
    * Do not instantiate me!
    */
   private Array() {}

   /**
    * Returns a new variable of array sort.
    * 
    * @param index
    *           the index of the variable.
    * @param domain
    *           the domain of the array represented by the variable.
    * @param range
    *           the range of the array represented by the variable.
    * @return a new variable of array sort.
    */
   public static Fun create(int index, ExprSort domain, ExprSort range) {
      return create(index, ArraySort.create(domain, range));
   }

   /**
    * Returns a new variable of array sort.
    * 
    * @param index
    *           the index of the variable.
    * @param sort
    *           the sort of the array represented by the variable.
    * @return a new variable of array sort.
    */
   public static Fun create(int index, ArraySort sort) {
      return Exprs.mkConst(index, sort);
   }
}