package julia.expr.ast.array;

import java.util.Arrays;
import java.util.List;
import julia.cache.satdelta.SatDelta;
import julia.expr.ast.Expr;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.exc.UndefinedSatDeltaError;
import julia.expr.model.Model;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Node representing the extraction of an element from an array.
 */
public final class ArraySelect extends Expr {
   public final static int OPCODE = PrimeDispatcher.getPrime(0);

   /**
    * Creates a new node representing the extraction of an element from an array.
    * 
    * @param array
    *           the input array.
    * @param index
    *           the index of the value to extract.
    */
   private ArraySelect(IExpr array, IExpr index) {
      super(Arrays.asList(array, index));
   }

   /**
    * Returns the child of this node representing the array.
    * 
    * @return the child of this node representing the array.
    */
   public IExpr getArray() {
      return this.getExpr(0);
   }

   /**
    * Returns the child of this node representing the index.
    * 
    * @return the child of this node representing the index.
    */
   public IExpr getIndex() {
      return this.getExpr(1);
   }

   /**
    * Returns a new node representing the extraction of an element from an array.
    * <p>
    * If the array and the index are both constants, the appropriate element is returned.
    * 
    * @param array
    *           the input array.
    * @param index
    *           the index of the value to extract.
    */
   public static IExpr create(IExpr array, IExpr index) {
      if (Expr.CHECK_PARAMS) {
         checkParams(array, index);
      }

      if (array instanceof ArrayVal && index instanceof IConst) {
         ArrayVal carray = (ArrayVal) array;
         IConst cindex = (IConst) index;
         return carray.getValue().get(cindex);
      } else {
         return new ArraySelect(array, index);
      }
   }

   public static void checkParams(IExpr array, IExpr index) {
      Preconditions.checkArgument(
            array.sort() instanceof ArraySort,
            "ArraySelect first child must have array sort, got %s.",
            array.sort());

      Preconditions
            .checkArgument(
                  index.sort().equals(((ArraySort) array.sort()).domain()),
                  "ArraySelect second child must have the sort of the domain of the first child, got %s.",
                  index.sort());
   }

   @Override
   protected IExpr make(List<IExpr> exprs, List<Object> args) {
      return create(exprs.get(0), exprs.get(1));
   }

   @Override
   public int opcode() {
      return OPCODE;
   }

   @Override
   public ExprSort sort() {
      return ((ArraySort) this.getArray().sort()).range();
   }

   @Override
   public String toSmt() {
      return String.format("(select %s %s)", this.getArray().toSmt(), this.getIndex().toSmt());
   }

   @Override
   public SatDelta satDelta(Model model) {
      if (!(this.sort() instanceof BoolSort)) {
         throw new UndefinedSatDeltaError("SatDelta is only defined for boolean expressions.");
      }
      IExpr expr = this.eval(model);
      if (expr instanceof BoolVal) {
         return expr.satDelta(model);
      }
      throw new UndefinedSatDeltaError(this, model);
   }
}