package julia.expr.exc;

import julia.expr.sort.ExprSort;

/**
 * This error is thrown in case of unrecognized sorts.
 * <p>
 * This error should never be thrown, if it is thrown it means that the block
 * that threw it must be modified to incorporate the missing sort.
 */
public class UnhandledSortError extends Error {
   private static final long serialVersionUID = 8134166393180649565L;

   /**
    * Creates a new UnhandledSortError.
    */
   public UnhandledSortError(ExprSort sort) {
      super(sort.toSmt());
   }
}
