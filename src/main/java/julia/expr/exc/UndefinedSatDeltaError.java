package julia.expr.exc;

import julia.expr.ast.Expr;
import julia.expr.model.Model;

/**
 * Exception thrown when the sat-delta value of an expression cannot be
 * computed.
 */
public class UndefinedSatDeltaError extends Error {
   private static final long serialVersionUID = 35190698049241829L;

   /**
    * Creates a new UndefinedSatDeltaError.
    * 
    * @param message
    *           the message associated with this error.
    */
   public UndefinedSatDeltaError(String message) {
      super(message);
   }

   /**
    * Creates a new UndefinedSatDeltaError declaring that a sub-expression of
    * the given expression does not evaluate to a constant under the given
    * model.
    * 
    * @param expr
    *           the expression generating the error.
    * @param model
    *           the model generating the error.
    */
   public UndefinedSatDeltaError(Expr expr, Model model) {
      super(
            String
                  .format(
                        "A sub-expression of %s does not reduce to a constant under the model %s.",
                        expr.toString(),
                        model.toString()));
   }
}
