package julia.expr.utils;

import java.math.BigInteger;
import java.util.regex.Pattern;
import julia.utils.Hashing;
import com.google.common.base.Preconditions;

/**
 * Fixed size bit-vector constant.
 */
public class BitVector {
   private final static Pattern PATTERN = Pattern.compile("^[0#]((b[01]+)|(x[a-zA-Z0-9]+))$");

   private final BigInteger value;
   private final BigInteger size;

   /**
    * Creates a new bit-vector of the given size and with the given value.
    * 
    * @param value
    *           the value of the bit-vector.
    * @param size
    *           the size of the bit-vector.
    */
   public BitVector(long value, long size) {
      this(BigInteger.valueOf(value), BigInteger.valueOf(size));
   }

   /**
    * Creates a new bit-vector of the given size and with the given value.
    * 
    * @param value
    *           the value of the bit-vector.
    * @param size
    *           the size of the bit-vector.
    */
   public BitVector(BigInteger value, BigInteger size) {
      BigInteger r = value.remainder(BigInteger.ONE.shiftLeft(size.intValue()));
      this.value = r;
      this.size = size;
   }

   /**
    * Returns the value of this bit-vector.
    * 
    * @return the value of this bit-vector.
    */
   public BigInteger getValue() {
      return this.value;
   }

   /**
    * Returns the size of this bit-vector.
    * 
    * @return the size of this bit-vector.
    */
   public BigInteger getSize() {
      return this.size;
   }

   @Override
   public int hashCode() {
      return Hashing.combine(this.value, this.size);
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         BitVector o = ((BitVector) other);
         boolean sameValue = (this.value.equals(o.value));
         boolean sameSize = (this.size.equals(o.size));
         return sameValue && sameSize;
      }
   }

   @Override
   public String toString() {
      String result = "#b";
      for (int i = this.size.intValue() - 1; i >= 0; --i) {
         result += (this.value.testBit(i)) ? ("1") : ("0");
      }
      return result;
   }

   /**
    * Returns the natural number represented by this bit-vector.
    * 
    * @return the natural number represented by this bit-vector.
    */
   public BigInteger toUnsignedInteger() {
      return this.value;
   }

   /**
    * Returns the concatenation of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to concatenate to this bit-vector.
    * @return the concatenation of this bit-vector and the given bit-vector.
    */
   public BitVector concat(BitVector other) {
      BigInteger v = this.value.shiftLeft(other.size.intValue()).or(other.value);
      return new BitVector(v, this.size.add(other.size));
   }

   /**
    * Returns the bit-vector obtained extracting the bits of this bit-vector from high-th
    * to low-th.
    * 
    * @param high
    *           the index of the leftmost bit to extract.
    * @param low
    *           the index of the rightmost bit to extract.
    * @return the bit-vector obtained extracting the bits of this bit-vector from high-th
    *         to low-th.
    */
   public BitVector extract(long high, long low) {
      return this.extract(BigInteger.valueOf(high), BigInteger.valueOf(low));
   }

   /**
    * Returns the bit-vector obtained extracting the bits of this bit-vector from high-th
    * to low-th.
    * 
    * @param high
    *           the index of the leftmost bit to extract.
    * @param low
    *           the index of the rightmost bit to extract.
    * @return the bit-vector obtained extracting the bits of this bit-vector from high-th
    *         to low-th.
    */
   public BitVector extract(BigInteger high, BigInteger low) {
      BigInteger size = high.subtract(low).add(BigInteger.ONE);
      BigInteger mask = BigInteger.ONE
            .shiftLeft(size.intValue())
            .subtract(BigInteger.ONE)
            .shiftLeft(low.intValue());
      BigInteger v = this.value.and(mask).shiftRight(low.intValue());
      return new BitVector(v, size);
   }

   /**
    * Returns the bitwise not of this bit-vector.
    * 
    * @return the bitwise not of this bit-vector.
    */
   public BitVector not() {
      BigInteger mask = BigInteger.ONE
            .shiftLeft(this.size.intValue())
            .subtract(BigInteger.ONE);
      BigInteger v = this.value.xor(mask);
      return new BitVector(v, this.size);
   }

   /**
    * Returns the bitwise and of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to put in bitwise and with this bit-vector.
    * @return the bitwise and of this bit-vector and the given bit-vector.
    */
   public BitVector and(BitVector other) {
      return new BitVector(this.value.and(other.value), this.size);
   }

   /**
    * Returns the bitwise or of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to put in bitwise or with this bit-vector.
    * @return the bitwise or of this bit-vector and the given bit-vector.
    */
   public BitVector or(BitVector other) {
      return new BitVector(this.value.or(other.value), this.size);
   }

   /**
    * Returns the bitwise exclusive-or of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to put in bitwise exclusive-or with this bit-vector.
    * @return the bitwise exclusive-or of this bit-vector and the given bit-vector.
    */
   public BitVector xor(BitVector other) {
      return new BitVector(this.value.xor(other.value), this.size);
   }

   /**
    * Returns the unary minus of this bit-vector.
    * 
    * @return the unary minus of this bit-vector.
    */
   public BitVector neg() {
      BigInteger v = BigInteger.ONE;
      v = v.shiftLeft(this.size.intValue());
      v = v.subtract(this.toUnsignedInteger());
      return new BitVector(v, this.size);
   }

   /**
    * Returns the sum of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to add to this bit-vector.
    * @return the sum of this bit-vector and the given bit-vector.
    */
   public BitVector add(BitVector other) {
      BigInteger ui1 = this.toUnsignedInteger();
      BigInteger ui2 = other.toUnsignedInteger();
      return new BitVector(ui1.add(ui2), this.size);
   }

   /**
    * Returns the multiplication of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to multiply to this bit-vector.
    * @return the multiplication of this bit-vector and the given bit-vector.
    */
   public BitVector mul(BitVector other) {
      BigInteger ui1 = this.toUnsignedInteger();
      BigInteger ui2 = other.toUnsignedInteger();
      return new BitVector(ui1.multiply(ui2), this.size);
   }

   /**
    * Returns the unsigned division of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the divisor.
    * @return the unsigned division of this bit-vector and the given bit-vector.
    */
   public BitVector udiv(BitVector other) {
      BigInteger ui1 = this.toUnsignedInteger();
      BigInteger ui2 = other.toUnsignedInteger();
      return new BitVector(ui1.divide(ui2), this.size);
   }

   /**
    * Returns the subtraction of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to subtract to this bit-vector.
    * @return the subtraction of this bit-vector and the given bit-vector.
    */
   public BitVector sub(BitVector other) {
      if (this.value.compareTo(other.value) >= 0) {
         return new BitVector(this.value.subtract(other.value), this.size);
      } else {
         BigInteger v = BigInteger.ONE.shiftLeft(this.size.intValue()).subtract(
               other.value.subtract(this.value));
         return new BitVector(v, this.size);
      }
   }

   /**
    * Returns the remainder of the unsigned division of this bit-vector and the given
    * bit-vector.
    * 
    * @param other
    *           the divisor.
    * @return the remainder of the unsigned division of this bit-vector and the given
    *         bit-vector.
    */
   public BitVector urem(BitVector other) {
      BigInteger dividend = this.toUnsignedInteger();
      BigInteger divisor = other.toUnsignedInteger();
      BigInteger value = dividend.remainder(divisor);
      return new BitVector(value, this.size);
   }

   /**
    * Returns the remainder of the signed division of this bit-vector and the given
    * bit-vector.
    * 
    * @param other
    *           the divisor.
    * @return the remainder of the signed division of this bit-vector and the given
    *         bit-vector.
    */
   public BitVector srem(BitVector other) {
      BigInteger dividend = this.toSignedInteger();
      BigInteger divisor = other.toSignedInteger();
      BigInteger value = dividend.remainder(divisor);

      if (value.signum() <= 0) {
         value = BigInteger.ONE.shiftLeft(this.size.intValue()).add(value);
      }

      return new BitVector(value, this.size);
   }

   /**
    * Returns the left shift of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to shift this bit-vector by.
    * @return the left shift of this bit-vector and the given bit-vector.
    */
   public BitVector shl(BitVector other) {
      return new BitVector(this.toUnsignedInteger().multiply(
            BigInteger.ONE.shiftLeft(other.toUnsignedInteger().intValue())), this.size);
   }

   /**
    * Returns the logical right shift of this bit-vector and the given bit-vector.
    * 
    * @param other
    *           the bit-vector to shift this bit-vector by.
    * @return the logical right shift of this bit-vector and the given bit-vector.
    */
   public BitVector lshr(BitVector other) {
      return new BitVector(this.toUnsignedInteger().divide(
            BigInteger.ONE.shiftLeft(other.toUnsignedInteger().intValue())), this.size);
   }

   /**
    * Returns the arithmetic (that is, signed) right shift of this bit-vector and the
    * given bit-vector.
    * 
    * @param other
    *           the bit-vector to shift this bit-vector by.
    * @return the arithmetic right shift of this bit-vector and the given bit-vector.
    */
   public BitVector ashr(BitVector other) {
      BigInteger lhsValue = this.toUnsignedInteger();
      BigInteger rhsValue = other.toUnsignedInteger();

      if (rhsValue.signum() == 0) {
         return this;
      }

      BigInteger value = lhsValue.shiftRight(rhsValue.intValue());

      if (this.sign()) {
         return new BitVector(value, this.size);
      } else {
         BigInteger mask = BigInteger.ONE;
         mask = mask.shiftLeft(rhsValue.intValue());
         mask = mask.subtract(BigInteger.ONE);
         mask = mask.shiftLeft(this.size.subtract(rhsValue).intValue());
         return new BitVector(mask.or(value), this.size);
      }
   }

   /**
    * Returns true if the unsigned representation of this bit-vector is less than or equal
    * to the unsigned representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the unsigned representation of this bit-vector is less than or equal
    *         to the unsigned representation of the given bit-vector.
    */
   public boolean ule(BitVector other) {
      return (this.toUnsignedInteger().compareTo(other.toUnsignedInteger()) <= 0);
   }

   /**
    * Returns true if the unsigned representation of this bit-vector is less than the
    * unsigned representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the unsigned representation of this bit-vector is less than the
    *         unsigned representation of the given bit-vector.
    */
   public boolean ult(BitVector other) {
      return (this.toUnsignedInteger().compareTo(other.toUnsignedInteger()) < 0);
   }

   /**
    * Returns true if the unsigned representation of this bit-vector is greater than or
    * equal to the unsigned representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the unsigned representation of this bit-vector is greater than or
    *         equal to the unsigned representation of the given bit-vector.
    */
   public boolean uge(BitVector other) {
      return (this.toUnsignedInteger().compareTo(other.toUnsignedInteger()) >= 0);
   }

   /**
    * Returns true if the unsigned representation of this bit-vector is greater than the
    * unsigned representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the unsigned representation of this bit-vector is greater than the
    *         unsigned representation of the given bit-vector.
    */
   public boolean ugt(BitVector other) {
      return (this.toUnsignedInteger().compareTo(other.toUnsignedInteger()) > 0);
   }

   /**
    * Returns true if this bit-vector is positive.
    * 
    * @return true if this bit-vector is positive.
    */
   public boolean sign() {
      int s = this.size.subtract(BigInteger.ONE).intValue();
      return (this.value.shiftRight(s).signum() == 0);
   }

   /**
    * Returns -1 if this bit-vector is negative, 0 if it is zero, 1 if it is positive.
    * 
    * @return -1 if this bit-vector is negative, 0 if it is zero, 1 if it is positive.
    */
   public int signum() {
      if (this.getValue().signum() == 0) {
         return 0;
      } else if (this.sign()) {
         return 1;
      } else {
         return -1;
      }
   }

   /**
    * Returns the signed integer represented by this bit-vector.
    * 
    * @return the signed integer represented by this bit-vector
    */
   public BigInteger toSignedInteger() {
      int n = this.size.subtract(BigInteger.ONE).intValue();
      BigInteger k = BigInteger.ONE.shiftLeft(n).subtract(BigInteger.ONE);
      BigInteger tail = this.value.and(k);

      if (this.sign()) {
         return tail;
      } else {
         return tail.subtract(BigInteger.ONE.shiftLeft(n));
      }
   }

   /**
    * Returns true if the signed representation of this bit-vector is less than or equal
    * to the signed representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the signed representation of this bit-vector is less than or equal
    *         to the signed representation of the given bit-vector.
    * 
    */
   public boolean sle(BitVector other) {
      return (this.toSignedInteger().compareTo(other.toSignedInteger()) <= 0);
   }

   /**
    * Returns true if the signed representation of this bit-vector is less than the signed
    * representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the signed representation of this bit-vector is less than the signed
    *         representation of the given bit-vector.
    */
   public boolean slt(BitVector other) {
      return (this.toSignedInteger().compareTo(other.toSignedInteger()) < 0);
   }

   /**
    * Returns true if the signed representation of this bit-vector is greater than or
    * equal to the signed representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the signed representation of this bit-vector is greater than or
    *         equal to the signed representation of the given bit-vector.
    */
   public boolean sge(BitVector other) {
      return (this.toSignedInteger().compareTo(other.toSignedInteger()) >= 0);
   }

   /**
    * Returns true if the signed representation of this bit-vector is greater than the
    * signed representation of the given bit-vector.
    * 
    * @param other
    *           the bit-vector to compare to this bit-vector.
    * @return true if the signed representation of this bit-vector is greater than the
    *         signed representation of the given bit-vector.
    */
   public boolean sgt(BitVector other) {
      return (this.toSignedInteger().compareTo(other.toSignedInteger()) > 0);
   }

   /**
    * Returns this bit-vector extended with n zero bits.
    * 
    * @param n
    *           the number of zeros to add to the head of this bit-vector.
    * @return this bit-vector extended with n zero bits.
    */
   public BitVector zeroExt(long n) {
      return (new BitVector(BigInteger.ZERO, BigInteger.valueOf(n))).concat(this);
   }

   /**
    * Returns this bit-vector extended with n sign bits.
    * 
    * @param n
    *           the number of sign bits to add to the head of this bit-vector.
    * @return this bit-vector extended with n sign bits.
    */
   public BitVector signExt(long n) {
      // If the left-most bit of `bv` is 0 (i.e., bv is positive) return bv,
      // otherwise return `n` ones followed by bv.
      if (this.sign()) {
         return new BitVector(this.value, this.size.add(BigInteger.valueOf(n)));
      } else {
         BitVector bv = new BitVector(BigInteger.ZERO, BigInteger.valueOf(n));
         return bv.not().concat(this);
      }
   }

   /**
    * Returns the bit-vector represented by the given string.
    * 
    * @param s
    *           a string representing a bit-vector in binary or hexadecimal notation.
    *           Binary bit-vectors have the form [0#]b[0-9]+ while hexadecimal bit-vectors
    *           have the form [0#]x[0-9]+.
    * @return the bit-vector represented by the given string.
    */
   public static BitVector valueOf(String s) {
      Preconditions.checkArgument(
            PATTERN.asPredicate().test(s),
            "Expected string matching format %s, got %s",
            PATTERN,
            s);

      if (s.startsWith("0b") || s.startsWith("#b")) {
         // Binary bit-vector encoding.
         BigInteger result = BigInteger.ZERO;
         BigInteger size = BigInteger.ZERO;

         String digits = s.split("b")[1];
         for (int i = 0; i < digits.length(); ++i) {
            result = result.multiply(BigInteger.valueOf(2));
            result = result.add(BigInteger.valueOf(digits.charAt(i) - '0'));
            size = size.add(BigInteger.ONE);
         }
         return new BitVector(result, size);
      } else {
         // Hexadecimal bit-vector encoding.
         BigInteger result = BigInteger.ZERO;
         BigInteger size = BigInteger.ZERO;

         String digits = s.split("x")[1].toLowerCase();
         for (int i = 0; i < digits.length(); ++i) {
            long hexValue = "0123456789abcdef".indexOf(digits.charAt(i));

            result = result.multiply(BigInteger.valueOf(16));
            result = result.add(BigInteger.valueOf(hexValue));
            size = size.add(BigInteger.valueOf(4));
         }
         return new BitVector(result, size);
      }
   }
}
