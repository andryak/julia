package julia.expr.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import julia.expr.ast.IConst;
import julia.utils.Hashing;

/**
 * Array of concrete values.
 */
public class ConstArray {
   private final Map<IConst, IConst> map;
   private final IConst elseValue;
   private final int hash;

   /**
    * Creates a new constant concrete array.
    * 
    * @param c
    *           the default value assigned to all indexes.
    */
   public ConstArray(IConst c) {
      this(new HashMap<>(), c);
   }

   /**
    * Creates a new array of concrete values.
    * 
    * @param map
    *           a map from indexes to the values of this array.
    * @param elseValue
    *           the default value assigned to all indexes not in the map.
    */
   public ConstArray(Map<IConst, IConst> map, IConst elseValue) {
      this.map = map;
      this.elseValue = elseValue;
      this.hash = Hashing.combine(map, elseValue);
   }

   /**
    * Returns the set of pairs <index, value> stored in this array.
    * 
    * @return the set of pairs <index, value> stored in this array.
    */
   public Set<Entry<IConst, IConst>> getItems() {
      return this.map.entrySet();
   }

   /**
    * Returns the else value of this array.
    * 
    * @return the else value of this array.
    */
   public IConst getElse() {
      return this.elseValue;
   }

   /**
    * Returns the value at position index in this array, or elseValue if no element is
    * stored in that position.
    * 
    * @param index
    *           the index of the element to extract from the array.
    * @return the value at position index in this array, or elseValue if no element is
    *         stored in that position.
    */
   public IConst get(IConst index) {
      return this.map.getOrDefault(index, this.elseValue);
   }

   /**
    * Returns a new array in which the value at position index is the given value.
    * <p>
    * For all other indexes the new array behaves like this one.
    * 
    * @param index
    *           the index of the element to set.
    * @param value
    *           the new value associated with the given index.
    * @return a new array in which the value at position index is the given value.
    */
   public ConstArray set(IConst index, IConst value) {
      Map<IConst, IConst> map = new HashMap<>(this.map);
      map.put(index, value);
      return new ConstArray(map, this.elseValue);
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         ConstArray o = (ConstArray) other;
         boolean sameMap = this.map.equals(o.map);
         boolean sameElse = this.elseValue.equals(o.elseValue);
         return sameMap && sameElse;
      }
   }

   @Override
   public int hashCode() {
      return this.hash;
   }

   @Override
   public String toString() {
      return String.format(
            "ConstArray(%s, %s)",
            this.map.toString(),
            this.elseValue.toString());
   }
}
