package julia.expr.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import julia.expr.ast.IConst;
import julia.utils.Hashing;
import com.google.common.base.Preconditions;

/**
 * Array of concrete values.
 */
public class FunInter {
   private final Map<List<IConst>, IConst> map;
   private final IConst elseValue;
   private final int hash;

   /**
    * Creates a new constant concrete function.
    * 
    * @param c
    *           the default value assigned to all arguments.
    */
   public FunInter(IConst c) {
      this(new HashMap<>(), c);
   }

   /**
    * Creates a new function of concrete values.
    * 
    * @param map
    *           a map from arguments to the values of this function.
    * @param elseValue
    *           the default value assigned to all arguments not in the map.
    */
   public FunInter(Map<List<IConst>, IConst> map, IConst elseValue) {
      Preconditions.checkNotNull(map);
      Preconditions.checkNotNull(elseValue);

      this.map = map;
      this.elseValue = elseValue;
      this.hash = Hashing.combine(map, elseValue);
   }

   /**
    * Returns the set of pairs <arguments, value> stored in this function.
    * 
    * @return the set of pairs <arguments, value> stored in this function.
    */
   public Set<Entry<List<IConst>, IConst>> getItems() {
      return this.map.entrySet();
   }

   /**
    * Returns the else value of this function.
    * 
    * @return the else value of this function.
    */
   public IConst getElse() {
      return this.elseValue;
   }

   /**
    * Returns the value corresponding to the given arguments, or elseValue if no element
    * is associated to such arguments.
    * 
    * @param args
    *           the arguments to apply to the function.
    * @return the value corresponding to the given arguments, or elseValue if no element
    *         is associated to such arguments
    */
   public IConst get(List<IConst> args) {
      return this.map.getOrDefault(args, this.elseValue);
   }

   /**
    * Creates a new function in which the given arguments map to the given value.
    * <p>
    * For all other arguments the new function behaves like this one.
    * 
    * @param args
    *           the arguments to map to the given value.
    * @param value
    *           the new value associated with the given arguments.
    * @return a new function in which the given arguments map to the given value.
    */
   public FunInter set(List<IConst> args, IConst value) {
      Map<List<IConst>, IConst> map = new HashMap<>(this.map);
      map.put(args, value);
      return new FunInter(map, this.elseValue);
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         FunInter o = (FunInter) other;
         boolean sameMap = this.map.equals(o.map);
         boolean sameElse = this.elseValue.equals(o.elseValue);
         return sameMap && sameElse;
      }
   }

   @Override
   public int hashCode() {
      return this.hash;
   }

   @Override
   public String toString() {
      return "(const_fun " + this.map + " else " + this.elseValue + ")";
   }
}
