package julia.expr.utils;

import java.math.BigInteger;
import java.util.regex.Pattern;
import com.google.common.base.Preconditions;

/**
 * Rational number with arbitrary precision.
 * <p>
 * Implementation by Andrea Aquino, re-adapted from:
 * http://introcs.cs.princeton.edu/java/92symbolic/BigRational.java.html
 * <p>
 * This implementation also defines the concept of infinite rational numbers.
 */
public class BigRational implements Comparable<BigRational> {
   private final static Pattern FRACT_PATTERN = Pattern.compile("^-?[0-9]+/[0-9]+$");
   private final static Pattern FLOAT_PATTERN = Pattern.compile("^-?[0-9]+(\\.[0-9]+)?$");

   public static final BigRational ZERO;
   public static final BigRational ONE;

   public static final BigRational POS_INFINITY;
   public static final BigRational NEG_INFINITY;

   static {
      ZERO = BigRational.create(0);
      ONE = BigRational.create(1);

      POS_INFINITY = new BigRational(BigInteger.ONE, BigInteger.ZERO);
      NEG_INFINITY = new BigRational(BigInteger.ONE.negate(), BigInteger.ZERO);
   }

   private final BigInteger p; // The numerator.
   private final BigInteger q; // The denominator.

   /**
    * Creates a new rational with the given numerator and denominator.
    * <p>
    * The rational is automatically reduced and normalized so that the numerator carries
    * the sign and the denominator is non-negative.
    * <p>
    * If the denominator is zero this object represents infinity with sign equal to the
    * sign of the numerator.
    * 
    * @param p
    *           the numerator.
    * @param q
    *           the denominator.
    */
   private BigRational(BigInteger p, BigInteger q) {
      Preconditions.checkNotNull(p);
      Preconditions.checkNotNull(q);

      Preconditions.checkArgument(
            p.signum() != 0 || q.signum() != 0,
            "Numerator and denominator of rational cannot both be zero.");

      // If this rational is infinite, normalize the numerator and return.
      if (q.signum() == 0) {
         if (p.signum() > 0) {
            p = BigInteger.ONE;
         } else {
            p = BigInteger.ONE.negate();
         }
         this.p = p;
         this.q = q;
         return;
      }

      // Otherwise this rational is finite.

      // Reduce the fraction.
      // This also forces one representation for zero: 0/1.
      BigInteger gcd = p.gcd(q);
      p = p.divide(gcd);
      q = q.divide(gcd);

      // Ensure that the denominator is positive (i.e., the numerator carries the sign).
      if (q.signum() < 0) {
         p = p.negate();
         q = q.negate();
      }

      this.p = p;
      this.q = q;
   }

   /**
    * Creates the rational p/1.
    * 
    * @param p
    *           the numerator.
    */
   public static BigRational create(long p) {
      return create(p, 1);
   }

   /**
    * Creates the rational p/1.
    * 
    * @param p
    *           the numerator.
    */
   public static BigRational create(BigInteger p) {
      return create(p, BigInteger.ONE);
   }

   /**
    * Creates the rational described by the given string.
    * 
    * @param s
    *           a string representing a floating point number or a fraction.
    */
   public static BigRational create(String s) {
      boolean isFract = FRACT_PATTERN.asPredicate().test(s);
      boolean isFloat = FLOAT_PATTERN.asPredicate().test(s);

      Preconditions.checkArgument(
            (isFract || isFloat),
            "Cannot create rational from string %s.",
            s);

      BigInteger p = null;
      BigInteger q = null;

      if (isFract) {
         // Fraction notation.
         String[] tokens = s.split("/");
         p = new BigInteger(tokens[0]);
         q = new BigInteger(tokens[1]);
      } else {
         // Float notation.
         String[] tokens = s.split("\\.");
         if (tokens.length == 1) {
            // Integer notation.
            p = new BigInteger(tokens[0]);
            q = BigInteger.ONE;
         } else {
            // Decimal notation.
            p = new BigInteger(tokens[0] + tokens[1]);
            q = BigInteger.TEN.pow(tokens[1].length());
         }
      }

      assert (p != null);
      assert (q != null);
      return create(p, q);
   }

   /**
    * Creates the rational p/q.
    * 
    * @param p
    *           the numerator.
    * @param q
    *           the denominator.
    */
   public static BigRational create(long p, long q) {
      return create(BigInteger.valueOf(p), BigInteger.valueOf(q));
   }

   /**
    * Creates the rational p/q.
    * 
    * @param p
    *           the numerator.
    * @param q
    *           the denominator, which must not be zero.
    */
   public static BigRational create(BigInteger p, BigInteger q) {
      // Prevent instantiation of infinite rational numbers.
      Preconditions.checkArgument(q.signum() != 0, "Denominator of rational cannot be zero.");

      return new BigRational(p, q);
   }

   /**
    * Returns true if this rational represents a finite quantity.
    * 
    * @return true if this rational represents a finite quantity.
    */
   public boolean isFinite() {
      return this.q.signum() != 0;
   }

   /**
    * Returns true if this rational represents an infinite quantity.
    * 
    * @return true if this rational represents an infinite quantity.
    */
   public boolean isInfinite() {
      return (!this.isFinite());
   }

   /**
    * Returns the numerator of this rational.
    * 
    * @return the numerator of this rational.
    */
   public BigInteger p() {
      return this.p;
   }

   /**
    * Returns the denominator of this rational.
    * <p>
    * The denominator of an infinite rational is zero.
    * 
    * @return the denominator of this rational.
    */
   public BigInteger q() {
      return this.q;
   }

   /**
    * Returns a compact string representing this rational.
    * 
    * @return a compact string representing this rational.
    */
   @Override
   public String toString() {
      if (this.isInfinite()) {
         return (this.signum() < 0) ? "-inf" : "+inf";
      }

      StringBuilder builder = new StringBuilder(this.p.toString());
      if (!this.q.equals(BigInteger.ONE)) {
         builder.append("/");
         builder.append(this.q);
      }
      return builder.toString();
   }

   @Override
   public int compareTo(BigRational other) {
      boolean linf = this.isInfinite();
      boolean rinf = other.isInfinite();

      int lsign = this.signum();
      int rsign = other.signum();

      if (linf && rinf) {
         return Integer.compare(lsign, rsign);
      } else if (linf) {
         return (lsign > 0) ? 1 : -1;
      } else if (rinf) {
         return (rsign > 0) ? -1 : 1;
      } else {
         return this.p.multiply(other.q).compareTo(this.q.multiply(other.p));
      }
   }

   /**
    * Returns -1, 0 or 1 as this rational is negative, zero or positive.
    * 
    * @return -1, 0 or 1 as this rational is negative, zero or positive.
    */
   public int signum() {
      return this.p.signum();
   }

   /**
    * Returns true if the given object is a rational with the same value as this rational.
    * 
    * @return true if the given object is a rational with the same value as this rational.
    */
   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (this.getClass() != other.getClass()) {
         return false;
      } else {
         BigRational o = (BigRational) other;
         return this.compareTo(o) == 0;
      }
   }

   @Override
   public int hashCode() {
      return this.toString().hashCode();
   }

   /**
    * Returns the multiplication of this rational and the given rational.
    * 
    * @param other
    *           the rational to multiply to this one.
    * @return the multiplication of this rational and the given rational.
    */
   public BigRational multiply(BigRational other) {
      if (this.signum() == 0 && other.isInfinite()) {
         throw new ArithmeticException("Cannot multiply zero and an infinite quantity.");
      }

      if (this.isInfinite() && other.signum() == 0) {
         throw new ArithmeticException("Cannot multiply an infinite quantity and zero.");
      }

      BigInteger p = this.p.multiply(other.p);
      BigInteger q = this.q.multiply(other.q);
      return new BigRational(p, q);
   }

   /**
    * Returns the sum of this rational and the given rational.
    * 
    * @param other
    *           the rational to sum to this one.
    * @return the sum of this rational and the given rational.
    */
   public BigRational add(BigRational other) {
      boolean linf = this.isInfinite();
      boolean rinf = other.isInfinite();

      int lsign = this.signum();
      int rsign = other.signum();

      if (linf && rinf && lsign != rsign) {
         throw new ArithmeticException("Cannot add infinite quantities with different signs.");
      }

      if (linf) {
         return this;
      } else if (rinf) {
         return other;
      } else {
         BigInteger p = this.p.multiply(other.q).add(other.p.multiply(this.q));
         BigInteger q = this.q.multiply(other.q);
         return new BigRational(p, q);
      }
   }

   /**
    * Returns the arithmetic inverse of this rational (that is, -this).
    * 
    * @return the arithmetic inverse of this rational.
    */
   public BigRational negate() {
      return new BigRational(this.p.negate(), this.q);
   }

   /**
    * Returns the absolute value of this rational.
    * 
    * @return the absolute value of this rational.
    */
   public BigRational abs() {
      return (this.signum() >= 0) ? this : this.negate();
   }

   /**
    * Returns the subtraction of this rational and the given rational.
    * 
    * @param other
    *           the rational to subtract to this one.
    * @return the subtraction of this rational and the given rational.
    */
   public BigRational subtract(BigRational other) {
      return this.add(other.negate());
   }

   /**
    * Returns the reciprocal of this rational (that is, 1/this).
    * 
    * @return the reciprocal of this rational.
    */
   public BigRational reciprocal() {
      if (this.signum() == 0) {
         throw new ArithmeticException("Cannot calculate the reciprocal of zero.");
      }
      return new BigRational(this.q, this.p);
   }

   /**
    * Returns the division of this rational and the given rational.
    * 
    * @param other
    *           the divisor.
    * @return the division of this rational and the given rational.
    */
   public BigRational divide(BigRational other) {
      if (other.signum() == 0) {
         throw new ArithmeticException("Cannot divide by zero.");
      }

      if (this.isInfinite() && other.isInfinite()) {
         throw new ArithmeticException("Cannot divide infinite quantities.");
      }

      return this.multiply(other.reciprocal());
   }

   /**
    * Returns a BigInteger approximating this rational.
    * 
    * @return a BigInteger approximating this rational.
    */
   public BigInteger toInt() {
      if (this.isInfinite()) {
         throw new ArithmeticException("Cannot approximate an infinite rational.");
      }
      return this.p.divide(this.q);
   }

   /**
    * Returns true if this rational represents an exact integer.
    * 
    * @return true if this rational represents an exact integer.
    */
   public boolean isInt() {
      // Since BigRationals are normalized, this rational is an integer iff its
      // denominator is one.
      return this.q.equals(BigInteger.ONE);
   }

   /**
    * Returns the smallest between this and the given rational.
    * 
    * @param other
    *           another rational.
    * @return the smallest between this and the given rational.
    */
   public BigRational min(BigRational other) {
      return (this.compareTo(other) <= 0) ? this : other;
   }

   /**
    * Returns the largest between this and the given rational.
    * 
    * @param other
    *           another rational.
    * @return the largest between this and the given rational.
    */
   public BigRational max(BigRational other) {
      return (this.compareTo(other) >= 0) ? this : other;
   }

   /**
    * Returns a double approximating this rational.
    * 
    * @return a double approximating this rational.
    */
   public double doubleValue() {
      if (this.isInfinite()) {
         throw new ArithmeticException("Cannot approximate an infinite rational.");
      }
      return this.p.doubleValue() / this.q.doubleValue();
   }
}