package julia.expr.sort;

import julia.utils.Hashing;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * The sort of array expressions.
 */
public class ArraySort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(80);

   public final static ArraySort INT_INT;
   public final static ArraySort INT_BOOL;
   static {
      INT_INT = new ArraySort(IntSort.create(), IntSort.create());
      INT_BOOL = new ArraySort(IntSort.create(), BoolSort.create());
   }

   private final ExprSort domain;
   private final ExprSort range;
   private final int hash;

   /**
    * Creates a sort describing arrays with the given domain and range.
    * 
    * @param domain
    *           the domain of the arrays described by the sort.
    * @param range
    *           the range of the arrays described by the sort.
    */
   private ArraySort(ExprSort domain, ExprSort range) {
      Preconditions.checkNotNull(domain);
      Preconditions.checkNotNull(range);

      this.domain = domain;
      this.range = range;
      this.hash = Hashing.combine(ArraySort.OPCODE, domain, range);
   }

   /**
    * Returns the sort describing arrays with the given domain and range.
    * 
    * @param domain
    *           the domain of the arrays described by the sort.
    * @param domain
    *           the range of the arrays described by the sort.
    */
   public static ArraySort create(ExprSort domain, ExprSort range) {
      if (domain instanceof IntSort && range instanceof IntSort) {
         return INT_INT;
      } else {
         return new ArraySort(domain, range);
      }
   }

   /**
    * Returns the sort of arrays with integer domain and range.
    * 
    * @return the sort of arrays with integer domain and range.
    */
   public static ArraySort create() {
      return INT_INT;
   }

   /**
    * Returns the domain of the arrays described by this sort.
    * 
    * @return the domain of the arrays described by this sort.
    */
   public ExprSort domain() {
      return this.domain;
   }

   /**
    * Returns the range of the arrays described by this sort.
    * 
    * @return the range of the arrays described by this sort.
    */
   public ExprSort range() {
      return this.range;
   }

   @Override
   public String toString() {
      return String.format("[%s -> %s]", this.domain, this.range);
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         ArraySort o = (ArraySort) other;
         boolean sameDomain = this.domain.equals(o.domain);
         boolean sameRange = this.range.equals(o.range);
         return sameDomain && sameRange;
      }
   }

   @Override
   public int hashCode() {
      return this.hash;
   }

   @Override
   public String toSmt() {
      String domain = this.domain.toSmt();
      String range = this.range.toSmt();
      return "(Array " + domain + " " + range + ")";
   }

   @Override
   public String prefix() {
      return "array";
   }
}
