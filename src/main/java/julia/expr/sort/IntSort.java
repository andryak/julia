package julia.expr.sort;

import julia.utils.PrimeDispatcher;

/**
 * The sort of integer expressions.
 */
public class IntSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(85);

   private final static IntSort instance = new IntSort();

   /**
    * Creates a sort describing integer expression.
    */
   private IntSort() {}

   /**
    * Returns a reference to the singleton instance of IntSort.
    * 
    * @return a reference to the singleton instance of IntSort.
    */
   public static IntSort create() {
      return instance;
   }

   @Override
   public String toString() {
      return "Int";
   }

   @Override
   public boolean equals(Object other) {
      return (this == other);
   }

   @Override
   public int hashCode() {
      return IntSort.OPCODE;
   }

   @Override
   public String toSmt() {
      return "Int";
   }

   @Override
   public String prefix() {
      return "int";
   }
}
