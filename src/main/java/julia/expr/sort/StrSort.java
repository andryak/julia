package julia.expr.sort;

import julia.utils.PrimeDispatcher;

/**
 * The sort of string expressions.
 */
public class StrSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(88);

   private final static StrSort instance = new StrSort();

   /**
    * Creates a sort describing string expression.
    */
   private StrSort() {}

   /**
    * Returns a reference to the singleton instance of StrSort.
    * 
    * @return a reference to the singleton instance of StrSort.
    */
   public static StrSort create() {
      return instance;
   }

   @Override
   public String toString() {
      return "String";
   }

   @Override
   public boolean equals(Object other) {
      return (this == other);
   }

   @Override
   public int hashCode() {
      return StrSort.OPCODE;
   }

   @Override
   public String toSmt() {
      return "String";
   }

   @Override
   public String prefix() {
      return "string";
   }
}