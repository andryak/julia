package julia.expr.sort;

import julia.utils.Hashing;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * Custom sort with a given name.
 */
public class DeclareSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(83);

   public final static DeclareSort A = new DeclareSort("A");
   public final static DeclareSort B = new DeclareSort("B");
   public final static DeclareSort C = new DeclareSort("C");

   private final String name;
   private final int hash;

   /**
    * Creates a custom sort with the given name.
    * 
    * @param name
    *           the name of the custom sort.
    */
   private DeclareSort(String name) {
      Preconditions.checkArgument(name.length() > 0, "Declare sort name must be non-empty.");

      this.name = name;
      this.hash = Hashing.combine(DeclareSort.OPCODE, name);
   }

   /**
    * Returns a new custom sort with the given name.
    * 
    * @param name
    *           the name of the custom sort.
    * @return a new custom sort with the given name.
    */
   public static DeclareSort create(String name) {
      if (name.equals("A")) {
         return A;
      } else if (name.equals("B")) {
         return B;
      } else if (name.equals("C")) {
         return C;
      } else {
         return new DeclareSort(name);
      }
   }

   /**
    * Returns the name of this sort.
    * 
    * @return the name of this sort.
    */
   public String getName() {
      return this.name;
   }

   @Override
   public String toString() {
      return this.name;
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         DeclareSort o = (DeclareSort) other;
         return this.name.equals(o.name);
      }
   }

   @Override
   public int hashCode() {
      return this.hash;
   }

   @Override
   public String toSmt() {
      return this.name;
   }

   @Override
   public String prefix() {
      return "usort";
   }
}
