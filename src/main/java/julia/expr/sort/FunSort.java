package julia.expr.sort;

import java.util.Arrays;
import java.util.List;
import julia.utils.Hashing;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

/**
 * Sort of functions with a given domain and range.
 */
public class FunSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(84);

   public final static FunSort INT_INT;
   public final static FunSort INT_BOOL;
   static {
      INT_INT = new FunSort(Arrays.asList(IntSort.create()), IntSort.create());
      INT_BOOL = new FunSort(Arrays.asList(IntSort.create()), BoolSort.create());
   }

   private final List<ExprSort> domain;
   private final ExprSort range;
   private final int hash;

   /**
    * Creates a new sort describing functions with the given domain and range.
    * 
    * @param domain
    *           the list of the sorts of the parameters of the functions described by this
    *           sort.
    * @param range
    *           the sort of the output of the functions described by this sort.
    */
   private FunSort(List<ExprSort> domain, ExprSort range) {
      Preconditions.checkNotNull(domain);
      Preconditions.checkNotNull(range);

      Preconditions.checkArgument(
            domain.stream().noneMatch(e -> e instanceof FunSort),
            "Higher order functions are not allowed.");

      Preconditions.checkArgument(
            !(range instanceof FunSort),
            "Higher order functions are not allowed.");

      this.domain = domain;
      this.range = range;
      this.hash = Hashing.combine(ArraySort.OPCODE, domain, range);
   }

   /**
    * Returns a new custom sort with the given name.
    * 
    * @param name
    *           the name of the custom sort.
    * @return a new custom sort with the given name.
    */
   public static FunSort create(List<ExprSort> domain, ExprSort range) {
      return new FunSort(domain, range);
   }

   /**
    * Returns the domain of the functions described by this sort.
    * 
    * @return the domain of the functions described by this sort.
    */
   public List<ExprSort> domain() {
      return this.domain;
   }

   /**
    * Returns the range of the functions described by this sort.
    * 
    * @return the range of the functions described by this sort.
    */
   public ExprSort range() {
      return this.range;
   }

   /**
    * Returns the arity of the functions described by this sort.
    * 
    * @return the arity of the functions described by this sort.
    */
   public int arity() {
      return this.domain().size();
   }

   @Override
   public String toString() {
      return String.format("(%s) -> %s", Joiner.on(",").join(this.domain()), this.range);
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         FunSort o = (FunSort) other;
         boolean sameDomain = this.domain().equals(o.domain());
         boolean sameRange = this.range().equals(o.range());
         return sameDomain && sameRange;
      }
   }

   @Override
   public int hashCode() {
      return this.hash;
   }

   @Override
   public String toSmt() {
      throw new IllegalStateException("FunSort does not have a SMT-LIB v2 representation.");
   }

   @Override
   public String prefix() {
      return "fun";
   }
}
