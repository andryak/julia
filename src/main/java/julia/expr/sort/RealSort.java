package julia.expr.sort;

import julia.utils.PrimeDispatcher;

/**
 * The sort of integer expressions.
 */
public class RealSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(86);

   private final static RealSort instance = new RealSort();

   /**
    * Creates a sort describing real expressions.
    */
   private RealSort() {}

   /**
    * Returns a reference to the singleton instance of RealSort.
    * 
    * @return a reference to the singleton instance of RealSort.
    */
   public static RealSort create() {
      return instance;
   }

   @Override
   public String toString() {
      return "Real";
   }

   @Override
   public boolean equals(Object other) {
      return (this == other);
   }

   @Override
   public int hashCode() {
      return RealSort.OPCODE;
   }

   @Override
   public String toSmt() {
      return "Real";
   }

   @Override
   public String prefix() {
      return "real";
   }
}
