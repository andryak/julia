package julia.expr.sort;

import julia.utils.PrimeDispatcher;

/**
 * The sort of regular expressions over strings.
 */
public class ReSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(87);

   private final static ReSort instance = new ReSort();

   /**
    * Creates a sort describing regular expression over strings.
    */
   private ReSort() {}

   /**
    * Returns a reference to the singleton instance of RegexSort.
    * 
    * @return a reference to the singleton instance of RegexSort.
    */
   public static ReSort create() {
      return instance;
   }

   @Override
   public String toString() {
      return "Regex";
   }

   @Override
   public boolean equals(Object other) {
      return (this == other);
   }

   @Override
   public int hashCode() {
      return ReSort.OPCODE;
   }

   @Override
   public String toSmt() {
      return "(Regex String)";
   }

   @Override
   public String prefix() {
      return "regex";
   }
}