package julia.expr.sort;

import java.math.BigInteger;
import julia.utils.Hashing;
import julia.utils.PrimeDispatcher;
import com.google.common.base.Preconditions;

/**
 * The sort of bit-vector expressions of a given size.
 */
public class BitVecSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(81);

   public final static BitVecSort SIZE1;
   public final static BitVecSort SIZE2;
   public final static BitVecSort SIZE4;
   public final static BitVecSort SIZE8;
   public final static BitVecSort SIZE16;
   public final static BitVecSort SIZE32;
   public final static BitVecSort SIZE64;

   static {
      SIZE1 = new BitVecSort(BigInteger.ONE);
      SIZE2 = new BitVecSort(BigInteger.valueOf(2));
      SIZE4 = new BitVecSort(BigInteger.valueOf(4));
      SIZE8 = new BitVecSort(BigInteger.valueOf(8));
      SIZE16 = new BitVecSort(BigInteger.valueOf(16));
      SIZE32 = new BitVecSort(BigInteger.valueOf(32));
      SIZE64 = new BitVecSort(BigInteger.valueOf(64));
   }

   private final BigInteger size;
   private final int hash;

   /**
    * Creates a sort describing bit-vector expressions of the given size.
    * 
    * @param size
    *           the size of the bit-vector expressions described by this sort.
    */
   private BitVecSort(BigInteger size) {
      Preconditions.checkNotNull(size);

      Preconditions.checkArgument(
            size.signum() > 0,
            "Size of bit-vectors must be positive, got {}.",
            size);

      this.size = size;
      this.hash = Hashing.combine(BitVecSort.OPCODE, size);
   }

   /**
    * Returns the sort of bit-vectors of the given size.
    * 
    * @param size
    *           the size of the bit-vectors described by the resulting sort.
    * @return the sort of bit-vectors of the given size.
    */
   public static BitVecSort create(BigInteger size) {
      if (size.equals(BigInteger.ONE)) {
         return SIZE1;
      } else if (size.equals(BigInteger.valueOf(2))) {
         return SIZE2;
      } else if (size.equals(BigInteger.valueOf(4))) {
         return SIZE4;
      } else if (size.equals(BigInteger.valueOf(8))) {
         return SIZE8;
      } else if (size.equals(BigInteger.valueOf(16))) {
         return SIZE16;
      } else if (size.equals(BigInteger.valueOf(32))) {
         return SIZE32;
      } else if (size.equals(BigInteger.valueOf(64))) {
         return SIZE64;
      } else {
         return new BitVecSort(size);
      }
   }

   /**
    * Returns the sort of bit-vectors of the given size.
    * 
    * @param size
    *           the size of the bit-vectors described by the resulting sort.
    * @return the sort of bit-vectors of the given size.
    */
   public static BitVecSort create(long size) {
      return create(BigInteger.valueOf(size));
   }

   /**
    * Returns the size of the bit-vector expressions described by this sort.
    * 
    * @return the size of the bit-vector expressions described by this sort.
    */
   public BigInteger getSize() {
      return this.size;
   }

   @Override
   public String toString() {
      return "BitVec(" + this.size + ")";
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         BitVecSort o = (BitVecSort) other;
         return this.size.equals(o.size);
      }
   }

   @Override
   public int hashCode() {
      return this.hash;
   }

   @Override
   public String toSmt() {
      return "(_ BitVec " + this.size + ")";
   }

   @Override
   public String prefix() {
      return "bitvec";
   }
}
