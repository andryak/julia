package julia.expr.sort;

public abstract class ExprSort {
   /**
    * Returns true if the given object is a sort of the same kind of this sort and it is
    * structurally equal to this sort (in case of sorts with parameters).
    * 
    * @param other
    *           the object to compare this sort to.
    * @return true if the given object is a sort of the same kind of this sort and it is
    *         structurally equal to this sort (in case of sorts with parameters).
    */
   @Override
   public abstract boolean equals(Object other);

   /**
    * Returns a custom hash code representing this sort.
    * 
    * Equal sorts have the same hash codes.
    * 
    * @return an integer hash code representing this sort.
    */
   @Override
   public abstract int hashCode();

   /**
    * Returns a SMT-LIB v2 string representing this sort.
    * 
    * @return a SMT-LIB v2 string representing this sort.
    */
   public abstract String toSmt();

   /**
    * Returns a prefix used to declare indexed variables of this sort in SMT-LIB v2.
    * 
    * @return a prefix used to declare indexed variables of this sort in SMT-LIB v2.
    */
   public abstract String prefix();
}
