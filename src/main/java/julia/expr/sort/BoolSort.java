package julia.expr.sort;

import julia.utils.PrimeDispatcher;

/**
 * The sort of boolean expressions.
 */
public class BoolSort extends ExprSort {
   public final static int OPCODE = PrimeDispatcher.getPrime(82);

   private final static BoolSort instance = new BoolSort();

   /**
    * Creates a sort describing boolean expression.
    */
   private BoolSort() {}

   /**
    * Returns a reference to the singleton instance of BoolSort.
    * 
    * @return a reference to the singleton instance of BoolSort.
    */
   public static BoolSort create() {
      return instance;
   }

   @Override
   public String toString() {
      return "Bool";
   }

   @Override
   public boolean equals(Object other) {
      return (this == other);
   }

   @Override
   public int hashCode() {
      return BoolSort.OPCODE;
   }

   @Override
   public String toSmt() {
      return "Bool";
   }

   @Override
   public String prefix() {
      return "bool";
   }
}