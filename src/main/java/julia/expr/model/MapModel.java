package julia.expr.model;

import java.util.HashMap;
import java.util.Map;
import julia.expr.ast.IConst;
import julia.expr.ast.uf.Fun;

/**
 * A map from uninterpreted functions to constants.
 * <p>
 * <b>Notice:</b> This mapping is partial, thus its use for expression evaluation is
 * discouraged.
 */
public class MapModel extends HashMap<Fun, IConst> implements Model {
   private static final long serialVersionUID = 2597476387677440996L;

   /**
    * Creates a new empty map model.
    * <p>
    * This is intended to be used as a dummy model and cannot be used safely to evaluate
    * expressions containing functions.
    */
   public MapModel() {
      super();
   }

   /**
    * Creates a new map model from the given map.
    * 
    * @param map
    *           a map from functions to constants.
    */
   public MapModel(Map<Fun, IConst> map) {
      super(map);
   }

   @Override
   public IConst getValueFor(Fun f) {
      return this.get(f);
   }
}
