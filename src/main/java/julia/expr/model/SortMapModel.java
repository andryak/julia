package julia.expr.model;

import julia.expr.ast.IConst;
import julia.expr.ast.uf.Fun;

/**
 * A model which is both a SortModel and a MapModel.
 * <p>
 * This class implements a MapModel which relies on an SortModel to produce
 * interpretations for variables missing in the MapModel.
 * 
 * @see MapModel
 * @see SortModel
 */
public class SortMapModel implements Model {
   private final SortModel sortModel;
   private final MapModel mapModel;

   /**
    * Creates a new model from the given map and sort models, using an empty sort model as
    * the backing model.
    * 
    * @param mapModel
    *           a MapModel.
    */
   public SortMapModel(MapModel model) {
      this(new SortModel(), model);
   }

   /**
    * Creates a new model from the given map model and the given sort model.
    * 
    * @param sortModel
    *           a SortModel.
    * @param mapModel
    *           a MapModel.
    */
   public SortMapModel(SortModel sortModel, MapModel mapModel) {
      this.sortModel = sortModel;
      this.mapModel = mapModel;
   }

   @Override
   public IConst getValueFor(Fun f) {
      IConst result = this.mapModel.getValueFor(f);
      return (result != null) ? result : this.sortModel.getValueFor(f);
   }

   @Override
   public String toString() {
      return String.format("(%s, %s)", this.mapModel, this.sortModel);
   }
}
