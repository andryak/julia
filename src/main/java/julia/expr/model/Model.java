package julia.expr.model;

import julia.expr.ast.IConst;
import julia.expr.ast.uf.Fun;

/**
 * A mapping from variables to values.
 */
public interface Model {
   /**
    * Returns the value of the given function or null if it has no value.
    * 
    * @param f
    *           a function.
    * @return the value of the given function or null if it has no value.
    */
   public abstract IConst getValueFor(Fun f);

   /**
    * Returns a string representing this model.
    * 
    * @return a string representing this model.
    */
   @Override
   public abstract String toString();
}
