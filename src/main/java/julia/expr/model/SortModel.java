package julia.expr.model;

import java.math.BigInteger;
import java.util.function.Function;
import julia.expr.ast.IConst;
import julia.expr.ast.array.ArrayVal;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.uf.FunVal;
import julia.expr.ast.usort.USortVal;
import julia.expr.exc.UnhandledSortError;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.DeclareSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.FunSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import julia.expr.sort.StrSort;
import julia.expr.utils.ConstArray;
import julia.expr.utils.FunInter;

/**
 * A mapping from variable sorts to constant values.
 * <p>
 * This is an infinite total model.
 */
public class SortModel implements Model {
   public final static Function<BigInteger, BitVecVal> BITVEC0;
   public final static Function<String, USortVal> USORT0;
   static {
      BITVEC0 = (size -> BitVecVal.create(BigInteger.ZERO, size));
      USORT0 = (name -> USortVal.create(BigInteger.ZERO, DeclareSort.create(name)));
   }

   private BoolVal bool;
   private IntVal _int;
   private RealVal real;
   private StrVal string;
   private Function<BigInteger, BitVecVal> bitvec;
   private Function<String, USortVal> usort;

   /**
    * Creates a new model in which all variables are mapped to the given values.
    * 
    * @param bool
    *           the value associated to boolean variables.
    * @param _int
    *           the value associated to integer variables.
    * @param real
    *           the value associated to real variables.
    * @param bitvec
    *           a function from a size into a bit-vector of that size.
    * @param usort
    *           a function from a name into a constant of uninterpreted sort with that
    *           name.
    */
   public SortModel() {
      this.bool = BoolVal.FALSE;
      this._int = IntVal.ZERO;
      this.real = RealVal.ZERO;
      this.string = StrVal.EMPTY;
      this.bitvec = BITVEC0;
      this.usort = USORT0;
   }

   public void setBoolInter(BoolVal value) {
      this.bool = value;
   }

   public void setIntInter(IntVal value) {
      this._int = value;
   }

   public void setRealInter(RealVal value) {
      this.real = value;
   }

   public void setStringInter(StrVal value) {
      this.string = value;
   }

   public void setBitVecInter(Function<BigInteger, BitVecVal> f) {
      this.bitvec = f;
   }

   public void setUSortInter(Function<String, USortVal> f) {
      this.usort = f;
   }

   @Override
   public IConst getValueFor(Fun v) {
      return getValueFor(v.sort());
   }

   /**
    * Helper method to assign a value to a sort.
    * <p>
    * This method is particularly useful to derive values for variables representing
    * arrays and uninterpreted functions given the default interpretations for the other
    * sorts defined in this model.
    * 
    * @param sort
    *           the sort to assign a value to.
    * @return a value consistent with the given sort.
    */
   private IConst getValueFor(ExprSort sort) {
      if (sort instanceof BoolSort) {
         return this.bool;
      } else if (sort instanceof IntSort) {
         return this._int;
      } else if (sort instanceof RealSort) {
         return this.real;
      } else if (sort instanceof StrSort) {
         return this.string;
      } else if (sort instanceof DeclareSort) {
         DeclareSort declareSort = (DeclareSort) sort;
         return this.usort.apply(declareSort.getName());
      } else if (sort instanceof BitVecSort) {
         BitVecSort s = (BitVecSort) sort;
         return this.bitvec.apply(s.getSize());
      } else if (sort instanceof ArraySort) {
         ArraySort s = (ArraySort) sort;
         IConst value = getValueFor(s.range());
         return ArrayVal.create(new ConstArray(value), s);
      } else if (sort instanceof FunSort) {
         FunSort s = (FunSort) sort;
         IConst value = getValueFor(s.range());
         return FunVal.create(new FunInter(value), s);
      }
      throw new UnhandledSortError(sort);
   }

   @Override
   public String toString() {
      return String.format(
            "{Bool=%s, Int=%s, Real=%s, String=%s, BitVec(4)=%s}",
            this.bool,
            this._int,
            this.real,
            this.string,
            this.bitvec.apply(BigInteger.valueOf(4)));
   }
}
