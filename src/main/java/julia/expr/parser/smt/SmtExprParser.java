// Generated from SmtExpr.g4 by ANTLR 4.5.3
package julia.expr.parser.smt;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SmtExprParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, BIT_VEC_BIN=42, BIT_VEC_DEC=43, BIT_VEC_HEX=44, 
		INT=45, LET_BINDER=46, ID=47, WS=48;
	public static final int
		RULE_cmd = 0, RULE_option = 1, RULE_declaration = 2, RULE_exprSort = 3, 
		RULE_assertion = 4, RULE_expr = 5, RULE_binding = 6;
	public static final String[] ruleNames = {
		"cmd", "option", "declaration", "exprSort", "assertion", "expr", "binding"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "'set-logic'", "'QF_AUFBV'", "')'", "'set-option'", "':produce-models'", 
		"'true'", "'false'", "'declare-fun'", "'()'", "'Bool'", "'_'", "'BitVec'", 
		"'Array'", "'assert'", "'='", "'and'", "'or'", "'let'", "'ite'", "'bvadd'", 
		"'bvand'", "'bvashr'", "'bvlshr'", "'bvmul'", "'bvor'", "'bvshl'", "'bvslt'", 
		"'bvule'", "'bvult'", "'bvsub'", "'bvudiv'", "'bvurem'", "'bvsrem'", "'bvxor'", 
		"'concat'", "'extract'", "'sign_extend'", "'zero_extend'", "'select'", 
		"'store'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "BIT_VEC_BIN", "BIT_VEC_DEC", "BIT_VEC_HEX", 
		"INT", "LET_BINDER", "ID", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SmtExpr.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SmtExprParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class CmdContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SmtExprParser.EOF, 0); }
		public List<OptionContext> option() {
			return getRuleContexts(OptionContext.class);
		}
		public OptionContext option(int i) {
			return getRuleContext(OptionContext.class,i);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public List<AssertionContext> assertion() {
			return getRuleContexts(AssertionContext.class);
		}
		public AssertionContext assertion(int i) {
			return getRuleContext(AssertionContext.class,i);
		}
		public CmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cmd; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CmdContext cmd() throws RecognitionException {
		CmdContext _localctx = new CmdContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_cmd);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(17);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(14);
					option();
					}
					} 
				}
				setState(19);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(23);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(20);
					declaration();
					}
					} 
				}
				setState(25);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			setState(29);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(26);
				assertion();
				}
				}
				setState(31);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(32);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OptionContext extends ParserRuleContext {
		public OptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitOption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OptionContext option() throws RecognitionException {
		OptionContext _localctx = new OptionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_option);
		int _la;
		try {
			setState(43);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(34);
				match(T__0);
				setState(35);
				match(T__1);
				setState(36);
				match(T__2);
				setState(37);
				match(T__3);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(38);
				match(T__0);
				setState(39);
				match(T__4);
				setState(40);
				match(T__5);
				setState(41);
				_la = _input.LA(1);
				if ( !(_la==T__6 || _la==T__7) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(42);
				match(T__3);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Token name;
		public ExprSortContext sort;
		public TerminalNode ID() { return getToken(SmtExprParser.ID, 0); }
		public ExprSortContext exprSort() {
			return getRuleContext(ExprSortContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45);
			match(T__0);
			setState(46);
			match(T__8);
			{
			setState(47);
			((DeclarationContext)_localctx).name = match(ID);
			}
			setState(48);
			match(T__9);
			{
			setState(49);
			((DeclarationContext)_localctx).sort = exprSort();
			}
			setState(50);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprSortContext extends ParserRuleContext {
		public ExprSortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprSort; }
	 
		public ExprSortContext() { }
		public void copyFrom(ExprSortContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BitVecSortContext extends ExprSortContext {
		public Token size;
		public TerminalNode INT() { return getToken(SmtExprParser.INT, 0); }
		public BitVecSortContext(ExprSortContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecSort(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArraySortContext extends ExprSortContext {
		public ExprSortContext indexSort;
		public ExprSortContext valueSort;
		public List<ExprSortContext> exprSort() {
			return getRuleContexts(ExprSortContext.class);
		}
		public ExprSortContext exprSort(int i) {
			return getRuleContext(ExprSortContext.class,i);
		}
		public ArraySortContext(ExprSortContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitArraySort(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolSortContext extends ExprSortContext {
		public BoolSortContext(ExprSortContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBoolSort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprSortContext exprSort() throws RecognitionException {
		ExprSortContext _localctx = new ExprSortContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_exprSort);
		try {
			setState(64);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new BoolSortContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(52);
				match(T__10);
				}
				break;
			case 2:
				_localctx = new BitVecSortContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(53);
				match(T__0);
				setState(54);
				match(T__11);
				setState(55);
				match(T__12);
				{
				setState(56);
				((BitVecSortContext)_localctx).size = match(INT);
				}
				setState(57);
				match(T__3);
				}
				break;
			case 3:
				_localctx = new ArraySortContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(58);
				match(T__0);
				setState(59);
				match(T__13);
				{
				setState(60);
				((ArraySortContext)_localctx).indexSort = exprSort();
				}
				{
				setState(61);
				((ArraySortContext)_localctx).valueSort = exprSort();
				}
				setState(62);
				match(T__3);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssertionContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AssertionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assertion; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitAssertion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssertionContext assertion() throws RecognitionException {
		AssertionContext _localctx = new AssertionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_assertion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			match(T__0);
			setState(67);
			match(T__14);
			setState(68);
			expr();
			setState(69);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EqualExprContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EqualExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitEqualExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableContext extends ExprContext {
		public TerminalNode ID() { return getToken(SmtExprParser.ID, 0); }
		public VariableContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSltContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecSltContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecSlt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecXOrContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecXOrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecXOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IteExprContext extends ExprContext {
		public ExprContext condExpr;
		public ExprContext thenExpr;
		public ExprContext elseExpr;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IteExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitIteExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecValHexContext extends ExprContext {
		public TerminalNode BIT_VEC_HEX() { return getToken(SmtExprParser.BIT_VEC_HEX, 0); }
		public BitVecValHexContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecValHex(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUltContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecUltContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecUlt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoundVariableContext extends ExprContext {
		public TerminalNode LET_BINDER() { return getToken(SmtExprParser.LET_BINDER, 0); }
		public BoundVariableContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBoundVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSubContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecSubContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecSub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecLShRContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecLShRContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecLShR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSignExtContext extends ExprContext {
		public Token n;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode INT() { return getToken(SmtExprParser.INT, 0); }
		public BitVecSignExtContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecSignExt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecMulContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecMulContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecMul(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayStoreContext extends ExprContext {
		public ExprContext array;
		public ExprContext index;
		public ExprContext value;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ArrayStoreContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitArrayStore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolValFalseContext extends ExprContext {
		public BoolValFalseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBoolValFalse(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecAShRContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecAShRContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecAShR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArraySelectContext extends ExprContext {
		public ExprContext array;
		public ExprContext index;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ArraySelectContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitArraySelect(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolValTrueContext extends ExprContext {
		public BoolValTrueContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBoolValTrue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUleContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecUleContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecUle(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecAndContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecAndContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecShLContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecShLContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecShL(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LetExprContext extends ExprContext {
		public ExprContext targetExpr;
		public List<BindingContext> binding() {
			return getRuleContexts(BindingContext.class);
		}
		public BindingContext binding(int i) {
			return getRuleContext(BindingContext.class,i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public LetExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitLetExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecAddContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecAddContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecValBinContext extends ExprContext {
		public TerminalNode BIT_VEC_BIN() { return getToken(SmtExprParser.BIT_VEC_BIN, 0); }
		public BitVecValBinContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecValBin(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecExtractContext extends ExprContext {
		public Token i;
		public Token j;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<TerminalNode> INT() { return getTokens(SmtExprParser.INT); }
		public TerminalNode INT(int i) {
			return getToken(SmtExprParser.INT, i);
		}
		public BitVecExtractContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecExtract(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecValDecContext extends ExprContext {
		public Token value;
		public Token size;
		public TerminalNode BIT_VEC_DEC() { return getToken(SmtExprParser.BIT_VEC_DEC, 0); }
		public TerminalNode INT() { return getToken(SmtExprParser.INT, 0); }
		public BitVecValDecContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecValDec(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecOrContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecOrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUDivContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecUDivContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecUDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecURemContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecURemContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecURem(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecConcatContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecConcatContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSRemContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BitVecSRemContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecSRem(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecZeroExtContext extends ExprContext {
		public Token n;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode INT() { return getToken(SmtExprParser.INT, 0); }
		public BitVecZeroExtContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBitVecZeroExt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolOrContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BoolOrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBoolOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolAndContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BoolAndContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBoolAnd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_expr);
		int _la;
		try {
			setState(256);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				_localctx = new EqualExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(71);
				match(T__0);
				setState(72);
				match(T__15);
				{
				setState(73);
				((EqualExprContext)_localctx).lhs = expr();
				}
				{
				setState(74);
				((EqualExprContext)_localctx).rhs = expr();
				}
				setState(75);
				match(T__3);
				}
				break;
			case 2:
				_localctx = new BoolAndContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(77);
				match(T__0);
				setState(78);
				match(T__16);
				{
				setState(79);
				((BoolAndContext)_localctx).lhs = expr();
				}
				{
				setState(80);
				((BoolAndContext)_localctx).rhs = expr();
				}
				setState(81);
				match(T__3);
				}
				break;
			case 3:
				_localctx = new BoolOrContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(83);
				match(T__0);
				setState(84);
				match(T__17);
				{
				setState(85);
				((BoolOrContext)_localctx).lhs = expr();
				}
				{
				setState(86);
				((BoolOrContext)_localctx).rhs = expr();
				}
				setState(87);
				match(T__3);
				}
				break;
			case 4:
				_localctx = new LetExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(89);
				match(T__0);
				setState(90);
				match(T__18);
				setState(91);
				match(T__0);
				setState(93); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(92);
					binding();
					}
					}
					setState(95); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__0 );
				setState(97);
				match(T__3);
				{
				setState(98);
				((LetExprContext)_localctx).targetExpr = expr();
				}
				setState(99);
				match(T__3);
				}
				break;
			case 5:
				_localctx = new IteExprContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(101);
				match(T__0);
				setState(102);
				match(T__19);
				{
				setState(103);
				((IteExprContext)_localctx).condExpr = expr();
				}
				{
				setState(104);
				((IteExprContext)_localctx).thenExpr = expr();
				}
				{
				setState(105);
				((IteExprContext)_localctx).elseExpr = expr();
				}
				setState(106);
				match(T__3);
				}
				break;
			case 6:
				_localctx = new BitVecAddContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(108);
				match(T__0);
				setState(109);
				match(T__20);
				{
				setState(110);
				((BitVecAddContext)_localctx).lhs = expr();
				}
				{
				setState(111);
				((BitVecAddContext)_localctx).rhs = expr();
				}
				setState(112);
				match(T__3);
				}
				break;
			case 7:
				_localctx = new BitVecAndContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(114);
				match(T__0);
				setState(115);
				match(T__21);
				{
				setState(116);
				((BitVecAndContext)_localctx).lhs = expr();
				}
				{
				setState(117);
				((BitVecAndContext)_localctx).rhs = expr();
				}
				setState(118);
				match(T__3);
				}
				break;
			case 8:
				_localctx = new BitVecAShRContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(120);
				match(T__0);
				setState(121);
				match(T__22);
				{
				setState(122);
				((BitVecAShRContext)_localctx).lhs = expr();
				}
				{
				setState(123);
				((BitVecAShRContext)_localctx).rhs = expr();
				}
				setState(124);
				match(T__3);
				}
				break;
			case 9:
				_localctx = new BitVecLShRContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(126);
				match(T__0);
				setState(127);
				match(T__23);
				{
				setState(128);
				((BitVecLShRContext)_localctx).lhs = expr();
				}
				{
				setState(129);
				((BitVecLShRContext)_localctx).rhs = expr();
				}
				setState(130);
				match(T__3);
				}
				break;
			case 10:
				_localctx = new BitVecMulContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(132);
				match(T__0);
				setState(133);
				match(T__24);
				{
				setState(134);
				((BitVecMulContext)_localctx).lhs = expr();
				}
				{
				setState(135);
				((BitVecMulContext)_localctx).rhs = expr();
				}
				setState(136);
				match(T__3);
				}
				break;
			case 11:
				_localctx = new BitVecOrContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(138);
				match(T__0);
				setState(139);
				match(T__25);
				{
				setState(140);
				((BitVecOrContext)_localctx).lhs = expr();
				}
				{
				setState(141);
				((BitVecOrContext)_localctx).rhs = expr();
				}
				setState(142);
				match(T__3);
				}
				break;
			case 12:
				_localctx = new BitVecShLContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(144);
				match(T__0);
				setState(145);
				match(T__26);
				{
				setState(146);
				((BitVecShLContext)_localctx).lhs = expr();
				}
				{
				setState(147);
				((BitVecShLContext)_localctx).rhs = expr();
				}
				setState(148);
				match(T__3);
				}
				break;
			case 13:
				_localctx = new BitVecSltContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(150);
				match(T__0);
				setState(151);
				match(T__27);
				{
				setState(152);
				((BitVecSltContext)_localctx).lhs = expr();
				}
				{
				setState(153);
				((BitVecSltContext)_localctx).rhs = expr();
				}
				setState(154);
				match(T__3);
				}
				break;
			case 14:
				_localctx = new BitVecUleContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(156);
				match(T__0);
				setState(157);
				match(T__28);
				{
				setState(158);
				((BitVecUleContext)_localctx).lhs = expr();
				}
				{
				setState(159);
				((BitVecUleContext)_localctx).rhs = expr();
				}
				setState(160);
				match(T__3);
				}
				break;
			case 15:
				_localctx = new BitVecUltContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(162);
				match(T__0);
				setState(163);
				match(T__29);
				{
				setState(164);
				((BitVecUltContext)_localctx).lhs = expr();
				}
				{
				setState(165);
				((BitVecUltContext)_localctx).rhs = expr();
				}
				setState(166);
				match(T__3);
				}
				break;
			case 16:
				_localctx = new BitVecSubContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(168);
				match(T__0);
				setState(169);
				match(T__30);
				{
				setState(170);
				((BitVecSubContext)_localctx).lhs = expr();
				}
				{
				setState(171);
				((BitVecSubContext)_localctx).rhs = expr();
				}
				setState(172);
				match(T__3);
				}
				break;
			case 17:
				_localctx = new BitVecUDivContext(_localctx);
				enterOuterAlt(_localctx, 17);
				{
				setState(174);
				match(T__0);
				setState(175);
				match(T__31);
				{
				setState(176);
				((BitVecUDivContext)_localctx).lhs = expr();
				}
				{
				setState(177);
				((BitVecUDivContext)_localctx).rhs = expr();
				}
				setState(178);
				match(T__3);
				}
				break;
			case 18:
				_localctx = new BitVecURemContext(_localctx);
				enterOuterAlt(_localctx, 18);
				{
				setState(180);
				match(T__0);
				setState(181);
				match(T__32);
				{
				setState(182);
				((BitVecURemContext)_localctx).lhs = expr();
				}
				{
				setState(183);
				((BitVecURemContext)_localctx).rhs = expr();
				}
				setState(184);
				match(T__3);
				}
				break;
			case 19:
				_localctx = new BitVecSRemContext(_localctx);
				enterOuterAlt(_localctx, 19);
				{
				setState(186);
				match(T__0);
				setState(187);
				match(T__33);
				{
				setState(188);
				((BitVecSRemContext)_localctx).lhs = expr();
				}
				{
				setState(189);
				((BitVecSRemContext)_localctx).rhs = expr();
				}
				setState(190);
				match(T__3);
				}
				break;
			case 20:
				_localctx = new BitVecXOrContext(_localctx);
				enterOuterAlt(_localctx, 20);
				{
				setState(192);
				match(T__0);
				setState(193);
				match(T__34);
				{
				setState(194);
				((BitVecXOrContext)_localctx).lhs = expr();
				}
				{
				setState(195);
				((BitVecXOrContext)_localctx).rhs = expr();
				}
				setState(196);
				match(T__3);
				}
				break;
			case 21:
				_localctx = new BitVecConcatContext(_localctx);
				enterOuterAlt(_localctx, 21);
				{
				setState(198);
				match(T__0);
				setState(199);
				match(T__35);
				{
				setState(200);
				((BitVecConcatContext)_localctx).lhs = expr();
				}
				{
				setState(201);
				((BitVecConcatContext)_localctx).rhs = expr();
				}
				setState(202);
				match(T__3);
				}
				break;
			case 22:
				_localctx = new BitVecExtractContext(_localctx);
				enterOuterAlt(_localctx, 22);
				{
				setState(204);
				match(T__0);
				setState(205);
				match(T__0);
				setState(206);
				match(T__11);
				setState(207);
				match(T__36);
				{
				setState(208);
				((BitVecExtractContext)_localctx).i = match(INT);
				}
				{
				setState(209);
				((BitVecExtractContext)_localctx).j = match(INT);
				}
				setState(210);
				match(T__3);
				setState(211);
				expr();
				setState(212);
				match(T__3);
				}
				break;
			case 23:
				_localctx = new BitVecSignExtContext(_localctx);
				enterOuterAlt(_localctx, 23);
				{
				setState(214);
				match(T__0);
				setState(215);
				match(T__0);
				setState(216);
				match(T__11);
				setState(217);
				match(T__37);
				{
				setState(218);
				((BitVecSignExtContext)_localctx).n = match(INT);
				}
				setState(219);
				match(T__3);
				setState(220);
				expr();
				setState(221);
				match(T__3);
				}
				break;
			case 24:
				_localctx = new BitVecZeroExtContext(_localctx);
				enterOuterAlt(_localctx, 24);
				{
				setState(223);
				match(T__0);
				setState(224);
				match(T__0);
				setState(225);
				match(T__11);
				setState(226);
				match(T__38);
				{
				setState(227);
				((BitVecZeroExtContext)_localctx).n = match(INT);
				}
				setState(228);
				match(T__3);
				setState(229);
				expr();
				setState(230);
				match(T__3);
				}
				break;
			case 25:
				_localctx = new ArraySelectContext(_localctx);
				enterOuterAlt(_localctx, 25);
				{
				setState(232);
				match(T__0);
				setState(233);
				match(T__39);
				{
				setState(234);
				((ArraySelectContext)_localctx).array = expr();
				}
				{
				setState(235);
				((ArraySelectContext)_localctx).index = expr();
				}
				setState(236);
				match(T__3);
				}
				break;
			case 26:
				_localctx = new ArrayStoreContext(_localctx);
				enterOuterAlt(_localctx, 26);
				{
				setState(238);
				match(T__0);
				setState(239);
				match(T__40);
				{
				setState(240);
				((ArrayStoreContext)_localctx).array = expr();
				}
				{
				setState(241);
				((ArrayStoreContext)_localctx).index = expr();
				}
				{
				setState(242);
				((ArrayStoreContext)_localctx).value = expr();
				}
				setState(243);
				match(T__3);
				}
				break;
			case 27:
				_localctx = new BoolValTrueContext(_localctx);
				enterOuterAlt(_localctx, 27);
				{
				setState(245);
				match(T__6);
				}
				break;
			case 28:
				_localctx = new BoolValFalseContext(_localctx);
				enterOuterAlt(_localctx, 28);
				{
				setState(246);
				match(T__7);
				}
				break;
			case 29:
				_localctx = new BitVecValDecContext(_localctx);
				enterOuterAlt(_localctx, 29);
				{
				setState(247);
				match(T__0);
				setState(248);
				match(T__11);
				{
				setState(249);
				((BitVecValDecContext)_localctx).value = match(BIT_VEC_DEC);
				}
				{
				setState(250);
				((BitVecValDecContext)_localctx).size = match(INT);
				}
				setState(251);
				match(T__3);
				}
				break;
			case 30:
				_localctx = new BitVecValBinContext(_localctx);
				enterOuterAlt(_localctx, 30);
				{
				setState(252);
				match(BIT_VEC_BIN);
				}
				break;
			case 31:
				_localctx = new BitVecValHexContext(_localctx);
				enterOuterAlt(_localctx, 31);
				{
				setState(253);
				match(BIT_VEC_HEX);
				}
				break;
			case 32:
				_localctx = new BoundVariableContext(_localctx);
				enterOuterAlt(_localctx, 32);
				{
				setState(254);
				match(LET_BINDER);
				}
				break;
			case 33:
				_localctx = new VariableContext(_localctx);
				enterOuterAlt(_localctx, 33);
				{
				setState(255);
				match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BindingContext extends ParserRuleContext {
		public Token binder;
		public ExprContext bindedExpr;
		public TerminalNode LET_BINDER() { return getToken(SmtExprParser.LET_BINDER, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public BindingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binding; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmtExprVisitor ) return ((SmtExprVisitor<? extends T>)visitor).visitBinding(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BindingContext binding() throws RecognitionException {
		BindingContext _localctx = new BindingContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_binding);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			match(T__0);
			{
			setState(259);
			((BindingContext)_localctx).binder = match(LET_BINDER);
			}
			{
			setState(260);
			((BindingContext)_localctx).bindedExpr = expr();
			}
			setState(261);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\62\u010a\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\7\2\22\n\2\f\2\16"+
		"\2\25\13\2\3\2\7\2\30\n\2\f\2\16\2\33\13\2\3\2\7\2\36\n\2\f\2\16\2!\13"+
		"\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3.\n\3\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5C\n"+
		"\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\6\7`\n\7\r\7\16\7a\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7"+
		"\u0103\n\7\3\b\3\b\3\b\3\b\3\b\3\b\2\2\t\2\4\6\b\n\f\16\2\3\3\2\t\n\u0129"+
		"\2\23\3\2\2\2\4-\3\2\2\2\6/\3\2\2\2\bB\3\2\2\2\nD\3\2\2\2\f\u0102\3\2"+
		"\2\2\16\u0104\3\2\2\2\20\22\5\4\3\2\21\20\3\2\2\2\22\25\3\2\2\2\23\21"+
		"\3\2\2\2\23\24\3\2\2\2\24\31\3\2\2\2\25\23\3\2\2\2\26\30\5\6\4\2\27\26"+
		"\3\2\2\2\30\33\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2\2\32\37\3\2\2\2\33\31"+
		"\3\2\2\2\34\36\5\n\6\2\35\34\3\2\2\2\36!\3\2\2\2\37\35\3\2\2\2\37 \3\2"+
		"\2\2 \"\3\2\2\2!\37\3\2\2\2\"#\7\2\2\3#\3\3\2\2\2$%\7\3\2\2%&\7\4\2\2"+
		"&\'\7\5\2\2\'.\7\6\2\2()\7\3\2\2)*\7\7\2\2*+\7\b\2\2+,\t\2\2\2,.\7\6\2"+
		"\2-$\3\2\2\2-(\3\2\2\2.\5\3\2\2\2/\60\7\3\2\2\60\61\7\13\2\2\61\62\7\61"+
		"\2\2\62\63\7\f\2\2\63\64\5\b\5\2\64\65\7\6\2\2\65\7\3\2\2\2\66C\7\r\2"+
		"\2\678\7\3\2\289\7\16\2\29:\7\17\2\2:;\7/\2\2;C\7\6\2\2<=\7\3\2\2=>\7"+
		"\20\2\2>?\5\b\5\2?@\5\b\5\2@A\7\6\2\2AC\3\2\2\2B\66\3\2\2\2B\67\3\2\2"+
		"\2B<\3\2\2\2C\t\3\2\2\2DE\7\3\2\2EF\7\21\2\2FG\5\f\7\2GH\7\6\2\2H\13\3"+
		"\2\2\2IJ\7\3\2\2JK\7\22\2\2KL\5\f\7\2LM\5\f\7\2MN\7\6\2\2N\u0103\3\2\2"+
		"\2OP\7\3\2\2PQ\7\23\2\2QR\5\f\7\2RS\5\f\7\2ST\7\6\2\2T\u0103\3\2\2\2U"+
		"V\7\3\2\2VW\7\24\2\2WX\5\f\7\2XY\5\f\7\2YZ\7\6\2\2Z\u0103\3\2\2\2[\\\7"+
		"\3\2\2\\]\7\25\2\2]_\7\3\2\2^`\5\16\b\2_^\3\2\2\2`a\3\2\2\2a_\3\2\2\2"+
		"ab\3\2\2\2bc\3\2\2\2cd\7\6\2\2de\5\f\7\2ef\7\6\2\2f\u0103\3\2\2\2gh\7"+
		"\3\2\2hi\7\26\2\2ij\5\f\7\2jk\5\f\7\2kl\5\f\7\2lm\7\6\2\2m\u0103\3\2\2"+
		"\2no\7\3\2\2op\7\27\2\2pq\5\f\7\2qr\5\f\7\2rs\7\6\2\2s\u0103\3\2\2\2t"+
		"u\7\3\2\2uv\7\30\2\2vw\5\f\7\2wx\5\f\7\2xy\7\6\2\2y\u0103\3\2\2\2z{\7"+
		"\3\2\2{|\7\31\2\2|}\5\f\7\2}~\5\f\7\2~\177\7\6\2\2\177\u0103\3\2\2\2\u0080"+
		"\u0081\7\3\2\2\u0081\u0082\7\32\2\2\u0082\u0083\5\f\7\2\u0083\u0084\5"+
		"\f\7\2\u0084\u0085\7\6\2\2\u0085\u0103\3\2\2\2\u0086\u0087\7\3\2\2\u0087"+
		"\u0088\7\33\2\2\u0088\u0089\5\f\7\2\u0089\u008a\5\f\7\2\u008a\u008b\7"+
		"\6\2\2\u008b\u0103\3\2\2\2\u008c\u008d\7\3\2\2\u008d\u008e\7\34\2\2\u008e"+
		"\u008f\5\f\7\2\u008f\u0090\5\f\7\2\u0090\u0091\7\6\2\2\u0091\u0103\3\2"+
		"\2\2\u0092\u0093\7\3\2\2\u0093\u0094\7\35\2\2\u0094\u0095\5\f\7\2\u0095"+
		"\u0096\5\f\7\2\u0096\u0097\7\6\2\2\u0097\u0103\3\2\2\2\u0098\u0099\7\3"+
		"\2\2\u0099\u009a\7\36\2\2\u009a\u009b\5\f\7\2\u009b\u009c\5\f\7\2\u009c"+
		"\u009d\7\6\2\2\u009d\u0103\3\2\2\2\u009e\u009f\7\3\2\2\u009f\u00a0\7\37"+
		"\2\2\u00a0\u00a1\5\f\7\2\u00a1\u00a2\5\f\7\2\u00a2\u00a3\7\6\2\2\u00a3"+
		"\u0103\3\2\2\2\u00a4\u00a5\7\3\2\2\u00a5\u00a6\7 \2\2\u00a6\u00a7\5\f"+
		"\7\2\u00a7\u00a8\5\f\7\2\u00a8\u00a9\7\6\2\2\u00a9\u0103\3\2\2\2\u00aa"+
		"\u00ab\7\3\2\2\u00ab\u00ac\7!\2\2\u00ac\u00ad\5\f\7\2\u00ad\u00ae\5\f"+
		"\7\2\u00ae\u00af\7\6\2\2\u00af\u0103\3\2\2\2\u00b0\u00b1\7\3\2\2\u00b1"+
		"\u00b2\7\"\2\2\u00b2\u00b3\5\f\7\2\u00b3\u00b4\5\f\7\2\u00b4\u00b5\7\6"+
		"\2\2\u00b5\u0103\3\2\2\2\u00b6\u00b7\7\3\2\2\u00b7\u00b8\7#\2\2\u00b8"+
		"\u00b9\5\f\7\2\u00b9\u00ba\5\f\7\2\u00ba\u00bb\7\6\2\2\u00bb\u0103\3\2"+
		"\2\2\u00bc\u00bd\7\3\2\2\u00bd\u00be\7$\2\2\u00be\u00bf\5\f\7\2\u00bf"+
		"\u00c0\5\f\7\2\u00c0\u00c1\7\6\2\2\u00c1\u0103\3\2\2\2\u00c2\u00c3\7\3"+
		"\2\2\u00c3\u00c4\7%\2\2\u00c4\u00c5\5\f\7\2\u00c5\u00c6\5\f\7\2\u00c6"+
		"\u00c7\7\6\2\2\u00c7\u0103\3\2\2\2\u00c8\u00c9\7\3\2\2\u00c9\u00ca\7&"+
		"\2\2\u00ca\u00cb\5\f\7\2\u00cb\u00cc\5\f\7\2\u00cc\u00cd\7\6\2\2\u00cd"+
		"\u0103\3\2\2\2\u00ce\u00cf\7\3\2\2\u00cf\u00d0\7\3\2\2\u00d0\u00d1\7\16"+
		"\2\2\u00d1\u00d2\7\'\2\2\u00d2\u00d3\7/\2\2\u00d3\u00d4\7/\2\2\u00d4\u00d5"+
		"\7\6\2\2\u00d5\u00d6\5\f\7\2\u00d6\u00d7\7\6\2\2\u00d7\u0103\3\2\2\2\u00d8"+
		"\u00d9\7\3\2\2\u00d9\u00da\7\3\2\2\u00da\u00db\7\16\2\2\u00db\u00dc\7"+
		"(\2\2\u00dc\u00dd\7/\2\2\u00dd\u00de\7\6\2\2\u00de\u00df\5\f\7\2\u00df"+
		"\u00e0\7\6\2\2\u00e0\u0103\3\2\2\2\u00e1\u00e2\7\3\2\2\u00e2\u00e3\7\3"+
		"\2\2\u00e3\u00e4\7\16\2\2\u00e4\u00e5\7)\2\2\u00e5\u00e6\7/\2\2\u00e6"+
		"\u00e7\7\6\2\2\u00e7\u00e8\5\f\7\2\u00e8\u00e9\7\6\2\2\u00e9\u0103\3\2"+
		"\2\2\u00ea\u00eb\7\3\2\2\u00eb\u00ec\7*\2\2\u00ec\u00ed\5\f\7\2\u00ed"+
		"\u00ee\5\f\7\2\u00ee\u00ef\7\6\2\2\u00ef\u0103\3\2\2\2\u00f0\u00f1\7\3"+
		"\2\2\u00f1\u00f2\7+\2\2\u00f2\u00f3\5\f\7\2\u00f3\u00f4\5\f\7\2\u00f4"+
		"\u00f5\5\f\7\2\u00f5\u00f6\7\6\2\2\u00f6\u0103\3\2\2\2\u00f7\u0103\7\t"+
		"\2\2\u00f8\u0103\7\n\2\2\u00f9\u00fa\7\3\2\2\u00fa\u00fb\7\16\2\2\u00fb"+
		"\u00fc\7-\2\2\u00fc\u00fd\7/\2\2\u00fd\u0103\7\6\2\2\u00fe\u0103\7,\2"+
		"\2\u00ff\u0103\7.\2\2\u0100\u0103\7\60\2\2\u0101\u0103\7\61\2\2\u0102"+
		"I\3\2\2\2\u0102O\3\2\2\2\u0102U\3\2\2\2\u0102[\3\2\2\2\u0102g\3\2\2\2"+
		"\u0102n\3\2\2\2\u0102t\3\2\2\2\u0102z\3\2\2\2\u0102\u0080\3\2\2\2\u0102"+
		"\u0086\3\2\2\2\u0102\u008c\3\2\2\2\u0102\u0092\3\2\2\2\u0102\u0098\3\2"+
		"\2\2\u0102\u009e\3\2\2\2\u0102\u00a4\3\2\2\2\u0102\u00aa\3\2\2\2\u0102"+
		"\u00b0\3\2\2\2\u0102\u00b6\3\2\2\2\u0102\u00bc\3\2\2\2\u0102\u00c2\3\2"+
		"\2\2\u0102\u00c8\3\2\2\2\u0102\u00ce\3\2\2\2\u0102\u00d8\3\2\2\2\u0102"+
		"\u00e1\3\2\2\2\u0102\u00ea\3\2\2\2\u0102\u00f0\3\2\2\2\u0102\u00f7\3\2"+
		"\2\2\u0102\u00f8\3\2\2\2\u0102\u00f9\3\2\2\2\u0102\u00fe\3\2\2\2\u0102"+
		"\u00ff\3\2\2\2\u0102\u0100\3\2\2\2\u0102\u0101\3\2\2\2\u0103\r\3\2\2\2"+
		"\u0104\u0105\7\3\2\2\u0105\u0106\7\60\2\2\u0106\u0107\5\f\7\2\u0107\u0108"+
		"\7\6\2\2\u0108\17\3\2\2\2\t\23\31\37-Ba\u0102";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}