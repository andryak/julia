// Generated from SmtExpr.g4 by ANTLR 4.5.3
package julia.expr.parser.smt;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SmtExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SmtExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SmtExprParser#cmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCmd(SmtExprParser.CmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmtExprParser#option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOption(SmtExprParser.OptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmtExprParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(SmtExprParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolSort}
	 * labeled alternative in {@link SmtExprParser#exprSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolSort(SmtExprParser.BoolSortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSort}
	 * labeled alternative in {@link SmtExprParser#exprSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSort(SmtExprParser.BitVecSortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArraySort}
	 * labeled alternative in {@link SmtExprParser#exprSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArraySort(SmtExprParser.ArraySortContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmtExprParser#assertion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssertion(SmtExprParser.AssertionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EqualExpr}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualExpr(SmtExprParser.EqualExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolAnd}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAnd(SmtExprParser.BoolAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolOr}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolOr(SmtExprParser.BoolOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LetExpr}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLetExpr(SmtExprParser.LetExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IteExpr}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteExpr(SmtExprParser.IteExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecAdd}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecAdd(SmtExprParser.BitVecAddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecAnd}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecAnd(SmtExprParser.BitVecAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecAShR}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecAShR(SmtExprParser.BitVecAShRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecLShR}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecLShR(SmtExprParser.BitVecLShRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecMul}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecMul(SmtExprParser.BitVecMulContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecOr}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecOr(SmtExprParser.BitVecOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecShL}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecShL(SmtExprParser.BitVecShLContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSlt}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSlt(SmtExprParser.BitVecSltContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUle}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUle(SmtExprParser.BitVecUleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUlt}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUlt(SmtExprParser.BitVecUltContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSub}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSub(SmtExprParser.BitVecSubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUDiv}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUDiv(SmtExprParser.BitVecUDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecURem}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecURem(SmtExprParser.BitVecURemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSRem}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSRem(SmtExprParser.BitVecSRemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecXOr}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecXOr(SmtExprParser.BitVecXOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecConcat}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecConcat(SmtExprParser.BitVecConcatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecExtract}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecExtract(SmtExprParser.BitVecExtractContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSignExt}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSignExt(SmtExprParser.BitVecSignExtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecZeroExt}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecZeroExt(SmtExprParser.BitVecZeroExtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArraySelect}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArraySelect(SmtExprParser.ArraySelectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArrayStore}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayStore(SmtExprParser.ArrayStoreContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolValTrue}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolValTrue(SmtExprParser.BoolValTrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolValFalse}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolValFalse(SmtExprParser.BoolValFalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecValDec}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecValDec(SmtExprParser.BitVecValDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecValBin}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecValBin(SmtExprParser.BitVecValBinContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecValHex}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecValHex(SmtExprParser.BitVecValHexContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoundVariable}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoundVariable(SmtExprParser.BoundVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link SmtExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(SmtExprParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmtExprParser#binding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinding(SmtExprParser.BindingContext ctx);
}