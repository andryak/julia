package julia.expr.parser.smt;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import julia.expr.ast.IExpr;
import julia.expr.ast.array.ArraySelect;
import julia.expr.ast.array.ArrayStore;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Or;
import julia.expr.ast.bv.BitVecAShR;
import julia.expr.ast.bv.BitVecAdd;
import julia.expr.ast.bv.BitVecAnd;
import julia.expr.ast.bv.BitVecConcat;
import julia.expr.ast.bv.BitVecExtract;
import julia.expr.ast.bv.BitVecLShR;
import julia.expr.ast.bv.BitVecMul;
import julia.expr.ast.bv.BitVecOr;
import julia.expr.ast.bv.BitVecSRem;
import julia.expr.ast.bv.BitVecShL;
import julia.expr.ast.bv.BitVecSignExt;
import julia.expr.ast.bv.BitVecSlt;
import julia.expr.ast.bv.BitVecSub;
import julia.expr.ast.bv.BitVecUDiv;
import julia.expr.ast.bv.BitVecURem;
import julia.expr.ast.bv.BitVecUle;
import julia.expr.ast.bv.BitVecUlt;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.bv.BitVecXOr;
import julia.expr.ast.bv.BitVecZeroExt;
import julia.expr.ast.mixed.If;
import julia.expr.parser.smt.SmtExprParser.AssertionContext;
import julia.expr.parser.smt.SmtExprParser.BindingContext;
import julia.expr.parser.smt.SmtExprParser.DeclarationContext;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BitVector;
import julia.utils.Exprs;
import julia.utils.Pair;
import julia.utils.VarRenamer;

/**
 * Builder to create expressions from strings in SMT-LIB v2 notation.
 * 
 * @see grammars/SmtExpr.g4
 */
public class SmtExprBuilder extends SmtExprBaseVisitor<Object> {
   private VarRenamer renamer;
   private Map<String, ExprSort> declarations;
   private Map<String, IExpr> currentLetBindings;

   /**
    * Creates a new builder of expressions from SMT-LIB v2 strings.
    */
   public SmtExprBuilder() {
      this.renamer = new VarRenamer();
      this.declarations = new HashMap<>();
      this.currentLetBindings = new HashMap<>();
   }

   @Override
   public IExpr visitCmd(SmtExprParser.CmdContext ctx) {
      // Parse all declarations first.
      List<DeclarationContext> decls = ctx.declaration();
      for (final DeclarationContext decl : decls) {
         super.visit(decl);
      }

      // Then, parse all assertions and build the resulting expression.
      List<IExpr> asserts = new ArrayList<>();
      for (final AssertionContext assertion : ctx.assertion()) {
         asserts.add((IExpr) super.visit(assertion));
      }
      return And.create(asserts).compress();
   }

   @Override
   public Void visitDeclaration(SmtExprParser.DeclarationContext ctx) {
      String name = ctx.name.getText();
      ExprSort sort = (ExprSort) super.visit(ctx.sort);
      this.declarations.put(name, sort);
      return null;
   }

   @Override
   public BoolSort visitBoolSort(SmtExprParser.BoolSortContext ctx) {
      return BoolSort.create();
   }

   @Override
   public BitVecSort visitBitVecSort(SmtExprParser.BitVecSortContext ctx) {
      BigInteger size = new BigInteger(ctx.size.getText());
      return BitVecSort.create(size);
   }

   @Override
   public ArraySort visitArraySort(SmtExprParser.ArraySortContext ctx) {
      ExprSort domain = (ExprSort) super.visit(ctx.indexSort);
      ExprSort range = (ExprSort) super.visit(ctx.valueSort);
      return ArraySort.create(domain, range);
   }

   @Override
   public IExpr visitAssertion(SmtExprParser.AssertionContext ctx) {
      return (IExpr) super.visit(ctx.expr());
   }

   @Override
   public IExpr visitEqualExpr(SmtExprParser.EqualExprContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return Equal.create(lhs, rhs);
   }

   @Override
   public IExpr visitBoolAnd(SmtExprParser.BoolAndContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return And.create(lhs, rhs);
   }

   @Override
   public IExpr visitBoolOr(SmtExprParser.BoolOrContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return Or.create(lhs, rhs);
   }

   @Override
   public IExpr visitLetExpr(SmtExprParser.LetExprContext ctx) {
      // Remember the current bindings.
      Map<String, IExpr> oldBindings = new HashMap<>(this.currentLetBindings);

      List<BindingContext> bindings = ctx.binding();
      for (final BindingContext bindingCtx : bindings) {
         @SuppressWarnings("unchecked")
         Pair<String, IExpr> p = (Pair<String, IExpr>) super.visit(bindingCtx);

         // Momentarily set the new let bindings before recursively
         // building targetExpr.
         this.currentLetBindings.put(p.getL(), p.getR());
      }

      // Build the target expression under the updated bindings.
      IExpr targetExpr = (IExpr) super.visit(ctx.targetExpr);

      // Restore the old bindings.
      this.currentLetBindings = oldBindings;

      return targetExpr;
   }

   @Override
   public Pair<String, IExpr> visitBinding(SmtExprParser.BindingContext ctx) {
      String binder = ctx.binder.getText();
      IExpr bindedExpr = (IExpr) super.visit(ctx.bindedExpr);
      return new Pair<>(binder, bindedExpr);
   }

   @Override
   public IExpr visitIteExpr(SmtExprParser.IteExprContext ctx) {
      IExpr c = (IExpr) super.visit(ctx.condExpr);
      IExpr t = (IExpr) super.visit(ctx.thenExpr);
      IExpr e = (IExpr) super.visit(ctx.elseExpr);
      return If.create(c, t, e);
   }

   @Override
   public IExpr visitBitVecAdd(SmtExprParser.BitVecAddContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecAdd.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecAnd(SmtExprParser.BitVecAndContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecAnd.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecAShR(SmtExprParser.BitVecAShRContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecAShR.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecLShR(SmtExprParser.BitVecLShRContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecLShR.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecMul(SmtExprParser.BitVecMulContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecMul.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecOr(SmtExprParser.BitVecOrContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecOr.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecShL(SmtExprParser.BitVecShLContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecShL.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSlt(SmtExprParser.BitVecSltContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecSlt.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUle(SmtExprParser.BitVecUleContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecUle.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUlt(SmtExprParser.BitVecUltContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecUlt.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSub(SmtExprParser.BitVecSubContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecSub.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUDiv(SmtExprParser.BitVecUDivContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecUDiv.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecURem(SmtExprParser.BitVecURemContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecURem.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSRem(SmtExprParser.BitVecSRemContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecSRem.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecXOr(SmtExprParser.BitVecXOrContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecXOr.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecConcat(SmtExprParser.BitVecConcatContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return BitVecConcat.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecExtract(SmtExprParser.BitVecExtractContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      Integer i = Integer.parseInt(ctx.i.getText());
      Integer j = Integer.parseInt(ctx.j.getText());
      return BitVecExtract.create(expr, i, j);
   }

   @Override
   public IExpr visitBitVecSignExt(SmtExprParser.BitVecSignExtContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      Integer n = Integer.parseInt(ctx.n.getText());
      return BitVecSignExt.create(expr, n);
   }

   @Override
   public IExpr visitBitVecZeroExt(SmtExprParser.BitVecZeroExtContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      Integer n = Integer.parseInt(ctx.n.getText());
      return BitVecZeroExt.create(expr, n);
   }

   @Override
   public IExpr visitArraySelect(SmtExprParser.ArraySelectContext ctx) {
      IExpr array = (IExpr) super.visit(ctx.array);
      IExpr index = (IExpr) super.visit(ctx.index);
      return ArraySelect.create(array, index);
   }

   @Override
   public IExpr visitArrayStore(SmtExprParser.ArrayStoreContext ctx) {
      IExpr array = (IExpr) super.visit(ctx.array);
      IExpr index = (IExpr) super.visit(ctx.index);
      IExpr value = (IExpr) super.visit(ctx.value);
      return ArrayStore.create(array, index, value);
   }

   @Override
   public BoolVal visitBoolValTrue(SmtExprParser.BoolValTrueContext ctx) {
      return BoolVal.TRUE;
   }

   @Override
   public BoolVal visitBoolValFalse(SmtExprParser.BoolValFalseContext ctx) {
      return BoolVal.FALSE;
   }

   @Override
   public IExpr visitBitVecValDec(SmtExprParser.BitVecValDecContext ctx) {
      BigInteger value = new BigInteger(ctx.value.getText().replace("bv", ""));
      BigInteger size = new BigInteger(ctx.size.getText());
      return BitVecVal.create(value, size);
   }

   @Override
   public IExpr visitBitVecValBin(SmtExprParser.BitVecValBinContext ctx) {
      return BitVecVal.create(BitVector.valueOf(ctx.getText()));
   }

   @Override
   public IExpr visitBitVecValHex(SmtExprParser.BitVecValHexContext ctx) {
      return BitVecVal.create(BitVector.valueOf(ctx.getText()));
   }

   @Override
   public IExpr visitBoundVariable(SmtExprParser.BoundVariableContext ctx) {
      String name = ctx.getText();
      return this.currentLetBindings.get(name);
   }

   @Override
   public IExpr visitVariable(SmtExprParser.VariableContext ctx) {
      String name = ctx.getText();
      ExprSort sort = this.declarations.get(name);
      Integer index = this.renamer.getIndexFor(name);
      return Exprs.mkConst(index, sort);
   }
}
