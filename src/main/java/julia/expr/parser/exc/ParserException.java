package julia.expr.parser.exc;

/**
 * Exception thrown when a expression cannot be parsed.
 */
public class ParserException extends Exception {
   private static final long serialVersionUID = -7632725019286241175L;

   /**
    * Creates a new ParserException.
    * 
    * @param message
    *           the message associated with this exception.
    */
   public ParserException(String message) {
      super(message);
   }
}
