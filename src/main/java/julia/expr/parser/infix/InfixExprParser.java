// Generated from InfixExpr.g4 by ANTLR 4.5.3
package julia.expr.parser.infix;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class InfixExprParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, INT=40, REAL=41, BIN=42, HEX=43, WS=44;
	public static final int
		RULE_expr = 0, RULE_typedExpr = 1, RULE_boolExpr = 2, RULE_boolLiteral = 3, 
		RULE_andSymbol = 4, RULE_orSymbol = 5, RULE_notSymbol = 6, RULE_intIneq = 7, 
		RULE_realIneq = 8, RULE_bvIneq = 9, RULE_boolConst = 10, RULE_boolVar = 11, 
		RULE_intExpr = 12, RULE_intLiteral = 13, RULE_divSymbol = 14, RULE_modSymbol = 15, 
		RULE_intVar = 16, RULE_realExpr = 17, RULE_realLiteral = 18, RULE_realVar = 19, 
		RULE_bvExpr = 20, RULE_bvLiteral = 21, RULE_bvVar = 22;
	public static final String[] ruleNames = {
		"expr", "typedExpr", "boolExpr", "boolLiteral", "andSymbol", "orSymbol", 
		"notSymbol", "intIneq", "realIneq", "bvIneq", "boolConst", "boolVar", 
		"intExpr", "intLiteral", "divSymbol", "modSymbol", "intVar", "realExpr", 
		"realLiteral", "realVar", "bvExpr", "bvLiteral", "bvVar"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'and'", "'&'", "'&&'", "'^'", "'or'", "'|'", "'||'", 
		"'not'", "'!'", "'<'", "'>'", "'<='", "'>='", "'='", "'!='", "'<s'", "'>s'", 
		"'<=s'", "'>=s'", "'true'", "'false'", "'b'", "'abs('", "'-'", "'*'", 
		"'+'", "'div'", "'/'", "'mod'", "'%'", "'x'", "'r'", "'>>'", "'.'", "'>>>'", 
		"'%s'", "'bv'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, "INT", "REAL", "BIN", "HEX", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "InfixExpr.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public InfixExprParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ExprContext extends ParserRuleContext {
		public TypedExprContext typedExpr() {
			return getRuleContext(TypedExprContext.class,0);
		}
		public TerminalNode EOF() { return getToken(InfixExprParser.EOF, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			typedExpr();
			setState(47);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedExprContext extends ParserRuleContext {
		public TypedExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedExpr; }
	 
		public TypedExprContext() { }
		public void copyFrom(TypedExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RealExpressionContext extends TypedExprContext {
		public RealExprContext realExpr() {
			return getRuleContext(RealExprContext.class,0);
		}
		public RealExpressionContext(TypedExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntExpressionContext extends TypedExprContext {
		public IntExprContext intExpr() {
			return getRuleContext(IntExprContext.class,0);
		}
		public IntExpressionContext(TypedExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecExpressionContext extends TypedExprContext {
		public BvExprContext bvExpr() {
			return getRuleContext(BvExprContext.class,0);
		}
		public BitVecExpressionContext(TypedExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolExpressionContext extends TypedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public BoolExpressionContext(TypedExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypedExprContext typedExpr() throws RecognitionException {
		TypedExprContext _localctx = new TypedExprContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_typedExpr);
		try {
			setState(53);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				_localctx = new BoolExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(49);
				boolExpr(0);
				}
				break;
			case 2:
				_localctx = new IntExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(50);
				intExpr(0);
				}
				break;
			case 3:
				_localctx = new RealExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(51);
				realExpr(0);
				}
				break;
			case 4:
				_localctx = new BitVecExpressionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(52);
				bvExpr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolExprContext extends ParserRuleContext {
		public BoolExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolExpr; }
	 
		public BoolExprContext() { }
		public void copyFrom(BoolExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParBoolExprContext extends BoolExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public ParBoolExprContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitParBoolExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SingleBoolLiteralContext extends BoolExprContext {
		public BoolLiteralContext boolLiteral() {
			return getRuleContext(BoolLiteralContext.class,0);
		}
		public SingleBoolLiteralContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitSingleBoolLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolNotContext extends BoolExprContext {
		public NotSymbolContext notSymbol() {
			return getRuleContext(NotSymbolContext.class,0);
		}
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public BoolNotContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolNot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolOrContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public OrSymbolContext orSymbol() {
			return getRuleContext(OrSymbolContext.class,0);
		}
		public BoolOrContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolAndContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public AndSymbolContext andSymbol() {
			return getRuleContext(AndSymbolContext.class,0);
		}
		public BoolAndContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolAnd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolExprContext boolExpr() throws RecognitionException {
		return boolExpr(0);
	}

	private BoolExprContext boolExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BoolExprContext _localctx = new BoolExprContext(_ctx, _parentState);
		BoolExprContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_boolExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
				_localctx = new ParBoolExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(56);
				match(T__0);
				setState(57);
				boolExpr(0);
				setState(58);
				match(T__1);
				}
				break;
			case 2:
				{
				_localctx = new BoolNotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(60);
				notSymbol();
				setState(61);
				boolExpr(2);
				}
				break;
			case 3:
				{
				_localctx = new SingleBoolLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(63);
				boolLiteral();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(76);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(74);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
					case 1:
						{
						_localctx = new BoolAndContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(66);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(67);
						andSymbol();
						setState(68);
						boolExpr(5);
						}
						break;
					case 2:
						{
						_localctx = new BoolOrContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(70);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(71);
						orSymbol();
						setState(72);
						boolExpr(4);
						}
						break;
					}
					} 
				}
				setState(78);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BoolLiteralContext extends ParserRuleContext {
		public BoolLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolLiteral; }
	 
		public BoolLiteralContext() { }
		public void copyFrom(BoolLiteralContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolConstantContext extends BoolLiteralContext {
		public BoolConstContext boolConst() {
			return getRuleContext(BoolConstContext.class,0);
		}
		public BoolConstantContext(BoolLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolConstant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecInequalityContext extends BoolLiteralContext {
		public BvIneqContext bvIneq() {
			return getRuleContext(BvIneqContext.class,0);
		}
		public BitVecInequalityContext(BoolLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecInequality(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealInequalityContext extends BoolLiteralContext {
		public RealIneqContext realIneq() {
			return getRuleContext(RealIneqContext.class,0);
		}
		public RealInequalityContext(BoolLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealInequality(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolVariableContext extends BoolLiteralContext {
		public BoolVarContext boolVar() {
			return getRuleContext(BoolVarContext.class,0);
		}
		public BoolVariableContext(BoolLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntInequalityContext extends BoolLiteralContext {
		public IntIneqContext intIneq() {
			return getRuleContext(IntIneqContext.class,0);
		}
		public IntInequalityContext(BoolLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntInequality(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolLiteralContext boolLiteral() throws RecognitionException {
		BoolLiteralContext _localctx = new BoolLiteralContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_boolLiteral);
		try {
			setState(84);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new RealInequalityContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(79);
				realIneq();
				}
				break;
			case 2:
				_localctx = new IntInequalityContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(80);
				intIneq();
				}
				break;
			case 3:
				_localctx = new BitVecInequalityContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(81);
				bvIneq();
				}
				break;
			case 4:
				_localctx = new BoolVariableContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(82);
				boolVar();
				}
				break;
			case 5:
				_localctx = new BoolConstantContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(83);
				boolConst();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndSymbolContext extends ParserRuleContext {
		public AndSymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andSymbol; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitAndSymbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndSymbolContext andSymbol() throws RecognitionException {
		AndSymbolContext _localctx = new AndSymbolContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_andSymbol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrSymbolContext extends ParserRuleContext {
		public OrSymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orSymbol; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitOrSymbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrSymbolContext orSymbol() throws RecognitionException {
		OrSymbolContext _localctx = new OrSymbolContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_orSymbol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotSymbolContext extends ParserRuleContext {
		public NotSymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notSymbol; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitNotSymbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotSymbolContext notSymbol() throws RecognitionException {
		NotSymbolContext _localctx = new NotSymbolContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_notSymbol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			_la = _input.LA(1);
			if ( !(_la==T__9 || _la==T__10) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntIneqContext extends ParserRuleContext {
		public IntIneqContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intIneq; }
	 
		public IntIneqContext() { }
		public void copyFrom(IntIneqContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IntLeContext extends IntIneqContext {
		public IntExprContext lhs;
		public IntExprContext rhs;
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntLeContext(IntIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntLe(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntEqContext extends IntIneqContext {
		public IntExprContext lhs;
		public IntExprContext rhs;
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntEqContext(IntIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntEq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntGtContext extends IntIneqContext {
		public IntExprContext lhs;
		public IntExprContext rhs;
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntGtContext(IntIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntGt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntGeContext extends IntIneqContext {
		public IntExprContext lhs;
		public IntExprContext rhs;
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntGeContext(IntIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntGe(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntDistinctContext extends IntIneqContext {
		public IntExprContext lhs;
		public IntExprContext rhs;
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntDistinctContext(IntIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntDistinct(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntLtContext extends IntIneqContext {
		public IntExprContext lhs;
		public IntExprContext rhs;
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntLtContext(IntIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntLt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntIneqContext intIneq() throws RecognitionException {
		IntIneqContext _localctx = new IntIneqContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_intIneq);
		try {
			setState(116);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new IntLtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(92);
				((IntLtContext)_localctx).lhs = intExpr(0);
				}
				setState(93);
				match(T__11);
				{
				setState(94);
				((IntLtContext)_localctx).rhs = intExpr(0);
				}
				}
				break;
			case 2:
				_localctx = new IntGtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(96);
				((IntGtContext)_localctx).lhs = intExpr(0);
				}
				setState(97);
				match(T__12);
				{
				setState(98);
				((IntGtContext)_localctx).rhs = intExpr(0);
				}
				}
				break;
			case 3:
				_localctx = new IntLeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(100);
				((IntLeContext)_localctx).lhs = intExpr(0);
				}
				setState(101);
				match(T__13);
				{
				setState(102);
				((IntLeContext)_localctx).rhs = intExpr(0);
				}
				}
				break;
			case 4:
				_localctx = new IntGeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(104);
				((IntGeContext)_localctx).lhs = intExpr(0);
				}
				setState(105);
				match(T__14);
				{
				setState(106);
				((IntGeContext)_localctx).rhs = intExpr(0);
				}
				}
				break;
			case 5:
				_localctx = new IntEqContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(108);
				((IntEqContext)_localctx).lhs = intExpr(0);
				}
				setState(109);
				match(T__15);
				{
				setState(110);
				((IntEqContext)_localctx).rhs = intExpr(0);
				}
				}
				break;
			case 6:
				_localctx = new IntDistinctContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(112);
				((IntDistinctContext)_localctx).lhs = intExpr(0);
				}
				setState(113);
				match(T__16);
				{
				setState(114);
				((IntDistinctContext)_localctx).rhs = intExpr(0);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealIneqContext extends ParserRuleContext {
		public RealIneqContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realIneq; }
	 
		public RealIneqContext() { }
		public void copyFrom(RealIneqContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RealEqContext extends RealIneqContext {
		public RealExprContext lhs;
		public RealExprContext rhs;
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealEqContext(RealIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealEq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealGtContext extends RealIneqContext {
		public RealExprContext lhs;
		public RealExprContext rhs;
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealGtContext(RealIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealGt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealGeContext extends RealIneqContext {
		public RealExprContext lhs;
		public RealExprContext rhs;
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealGeContext(RealIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealGe(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealDistinctContext extends RealIneqContext {
		public RealExprContext lhs;
		public RealExprContext rhs;
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealDistinctContext(RealIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealDistinct(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealLtContext extends RealIneqContext {
		public RealExprContext lhs;
		public RealExprContext rhs;
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealLtContext(RealIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealLt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealLeContext extends RealIneqContext {
		public RealExprContext lhs;
		public RealExprContext rhs;
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealLeContext(RealIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealLe(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealIneqContext realIneq() throws RecognitionException {
		RealIneqContext _localctx = new RealIneqContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_realIneq);
		try {
			setState(142);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				_localctx = new RealLtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(118);
				((RealLtContext)_localctx).lhs = realExpr(0);
				}
				setState(119);
				match(T__11);
				{
				setState(120);
				((RealLtContext)_localctx).rhs = realExpr(0);
				}
				}
				break;
			case 2:
				_localctx = new RealGtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(122);
				((RealGtContext)_localctx).lhs = realExpr(0);
				}
				setState(123);
				match(T__12);
				{
				setState(124);
				((RealGtContext)_localctx).rhs = realExpr(0);
				}
				}
				break;
			case 3:
				_localctx = new RealLeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(126);
				((RealLeContext)_localctx).lhs = realExpr(0);
				}
				setState(127);
				match(T__13);
				{
				setState(128);
				((RealLeContext)_localctx).rhs = realExpr(0);
				}
				}
				break;
			case 4:
				_localctx = new RealGeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(130);
				((RealGeContext)_localctx).lhs = realExpr(0);
				}
				setState(131);
				match(T__14);
				{
				setState(132);
				((RealGeContext)_localctx).rhs = realExpr(0);
				}
				}
				break;
			case 5:
				_localctx = new RealEqContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(134);
				((RealEqContext)_localctx).lhs = realExpr(0);
				}
				setState(135);
				match(T__15);
				{
				setState(136);
				((RealEqContext)_localctx).rhs = realExpr(0);
				}
				}
				break;
			case 6:
				_localctx = new RealDistinctContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(138);
				((RealDistinctContext)_localctx).lhs = realExpr(0);
				}
				setState(139);
				match(T__16);
				{
				setState(140);
				((RealDistinctContext)_localctx).rhs = realExpr(0);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BvIneqContext extends ParserRuleContext {
		public BvIneqContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bvIneq; }
	 
		public BvIneqContext() { }
		public void copyFrom(BvIneqContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BitVecSleContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecSleContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecSle(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSltContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecSltContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecSlt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUleContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecUleContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecUle(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUltContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecUltContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecUlt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSgtContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecSgtContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecSgt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUgtContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecUgtContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecUgt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSgeContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecSgeContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecSge(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUgeContext extends BvIneqContext {
		public BvExprContext lhs;
		public BvExprContext rhs;
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecUgeContext(BvIneqContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecUge(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BvIneqContext bvIneq() throws RecognitionException {
		BvIneqContext _localctx = new BvIneqContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_bvIneq);
		try {
			setState(176);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new BitVecUltContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(144);
				((BitVecUltContext)_localctx).lhs = bvExpr(0);
				}
				setState(145);
				match(T__11);
				{
				setState(146);
				((BitVecUltContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 2:
				_localctx = new BitVecUgtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(148);
				((BitVecUgtContext)_localctx).lhs = bvExpr(0);
				}
				setState(149);
				match(T__12);
				{
				setState(150);
				((BitVecUgtContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 3:
				_localctx = new BitVecUleContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(152);
				((BitVecUleContext)_localctx).lhs = bvExpr(0);
				}
				setState(153);
				match(T__13);
				{
				setState(154);
				((BitVecUleContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 4:
				_localctx = new BitVecUgeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(156);
				((BitVecUgeContext)_localctx).lhs = bvExpr(0);
				}
				setState(157);
				match(T__14);
				{
				setState(158);
				((BitVecUgeContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 5:
				_localctx = new BitVecSltContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(160);
				((BitVecSltContext)_localctx).lhs = bvExpr(0);
				}
				setState(161);
				match(T__17);
				{
				setState(162);
				((BitVecSltContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 6:
				_localctx = new BitVecSgtContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(164);
				((BitVecSgtContext)_localctx).lhs = bvExpr(0);
				}
				setState(165);
				match(T__18);
				{
				setState(166);
				((BitVecSgtContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 7:
				_localctx = new BitVecSleContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				{
				setState(168);
				((BitVecSleContext)_localctx).lhs = bvExpr(0);
				}
				setState(169);
				match(T__19);
				{
				setState(170);
				((BitVecSleContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			case 8:
				_localctx = new BitVecSgeContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				{
				setState(172);
				((BitVecSgeContext)_localctx).lhs = bvExpr(0);
				}
				setState(173);
				match(T__20);
				{
				setState(174);
				((BitVecSgeContext)_localctx).rhs = bvExpr(0);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolConstContext extends ParserRuleContext {
		public BoolConstContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolConst; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolConst(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolConstContext boolConst() throws RecognitionException {
		BoolConstContext _localctx = new BoolConstContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_boolConst);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			_la = _input.LA(1);
			if ( !(_la==T__21 || _la==T__22) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolVarContext extends ParserRuleContext {
		public Token index;
		public TerminalNode INT() { return getToken(InfixExprParser.INT, 0); }
		public BoolVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolVar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBoolVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolVarContext boolVar() throws RecognitionException {
		BoolVarContext _localctx = new BoolVarContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_boolVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			match(T__23);
			{
			setState(181);
			((BoolVarContext)_localctx).index = match(INT);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntExprContext extends ParserRuleContext {
		public IntExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intExpr; }
	 
		public IntExprContext() { }
		public void copyFrom(IntExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IntMulContext extends IntExprContext {
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntMulContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntMul(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParIntExprContext extends IntExprContext {
		public IntExprContext intExpr() {
			return getRuleContext(IntExprContext.class,0);
		}
		public ParIntExprContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitParIntExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntNegContext extends IntExprContext {
		public IntExprContext intExpr() {
			return getRuleContext(IntExprContext.class,0);
		}
		public IntNegContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntNeg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntSubContext extends IntExprContext {
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntSubContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntSub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntAbsContext extends IntExprContext {
		public IntExprContext intExpr() {
			return getRuleContext(IntExprContext.class,0);
		}
		public IntAbsContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntAbs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntModContext extends IntExprContext {
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public ModSymbolContext modSymbol() {
			return getRuleContext(ModSymbolContext.class,0);
		}
		public IntModContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntMod(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntDivContext extends IntExprContext {
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public DivSymbolContext divSymbol() {
			return getRuleContext(DivSymbolContext.class,0);
		}
		public IntDivContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntAddContext extends IntExprContext {
		public List<IntExprContext> intExpr() {
			return getRuleContexts(IntExprContext.class);
		}
		public IntExprContext intExpr(int i) {
			return getRuleContext(IntExprContext.class,i);
		}
		public IntAddContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SingleIntLiteralContext extends IntExprContext {
		public IntLiteralContext intLiteral() {
			return getRuleContext(IntLiteralContext.class,0);
		}
		public SingleIntLiteralContext(IntExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitSingleIntLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntExprContext intExpr() throws RecognitionException {
		return intExpr(0);
	}

	private IntExprContext intExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		IntExprContext _localctx = new IntExprContext(_ctx, _parentState);
		IntExprContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_intExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			switch (_input.LA(1)) {
			case T__0:
				{
				_localctx = new ParIntExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(184);
				match(T__0);
				setState(185);
				intExpr(0);
				setState(186);
				match(T__1);
				}
				break;
			case T__24:
				{
				_localctx = new IntAbsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(188);
				match(T__24);
				setState(189);
				intExpr(0);
				setState(190);
				match(T__1);
				}
				break;
			case T__25:
				{
				_localctx = new IntNegContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(192);
				match(T__25);
				setState(193);
				intExpr(7);
				}
				break;
			case T__32:
			case INT:
				{
				_localctx = new SingleIntLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(194);
				intLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(216);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(214);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
					case 1:
						{
						_localctx = new IntModContext(new IntExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_intExpr);
						setState(197);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(198);
						modSymbol();
						setState(199);
						intExpr(7);
						}
						break;
					case 2:
						{
						_localctx = new IntDivContext(new IntExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_intExpr);
						setState(201);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(202);
						divSymbol();
						setState(203);
						intExpr(6);
						}
						break;
					case 3:
						{
						_localctx = new IntMulContext(new IntExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_intExpr);
						setState(205);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(206);
						match(T__26);
						setState(207);
						intExpr(5);
						}
						break;
					case 4:
						{
						_localctx = new IntAddContext(new IntExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_intExpr);
						setState(208);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(209);
						match(T__27);
						setState(210);
						intExpr(4);
						}
						break;
					case 5:
						{
						_localctx = new IntSubContext(new IntExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_intExpr);
						setState(211);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(212);
						match(T__25);
						setState(213);
						intExpr(3);
						}
						break;
					}
					} 
				}
				setState(218);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class IntLiteralContext extends ParserRuleContext {
		public IntLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intLiteral; }
	 
		public IntLiteralContext() { }
		public void copyFrom(IntLiteralContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IntConstantContext extends IntLiteralContext {
		public TerminalNode INT() { return getToken(InfixExprParser.INT, 0); }
		public IntConstantContext(IntLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntConstant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntVariableContext extends IntLiteralContext {
		public IntVarContext intVar() {
			return getRuleContext(IntVarContext.class,0);
		}
		public IntVariableContext(IntLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntLiteralContext intLiteral() throws RecognitionException {
		IntLiteralContext _localctx = new IntLiteralContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_intLiteral);
		try {
			setState(221);
			switch (_input.LA(1)) {
			case T__32:
				_localctx = new IntVariableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(219);
				intVar();
				}
				break;
			case INT:
				_localctx = new IntConstantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(220);
				match(INT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DivSymbolContext extends ParserRuleContext {
		public DivSymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_divSymbol; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitDivSymbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DivSymbolContext divSymbol() throws RecognitionException {
		DivSymbolContext _localctx = new DivSymbolContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_divSymbol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			_la = _input.LA(1);
			if ( !(_la==T__28 || _la==T__29) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModSymbolContext extends ParserRuleContext {
		public ModSymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modSymbol; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitModSymbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModSymbolContext modSymbol() throws RecognitionException {
		ModSymbolContext _localctx = new ModSymbolContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_modSymbol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225);
			_la = _input.LA(1);
			if ( !(_la==T__30 || _la==T__31) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntVarContext extends ParserRuleContext {
		public Token index;
		public TerminalNode INT() { return getToken(InfixExprParser.INT, 0); }
		public IntVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intVar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitIntVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntVarContext intVar() throws RecognitionException {
		IntVarContext _localctx = new IntVarContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_intVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			match(T__32);
			{
			setState(228);
			((IntVarContext)_localctx).index = match(INT);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealExprContext extends ParserRuleContext {
		public RealExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realExpr; }
	 
		public RealExprContext() { }
		public void copyFrom(RealExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParRealExprContext extends RealExprContext {
		public RealExprContext realExpr() {
			return getRuleContext(RealExprContext.class,0);
		}
		public ParRealExprContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitParRealExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SingleRealLiteralContext extends RealExprContext {
		public RealLiteralContext realLiteral() {
			return getRuleContext(RealLiteralContext.class,0);
		}
		public SingleRealLiteralContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitSingleRealLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealDivContext extends RealExprContext {
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public DivSymbolContext divSymbol() {
			return getRuleContext(DivSymbolContext.class,0);
		}
		public RealDivContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealAddContext extends RealExprContext {
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealAddContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealNegContext extends RealExprContext {
		public RealExprContext realExpr() {
			return getRuleContext(RealExprContext.class,0);
		}
		public RealNegContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealNeg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealSubContext extends RealExprContext {
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealSubContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealSub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealAbsContext extends RealExprContext {
		public RealExprContext realExpr() {
			return getRuleContext(RealExprContext.class,0);
		}
		public RealAbsContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealAbs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealModContext extends RealExprContext {
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public ModSymbolContext modSymbol() {
			return getRuleContext(ModSymbolContext.class,0);
		}
		public RealModContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealMod(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealMulContext extends RealExprContext {
		public List<RealExprContext> realExpr() {
			return getRuleContexts(RealExprContext.class);
		}
		public RealExprContext realExpr(int i) {
			return getRuleContext(RealExprContext.class,i);
		}
		public RealMulContext(RealExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealMul(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealExprContext realExpr() throws RecognitionException {
		return realExpr(0);
	}

	private RealExprContext realExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RealExprContext _localctx = new RealExprContext(_ctx, _parentState);
		RealExprContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_realExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(242);
			switch (_input.LA(1)) {
			case T__0:
				{
				_localctx = new ParRealExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(231);
				match(T__0);
				setState(232);
				realExpr(0);
				setState(233);
				match(T__1);
				}
				break;
			case T__24:
				{
				_localctx = new RealAbsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(235);
				match(T__24);
				setState(236);
				realExpr(0);
				setState(237);
				match(T__1);
				}
				break;
			case T__25:
				{
				_localctx = new RealNegContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(239);
				match(T__25);
				setState(240);
				realExpr(7);
				}
				break;
			case T__33:
			case REAL:
				{
				_localctx = new SingleRealLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(241);
				realLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(263);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(261);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
					case 1:
						{
						_localctx = new RealModContext(new RealExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_realExpr);
						setState(244);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(245);
						modSymbol();
						setState(246);
						realExpr(7);
						}
						break;
					case 2:
						{
						_localctx = new RealDivContext(new RealExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_realExpr);
						setState(248);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(249);
						divSymbol();
						setState(250);
						realExpr(6);
						}
						break;
					case 3:
						{
						_localctx = new RealMulContext(new RealExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_realExpr);
						setState(252);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(253);
						match(T__26);
						setState(254);
						realExpr(5);
						}
						break;
					case 4:
						{
						_localctx = new RealAddContext(new RealExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_realExpr);
						setState(255);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(256);
						match(T__27);
						setState(257);
						realExpr(4);
						}
						break;
					case 5:
						{
						_localctx = new RealSubContext(new RealExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_realExpr);
						setState(258);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(259);
						match(T__25);
						setState(260);
						realExpr(3);
						}
						break;
					}
					} 
				}
				setState(265);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RealLiteralContext extends ParserRuleContext {
		public RealLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realLiteral; }
	 
		public RealLiteralContext() { }
		public void copyFrom(RealLiteralContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RealConstantContext extends RealLiteralContext {
		public TerminalNode REAL() { return getToken(InfixExprParser.REAL, 0); }
		public RealConstantContext(RealLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealConstant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealVariableContext extends RealLiteralContext {
		public RealVarContext realVar() {
			return getRuleContext(RealVarContext.class,0);
		}
		public RealVariableContext(RealLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealLiteralContext realLiteral() throws RecognitionException {
		RealLiteralContext _localctx = new RealLiteralContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_realLiteral);
		try {
			setState(268);
			switch (_input.LA(1)) {
			case T__33:
				_localctx = new RealVariableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(266);
				realVar();
				}
				break;
			case REAL:
				_localctx = new RealConstantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(267);
				match(REAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealVarContext extends ParserRuleContext {
		public Token index;
		public TerminalNode INT() { return getToken(InfixExprParser.INT, 0); }
		public RealVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realVar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitRealVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealVarContext realVar() throws RecognitionException {
		RealVarContext _localctx = new RealVarContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_realVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			match(T__33);
			{
			setState(271);
			((RealVarContext)_localctx).index = match(INT);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BvExprContext extends ParserRuleContext {
		public BvExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bvExpr; }
	 
		public BvExprContext() { }
		public void copyFrom(BvExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BitVecAndContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecAndContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecXOrContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecXOrContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecXOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSubContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecSubContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecSub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecLShRContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecLShRContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecLShR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecAddContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecAddContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecMulContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecMulContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecMul(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecAShRContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecAShRContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecAShR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SingleBitVecLiteralContext extends BvExprContext {
		public BvLiteralContext bvLiteral() {
			return getRuleContext(BvLiteralContext.class,0);
		}
		public SingleBitVecLiteralContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitSingleBitVecLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecOrContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecOrContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecUDivContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecUDivContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecUDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecConcatContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecConcatContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecURemContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecURemContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecURem(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParBitVecExprContext extends BvExprContext {
		public BvExprContext bvExpr() {
			return getRuleContext(BvExprContext.class,0);
		}
		public ParBitVecExprContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitParBitVecExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecSRemContext extends BvExprContext {
		public List<BvExprContext> bvExpr() {
			return getRuleContexts(BvExprContext.class);
		}
		public BvExprContext bvExpr(int i) {
			return getRuleContext(BvExprContext.class,i);
		}
		public BitVecSRemContext(BvExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecSRem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BvExprContext bvExpr() throws RecognitionException {
		return bvExpr(0);
	}

	private BvExprContext bvExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BvExprContext _localctx = new BvExprContext(_ctx, _parentState);
		BvExprContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_bvExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(279);
			switch (_input.LA(1)) {
			case T__0:
				{
				_localctx = new ParBitVecExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(274);
				match(T__0);
				setState(275);
				bvExpr(0);
				setState(276);
				match(T__1);
				}
				break;
			case T__38:
			case BIN:
			case HEX:
				{
				_localctx = new SingleBitVecLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(278);
				bvLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(319);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(317);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
					case 1:
						{
						_localctx = new BitVecAddContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(281);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(282);
						match(T__27);
						setState(283);
						bvExpr(14);
						}
						break;
					case 2:
						{
						_localctx = new BitVecAndContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(284);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(285);
						match(T__3);
						setState(286);
						bvExpr(13);
						}
						break;
					case 3:
						{
						_localctx = new BitVecAShRContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(287);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(288);
						match(T__34);
						setState(289);
						bvExpr(12);
						}
						break;
					case 4:
						{
						_localctx = new BitVecConcatContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(290);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(291);
						match(T__35);
						setState(292);
						bvExpr(11);
						}
						break;
					case 5:
						{
						_localctx = new BitVecLShRContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(293);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(294);
						match(T__36);
						setState(295);
						bvExpr(10);
						}
						break;
					case 6:
						{
						_localctx = new BitVecMulContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(296);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(297);
						match(T__26);
						setState(298);
						bvExpr(9);
						}
						break;
					case 7:
						{
						_localctx = new BitVecOrContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(299);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(300);
						match(T__7);
						setState(301);
						bvExpr(8);
						}
						break;
					case 8:
						{
						_localctx = new BitVecSRemContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(302);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(303);
						match(T__37);
						setState(304);
						bvExpr(7);
						}
						break;
					case 9:
						{
						_localctx = new BitVecSubContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(305);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(306);
						match(T__25);
						setState(307);
						bvExpr(6);
						}
						break;
					case 10:
						{
						_localctx = new BitVecUDivContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(308);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(309);
						match(T__29);
						setState(310);
						bvExpr(5);
						}
						break;
					case 11:
						{
						_localctx = new BitVecURemContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(311);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(312);
						match(T__31);
						setState(313);
						bvExpr(4);
						}
						break;
					case 12:
						{
						_localctx = new BitVecXOrContext(new BvExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_bvExpr);
						setState(314);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(315);
						match(T__5);
						setState(316);
						bvExpr(3);
						}
						break;
					}
					} 
				}
				setState(321);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BvLiteralContext extends ParserRuleContext {
		public BvLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bvLiteral; }
	 
		public BvLiteralContext() { }
		public void copyFrom(BvLiteralContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class HexBitVecConstantContext extends BvLiteralContext {
		public TerminalNode HEX() { return getToken(InfixExprParser.HEX, 0); }
		public HexBitVecConstantContext(BvLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitHexBitVecConstant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BinBitVecConstantContext extends BvLiteralContext {
		public TerminalNode BIN() { return getToken(InfixExprParser.BIN, 0); }
		public BinBitVecConstantContext(BvLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBinBitVecConstant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitVecVariableContext extends BvLiteralContext {
		public BvVarContext bvVar() {
			return getRuleContext(BvVarContext.class,0);
		}
		public BitVecVariableContext(BvLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBitVecVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BvLiteralContext bvLiteral() throws RecognitionException {
		BvLiteralContext _localctx = new BvLiteralContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_bvLiteral);
		try {
			setState(325);
			switch (_input.LA(1)) {
			case T__38:
				_localctx = new BitVecVariableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(322);
				bvVar();
				}
				break;
			case HEX:
				_localctx = new HexBitVecConstantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(323);
				match(HEX);
				}
				break;
			case BIN:
				_localctx = new BinBitVecConstantContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(324);
				match(BIN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BvVarContext extends ParserRuleContext {
		public Token index;
		public Token size;
		public List<TerminalNode> INT() { return getTokens(InfixExprParser.INT); }
		public TerminalNode INT(int i) {
			return getToken(InfixExprParser.INT, i);
		}
		public BvVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bvVar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof InfixExprVisitor ) return ((InfixExprVisitor<? extends T>)visitor).visitBvVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BvVarContext bvVar() throws RecognitionException {
		BvVarContext _localctx = new BvVarContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_bvVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(327);
			match(T__38);
			{
			setState(328);
			((BvVarContext)_localctx).index = match(INT);
			}
			setState(329);
			match(T__0);
			{
			setState(330);
			((BvVarContext)_localctx).size = match(INT);
			}
			setState(331);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return boolExpr_sempred((BoolExprContext)_localctx, predIndex);
		case 12:
			return intExpr_sempred((IntExprContext)_localctx, predIndex);
		case 17:
			return realExpr_sempred((RealExprContext)_localctx, predIndex);
		case 20:
			return bvExpr_sempred((BvExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean boolExpr_sempred(BoolExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean intExpr_sempred(IntExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 4);
		case 5:
			return precpred(_ctx, 3);
		case 6:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean realExpr_sempred(RealExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 6);
		case 8:
			return precpred(_ctx, 5);
		case 9:
			return precpred(_ctx, 4);
		case 10:
			return precpred(_ctx, 3);
		case 11:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean bvExpr_sempred(BvExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 12:
			return precpred(_ctx, 13);
		case 13:
			return precpred(_ctx, 12);
		case 14:
			return precpred(_ctx, 11);
		case 15:
			return precpred(_ctx, 10);
		case 16:
			return precpred(_ctx, 9);
		case 17:
			return precpred(_ctx, 8);
		case 18:
			return precpred(_ctx, 7);
		case 19:
			return precpred(_ctx, 6);
		case 20:
			return precpred(_ctx, 5);
		case 21:
			return precpred(_ctx, 4);
		case 22:
			return precpred(_ctx, 3);
		case 23:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3.\u0150\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\3\2\3"+
		"\2\3\3\3\3\3\3\3\3\5\38\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4C\n"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4M\n\4\f\4\16\4P\13\4\3\5\3\5\3\5"+
		"\3\5\3\5\5\5W\n\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5"+
		"\tw\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u0091\n\n\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5"+
		"\13\u00b3\n\13\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\5\16\u00c6\n\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u00d9\n\16"+
		"\f\16\16\16\u00dc\13\16\3\17\3\17\5\17\u00e0\n\17\3\20\3\20\3\21\3\21"+
		"\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\5\23\u00f5\n\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u0108\n\23\f\23\16\23\u010b\13"+
		"\23\3\24\3\24\5\24\u010f\n\24\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\5\26\u011a\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u0140"+
		"\n\26\f\26\16\26\u0143\13\26\3\27\3\27\3\27\5\27\u0148\n\27\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\2\6\6\32$*\31\2\4\6\b\n\f\16\20\22\24\26\30"+
		"\32\34\36 \"$&(*,.\2\b\3\2\5\b\3\2\t\13\3\2\f\r\3\2\30\31\3\2\37 \3\2"+
		"!\"\u0175\2\60\3\2\2\2\4\67\3\2\2\2\6B\3\2\2\2\bV\3\2\2\2\nX\3\2\2\2\f"+
		"Z\3\2\2\2\16\\\3\2\2\2\20v\3\2\2\2\22\u0090\3\2\2\2\24\u00b2\3\2\2\2\26"+
		"\u00b4\3\2\2\2\30\u00b6\3\2\2\2\32\u00c5\3\2\2\2\34\u00df\3\2\2\2\36\u00e1"+
		"\3\2\2\2 \u00e3\3\2\2\2\"\u00e5\3\2\2\2$\u00f4\3\2\2\2&\u010e\3\2\2\2"+
		"(\u0110\3\2\2\2*\u0119\3\2\2\2,\u0147\3\2\2\2.\u0149\3\2\2\2\60\61\5\4"+
		"\3\2\61\62\7\2\2\3\62\3\3\2\2\2\638\5\6\4\2\648\5\32\16\2\658\5$\23\2"+
		"\668\5*\26\2\67\63\3\2\2\2\67\64\3\2\2\2\67\65\3\2\2\2\67\66\3\2\2\28"+
		"\5\3\2\2\29:\b\4\1\2:;\7\3\2\2;<\5\6\4\2<=\7\4\2\2=C\3\2\2\2>?\5\16\b"+
		"\2?@\5\6\4\4@C\3\2\2\2AC\5\b\5\2B9\3\2\2\2B>\3\2\2\2BA\3\2\2\2CN\3\2\2"+
		"\2DE\f\6\2\2EF\5\n\6\2FG\5\6\4\7GM\3\2\2\2HI\f\5\2\2IJ\5\f\7\2JK\5\6\4"+
		"\6KM\3\2\2\2LD\3\2\2\2LH\3\2\2\2MP\3\2\2\2NL\3\2\2\2NO\3\2\2\2O\7\3\2"+
		"\2\2PN\3\2\2\2QW\5\22\n\2RW\5\20\t\2SW\5\24\13\2TW\5\30\r\2UW\5\26\f\2"+
		"VQ\3\2\2\2VR\3\2\2\2VS\3\2\2\2VT\3\2\2\2VU\3\2\2\2W\t\3\2\2\2XY\t\2\2"+
		"\2Y\13\3\2\2\2Z[\t\3\2\2[\r\3\2\2\2\\]\t\4\2\2]\17\3\2\2\2^_\5\32\16\2"+
		"_`\7\16\2\2`a\5\32\16\2aw\3\2\2\2bc\5\32\16\2cd\7\17\2\2de\5\32\16\2e"+
		"w\3\2\2\2fg\5\32\16\2gh\7\20\2\2hi\5\32\16\2iw\3\2\2\2jk\5\32\16\2kl\7"+
		"\21\2\2lm\5\32\16\2mw\3\2\2\2no\5\32\16\2op\7\22\2\2pq\5\32\16\2qw\3\2"+
		"\2\2rs\5\32\16\2st\7\23\2\2tu\5\32\16\2uw\3\2\2\2v^\3\2\2\2vb\3\2\2\2"+
		"vf\3\2\2\2vj\3\2\2\2vn\3\2\2\2vr\3\2\2\2w\21\3\2\2\2xy\5$\23\2yz\7\16"+
		"\2\2z{\5$\23\2{\u0091\3\2\2\2|}\5$\23\2}~\7\17\2\2~\177\5$\23\2\177\u0091"+
		"\3\2\2\2\u0080\u0081\5$\23\2\u0081\u0082\7\20\2\2\u0082\u0083\5$\23\2"+
		"\u0083\u0091\3\2\2\2\u0084\u0085\5$\23\2\u0085\u0086\7\21\2\2\u0086\u0087"+
		"\5$\23\2\u0087\u0091\3\2\2\2\u0088\u0089\5$\23\2\u0089\u008a\7\22\2\2"+
		"\u008a\u008b\5$\23\2\u008b\u0091\3\2\2\2\u008c\u008d\5$\23\2\u008d\u008e"+
		"\7\23\2\2\u008e\u008f\5$\23\2\u008f\u0091\3\2\2\2\u0090x\3\2\2\2\u0090"+
		"|\3\2\2\2\u0090\u0080\3\2\2\2\u0090\u0084\3\2\2\2\u0090\u0088\3\2\2\2"+
		"\u0090\u008c\3\2\2\2\u0091\23\3\2\2\2\u0092\u0093\5*\26\2\u0093\u0094"+
		"\7\16\2\2\u0094\u0095\5*\26\2\u0095\u00b3\3\2\2\2\u0096\u0097\5*\26\2"+
		"\u0097\u0098\7\17\2\2\u0098\u0099\5*\26\2\u0099\u00b3\3\2\2\2\u009a\u009b"+
		"\5*\26\2\u009b\u009c\7\20\2\2\u009c\u009d\5*\26\2\u009d\u00b3\3\2\2\2"+
		"\u009e\u009f\5*\26\2\u009f\u00a0\7\21\2\2\u00a0\u00a1\5*\26\2\u00a1\u00b3"+
		"\3\2\2\2\u00a2\u00a3\5*\26\2\u00a3\u00a4\7\24\2\2\u00a4\u00a5\5*\26\2"+
		"\u00a5\u00b3\3\2\2\2\u00a6\u00a7\5*\26\2\u00a7\u00a8\7\25\2\2\u00a8\u00a9"+
		"\5*\26\2\u00a9\u00b3\3\2\2\2\u00aa\u00ab\5*\26\2\u00ab\u00ac\7\26\2\2"+
		"\u00ac\u00ad\5*\26\2\u00ad\u00b3\3\2\2\2\u00ae\u00af\5*\26\2\u00af\u00b0"+
		"\7\27\2\2\u00b0\u00b1\5*\26\2\u00b1\u00b3\3\2\2\2\u00b2\u0092\3\2\2\2"+
		"\u00b2\u0096\3\2\2\2\u00b2\u009a\3\2\2\2\u00b2\u009e\3\2\2\2\u00b2\u00a2"+
		"\3\2\2\2\u00b2\u00a6\3\2\2\2\u00b2\u00aa\3\2\2\2\u00b2\u00ae\3\2\2\2\u00b3"+
		"\25\3\2\2\2\u00b4\u00b5\t\5\2\2\u00b5\27\3\2\2\2\u00b6\u00b7\7\32\2\2"+
		"\u00b7\u00b8\7*\2\2\u00b8\31\3\2\2\2\u00b9\u00ba\b\16\1\2\u00ba\u00bb"+
		"\7\3\2\2\u00bb\u00bc\5\32\16\2\u00bc\u00bd\7\4\2\2\u00bd\u00c6\3\2\2\2"+
		"\u00be\u00bf\7\33\2\2\u00bf\u00c0\5\32\16\2\u00c0\u00c1\7\4\2\2\u00c1"+
		"\u00c6\3\2\2\2\u00c2\u00c3\7\34\2\2\u00c3\u00c6\5\32\16\t\u00c4\u00c6"+
		"\5\34\17\2\u00c5\u00b9\3\2\2\2\u00c5\u00be\3\2\2\2\u00c5\u00c2\3\2\2\2"+
		"\u00c5\u00c4\3\2\2\2\u00c6\u00da\3\2\2\2\u00c7\u00c8\f\b\2\2\u00c8\u00c9"+
		"\5 \21\2\u00c9\u00ca\5\32\16\t\u00ca\u00d9\3\2\2\2\u00cb\u00cc\f\7\2\2"+
		"\u00cc\u00cd\5\36\20\2\u00cd\u00ce\5\32\16\b\u00ce\u00d9\3\2\2\2\u00cf"+
		"\u00d0\f\6\2\2\u00d0\u00d1\7\35\2\2\u00d1\u00d9\5\32\16\7\u00d2\u00d3"+
		"\f\5\2\2\u00d3\u00d4\7\36\2\2\u00d4\u00d9\5\32\16\6\u00d5\u00d6\f\4\2"+
		"\2\u00d6\u00d7\7\34\2\2\u00d7\u00d9\5\32\16\5\u00d8\u00c7\3\2\2\2\u00d8"+
		"\u00cb\3\2\2\2\u00d8\u00cf\3\2\2\2\u00d8\u00d2\3\2\2\2\u00d8\u00d5\3\2"+
		"\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db"+
		"\33\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00e0\5\"\22\2\u00de\u00e0\7*\2"+
		"\2\u00df\u00dd\3\2\2\2\u00df\u00de\3\2\2\2\u00e0\35\3\2\2\2\u00e1\u00e2"+
		"\t\6\2\2\u00e2\37\3\2\2\2\u00e3\u00e4\t\7\2\2\u00e4!\3\2\2\2\u00e5\u00e6"+
		"\7#\2\2\u00e6\u00e7\7*\2\2\u00e7#\3\2\2\2\u00e8\u00e9\b\23\1\2\u00e9\u00ea"+
		"\7\3\2\2\u00ea\u00eb\5$\23\2\u00eb\u00ec\7\4\2\2\u00ec\u00f5\3\2\2\2\u00ed"+
		"\u00ee\7\33\2\2\u00ee\u00ef\5$\23\2\u00ef\u00f0\7\4\2\2\u00f0\u00f5\3"+
		"\2\2\2\u00f1\u00f2\7\34\2\2\u00f2\u00f5\5$\23\t\u00f3\u00f5\5&\24\2\u00f4"+
		"\u00e8\3\2\2\2\u00f4\u00ed\3\2\2\2\u00f4\u00f1\3\2\2\2\u00f4\u00f3\3\2"+
		"\2\2\u00f5\u0109\3\2\2\2\u00f6\u00f7\f\b\2\2\u00f7\u00f8\5 \21\2\u00f8"+
		"\u00f9\5$\23\t\u00f9\u0108\3\2\2\2\u00fa\u00fb\f\7\2\2\u00fb\u00fc\5\36"+
		"\20\2\u00fc\u00fd\5$\23\b\u00fd\u0108\3\2\2\2\u00fe\u00ff\f\6\2\2\u00ff"+
		"\u0100\7\35\2\2\u0100\u0108\5$\23\7\u0101\u0102\f\5\2\2\u0102\u0103\7"+
		"\36\2\2\u0103\u0108\5$\23\6\u0104\u0105\f\4\2\2\u0105\u0106\7\34\2\2\u0106"+
		"\u0108\5$\23\5\u0107\u00f6\3\2\2\2\u0107\u00fa\3\2\2\2\u0107\u00fe\3\2"+
		"\2\2\u0107\u0101\3\2\2\2\u0107\u0104\3\2\2\2\u0108\u010b\3\2\2\2\u0109"+
		"\u0107\3\2\2\2\u0109\u010a\3\2\2\2\u010a%\3\2\2\2\u010b\u0109\3\2\2\2"+
		"\u010c\u010f\5(\25\2\u010d\u010f\7+\2\2\u010e\u010c\3\2\2\2\u010e\u010d"+
		"\3\2\2\2\u010f\'\3\2\2\2\u0110\u0111\7$\2\2\u0111\u0112\7*\2\2\u0112)"+
		"\3\2\2\2\u0113\u0114\b\26\1\2\u0114\u0115\7\3\2\2\u0115\u0116\5*\26\2"+
		"\u0116\u0117\7\4\2\2\u0117\u011a\3\2\2\2\u0118\u011a\5,\27\2\u0119\u0113"+
		"\3\2\2\2\u0119\u0118\3\2\2\2\u011a\u0141\3\2\2\2\u011b\u011c\f\17\2\2"+
		"\u011c\u011d\7\36\2\2\u011d\u0140\5*\26\20\u011e\u011f\f\16\2\2\u011f"+
		"\u0120\7\6\2\2\u0120\u0140\5*\26\17\u0121\u0122\f\r\2\2\u0122\u0123\7"+
		"%\2\2\u0123\u0140\5*\26\16\u0124\u0125\f\f\2\2\u0125\u0126\7&\2\2\u0126"+
		"\u0140\5*\26\r\u0127\u0128\f\13\2\2\u0128\u0129\7\'\2\2\u0129\u0140\5"+
		"*\26\f\u012a\u012b\f\n\2\2\u012b\u012c\7\35\2\2\u012c\u0140\5*\26\13\u012d"+
		"\u012e\f\t\2\2\u012e\u012f\7\n\2\2\u012f\u0140\5*\26\n\u0130\u0131\f\b"+
		"\2\2\u0131\u0132\7(\2\2\u0132\u0140\5*\26\t\u0133\u0134\f\7\2\2\u0134"+
		"\u0135\7\34\2\2\u0135\u0140\5*\26\b\u0136\u0137\f\6\2\2\u0137\u0138\7"+
		" \2\2\u0138\u0140\5*\26\7\u0139\u013a\f\5\2\2\u013a\u013b\7\"\2\2\u013b"+
		"\u0140\5*\26\6\u013c\u013d\f\4\2\2\u013d\u013e\7\b\2\2\u013e\u0140\5*"+
		"\26\5\u013f\u011b\3\2\2\2\u013f\u011e\3\2\2\2\u013f\u0121\3\2\2\2\u013f"+
		"\u0124\3\2\2\2\u013f\u0127\3\2\2\2\u013f\u012a\3\2\2\2\u013f\u012d\3\2"+
		"\2\2\u013f\u0130\3\2\2\2\u013f\u0133\3\2\2\2\u013f\u0136\3\2\2\2\u013f"+
		"\u0139\3\2\2\2\u013f\u013c\3\2\2\2\u0140\u0143\3\2\2\2\u0141\u013f\3\2"+
		"\2\2\u0141\u0142\3\2\2\2\u0142+\3\2\2\2\u0143\u0141\3\2\2\2\u0144\u0148"+
		"\5.\30\2\u0145\u0148\7-\2\2\u0146\u0148\7,\2\2\u0147\u0144\3\2\2\2\u0147"+
		"\u0145\3\2\2\2\u0147\u0146\3\2\2\2\u0148-\3\2\2\2\u0149\u014a\7)\2\2\u014a"+
		"\u014b\7*\2\2\u014b\u014c\7\3\2\2\u014c\u014d\7*\2\2\u014d\u014e\7\4\2"+
		"\2\u014e/\3\2\2\2\26\67BLNVv\u0090\u00b2\u00c5\u00d8\u00da\u00df\u00f4"+
		"\u0107\u0109\u010e\u0119\u013f\u0141\u0147";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}