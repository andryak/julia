// Generated from InfixExpr.g4 by ANTLR 4.5.3
package julia.expr.parser.infix;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link InfixExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface InfixExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(InfixExprParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolExpression}
	 * labeled alternative in {@link InfixExprParser#typedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpression(InfixExprParser.BoolExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntExpression}
	 * labeled alternative in {@link InfixExprParser#typedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntExpression(InfixExprParser.IntExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealExpression}
	 * labeled alternative in {@link InfixExprParser#typedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealExpression(InfixExprParser.RealExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecExpression}
	 * labeled alternative in {@link InfixExprParser#typedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecExpression(InfixExprParser.BitVecExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParBoolExpr}
	 * labeled alternative in {@link InfixExprParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParBoolExpr(InfixExprParser.ParBoolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SingleBoolLiteral}
	 * labeled alternative in {@link InfixExprParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleBoolLiteral(InfixExprParser.SingleBoolLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolNot}
	 * labeled alternative in {@link InfixExprParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolNot(InfixExprParser.BoolNotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolOr}
	 * labeled alternative in {@link InfixExprParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolOr(InfixExprParser.BoolOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolAnd}
	 * labeled alternative in {@link InfixExprParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAnd(InfixExprParser.BoolAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealInequality}
	 * labeled alternative in {@link InfixExprParser#boolLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealInequality(InfixExprParser.RealInequalityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntInequality}
	 * labeled alternative in {@link InfixExprParser#boolLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntInequality(InfixExprParser.IntInequalityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecInequality}
	 * labeled alternative in {@link InfixExprParser#boolLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecInequality(InfixExprParser.BitVecInequalityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolVariable}
	 * labeled alternative in {@link InfixExprParser#boolLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolVariable(InfixExprParser.BoolVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolConstant}
	 * labeled alternative in {@link InfixExprParser#boolLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolConstant(InfixExprParser.BoolConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#andSymbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndSymbol(InfixExprParser.AndSymbolContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#orSymbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrSymbol(InfixExprParser.OrSymbolContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#notSymbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotSymbol(InfixExprParser.NotSymbolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntLt}
	 * labeled alternative in {@link InfixExprParser#intIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntLt(InfixExprParser.IntLtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntGt}
	 * labeled alternative in {@link InfixExprParser#intIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntGt(InfixExprParser.IntGtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntLe}
	 * labeled alternative in {@link InfixExprParser#intIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntLe(InfixExprParser.IntLeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntGe}
	 * labeled alternative in {@link InfixExprParser#intIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntGe(InfixExprParser.IntGeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntEq}
	 * labeled alternative in {@link InfixExprParser#intIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntEq(InfixExprParser.IntEqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntDistinct}
	 * labeled alternative in {@link InfixExprParser#intIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntDistinct(InfixExprParser.IntDistinctContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealLt}
	 * labeled alternative in {@link InfixExprParser#realIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealLt(InfixExprParser.RealLtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealGt}
	 * labeled alternative in {@link InfixExprParser#realIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealGt(InfixExprParser.RealGtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealLe}
	 * labeled alternative in {@link InfixExprParser#realIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealLe(InfixExprParser.RealLeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealGe}
	 * labeled alternative in {@link InfixExprParser#realIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealGe(InfixExprParser.RealGeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealEq}
	 * labeled alternative in {@link InfixExprParser#realIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealEq(InfixExprParser.RealEqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealDistinct}
	 * labeled alternative in {@link InfixExprParser#realIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealDistinct(InfixExprParser.RealDistinctContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUlt}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUlt(InfixExprParser.BitVecUltContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUgt}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUgt(InfixExprParser.BitVecUgtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUle}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUle(InfixExprParser.BitVecUleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUge}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUge(InfixExprParser.BitVecUgeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSlt}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSlt(InfixExprParser.BitVecSltContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSgt}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSgt(InfixExprParser.BitVecSgtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSle}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSle(InfixExprParser.BitVecSleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSge}
	 * labeled alternative in {@link InfixExprParser#bvIneq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSge(InfixExprParser.BitVecSgeContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#boolConst}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolConst(InfixExprParser.BoolConstContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#boolVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolVar(InfixExprParser.BoolVarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntMul}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntMul(InfixExprParser.IntMulContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParIntExpr}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParIntExpr(InfixExprParser.ParIntExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntNeg}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntNeg(InfixExprParser.IntNegContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntSub}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntSub(InfixExprParser.IntSubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntAbs}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntAbs(InfixExprParser.IntAbsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntMod}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntMod(InfixExprParser.IntModContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntDiv}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntDiv(InfixExprParser.IntDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntAdd}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntAdd(InfixExprParser.IntAddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SingleIntLiteral}
	 * labeled alternative in {@link InfixExprParser#intExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleIntLiteral(InfixExprParser.SingleIntLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntVariable}
	 * labeled alternative in {@link InfixExprParser#intLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntVariable(InfixExprParser.IntVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntConstant}
	 * labeled alternative in {@link InfixExprParser#intLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntConstant(InfixExprParser.IntConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#divSymbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivSymbol(InfixExprParser.DivSymbolContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#modSymbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModSymbol(InfixExprParser.ModSymbolContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#intVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntVar(InfixExprParser.IntVarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParRealExpr}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParRealExpr(InfixExprParser.ParRealExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SingleRealLiteral}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleRealLiteral(InfixExprParser.SingleRealLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealDiv}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealDiv(InfixExprParser.RealDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealAdd}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealAdd(InfixExprParser.RealAddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealNeg}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealNeg(InfixExprParser.RealNegContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealSub}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealSub(InfixExprParser.RealSubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealAbs}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealAbs(InfixExprParser.RealAbsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealMod}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealMod(InfixExprParser.RealModContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealMul}
	 * labeled alternative in {@link InfixExprParser#realExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealMul(InfixExprParser.RealMulContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealVariable}
	 * labeled alternative in {@link InfixExprParser#realLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealVariable(InfixExprParser.RealVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealConstant}
	 * labeled alternative in {@link InfixExprParser#realLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealConstant(InfixExprParser.RealConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#realVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealVar(InfixExprParser.RealVarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecAnd}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecAnd(InfixExprParser.BitVecAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecXOr}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecXOr(InfixExprParser.BitVecXOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSub}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSub(InfixExprParser.BitVecSubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecLShR}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecLShR(InfixExprParser.BitVecLShRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecAdd}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecAdd(InfixExprParser.BitVecAddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecMul}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecMul(InfixExprParser.BitVecMulContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecAShR}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecAShR(InfixExprParser.BitVecAShRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SingleBitVecLiteral}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleBitVecLiteral(InfixExprParser.SingleBitVecLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecOr}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecOr(InfixExprParser.BitVecOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecUDiv}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecUDiv(InfixExprParser.BitVecUDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecConcat}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecConcat(InfixExprParser.BitVecConcatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecURem}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecURem(InfixExprParser.BitVecURemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParBitVecExpr}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParBitVecExpr(InfixExprParser.ParBitVecExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecSRem}
	 * labeled alternative in {@link InfixExprParser#bvExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSRem(InfixExprParser.BitVecSRemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecVariable}
	 * labeled alternative in {@link InfixExprParser#bvLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecVariable(InfixExprParser.BitVecVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code HexBitVecConstant}
	 * labeled alternative in {@link InfixExprParser#bvLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHexBitVecConstant(InfixExprParser.HexBitVecConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinBitVecConstant}
	 * labeled alternative in {@link InfixExprParser#bvLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinBitVecConstant(InfixExprParser.BinBitVecConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link InfixExprParser#bvVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBvVar(InfixExprParser.BvVarContext ctx);
}