package julia.expr.parser.infix;

import java.math.BigInteger;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Distinct;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Not;
import julia.expr.ast.bool.Or;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecAShR;
import julia.expr.ast.bv.BitVecAdd;
import julia.expr.ast.bv.BitVecAnd;
import julia.expr.ast.bv.BitVecConcat;
import julia.expr.ast.bv.BitVecLShR;
import julia.expr.ast.bv.BitVecMul;
import julia.expr.ast.bv.BitVecOr;
import julia.expr.ast.bv.BitVecSRem;
import julia.expr.ast.bv.BitVecSge;
import julia.expr.ast.bv.BitVecSgt;
import julia.expr.ast.bv.BitVecSle;
import julia.expr.ast.bv.BitVecSlt;
import julia.expr.ast.bv.BitVecSub;
import julia.expr.ast.bv.BitVecUDiv;
import julia.expr.ast.bv.BitVecURem;
import julia.expr.ast.bv.BitVecUge;
import julia.expr.ast.bv.BitVecUgt;
import julia.expr.ast.bv.BitVecUle;
import julia.expr.ast.bv.BitVecUlt;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.bv.BitVecXOr;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntAbs;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.ints.IntDiv;
import julia.expr.ast.ints.IntGe;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLe;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntMod;
import julia.expr.ast.ints.IntMul;
import julia.expr.ast.ints.IntNeg;
import julia.expr.ast.ints.IntSub;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealAbs;
import julia.expr.ast.reals.RealAdd;
import julia.expr.ast.reals.RealDiv;
import julia.expr.ast.reals.RealGe;
import julia.expr.ast.reals.RealGt;
import julia.expr.ast.reals.RealLe;
import julia.expr.ast.reals.RealLt;
import julia.expr.ast.reals.RealMul;
import julia.expr.ast.reals.RealNeg;
import julia.expr.ast.reals.RealSub;
import julia.expr.ast.reals.RealVal;
import julia.expr.parser.infix.InfixExprParser.BinBitVecConstantContext;
import julia.expr.parser.infix.InfixExprParser.BitVecAShRContext;
import julia.expr.parser.infix.InfixExprParser.BitVecAddContext;
import julia.expr.parser.infix.InfixExprParser.BitVecAndContext;
import julia.expr.parser.infix.InfixExprParser.BitVecConcatContext;
import julia.expr.parser.infix.InfixExprParser.BitVecExpressionContext;
import julia.expr.parser.infix.InfixExprParser.BitVecInequalityContext;
import julia.expr.parser.infix.InfixExprParser.BitVecLShRContext;
import julia.expr.parser.infix.InfixExprParser.BitVecMulContext;
import julia.expr.parser.infix.InfixExprParser.BitVecOrContext;
import julia.expr.parser.infix.InfixExprParser.BitVecSRemContext;
import julia.expr.parser.infix.InfixExprParser.BitVecSgeContext;
import julia.expr.parser.infix.InfixExprParser.BitVecSgtContext;
import julia.expr.parser.infix.InfixExprParser.BitVecSleContext;
import julia.expr.parser.infix.InfixExprParser.BitVecSltContext;
import julia.expr.parser.infix.InfixExprParser.BitVecSubContext;
import julia.expr.parser.infix.InfixExprParser.BitVecUDivContext;
import julia.expr.parser.infix.InfixExprParser.BitVecURemContext;
import julia.expr.parser.infix.InfixExprParser.BitVecUgeContext;
import julia.expr.parser.infix.InfixExprParser.BitVecUgtContext;
import julia.expr.parser.infix.InfixExprParser.BitVecUleContext;
import julia.expr.parser.infix.InfixExprParser.BitVecUltContext;
import julia.expr.parser.infix.InfixExprParser.BitVecVariableContext;
import julia.expr.parser.infix.InfixExprParser.BitVecXOrContext;
import julia.expr.parser.infix.InfixExprParser.BoolAndContext;
import julia.expr.parser.infix.InfixExprParser.BoolConstContext;
import julia.expr.parser.infix.InfixExprParser.BoolConstantContext;
import julia.expr.parser.infix.InfixExprParser.BoolExpressionContext;
import julia.expr.parser.infix.InfixExprParser.BoolNotContext;
import julia.expr.parser.infix.InfixExprParser.BoolOrContext;
import julia.expr.parser.infix.InfixExprParser.BoolVarContext;
import julia.expr.parser.infix.InfixExprParser.BoolVariableContext;
import julia.expr.parser.infix.InfixExprParser.BvVarContext;
import julia.expr.parser.infix.InfixExprParser.ExprContext;
import julia.expr.parser.infix.InfixExprParser.HexBitVecConstantContext;
import julia.expr.parser.infix.InfixExprParser.IntAbsContext;
import julia.expr.parser.infix.InfixExprParser.IntAddContext;
import julia.expr.parser.infix.InfixExprParser.IntConstantContext;
import julia.expr.parser.infix.InfixExprParser.IntDistinctContext;
import julia.expr.parser.infix.InfixExprParser.IntDivContext;
import julia.expr.parser.infix.InfixExprParser.IntEqContext;
import julia.expr.parser.infix.InfixExprParser.IntExpressionContext;
import julia.expr.parser.infix.InfixExprParser.IntGeContext;
import julia.expr.parser.infix.InfixExprParser.IntGtContext;
import julia.expr.parser.infix.InfixExprParser.IntInequalityContext;
import julia.expr.parser.infix.InfixExprParser.IntLeContext;
import julia.expr.parser.infix.InfixExprParser.IntLtContext;
import julia.expr.parser.infix.InfixExprParser.IntModContext;
import julia.expr.parser.infix.InfixExprParser.IntMulContext;
import julia.expr.parser.infix.InfixExprParser.IntNegContext;
import julia.expr.parser.infix.InfixExprParser.IntSubContext;
import julia.expr.parser.infix.InfixExprParser.IntVarContext;
import julia.expr.parser.infix.InfixExprParser.IntVariableContext;
import julia.expr.parser.infix.InfixExprParser.ParBitVecExprContext;
import julia.expr.parser.infix.InfixExprParser.ParBoolExprContext;
import julia.expr.parser.infix.InfixExprParser.ParIntExprContext;
import julia.expr.parser.infix.InfixExprParser.ParRealExprContext;
import julia.expr.parser.infix.InfixExprParser.RealAbsContext;
import julia.expr.parser.infix.InfixExprParser.RealAddContext;
import julia.expr.parser.infix.InfixExprParser.RealConstantContext;
import julia.expr.parser.infix.InfixExprParser.RealDistinctContext;
import julia.expr.parser.infix.InfixExprParser.RealDivContext;
import julia.expr.parser.infix.InfixExprParser.RealEqContext;
import julia.expr.parser.infix.InfixExprParser.RealExpressionContext;
import julia.expr.parser.infix.InfixExprParser.RealGeContext;
import julia.expr.parser.infix.InfixExprParser.RealGtContext;
import julia.expr.parser.infix.InfixExprParser.RealLeContext;
import julia.expr.parser.infix.InfixExprParser.RealLtContext;
import julia.expr.parser.infix.InfixExprParser.RealMulContext;
import julia.expr.parser.infix.InfixExprParser.RealNegContext;
import julia.expr.parser.infix.InfixExprParser.RealSubContext;
import julia.expr.parser.infix.InfixExprParser.RealVarContext;
import julia.expr.parser.infix.InfixExprParser.RealVariableContext;
import julia.expr.parser.infix.InfixExprParser.SingleBitVecLiteralContext;
import julia.expr.parser.infix.InfixExprParser.SingleBoolLiteralContext;
import julia.expr.parser.infix.InfixExprParser.SingleIntLiteralContext;
import julia.expr.utils.BigRational;
import julia.expr.utils.BitVector;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Builder to create expressions from strings in infix notation.
 * 
 * @see grammars/InfixExpr.g4
 */
public class InfixExprBuilder extends InfixExprBaseVisitor<IExpr> {
   @Override
   public IExpr visit(ParseTree tree) {
      return super.visit(tree);
   }

   @Override
   public IExpr visitExpr(ExprContext ctx) {
      return super.visit(ctx.typedExpr());
   }

   @Override
   public IExpr visitBoolExpression(BoolExpressionContext ctx) {
      return super.visit(ctx.boolExpr());
   }

   @Override
   public IExpr visitIntExpression(IntExpressionContext ctx) {
      return super.visit(ctx.intExpr());
   }

   @Override
   public IExpr visitRealExpression(RealExpressionContext ctx) {
      return super.visit(ctx.realExpr());
   }

   @Override
   public IExpr visitBitVecExpression(BitVecExpressionContext ctx) {
      return super.visit(ctx.bvExpr());
   }

   @Override
   public IExpr visitParBoolExpr(ParBoolExprContext ctx) {
      return super.visit(ctx.boolExpr());
   }

   @Override
   public IExpr visitParIntExpr(ParIntExprContext ctx) {
      return super.visit(ctx.intExpr());
   }

   @Override
   public IExpr visitParRealExpr(ParRealExprContext ctx) {
      return super.visit(ctx.realExpr());
   }

   @Override
   public IExpr visitParBitVecExpr(ParBitVecExprContext ctx) {
      return super.visit(ctx.bvExpr());
   }

   @Override
   public IExpr visitBoolConstant(BoolConstantContext ctx) {
      return super.visit(ctx.boolConst());
   }

   @Override
   public IExpr visitBoolNot(BoolNotContext ctx) {
      return Not.create(super.visit(ctx.boolExpr()));
   }

   @Override
   public IExpr visitIntInequality(IntInequalityContext ctx) {
      return super.visit(ctx.intIneq());
   }

   @Override
   public IExpr visitBitVecInequality(BitVecInequalityContext ctx) {
      return super.visit(ctx.bvIneq());
   }

   @Override
   public IExpr visitBoolOr(BoolOrContext ctx) {
      IExpr lhs = super.visit(ctx.boolExpr(0));
      IExpr rhs = super.visit(ctx.boolExpr(1));
      return Or.create(lhs, rhs);
   }

   @Override
   public IExpr visitBoolAnd(BoolAndContext ctx) {
      IExpr lhs = super.visit(ctx.boolExpr(0));
      IExpr rhs = super.visit(ctx.boolExpr(1));
      return And.create(lhs, rhs);
   }

   @Override
   public IExpr visitBoolVariable(BoolVariableContext ctx) {
      return super.visit(ctx.boolVar());
   }

   @Override
   public IExpr visitIntLt(IntLtContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return IntLt.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntGt(IntGtContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return IntGt.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntLe(IntLeContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return IntLe.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntGe(IntGeContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return IntGe.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntEq(IntEqContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return Equal.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntDistinct(IntDistinctContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return Distinct.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealLt(RealLtContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return RealLt.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealGt(RealGtContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return RealGt.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealLe(RealLeContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return RealLe.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealGe(RealGeContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return RealGe.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealEq(RealEqContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return Equal.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealDistinct(RealDistinctContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return Distinct.create(lhs, rhs);
   }

   @Override
   public IExpr visitBoolConst(BoolConstContext ctx) {
      if (ctx.getText().equals("true")) {
         return BoolVal.create(true);
      } else {
         return BoolVal.create(false);
      }
   }

   @Override
   public IExpr visitBoolVar(BoolVarContext ctx) {
      return Bool.create(Integer.parseInt(ctx.INT().getText()));
   }

   @Override
   public IExpr visitIntAdd(IntAddContext ctx) {
      IExpr lhs = super.visit(ctx.intExpr(0));
      IExpr rhs = super.visit(ctx.intExpr(1));
      return IntAdd.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntDiv(IntDivContext ctx) {
      IExpr lhs = super.visit(ctx.intExpr(0));
      IExpr rhs = super.visit(ctx.intExpr(1));
      return IntDiv.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntNeg(IntNegContext ctx) {
      return IntNeg.create(super.visit(ctx.intExpr()));
   }

   @Override
   public IExpr visitIntSub(IntSubContext ctx) {
      IExpr lhs = super.visit(ctx.intExpr(0));
      IExpr rhs = super.visit(ctx.intExpr(1));
      return IntSub.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntAbs(IntAbsContext ctx) {
      return IntAbs.create(super.visit(ctx.intExpr()));
   }

   @Override
   public IExpr visitIntMod(IntModContext ctx) {
      IExpr lhs = super.visit(ctx.intExpr(0));
      IExpr rhs = super.visit(ctx.intExpr(1));
      return IntMod.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntMul(IntMulContext ctx) {
      IExpr lhs = super.visit(ctx.intExpr(0));
      IExpr rhs = super.visit(ctx.intExpr(1));
      return IntMul.create(lhs, rhs);
   }

   @Override
   public IExpr visitIntConstant(IntConstantContext ctx) {
      return IntVal.create(new BigInteger(ctx.INT().getText()));
   }

   @Override
   public IExpr visitIntVariable(IntVariableContext ctx) {
      return super.visit(ctx.intVar());
   }

   @Override
   public IExpr visitIntVar(IntVarContext ctx) {
      return Int.create(Integer.parseInt(ctx.INT().getText()));
   }

   @Override
   public IExpr visitRealAdd(RealAddContext ctx) {
      IExpr lhs = super.visit(ctx.realExpr(0));
      IExpr rhs = super.visit(ctx.realExpr(1));
      return RealAdd.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealDiv(RealDivContext ctx) {
      IExpr lhs = super.visit(ctx.realExpr(0));
      IExpr rhs = super.visit(ctx.realExpr(1));
      return RealDiv.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealNeg(RealNegContext ctx) {
      return RealNeg.create(super.visit(ctx.realExpr()));
   }

   @Override
   public IExpr visitRealSub(RealSubContext ctx) {
      IExpr lhs = super.visit(ctx.realExpr(0));
      IExpr rhs = super.visit(ctx.realExpr(1));
      return RealSub.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealAbs(RealAbsContext ctx) {
      return RealAbs.create(super.visit(ctx.realExpr()));
   }

   @Override
   public IExpr visitRealMul(RealMulContext ctx) {
      IExpr lhs = super.visit(ctx.realExpr(0));
      IExpr rhs = super.visit(ctx.realExpr(1));
      return RealMul.create(lhs, rhs);
   }

   @Override
   public IExpr visitRealConstant(RealConstantContext ctx) {
      String s = ctx.REAL().getText();
      String[] tokens = s.split("\\.");

      if (tokens.length == 1) {
         return RealVal.create(BigRational.create(tokens[0]));
      } else {
         BigInteger p = new BigInteger(tokens[0] + tokens[1]);
         BigInteger q = BigInteger.TEN.pow(tokens[1].length());
         return RealVal.create(BigRational.create(p, q));
      }
   }

   @Override
   public IExpr visitRealVariable(RealVariableContext ctx) {
      return super.visit(ctx.realVar());
   }

   @Override
   public IExpr visitRealVar(RealVarContext ctx) {
      return Real.create(Integer.parseInt(ctx.INT().getText()));
   }

   @Override
   public IExpr visitBitVecUlt(BitVecUltContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecUlt.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUle(BitVecUleContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecUle.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUgt(BitVecUgtContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecUgt.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUge(BitVecUgeContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecUge.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSlt(BitVecSltContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecSlt.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSle(BitVecSleContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecSle.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSgt(BitVecSgtContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecSgt.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSge(BitVecSgeContext ctx) {
      IExpr lhs = super.visit(ctx.lhs);
      IExpr rhs = super.visit(ctx.rhs);
      return BitVecSge.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecAdd(BitVecAddContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecAdd.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecAnd(BitVecAndContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecAnd.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecAShR(BitVecAShRContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecAShR.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecConcat(BitVecConcatContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecConcat.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecLShR(BitVecLShRContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecLShR.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecMul(BitVecMulContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecMul.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecOr(BitVecOrContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecOr.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSRem(BitVecSRemContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecSRem.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecSub(BitVecSubContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecSub.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecUDiv(BitVecUDivContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecUDiv.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecURem(BitVecURemContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecURem.create(lhs, rhs);
   }

   @Override
   public IExpr visitBitVecXOr(BitVecXOrContext ctx) {
      IExpr lhs = super.visit(ctx.bvExpr(0));
      IExpr rhs = super.visit(ctx.bvExpr(1));
      return BitVecXOr.create(lhs, rhs);
   }

   @Override
   public IExpr visitBinBitVecConstant(BinBitVecConstantContext ctx) {
      return BitVecVal.create(BitVector.valueOf(ctx.BIN().getText()));
   }

   @Override
   public IExpr visitHexBitVecConstant(HexBitVecConstantContext ctx) {
      return BitVecVal.create(BitVector.valueOf(ctx.HEX().getText()));
   }

   @Override
   public IExpr visitBitVecVariable(BitVecVariableContext ctx) {
      return super.visit(ctx.bvVar());
   }

   @Override
   public IExpr visitBvVar(BvVarContext ctx) {
      return BitVec.create(
            Integer.parseInt(ctx.index.getText()),
            Long.parseLong(ctx.size.getText()));
   }

   @Override
   public IExpr visitSingleBoolLiteral(SingleBoolLiteralContext ctx) {
      return super.visit(ctx.boolLiteral());
   }

   @Override
   public IExpr visitSingleIntLiteral(SingleIntLiteralContext ctx) {
      return super.visit(ctx.intLiteral());
   }

   @Override
   public IExpr visitSingleBitVecLiteral(SingleBitVecLiteralContext ctx) {
      return super.visit(ctx.bvLiteral());
   }
}
