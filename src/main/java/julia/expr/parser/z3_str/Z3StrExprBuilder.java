package julia.expr.parser.z3_str;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Imply;
import julia.expr.ast.bool.Not;
import julia.expr.ast.bool.Or;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.ints.IntDiv;
import julia.expr.ast.ints.IntGe;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLe;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntNeg;
import julia.expr.ast.ints.IntSub;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.mixed.If;
import julia.expr.ast.re.Match;
import julia.expr.ast.re.ReConcat;
import julia.expr.ast.re.ReStar;
import julia.expr.ast.re.ReUnion;
import julia.expr.ast.re.StrToRe;
import julia.expr.ast.str.StrConcat;
import julia.expr.ast.str.StrContains;
import julia.expr.ast.str.StrIndexOf;
import julia.expr.ast.str.StrLastIndexOf;
import julia.expr.ast.str.StrLen;
import julia.expr.ast.str.StrPrefixOf;
import julia.expr.ast.str.StrReplace;
import julia.expr.ast.str.StrSuffixOf;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.str.SubStr;
import julia.expr.parser.z3_str.Z3StrExprParser.AssertionContext;
import julia.expr.parser.z3_str.Z3StrExprParser.DeclarationContext;
import julia.expr.parser.z3_str.Z3StrExprParser.ExprContext;
import julia.expr.sort.BoolSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;
import julia.utils.Exprs;
import julia.utils.StringUtils;
import julia.utils.VarRenamer;
import julia.utils.Z3StrUtils;

/**
 * Builder to create expressions from strings in SMT-LIB v2 notation.
 * 
 * @see grammars/Z3StrExpr.g4
 */
public class Z3StrExprBuilder extends Z3StrExprBaseVisitor<Object> {
   private VarRenamer renamer;
   private Map<String, ExprSort> declarations;

   /**
    * Creates a new builder of expressions from SMT-LIB v2 strings.
    */
   public Z3StrExprBuilder() {
      this.renamer = new VarRenamer();
      this.declarations = new HashMap<>();
   }

   @Override
   public IExpr visitCmd(Z3StrExprParser.CmdContext ctx) {
      // Parse all declarations first.
      List<DeclarationContext> decls = ctx.declaration();
      for (final DeclarationContext decl : decls) {
         super.visit(decl);
      }

      // Then, parse all assertions and build the resulting expression.
      List<IExpr> asserts = new ArrayList<>();
      for (final AssertionContext assertion : ctx.assertion()) {
         asserts.add((IExpr) super.visit(assertion));
      }
      return And.create(asserts).compress();
   }

   @Override
   public Void visitDeclaration(Z3StrExprParser.DeclarationContext ctx) {
      String name = ctx.name.getText();
      String sort = ctx.sort.getText();

      if (sort.equals("Bool")) {
         this.declarations.put(name, BoolSort.create());
      } else if (sort.equals("Int")) {
         this.declarations.put(name, IntSort.create());
      } else if (sort.equals("String")) {
         this.declarations.put(name, StrSort.create());
      } else {
         throw new IllegalArgumentException(String.format("Unrecognized sort: %s.", sort));
      }

      return null;
   }

   @Override
   public IExpr visitAssertion(Z3StrExprParser.AssertionContext ctx) {
      return (IExpr) super.visit(ctx.expr());
   }

   @Override
   public IExpr visitEqual(Z3StrExprParser.EqualContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return Equal.create(lhs, rhs);
   }

   @Override
   public IExpr visitAnd(Z3StrExprParser.AndContext ctx) {
      List<IExpr> parts = new ArrayList<>();
      for (final ExprContext exprCtx : ctx.expr()) {
         parts.add((IExpr) super.visit(exprCtx));
      }
      return And.create(parts);
   }

   @Override
   public IExpr visitOr(Z3StrExprParser.OrContext ctx) {
      List<IExpr> parts = new ArrayList<>();
      for (final ExprContext exprCtx : ctx.expr()) {
         parts.add((IExpr) super.visit(exprCtx));
      }
      return Or.create(parts);
   }

   @Override
   public IExpr visitNot(Z3StrExprParser.NotContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      return Not.create(expr);
   }

   @Override
   public IExpr visitIf(Z3StrExprParser.IfContext ctx) {
      IExpr c = (IExpr) super.visit(ctx.condExpr);
      IExpr t = (IExpr) super.visit(ctx.thenExpr);
      IExpr e = (IExpr) super.visit(ctx.elseExpr);
      return If.create(c, t, e);
   }

   @Override
   public IExpr visitImply(Z3StrExprParser.ImplyContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return Imply.create(lhs, rhs);
   }

   @Override
   public IExpr visitConcat(Z3StrExprParser.ConcatContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return StrConcat.create(lhs, rhs);
   }

   @Override
   public IExpr visitContains(Z3StrExprParser.ContainsContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return StrContains.create(lhs, rhs);
   }

   @Override
   public IExpr visitEndsWith(Z3StrExprParser.EndsWithContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return StrSuffixOf.create(rhs, lhs);
   }

   @Override
   public IExpr visitIndexof(Z3StrExprParser.IndexofContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return StrIndexOf.create(lhs, rhs);
   }

   @Override
   public IExpr visitLength(Z3StrExprParser.LengthContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      return StrLen.create(expr);
   }

   @Override
   public IExpr visitReplace(Z3StrExprParser.ReplaceContext ctx) {
      IExpr target = (IExpr) super.visit(ctx.target);
      IExpr pattern = (IExpr) super.visit(ctx.pattern);
      IExpr repl = (IExpr) super.visit(ctx.repl);
      return StrReplace.create(target, pattern, repl);
   }

   @Override
   public IExpr visitStartsWith(Z3StrExprParser.StartsWithContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return StrPrefixOf.create(rhs, lhs);
   }

   @Override
   public IExpr visitSubstring(Z3StrExprParser.SubstringContext ctx) {
      IExpr target = (IExpr) super.visit(ctx.target);
      IExpr from = (IExpr) super.visit(ctx.from);
      IExpr to = (IExpr) super.visit(ctx.to);
      return SubStr.create(target, from, to);
   }

   @Override
   public IExpr visitLastIndexof(Z3StrExprParser.LastIndexofContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return StrLastIndexOf.create(lhs, rhs);
   }

   @Override
   public IExpr visitRegexConcat(Z3StrExprParser.RegexConcatContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return ReConcat.create(lhs, rhs);
   }

   @Override
   public IExpr visitRegexIn(Z3StrExprParser.RegexInContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return Match.create(lhs, rhs);
   }

   @Override
   public IExpr visitRegexStar(Z3StrExprParser.RegexStarContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      return ReStar.create(expr);
   }

   @Override
   public IExpr visitRegexUnion(Z3StrExprParser.RegexUnionContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return ReUnion.create(lhs, rhs);
   }

   @Override
   public IExpr visitStr2Reg(Z3StrExprParser.Str2RegContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      return StrToRe.create(expr);
   }

   @Override
   public IExpr visitAdd(Z3StrExprParser.AddContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntAdd.create(lhs, rhs);
   }

   @Override
   public IExpr visitNeg(Z3StrExprParser.NegContext ctx) {
      IExpr expr = (IExpr) super.visit(ctx.expr());
      return IntNeg.create(expr);
   }

   @Override
   public IExpr visitSub(Z3StrExprParser.SubContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntSub.create(lhs, rhs);
   }

   @Override
   public IExpr visitDiv(Z3StrExprParser.DivContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntDiv.create(lhs, rhs);
   }

   @Override
   public IExpr visitLt(Z3StrExprParser.LtContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntLt.create(lhs, rhs);
   }

   @Override
   public IExpr visitGt(Z3StrExprParser.GtContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntGt.create(lhs, rhs);
   }

   @Override
   public IExpr visitLe(Z3StrExprParser.LeContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntLe.create(lhs, rhs);
   }

   @Override
   public IExpr visitGe(Z3StrExprParser.GeContext ctx) {
      IExpr lhs = (IExpr) super.visit(ctx.lhs);
      IExpr rhs = (IExpr) super.visit(ctx.rhs);
      return IntGe.create(lhs, rhs);
   }

   @Override
   public BoolVal visitBoolVal(Z3StrExprParser.BoolValContext ctx) {
      if (ctx.getText().equals("true")) {
         return BoolVal.TRUE;
      }
      return BoolVal.FALSE;
   }

   @Override
   public IntVal visitIntVal(Z3StrExprParser.IntValContext ctx) {
      return IntVal.create(ctx.getText());
   }

   @Override
   public StrVal visitStrVal(Z3StrExprParser.StrValContext ctx) {
      return StrVal.create(StringUtils.unquote(Z3StrUtils.unescape(ctx.getText())));
   }

   @Override
   public IExpr visitVariable(Z3StrExprParser.VariableContext ctx) {
      String name = ctx.getText();
      ExprSort sort = this.declarations.get(name);
      Integer index = this.renamer.getIndexFor(name);
      return Exprs.mkConst(index, sort);
   }
}
