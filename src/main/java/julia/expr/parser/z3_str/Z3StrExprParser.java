// Generated from Z3StrExpr.g4 by ANTLR 4.5.3
package julia.expr.parser.z3_str;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Z3StrExprParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, BOOL=37, INT=38, ID=39, 
		STR=40, WS=41;
	public static final int
		RULE_cmd = 0, RULE_declaration = 1, RULE_assertion = 2, RULE_expr = 3, 
		RULE_exprSort = 4;
	public static final String[] ruleNames = {
		"cmd", "declaration", "assertion", "expr", "exprSort"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "'declare-variable'", "')'", "'declare-fun'", "'()'", "'assert'", 
		"'='", "'and'", "'or'", "'not'", "'ite'", "'implies'", "'Concat'", "'Contains'", 
		"'EndsWith'", "'Indexof'", "'Length'", "'Replace'", "'StartsWith'", "'Substring'", 
		"'LastIndexof'", "'RegexConcat'", "'RegexIn'", "'RegexStar'", "'RegexUnion'", 
		"'Str2Reg'", "'+'", "'-'", "'div'", "'>'", "'<'", "'>='", "'<='", "'Bool'", 
		"'Int'", "'String'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, "BOOL", "INT", "ID", "STR", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Z3StrExpr.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public Z3StrExprParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class CmdContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(Z3StrExprParser.EOF, 0); }
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public List<AssertionContext> assertion() {
			return getRuleContexts(AssertionContext.class);
		}
		public AssertionContext assertion(int i) {
			return getRuleContext(AssertionContext.class,i);
		}
		public CmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cmd; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CmdContext cmd() throws RecognitionException {
		CmdContext _localctx = new CmdContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_cmd);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(13);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(10);
					declaration();
					}
					} 
				}
				setState(15);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(19);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(16);
				assertion();
				}
				}
				setState(21);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(22);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Token name;
		public ExprSortContext sort;
		public TerminalNode ID() { return getToken(Z3StrExprParser.ID, 0); }
		public ExprSortContext exprSort() {
			return getRuleContext(ExprSortContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		try {
			setState(37);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(24);
				match(T__0);
				setState(25);
				match(T__1);
				{
				setState(26);
				((DeclarationContext)_localctx).name = match(ID);
				}
				{
				setState(27);
				((DeclarationContext)_localctx).sort = exprSort();
				}
				setState(28);
				match(T__2);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(30);
				match(T__0);
				setState(31);
				match(T__3);
				{
				setState(32);
				((DeclarationContext)_localctx).name = match(ID);
				}
				setState(33);
				match(T__4);
				{
				setState(34);
				((DeclarationContext)_localctx).sort = exprSort();
				}
				setState(35);
				match(T__2);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssertionContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AssertionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assertion; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitAssertion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssertionContext assertion() throws RecognitionException {
		AssertionContext _localctx = new AssertionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_assertion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(39);
			match(T__0);
			setState(40);
			match(T__5);
			setState(41);
			expr();
			setState(42);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AddContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AddContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public OrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableContext extends ExprContext {
		public TerminalNode ID() { return getToken(Z3StrExprParser.ID, 0); }
		public VariableContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IndexofContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IndexofContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitIndexof(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntValContext extends ExprContext {
		public TerminalNode INT() { return getToken(Z3StrExprParser.INT, 0); }
		public IntValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitIntVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StartsWithContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public StartsWithContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitStartsWith(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GtContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public GtContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitGt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImplyContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ImplyContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitImply(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConcatContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ConcatContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RegexStarContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public RegexStarContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitRegexStar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LeContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitLe(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LastIndexofContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LastIndexofContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitLastIndexof(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RegexInContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RegexInContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitRegexIn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubstringContext extends ExprContext {
		public ExprContext target;
		public ExprContext from;
		public ExprContext to;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public SubstringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitSubstring(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReplaceContext extends ExprContext {
		public ExprContext target;
		public ExprContext pattern;
		public ExprContext repl;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ReplaceContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitReplace(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public SubContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitSub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolValContext extends ExprContext {
		public TerminalNode BOOL() { return getToken(Z3StrExprParser.BOOL, 0); }
		public BoolValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitBoolVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Str2RegContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Str2RegContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitStr2Reg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LtContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LtContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitLt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StrValContext extends ExprContext {
		public TerminalNode STR() { return getToken(Z3StrExprParser.STR, 0); }
		public StrValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitStrVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public DivContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RegexConcatContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RegexConcatContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitRegexConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NegContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public NegContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitNeg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public NotContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitNot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RegexUnionContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RegexUnionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitRegexUnion(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LengthContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public LengthContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitLength(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AndContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContainsContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ContainsContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitContains(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfContext extends ExprContext {
		public ExprContext condExpr;
		public ExprContext thenExpr;
		public ExprContext elseExpr;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IfContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitIf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EndsWithContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EndsWithContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitEndsWith(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GeContext extends ExprContext {
		public ExprContext lhs;
		public ExprContext rhs;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public GeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitGe(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_expr);
		int _la;
		try {
			setState(220);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new EqualContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(44);
				match(T__0);
				setState(45);
				match(T__6);
				{
				setState(46);
				((EqualContext)_localctx).lhs = expr();
				}
				{
				setState(47);
				((EqualContext)_localctx).rhs = expr();
				}
				setState(48);
				match(T__2);
				}
				break;
			case 2:
				_localctx = new AndContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(50);
				match(T__0);
				setState(51);
				match(T__7);
				setState(53); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(52);
					expr();
					}
					}
					setState(55); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << BOOL) | (1L << INT) | (1L << ID) | (1L << STR))) != 0) );
				setState(57);
				match(T__2);
				}
				break;
			case 3:
				_localctx = new OrContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(59);
				match(T__0);
				setState(60);
				match(T__8);
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(61);
					expr();
					}
					}
					setState(64); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << BOOL) | (1L << INT) | (1L << ID) | (1L << STR))) != 0) );
				setState(66);
				match(T__2);
				}
				break;
			case 4:
				_localctx = new NotContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(68);
				match(T__0);
				setState(69);
				match(T__9);
				setState(70);
				expr();
				setState(71);
				match(T__2);
				}
				break;
			case 5:
				_localctx = new IfContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(73);
				match(T__0);
				setState(74);
				match(T__10);
				{
				setState(75);
				((IfContext)_localctx).condExpr = expr();
				}
				{
				setState(76);
				((IfContext)_localctx).thenExpr = expr();
				}
				{
				setState(77);
				((IfContext)_localctx).elseExpr = expr();
				}
				setState(78);
				match(T__2);
				}
				break;
			case 6:
				_localctx = new ImplyContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(80);
				match(T__0);
				setState(81);
				match(T__11);
				{
				setState(82);
				((ImplyContext)_localctx).lhs = expr();
				}
				{
				setState(83);
				((ImplyContext)_localctx).rhs = expr();
				}
				setState(84);
				match(T__2);
				}
				break;
			case 7:
				_localctx = new ConcatContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(86);
				match(T__0);
				setState(87);
				match(T__12);
				{
				setState(88);
				((ConcatContext)_localctx).lhs = expr();
				}
				{
				setState(89);
				((ConcatContext)_localctx).rhs = expr();
				}
				setState(90);
				match(T__2);
				}
				break;
			case 8:
				_localctx = new ContainsContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(92);
				match(T__0);
				setState(93);
				match(T__13);
				{
				setState(94);
				((ContainsContext)_localctx).lhs = expr();
				}
				{
				setState(95);
				((ContainsContext)_localctx).rhs = expr();
				}
				setState(96);
				match(T__2);
				}
				break;
			case 9:
				_localctx = new EndsWithContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(98);
				match(T__0);
				setState(99);
				match(T__14);
				{
				setState(100);
				((EndsWithContext)_localctx).lhs = expr();
				}
				{
				setState(101);
				((EndsWithContext)_localctx).rhs = expr();
				}
				setState(102);
				match(T__2);
				}
				break;
			case 10:
				_localctx = new IndexofContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(104);
				match(T__0);
				setState(105);
				match(T__15);
				{
				setState(106);
				((IndexofContext)_localctx).lhs = expr();
				}
				{
				setState(107);
				((IndexofContext)_localctx).rhs = expr();
				}
				setState(108);
				match(T__2);
				}
				break;
			case 11:
				_localctx = new LengthContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(110);
				match(T__0);
				setState(111);
				match(T__16);
				setState(112);
				expr();
				setState(113);
				match(T__2);
				}
				break;
			case 12:
				_localctx = new ReplaceContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(115);
				match(T__0);
				setState(116);
				match(T__17);
				{
				setState(117);
				((ReplaceContext)_localctx).target = expr();
				}
				{
				setState(118);
				((ReplaceContext)_localctx).pattern = expr();
				}
				{
				setState(119);
				((ReplaceContext)_localctx).repl = expr();
				}
				setState(120);
				match(T__2);
				}
				break;
			case 13:
				_localctx = new StartsWithContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(122);
				match(T__0);
				setState(123);
				match(T__18);
				{
				setState(124);
				((StartsWithContext)_localctx).lhs = expr();
				}
				{
				setState(125);
				((StartsWithContext)_localctx).rhs = expr();
				}
				setState(126);
				match(T__2);
				}
				break;
			case 14:
				_localctx = new SubstringContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(128);
				match(T__0);
				setState(129);
				match(T__19);
				{
				setState(130);
				((SubstringContext)_localctx).target = expr();
				}
				{
				setState(131);
				((SubstringContext)_localctx).from = expr();
				}
				{
				setState(132);
				((SubstringContext)_localctx).to = expr();
				}
				setState(133);
				match(T__2);
				}
				break;
			case 15:
				_localctx = new LastIndexofContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(135);
				match(T__0);
				setState(136);
				match(T__20);
				{
				setState(137);
				((LastIndexofContext)_localctx).lhs = expr();
				}
				{
				setState(138);
				((LastIndexofContext)_localctx).rhs = expr();
				}
				setState(139);
				match(T__2);
				}
				break;
			case 16:
				_localctx = new RegexConcatContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(141);
				match(T__0);
				setState(142);
				match(T__21);
				{
				setState(143);
				((RegexConcatContext)_localctx).lhs = expr();
				}
				{
				setState(144);
				((RegexConcatContext)_localctx).rhs = expr();
				}
				setState(145);
				match(T__2);
				}
				break;
			case 17:
				_localctx = new RegexInContext(_localctx);
				enterOuterAlt(_localctx, 17);
				{
				setState(147);
				match(T__0);
				setState(148);
				match(T__22);
				{
				setState(149);
				((RegexInContext)_localctx).lhs = expr();
				}
				{
				setState(150);
				((RegexInContext)_localctx).rhs = expr();
				}
				setState(151);
				match(T__2);
				}
				break;
			case 18:
				_localctx = new RegexStarContext(_localctx);
				enterOuterAlt(_localctx, 18);
				{
				setState(153);
				match(T__0);
				setState(154);
				match(T__23);
				setState(155);
				expr();
				setState(156);
				match(T__2);
				}
				break;
			case 19:
				_localctx = new RegexUnionContext(_localctx);
				enterOuterAlt(_localctx, 19);
				{
				setState(158);
				match(T__0);
				setState(159);
				match(T__24);
				{
				setState(160);
				((RegexUnionContext)_localctx).lhs = expr();
				}
				{
				setState(161);
				((RegexUnionContext)_localctx).rhs = expr();
				}
				setState(162);
				match(T__2);
				}
				break;
			case 20:
				_localctx = new Str2RegContext(_localctx);
				enterOuterAlt(_localctx, 20);
				{
				setState(164);
				match(T__0);
				setState(165);
				match(T__25);
				setState(166);
				expr();
				setState(167);
				match(T__2);
				}
				break;
			case 21:
				_localctx = new AddContext(_localctx);
				enterOuterAlt(_localctx, 21);
				{
				setState(169);
				match(T__0);
				setState(170);
				match(T__26);
				{
				setState(171);
				((AddContext)_localctx).lhs = expr();
				}
				{
				setState(172);
				((AddContext)_localctx).rhs = expr();
				}
				setState(173);
				match(T__2);
				}
				break;
			case 22:
				_localctx = new NegContext(_localctx);
				enterOuterAlt(_localctx, 22);
				{
				setState(175);
				match(T__0);
				setState(176);
				match(T__27);
				setState(177);
				expr();
				setState(178);
				match(T__2);
				}
				break;
			case 23:
				_localctx = new SubContext(_localctx);
				enterOuterAlt(_localctx, 23);
				{
				setState(180);
				match(T__0);
				setState(181);
				match(T__27);
				{
				setState(182);
				((SubContext)_localctx).lhs = expr();
				}
				{
				setState(183);
				((SubContext)_localctx).rhs = expr();
				}
				setState(184);
				match(T__2);
				}
				break;
			case 24:
				_localctx = new DivContext(_localctx);
				enterOuterAlt(_localctx, 24);
				{
				setState(186);
				match(T__0);
				setState(187);
				match(T__28);
				{
				setState(188);
				((DivContext)_localctx).lhs = expr();
				}
				{
				setState(189);
				((DivContext)_localctx).rhs = expr();
				}
				setState(190);
				match(T__2);
				}
				break;
			case 25:
				_localctx = new GtContext(_localctx);
				enterOuterAlt(_localctx, 25);
				{
				setState(192);
				match(T__0);
				setState(193);
				match(T__29);
				{
				setState(194);
				((GtContext)_localctx).lhs = expr();
				}
				{
				setState(195);
				((GtContext)_localctx).rhs = expr();
				}
				setState(196);
				match(T__2);
				}
				break;
			case 26:
				_localctx = new LtContext(_localctx);
				enterOuterAlt(_localctx, 26);
				{
				setState(198);
				match(T__0);
				setState(199);
				match(T__30);
				{
				setState(200);
				((LtContext)_localctx).lhs = expr();
				}
				{
				setState(201);
				((LtContext)_localctx).rhs = expr();
				}
				setState(202);
				match(T__2);
				}
				break;
			case 27:
				_localctx = new GeContext(_localctx);
				enterOuterAlt(_localctx, 27);
				{
				setState(204);
				match(T__0);
				setState(205);
				match(T__31);
				{
				setState(206);
				((GeContext)_localctx).lhs = expr();
				}
				{
				setState(207);
				((GeContext)_localctx).rhs = expr();
				}
				setState(208);
				match(T__2);
				}
				break;
			case 28:
				_localctx = new LeContext(_localctx);
				enterOuterAlt(_localctx, 28);
				{
				setState(210);
				match(T__0);
				setState(211);
				match(T__32);
				{
				setState(212);
				((LeContext)_localctx).lhs = expr();
				}
				{
				setState(213);
				((LeContext)_localctx).rhs = expr();
				}
				setState(214);
				match(T__2);
				}
				break;
			case 29:
				_localctx = new BoolValContext(_localctx);
				enterOuterAlt(_localctx, 29);
				{
				setState(216);
				match(BOOL);
				}
				break;
			case 30:
				_localctx = new StrValContext(_localctx);
				enterOuterAlt(_localctx, 30);
				{
				setState(217);
				match(STR);
				}
				break;
			case 31:
				_localctx = new IntValContext(_localctx);
				enterOuterAlt(_localctx, 31);
				{
				setState(218);
				match(INT);
				}
				break;
			case 32:
				_localctx = new VariableContext(_localctx);
				enterOuterAlt(_localctx, 32);
				{
				setState(219);
				match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprSortContext extends ParserRuleContext {
		public ExprSortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprSort; }
	 
		public ExprSortContext() { }
		public void copyFrom(ExprSortContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StrSortContext extends ExprSortContext {
		public StrSortContext(ExprSortContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitStrSort(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntSortContext extends ExprSortContext {
		public IntSortContext(ExprSortContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitIntSort(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolSortContext extends ExprSortContext {
		public BoolSortContext(ExprSortContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Z3StrExprVisitor ) return ((Z3StrExprVisitor<? extends T>)visitor).visitBoolSort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprSortContext exprSort() throws RecognitionException {
		ExprSortContext _localctx = new ExprSortContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_exprSort);
		try {
			setState(225);
			switch (_input.LA(1)) {
			case T__33:
				_localctx = new BoolSortContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(222);
				match(T__33);
				}
				break;
			case T__34:
				_localctx = new IntSortContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(223);
				match(T__34);
				}
				break;
			case T__35:
				_localctx = new StrSortContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(224);
				match(T__35);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3+\u00e6\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\7\2\16\n\2\f\2\16\2\21\13\2\3\2\7\2"+
		"\24\n\2\f\2\16\2\27\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\5\3(\n\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\6\58\n\5\r\5\16\59\3\5\3\5\3\5\3\5\3\5\6\5A\n\5\r\5\16\5B\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u00df\n\5\3"+
		"\6\3\6\3\6\5\6\u00e4\n\6\3\6\2\2\7\2\4\6\b\n\2\2\u0106\2\17\3\2\2\2\4"+
		"\'\3\2\2\2\6)\3\2\2\2\b\u00de\3\2\2\2\n\u00e3\3\2\2\2\f\16\5\4\3\2\r\f"+
		"\3\2\2\2\16\21\3\2\2\2\17\r\3\2\2\2\17\20\3\2\2\2\20\25\3\2\2\2\21\17"+
		"\3\2\2\2\22\24\5\6\4\2\23\22\3\2\2\2\24\27\3\2\2\2\25\23\3\2\2\2\25\26"+
		"\3\2\2\2\26\30\3\2\2\2\27\25\3\2\2\2\30\31\7\2\2\3\31\3\3\2\2\2\32\33"+
		"\7\3\2\2\33\34\7\4\2\2\34\35\7)\2\2\35\36\5\n\6\2\36\37\7\5\2\2\37(\3"+
		"\2\2\2 !\7\3\2\2!\"\7\6\2\2\"#\7)\2\2#$\7\7\2\2$%\5\n\6\2%&\7\5\2\2&("+
		"\3\2\2\2\'\32\3\2\2\2\' \3\2\2\2(\5\3\2\2\2)*\7\3\2\2*+\7\b\2\2+,\5\b"+
		"\5\2,-\7\5\2\2-\7\3\2\2\2./\7\3\2\2/\60\7\t\2\2\60\61\5\b\5\2\61\62\5"+
		"\b\5\2\62\63\7\5\2\2\63\u00df\3\2\2\2\64\65\7\3\2\2\65\67\7\n\2\2\668"+
		"\5\b\5\2\67\66\3\2\2\289\3\2\2\29\67\3\2\2\29:\3\2\2\2:;\3\2\2\2;<\7\5"+
		"\2\2<\u00df\3\2\2\2=>\7\3\2\2>@\7\13\2\2?A\5\b\5\2@?\3\2\2\2AB\3\2\2\2"+
		"B@\3\2\2\2BC\3\2\2\2CD\3\2\2\2DE\7\5\2\2E\u00df\3\2\2\2FG\7\3\2\2GH\7"+
		"\f\2\2HI\5\b\5\2IJ\7\5\2\2J\u00df\3\2\2\2KL\7\3\2\2LM\7\r\2\2MN\5\b\5"+
		"\2NO\5\b\5\2OP\5\b\5\2PQ\7\5\2\2Q\u00df\3\2\2\2RS\7\3\2\2ST\7\16\2\2T"+
		"U\5\b\5\2UV\5\b\5\2VW\7\5\2\2W\u00df\3\2\2\2XY\7\3\2\2YZ\7\17\2\2Z[\5"+
		"\b\5\2[\\\5\b\5\2\\]\7\5\2\2]\u00df\3\2\2\2^_\7\3\2\2_`\7\20\2\2`a\5\b"+
		"\5\2ab\5\b\5\2bc\7\5\2\2c\u00df\3\2\2\2de\7\3\2\2ef\7\21\2\2fg\5\b\5\2"+
		"gh\5\b\5\2hi\7\5\2\2i\u00df\3\2\2\2jk\7\3\2\2kl\7\22\2\2lm\5\b\5\2mn\5"+
		"\b\5\2no\7\5\2\2o\u00df\3\2\2\2pq\7\3\2\2qr\7\23\2\2rs\5\b\5\2st\7\5\2"+
		"\2t\u00df\3\2\2\2uv\7\3\2\2vw\7\24\2\2wx\5\b\5\2xy\5\b\5\2yz\5\b\5\2z"+
		"{\7\5\2\2{\u00df\3\2\2\2|}\7\3\2\2}~\7\25\2\2~\177\5\b\5\2\177\u0080\5"+
		"\b\5\2\u0080\u0081\7\5\2\2\u0081\u00df\3\2\2\2\u0082\u0083\7\3\2\2\u0083"+
		"\u0084\7\26\2\2\u0084\u0085\5\b\5\2\u0085\u0086\5\b\5\2\u0086\u0087\5"+
		"\b\5\2\u0087\u0088\7\5\2\2\u0088\u00df\3\2\2\2\u0089\u008a\7\3\2\2\u008a"+
		"\u008b\7\27\2\2\u008b\u008c\5\b\5\2\u008c\u008d\5\b\5\2\u008d\u008e\7"+
		"\5\2\2\u008e\u00df\3\2\2\2\u008f\u0090\7\3\2\2\u0090\u0091\7\30\2\2\u0091"+
		"\u0092\5\b\5\2\u0092\u0093\5\b\5\2\u0093\u0094\7\5\2\2\u0094\u00df\3\2"+
		"\2\2\u0095\u0096\7\3\2\2\u0096\u0097\7\31\2\2\u0097\u0098\5\b\5\2\u0098"+
		"\u0099\5\b\5\2\u0099\u009a\7\5\2\2\u009a\u00df\3\2\2\2\u009b\u009c\7\3"+
		"\2\2\u009c\u009d\7\32\2\2\u009d\u009e\5\b\5\2\u009e\u009f\7\5\2\2\u009f"+
		"\u00df\3\2\2\2\u00a0\u00a1\7\3\2\2\u00a1\u00a2\7\33\2\2\u00a2\u00a3\5"+
		"\b\5\2\u00a3\u00a4\5\b\5\2\u00a4\u00a5\7\5\2\2\u00a5\u00df\3\2\2\2\u00a6"+
		"\u00a7\7\3\2\2\u00a7\u00a8\7\34\2\2\u00a8\u00a9\5\b\5\2\u00a9\u00aa\7"+
		"\5\2\2\u00aa\u00df\3\2\2\2\u00ab\u00ac\7\3\2\2\u00ac\u00ad\7\35\2\2\u00ad"+
		"\u00ae\5\b\5\2\u00ae\u00af\5\b\5\2\u00af\u00b0\7\5\2\2\u00b0\u00df\3\2"+
		"\2\2\u00b1\u00b2\7\3\2\2\u00b2\u00b3\7\36\2\2\u00b3\u00b4\5\b\5\2\u00b4"+
		"\u00b5\7\5\2\2\u00b5\u00df\3\2\2\2\u00b6\u00b7\7\3\2\2\u00b7\u00b8\7\36"+
		"\2\2\u00b8\u00b9\5\b\5\2\u00b9\u00ba\5\b\5\2\u00ba\u00bb\7\5\2\2\u00bb"+
		"\u00df\3\2\2\2\u00bc\u00bd\7\3\2\2\u00bd\u00be\7\37\2\2\u00be\u00bf\5"+
		"\b\5\2\u00bf\u00c0\5\b\5\2\u00c0\u00c1\7\5\2\2\u00c1\u00df\3\2\2\2\u00c2"+
		"\u00c3\7\3\2\2\u00c3\u00c4\7 \2\2\u00c4\u00c5\5\b\5\2\u00c5\u00c6\5\b"+
		"\5\2\u00c6\u00c7\7\5\2\2\u00c7\u00df\3\2\2\2\u00c8\u00c9\7\3\2\2\u00c9"+
		"\u00ca\7!\2\2\u00ca\u00cb\5\b\5\2\u00cb\u00cc\5\b\5\2\u00cc\u00cd\7\5"+
		"\2\2\u00cd\u00df\3\2\2\2\u00ce\u00cf\7\3\2\2\u00cf\u00d0\7\"\2\2\u00d0"+
		"\u00d1\5\b\5\2\u00d1\u00d2\5\b\5\2\u00d2\u00d3\7\5\2\2\u00d3\u00df\3\2"+
		"\2\2\u00d4\u00d5\7\3\2\2\u00d5\u00d6\7#\2\2\u00d6\u00d7\5\b\5\2\u00d7"+
		"\u00d8\5\b\5\2\u00d8\u00d9\7\5\2\2\u00d9\u00df\3\2\2\2\u00da\u00df\7\'"+
		"\2\2\u00db\u00df\7*\2\2\u00dc\u00df\7(\2\2\u00dd\u00df\7)\2\2\u00de.\3"+
		"\2\2\2\u00de\64\3\2\2\2\u00de=\3\2\2\2\u00deF\3\2\2\2\u00deK\3\2\2\2\u00de"+
		"R\3\2\2\2\u00deX\3\2\2\2\u00de^\3\2\2\2\u00ded\3\2\2\2\u00dej\3\2\2\2"+
		"\u00dep\3\2\2\2\u00deu\3\2\2\2\u00de|\3\2\2\2\u00de\u0082\3\2\2\2\u00de"+
		"\u0089\3\2\2\2\u00de\u008f\3\2\2\2\u00de\u0095\3\2\2\2\u00de\u009b\3\2"+
		"\2\2\u00de\u00a0\3\2\2\2\u00de\u00a6\3\2\2\2\u00de\u00ab\3\2\2\2\u00de"+
		"\u00b1\3\2\2\2\u00de\u00b6\3\2\2\2\u00de\u00bc\3\2\2\2\u00de\u00c2\3\2"+
		"\2\2\u00de\u00c8\3\2\2\2\u00de\u00ce\3\2\2\2\u00de\u00d4\3\2\2\2\u00de"+
		"\u00da\3\2\2\2\u00de\u00db\3\2\2\2\u00de\u00dc\3\2\2\2\u00de\u00dd\3\2"+
		"\2\2\u00df\t\3\2\2\2\u00e0\u00e4\7$\2\2\u00e1\u00e4\7%\2\2\u00e2\u00e4"+
		"\7&\2\2\u00e3\u00e0\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e3\u00e2\3\2\2\2\u00e4"+
		"\13\3\2\2\2\t\17\25\'9B\u00de\u00e3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}