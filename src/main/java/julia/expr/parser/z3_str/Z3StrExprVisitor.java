// Generated from Z3StrExpr.g4 by ANTLR 4.5.3
package julia.expr.parser.z3_str;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Z3StrExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface Z3StrExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Z3StrExprParser#cmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCmd(Z3StrExprParser.CmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3StrExprParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(Z3StrExprParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3StrExprParser#assertion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssertion(Z3StrExprParser.AssertionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual(Z3StrExprParser.EqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code And}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(Z3StrExprParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(Z3StrExprParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(Z3StrExprParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code If}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(Z3StrExprParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Imply}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImply(Z3StrExprParser.ImplyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Concat}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcat(Z3StrExprParser.ConcatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Contains}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContains(Z3StrExprParser.ContainsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EndsWith}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEndsWith(Z3StrExprParser.EndsWithContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Indexof}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexof(Z3StrExprParser.IndexofContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Length}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLength(Z3StrExprParser.LengthContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Replace}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(Z3StrExprParser.ReplaceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StartsWith}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStartsWith(Z3StrExprParser.StartsWithContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Substring}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstring(Z3StrExprParser.SubstringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LastIndexof}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastIndexof(Z3StrExprParser.LastIndexofContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RegexConcat}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegexConcat(Z3StrExprParser.RegexConcatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RegexIn}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegexIn(Z3StrExprParser.RegexInContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RegexStar}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegexStar(Z3StrExprParser.RegexStarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RegexUnion}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegexUnion(Z3StrExprParser.RegexUnionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Str2Reg}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStr2Reg(Z3StrExprParser.Str2RegContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Add}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(Z3StrExprParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Neg}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeg(Z3StrExprParser.NegContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Sub}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSub(Z3StrExprParser.SubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Div}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiv(Z3StrExprParser.DivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Gt}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGt(Z3StrExprParser.GtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Lt}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLt(Z3StrExprParser.LtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Ge}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGe(Z3StrExprParser.GeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Le}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLe(Z3StrExprParser.LeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolVal}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolVal(Z3StrExprParser.BoolValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StrVal}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrVal(Z3StrExprParser.StrValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntVal}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntVal(Z3StrExprParser.IntValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link Z3StrExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(Z3StrExprParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolSort}
	 * labeled alternative in {@link Z3StrExprParser#exprSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolSort(Z3StrExprParser.BoolSortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntSort}
	 * labeled alternative in {@link Z3StrExprParser#exprSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntSort(Z3StrExprParser.IntSortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StrSort}
	 * labeled alternative in {@link Z3StrExprParser#exprSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrSort(Z3StrExprParser.StrSortContext ctx);
}