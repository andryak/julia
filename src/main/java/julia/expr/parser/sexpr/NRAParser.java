package julia.expr.parser.sexpr;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Distinct;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Or;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealAdd;
import julia.expr.ast.reals.RealDiv;
import julia.expr.ast.reals.RealGe;
import julia.expr.ast.reals.RealGt;
import julia.expr.ast.reals.RealLe;
import julia.expr.ast.reals.RealLt;
import julia.expr.ast.reals.RealMul;
import julia.expr.ast.reals.RealVal;
import julia.expr.parser.exc.ParserException;

/**
 * Class to extract boolean expressions over reals from SEXPR data set files.
 */
public class NRAParser implements SexprParser {
   /**
    * Returns an real expression reading it from the given scanner.
    * 
    * @param scanner
    *           the scanner from which the expression will be extracted.
    * @return a real expression extracted from the given scanner.
    * @throws ParserException
    *            if the scanner does not contain a valid real expression.
    */
   private IExpr parseRealExpr(Scanner scanner) throws ParserException {
      String operator = null;
      try {
         operator = scanner.next();
      } catch (NoSuchElementException e) {
         throw new ParserException("Missing operator");
      } catch (IllegalStateException e) {
         throw new ParserException("The stream has been closed");
      }

      if (operator.equals("c")) {
         String value = null;
         try {
            value = scanner.next("-?[0-9]+(\\.[0-9]+)?");
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "A real value for a constant was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing intenger value for a constant");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }
         return RealVal.create(value);
      } else if (operator.equals("v")) {
         Integer value = null;
         try {
            value = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "A real value as index of a variable was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing index for a variable");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }
         return Real.create(value);
      } else if (operator.equals("+")) {
         Integer arity = null;

         try {
            arity = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "A real value as the arity of a sum operator was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing arity for sum operator");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }

         if (arity < 2) {
            throw new ParserException("Arity for operator " + operator
                  + " should be at least 2, got " + arity + " instead");
         }

         IExpr expr = this.parseRealExpr(scanner);
         for (int i = 1; i < arity; ++i) {
            expr = RealAdd.create(expr, this.parseRealExpr(scanner));
         }
         return expr;
      } else if (operator.equals("*")) {
         Integer arity = null;

         try {
            arity = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "A real value as the arity of a mul operator was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing arity for mul operator");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }

         if (arity < 2) {
            throw new ParserException("Arity for operator " + operator
                  + " should be at least 2, got " + arity + " instead");
         }

         IExpr expr = this.parseRealExpr(scanner);
         for (int i = 1; i < arity; ++i) {
            expr = RealMul.create(expr, this.parseRealExpr(scanner));
         }
         return expr;
      } else if (operator.equals("/")) {
         IExpr dividend = this.parseRealExpr(scanner);
         IExpr divisor = this.parseRealExpr(scanner);
         return RealDiv.create(dividend, divisor);
      }
      throw new ParserException("Unexpected operator " + operator);
   }

   /**
    * Returns a boolean expression reading it from the given scanner.
    * 
    * @param scanner
    *           the scanner from which the expression will be extracted.
    * @return a boolean expression extracted from the given scanner.
    */
   private IExpr parseBoolExpr(Scanner scanner) throws ParserException {
      List<IExpr> clauses = new ArrayList<>();

      Integer nrClauses = null;
      try {
         nrClauses = scanner.nextInt();
      } catch (InputMismatchException e) {
         throw new ParserException(
               "A real value as the number of clauses of an expression was expected, but something else was found");
      } catch (NoSuchElementException e) {
         throw new ParserException("Missing number of clauses of an expression");
      } catch (IllegalStateException e) {
         throw new ParserException("The stream has been closed");
      }
      for (int i = 0; i < nrClauses; ++i) {
         List<IExpr> inequalities = new ArrayList<>();

         Integer nrIneqs = null;
         try {
            nrIneqs = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "A real value as the number of inequalities of a clause was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing number of inequalities of a clause");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }

         for (int j = 0; j < nrIneqs; ++j) {
            IExpr inequality = null;

            String comparison = null;
            try {
               comparison = scanner.next();
            } catch (NoSuchElementException e) {
               throw new ParserException("Missing comparison of an inequality");
            } catch (IllegalStateException e) {
               throw new ParserException("The stream has been closed");
            }

            IExpr lhs = this.parseRealExpr(scanner);
            IExpr rhs = this.parseRealExpr(scanner);

            if (comparison.equals("lt")) {
               inequality = RealLt.create(lhs, rhs);
            } else if (comparison.equals("le")) {
               inequality = RealLe.create(lhs, rhs);
            } else if (comparison.equals("gt")) {
               inequality = RealGt.create(lhs, rhs);
            } else if (comparison.equals("ge")) {
               inequality = RealGe.create(lhs, rhs);
            } else if (comparison.equals("eq")) {
               inequality = Equal.create(lhs, rhs);
            } else if (comparison.equals("ne")) {
               inequality = Distinct.create(lhs, rhs);
            } else {
               throw new ParserException("Unexpected comparison operator " + comparison);
            }
            assert (inequality != null);
            inequalities.add(inequality);
         }
         clauses.add(Or.create(inequalities));
      }
      return And.create(clauses);
   }

   /**
    * Returns a boolean expression from the given string.
    * 
    * @param s
    *           a string in SEXPR format.
    * @return a boolean expression from the given string.
    */
   public IExpr parse(String s) throws ParserException {
      return this.parseBoolExpr(new Scanner(new ByteArrayInputStream(s.getBytes())));
   }
}
