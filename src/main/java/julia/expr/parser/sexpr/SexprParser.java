package julia.expr.parser.sexpr;

import julia.expr.ast.IExpr;
import julia.expr.parser.exc.ParserException;

/**
 * Simple interface for parsers parsing expressions in SEXPR format.
 */
public interface SexprParser {
   /**
    * Returns a new expression from the given string.
    */
   public IExpr parse(String s) throws ParserException;
}
