package julia.expr.parser.sexpr;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Distinct;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Or;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.ints.IntGe;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLe;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntMul;
import julia.expr.ast.ints.IntVal;
import julia.expr.parser.exc.ParserException;

/**
 * Class to extract boolean expressions over integers from SEXPR data set files.
 */
public class LIAParser implements SexprParser {
   /**
    * Returns an integer expression reading it from the given scanner.
    * 
    * @param scanner
    *           the scanner from which the expression will be extracted.
    * @return a integer expression extracted from the given scanner.
    * @throws ParserException
    *            if the scanner does not contain a valid integer expression.
    */
   private IExpr parseIntExpr(Scanner scanner) throws ParserException {
      String operator = null;
      try {
         operator = scanner.next();
      } catch (NoSuchElementException e) {
         throw new ParserException("Missing operator");
      } catch (IllegalStateException e) {
         throw new ParserException("The stream has been closed");
      }

      if (operator.equals("c")) {
         BigInteger value = null;
         try {
            value = scanner.nextBigInteger();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "An integer value for a constant was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing intenger value for a constant");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }
         return IntVal.create(value);
      } else if (operator.equals("v")) {
         Integer value = null;
         try {
            value = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "An integer value as index of a variable was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing index for a variable");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }
         return Int.create(value);
      } else if (operator.equals("+")) {
         Integer arity = null;

         try {
            arity = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "An integer value as the arity of a sum operator was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing arity for sum operator");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }

         if (arity < 2) {
            throw new ParserException("Arity for operator " + operator
                  + " should be at least 2, got " + arity + " instead");
         }

         IExpr expr = this.parseIntExpr(scanner);
         for (int i = 1; i < arity; ++i) {
            expr = IntAdd.create(expr, this.parseIntExpr(scanner));
         }
         return expr;
      } else if (operator.equals("*")) {
         Integer arity = null;

         try {
            arity = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "An integer value as the arity of a mul operator was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing arity for mul operator");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }

         if (arity < 2) {
            throw new ParserException("Arity for operator " + operator
                  + " should be at least 2, got " + arity + " instead");
         }

         IExpr expr = this.parseIntExpr(scanner);
         for (int i = 1; i < arity; ++i) {
            expr = IntMul.create(expr, this.parseIntExpr(scanner));
         }
         return expr;
      }
      throw new ParserException("Unexpected operator " + operator);
   }

   /**
    * Returns a boolean expression reading it from the given scanner.
    * 
    * @param scanner
    *           the scanner from which the expression will be extracted.
    * @return a boolean expression extracted from the given scanner.
    */
   private IExpr parseBoolExpr(Scanner scanner) throws ParserException {
      List<IExpr> clauses = new ArrayList<>();

      Integer nrClauses = null;
      try {
         nrClauses = scanner.nextInt();
      } catch (InputMismatchException e) {
         throw new ParserException(
               "An integer value as the number of clauses of an expression was expected, but something else was found");
      } catch (NoSuchElementException e) {
         throw new ParserException("Missing number of clauses of an expression");
      } catch (IllegalStateException e) {
         throw new ParserException("The stream has been closed");
      }
      for (int i = 0; i < nrClauses; ++i) {
         List<IExpr> inequalities = new ArrayList<>();

         Integer nrIneqs = null;
         try {
            nrIneqs = scanner.nextInt();
         } catch (InputMismatchException e) {
            throw new ParserException(
                  "An integer value as the number of inequalities of a clause was expected, but something else was found");
         } catch (NoSuchElementException e) {
            throw new ParserException("Missing number of inequalities of a clause");
         } catch (IllegalStateException e) {
            throw new ParserException("The stream has been closed");
         }

         for (int j = 0; j < nrIneqs; ++j) {
            IExpr inequality = null;

            String comparison = null;
            try {
               comparison = scanner.next();
            } catch (NoSuchElementException e) {
               throw new ParserException("Missing comparison of an inequality");
            } catch (IllegalStateException e) {
               throw new ParserException("The stream has been closed");
            }

            IExpr lhs = this.parseIntExpr(scanner);
            IExpr rhs = this.parseIntExpr(scanner);

            if (comparison.equals("lt")) {
               inequality = IntLt.create(lhs, rhs);
            } else if (comparison.equals("le")) {
               inequality = IntLe.create(lhs, rhs);
            } else if (comparison.equals("gt")) {
               inequality = IntGt.create(lhs, rhs);
            } else if (comparison.equals("ge")) {
               inequality = IntGe.create(lhs, rhs);
            } else if (comparison.equals("eq")) {
               inequality = Equal.create(lhs, rhs);
            } else if (comparison.equals("ne")) {
               inequality = Distinct.create(lhs, rhs);
            } else {
               throw new ParserException("Unexpected comparison operator " + comparison);
            }
            assert (inequality != null);
            inequalities.add(inequality);
         }
         clauses.add(Or.create(inequalities));
      }
      return And.create(clauses);
   }

   /**
    * Returns a boolean expression from the given string.
    * 
    * @param s
    *           a string in SEXPR format.
    * @return a boolean expression from the given string.
    */
   public IExpr parse(String s) throws ParserException {
      return this.parseBoolExpr(new Scanner(new ByteArrayInputStream(s.getBytes())));
   }
}
