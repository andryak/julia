package julia.expr.slicer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.utils.Exprs;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 * Helper class to slice an expression with respect to a set of conjuncts.
 */
public final class ExprSlicer {
   /**
    * Do not instantiate me!
    */
   private ExprSlicer() {}

   /**
    * Updates the given variable graph with the given variables assuming they come from
    * the same conjunct.
    * 
    * @param graph
    *           a variable graph.
    * @param vars
    *           the set of variables of a given conjunct.
    */
   private static void updateVariableGraph(
         UndirectedGraph<Fun, DefaultEdge> graph,
         Set<Fun> vars) {

      Fun[] varsArray = vars.toArray(new Fun[vars.size()]);

      // Add new nodes.
      for (int i = 0; i < varsArray.length; ++i) {
         graph.addVertex(varsArray[i]);
      }

      // Add new edges.
      for (int i = 0; i < varsArray.length - 1; ++i) {
         Fun v1 = varsArray[i];
         Fun v2 = varsArray[i + 1];
         graph.addEdge(v1, v2);
      }
   }

   /**
    * Returns the set of boolean expressions that form a slice with the given expression.
    * <p>
    * <b>Notice:</b> The conjunction of the given conjuncts is assumed satisfiable.
    * <p>
    * The slice of an expression with respect to a set of conjuncts is the conjunction of
    * the given expression with all conjuncts that might affect its satisfiability value
    * (that is, all conjuncts operating directly or indirectly on the same variables).
    * 
    * @param conjuncts
    *           a set of boolean expressions whose conjunction is satisfiable.
    * @param expr
    *           the expression to slice with respect to the given conjuncts.
    * @return the set of boolean expressions that form a slice with the given expression.
    */
   private static Set<IExpr> slicedSet(Set<IExpr> conjuncts, IExpr expr) {
      // A constant expression is a trivial slice.
      Set<Fun> vars = expr.getVars();
      if (vars.isEmpty()) {
         Set<IExpr> slice = new HashSet<>();
         slice.add(expr);
         return slice;
      }

      // Build a map from conjuncts to the variables they contain.
      Map<IExpr, Set<Fun>> conjunctToVars = new HashMap<>();
      conjunctToVars.put(expr, vars);
      for (final IExpr conjunct : conjuncts) {
         conjunctToVars.put(conjunct, conjunct.getVars());
      }

      // Build a graph of variable co-occurrences in the conjuncts.
      UndirectedGraph<Fun, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);

      updateVariableGraph(graph, conjunctToVars.get(expr));
      for (final IExpr conjunct : conjuncts) {
         updateVariableGraph(graph, conjunctToVars.get(conjunct));
      }

      // Get connected components.
      ConnectivityInspector<Fun, DefaultEdge> inspector = new ConnectivityInspector<>(graph);
      Set<Fun> cc = inspector.connectedSetOf(vars.iterator().next());

      Set<IExpr> slice = new HashSet<>();
      slice.add(expr);
      for (final IExpr conjunct : conjuncts) {
         final Fun firstVar = conjunctToVars.get(conjunct).iterator().next();
         if (cc.contains(firstVar)) {
            slice.add(conjunct);
         }
      }
      return slice;
   }

   /**
    * Returns a new boolean expression obtained slicing the given expression with respect
    * to the given conjuncts.
    * <p>
    * <b>Notice:</b> The conjunction of the given conjuncts is assumed satisfiable.
    * <p>
    * The slice of an expression with respect to a set of conjuncts is the conjunction of
    * the given expression with all conjuncts that might affect its satisfiability value
    * (that is, all conjuncts operating directly or indirectly on the same variables).
    * 
    * @param conjuncts
    *           a set of boolean expressions whose conjunction is satisfiable.
    * @param expr
    *           the expression to slice with respect to the given conjuncts.
    * @return the resulting sliced expression.
    */
   public static IExpr slice(Set<IExpr> conjuncts, IExpr expr) {
      return And.create(new ArrayList<>(slicedSet(conjuncts, expr)));
   }

   /**
    * Returns a new boolean expression obtained slicing the given expression with respect
    * to the given prefix.
    * <p>
    * <b>Notice:</b> The prefix must be a satisfiable expression in conjunctive normal
    * form.
    * 
    * @param prefix
    *           a satisfiable expression in CNF representing one or more conjuncts.
    * @param expr
    *           the expression to slice with respect to the prefix.
    * @return the resulting sliced expression.
    */
   public static IExpr slice(IExpr prefix, IExpr expr) {
      return slice(Exprs.getConjuncts(prefix), expr);
   }

   /**
    * Returns the set of independent boolean expressions obtained slicing the given set of
    * conjuncts.
    * <p>
    * Two expressions are independent if they do not share any variables.
    * 
    * @param conjuncts
    *           the set of conjuncts to slice.
    * @return the resulting sliced expressions.
    */
   public static Set<IExpr> selfSlice(Set<IExpr> conjuncts) {
      // Copy the conjuncts set to prevent side effects.
      Set<IExpr> conjunctsCopy = new HashSet<>(conjuncts);

      // Remove satisfiable constant conjuncts and abort
      // in presence of unsatisfiable constant conjuncts.
      for (final IExpr conjunct : conjuncts) {
         if (conjunct.getVars().isEmpty()) {
            IExpr evalued = conjunct.eval();
            if (evalued.equals(BoolVal.TRUE)) {
               conjunctsCopy.remove(conjunct);
            } else if (evalued.equals(BoolVal.FALSE)) {
               Set<IExpr> slice = new HashSet<>();
               slice.add(BoolVal.FALSE);
               return slice;
            }
         }
      }

      Set<IExpr> sliceSet = new HashSet<>();
      while (!conjunctsCopy.isEmpty()) {
         // Get one arbitrary conjunct from the conjuncts set.
         IExpr firstConjunct = conjunctsCopy.iterator().next();
         conjunctsCopy.remove(firstConjunct);

         // Slice it with respect to the other conjuncts.
         Set<IExpr> slice = slicedSet(conjunctsCopy, firstConjunct);
         sliceSet.add(And.create(new ArrayList<>(slice)));

         // Remove the extracted conjuncts from the conjuncts set and repeat.
         conjunctsCopy.removeAll(slice);
      }
      return sliceSet;
   }

   /**
    * Returns the set of independent boolean expressions obtained slicing the given
    * expression in conjunctive normal form.
    * <p>
    * Two expressions are independent if they do not share any variables.
    * <p>
    * The returned expressions are <b>NOT</b> compressed.
    * 
    * @param expr
    *           the expression to slice.
    * @return the resulting sliced expressions.
    */
   public static Set<IExpr> selfSlice(IExpr expr) {
      return selfSlice(Exprs.getConjuncts(expr));
   }
}
