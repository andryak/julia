package julia.solver;

/**
 * The satisfiability value of a boolean expression.
 * <ul>
 * <li>SAT: the expression is satisfiable,</li>
 * <li>UNSAT: the expression is unsatisfiable,</li>
 * <li>UNKNOWN: the expression might be satisfiable or unsatisfiable.</li>
 * </ul>
 */
public enum CheckResult {
   SAT, UNSAT, UNKNOWN
}