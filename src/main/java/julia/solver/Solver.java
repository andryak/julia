package julia.solver;

import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.solver.exc.NoModelException;
import julia.solver.exc.NoUnsatCoreException;

/**
 * Abstract solver implementing methods to get and set models and unsat-cores.
 */
public abstract class Solver implements ISolver {
   private Model model;
   private Set<IExpr> unsatCore;

   /**
    * Creates a new solver.
    */
   public Solver() {
      this.model = null;
      this.unsatCore = null;
   }

   @Override
   public Model getModel() throws NoModelException {
      if (model == null) {
         throw new NoModelException();
      }
      return model;
   }

   @Override
   public void setModel(Model model) {
      this.model = model;
   }

   @Override
   public Set<IExpr> getUnsatCore() throws NoUnsatCoreException {
      if (unsatCore == null) {
         throw new NoUnsatCoreException();
      }
      return unsatCore;
   }

   @Override
   public void setUnsatCore(Set<IExpr> unsatCore) {
      this.unsatCore = unsatCore;
   }
}
