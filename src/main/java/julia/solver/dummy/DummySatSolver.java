package julia.solver.dummy;

import julia.expr.ast.IExpr;
import julia.solver.CheckResult;
import julia.solver.Solver;

/**
 * Solver that always returns sat.
 */
public final class DummySatSolver extends Solver {
   /**
    * Always returns CheckResult.SAT.
    * 
    * @return always CheckResult.SAT.
    */
   @Override
   public CheckResult check(IExpr expr) {
      return CheckResult.SAT;
   }
}
