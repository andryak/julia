package julia.solver.dummy;

import julia.expr.ast.IExpr;
import julia.solver.CheckResult;
import julia.solver.Solver;

/**
 * Solver that always returns CheckResult.UNSAT.
 */
public final class DummyUnsatSolver extends Solver {
   /**
    * Always returns CheckResult.UNSAT.
    * 
    * @return always CheckResult.UNSAT.
    */
   @Override
   public CheckResult check(IExpr expr) {
      return CheckResult.UNSAT;
   }
}
