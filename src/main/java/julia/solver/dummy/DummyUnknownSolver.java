package julia.solver.dummy;

import julia.expr.ast.IExpr;
import julia.solver.CheckResult;
import julia.solver.Solver;

/**
 * Solver that always returns unknown.
 */
public final class DummyUnknownSolver extends Solver {
   /**
    * Always returns CheckResult.UNKNOWN.
    * 
    * @return always CheckResult.UNKNOWN.
    */
   @Override
   public CheckResult check(IExpr expr) {
      return CheckResult.UNKNOWN;
   }
}
