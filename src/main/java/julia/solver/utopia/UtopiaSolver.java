package julia.solver.utopia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import julia.cache.cex.CexCache;
import julia.cache.satdelta.CacheEntry;
import julia.cache.satdelta.SatDelta;
import julia.cache.satdelta.SatDeltaCache;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.uf.Fun;
import julia.expr.model.Model;
import julia.expr.model.SortModel;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BigRational;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.exc.NoModelException;
import julia.solver.exc.NoUnsatCoreException;
import julia.utils.Counter;
import julia.utils.Exprs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a solver which relies on the Utopia caching framework first to solve formulas.
 */
public class UtopiaSolver extends Solver {
   final Logger logger = LoggerFactory.getLogger(UtopiaSolver.class);

   /**
    * A list of reference models to calculate the sat-delta value of boolean expressions.
    * 
    * Notice: it is recommended to use total models, so that sat-delta can always be
    * computed for any boolean expression.
    */
   private List<Model> refModels;

   /**
    * The underlying solver of this Utopia solver.
    */
   private Solver solver;

   /**
    * The number of candidates extracted from the counter-example and sat-delta caches
    * during every check of an expression.
    */
   private int nrCandidates;

   /**
    * If set to true, the quick hash caches are checked prior to the computation of
    * sat-delta.
    */
   private boolean checkHashCaches;

   // The following two maps are hash maps by design.
   private HashMap<IExpr, Model> satHashCache;
   private HashMap<IExpr, Set<IExpr>> unsatHashCache;

   private SatDeltaCache satDeltaCache;
   private CexCache cexCache;

   private final List<UtopiaSolverListener> listeners;

   /**
    * Creates a new UtopiaSolver which is based on the given underlying solver, the given
    * reference models for the calculation of sat-delta and that extracts the given number
    * of candidates from the cex and sat-delta caches during the check of each expression.
    * 
    * @param refModels
    *           a list of models for the calculation of sat-delta.
    * @param solver
    *           the underlying solver of this cache.
    * @param nrCandidates
    *           the number of candidates extracted from the cex and sat-delta caches
    *           during the check of each expression.
    */
   public UtopiaSolver(List<Model> refModels, Solver solver, int nrCandidates) {
      this.refModels = refModels;

      this.solver = solver;
      this.setNrCandidates(nrCandidates); // Always a non-negative integer.

      this.checkHashCaches = true;
      this.satHashCache = new HashMap<>();
      this.unsatHashCache = new HashMap<>();

      this.satDeltaCache = new SatDeltaCache();
      this.cexCache = new CexCache();

      this.listeners = new ArrayList<>();
   }

   /**
    * Adds a new listener to this solver.
    * 
    * @param listener
    *           the listener to add.
    */
   public void addListener(UtopiaSolverListener listener) {
      this.listeners.add(listener);
   }

   /**
    * Returns the number of candidates extracted from the cex and sat-delta caches during
    * the check of each expression.
    * 
    * @return the number of candidates extracted from the cex and sat-delta caches during
    *         the check of each expression.
    */
   public int getNrCandidates() {
      return this.nrCandidates;
   }

   /**
    * Updates the number of candidates extracted from the cex and sat-delta caches during
    * the check of each expression to the given value.
    * 
    * @param nrCandidates
    *           the new number of candidates to extract from the cex and sat-delta caches
    *           during the check of each expression.
    */
   public void setNrCandidates(int nrCandidates) {
      this.nrCandidates = Math.max(nrCandidates, 0);
   }

   /**
    * Turns the hash caches on or off.
    * 
    * @param value
    *           if {@code true}, the hash caches are turned on, otherwise off.
    */
   public void setCheckHashCaches(boolean value) {
      this.checkHashCaches = value;
   }

   private CheckResult checkSatHashCache(IExpr expr) {
      Model model = this.satHashCache.get(expr);
      if (model != null) {
         logger.info("Model {} found in the sat-hash-cache", model);
         this.setModel(model);
         return CheckResult.SAT;
      }
      return CheckResult.UNKNOWN;
   }

   private CheckResult checkUnsatHashCache(IExpr expr) {
      Set<IExpr> unsatCore = this.unsatHashCache.get(expr);
      if (unsatCore != null) {
         logger.info("Unsat-core {} found in the unsat-hash-cache", unsatCore);
         this.setUnsatCore(unsatCore);
         return CheckResult.UNSAT;
      }
      return CheckResult.UNKNOWN;
   }

   private BigRational computeSatDelta(IExpr expr) {
      BigRational result = BigRational.POS_INFINITY;

      for (final Model model : this.refModels) {
         SatDelta satDelta = null;
         try {
            satDelta = expr.satDelta(model);
         } catch (Exception e) {
            continue;
         }
         logger.info("The sat-delta w.r.t. model {} is {}", model, satDelta);

         if (satDelta.dir().signum() == 0) {
            logger.info("The sat-delta is zero");
            this.setModel(model);

            logger.info("Updating the sat-hash-cache");
            this.satHashCache.put(expr, model);

            logger.info("Interrupting computation");
            return BigRational.ZERO;
         }
         result = result.min(satDelta.dir());
      }

      logger.info("Resulting sat-delta is {}", result);
      return result;
   }

   private CheckResult checkTrivialExpr(IExpr expr, int nrVars) {
      // Check whether the expression is trivially solvable.
      if (nrVars == 0) {
         logger.info("Expression has no variables");
         logger.info("Evaluating the expression");

         // Notice: boolean expressions containing no variables are guaranteed
         // to evaluate to either the boolean constant true or the boolean
         // constant false.

         IExpr status = expr.eval();

         if (status.equals(BoolVal.TRUE)) {
            Model model = new SortModel();

            logger.info("The expression is sat");
            this.setModel(model);

            logger.info("Updating the sat-hash-cache");
            this.satHashCache.put(expr, model);

            return CheckResult.SAT;
         } else if (status.equals(BoolVal.FALSE)) {
            Set<IExpr> unsatCore = new HashSet<>();

            logger.info("The expression is unsat");
            this.setUnsatCore(unsatCore);

            logger.info("Updating the unsat-hash-cache");
            this.unsatHashCache.put(expr, unsatCore);

            return CheckResult.UNSAT;
         }
      }
      return CheckResult.UNKNOWN;
   }

   private CheckResult checkSatDeltaCache(
         IExpr expr,
         BigRational satDelta,
         Counter<ExprSort> vars) {

      // Extract candidate models to solve the given expression.
      logger.info("Filtering candidates from the sat-delta-cache");
      List<CacheEntry> candidates = this.satDeltaCache.filter(
            satDelta,
            vars,
            this.nrCandidates);

      logger.info(
            "Filtered {} candidates out of {} entries",
            candidates.size(),
            this.satDeltaCache.size());

      // Try to reuse the retrieved models to solve the given expression.
      logger.info("Trying to reuse models");
      for (final CacheEntry candidate : candidates) {
         Model model = candidate.getModel();

         logger.info("Checking model {} (with sat-delta {})", model, candidate.getScore());

         try {
            // This operation can throw exceptions because
            // of divisions by zero and the like.
            IExpr evaluedExpr = expr.eval(model);

            if (evaluedExpr.equals(BoolVal.TRUE)) {
               logger.info("This model is reusable");
               this.setModel(model);

               logger.info("Updating the sat-hash-cache");
               this.satHashCache.put(expr, model);

               return CheckResult.SAT;
            }
         } catch (Exception e) {
            logger.warn("Cannot reuse model due to exception", e);
         }
      }
      return CheckResult.UNKNOWN;
   }

   private CheckResult checkCexCache(IExpr expr, Set<IExpr> conjuncts, int nrClauses) {
      // Extract candidate unsat-cores to solve the given expression.
      logger.info("Filtering candidates from the cex-cache");
      List<Set<IExpr>> candidates = this.cexCache.getSubsets(conjuncts, this.nrCandidates);

      logger.info(
            "Filtered {} candidates out of {} entries",
            candidates.size(),
            this.cexCache.size());

      // Try to reuse the retrieved unsat-cores to solve the given expression.
      logger.info("Trying to reuse unsat-cores");
      for (final Set<IExpr> unsatCore : candidates) {
         logger.info("Checking unsat-core {} (with {} clauses)", unsatCore, unsatCore.size());

         if (conjuncts.containsAll(unsatCore)) {
            logger.info("This unsat-core is reusable");
            this.setUnsatCore(unsatCore);

            logger.info("Updating the unsat-hash-cache");
            this.unsatHashCache.put(expr, unsatCore);

            return CheckResult.UNSAT;
         }
      }

      return CheckResult.UNKNOWN;
   }

   private CheckResult callSolver(
         IExpr expr,
         BigRational satDelta,
         Counter<ExprSort> vars,
         int nrClauses) {

      CheckResult status = this.solver.check(expr);
      logger.info("The solver returned {}", status);

      if (status == CheckResult.SAT) {
         try {
            Model model = this.solver.getModel();
            this.setModel(model);

            if (this.checkHashCaches) {
               logger.info("Updating the sat-hash-cache");
               this.satHashCache.put(expr, model);
            }

            logger.info("Updating the sat-delta-cache");
            CacheEntry entry = new CacheEntry(satDelta, vars, model);
            this.satDeltaCache.store(entry);
         } catch (NoModelException e) {
            logger.warn("Model is not available");
         }
      } else if (status == CheckResult.UNSAT) {
         try {
            Set<IExpr> unsatCore = this.solver.getUnsatCore();
            this.setUnsatCore(unsatCore);

            if (this.checkHashCaches) {
               logger.info("Updating the unsat-hash-cache");
               this.unsatHashCache.put(expr, unsatCore);
            }

            logger.info("Updating the cex-cache");
            this.cexCache.put(unsatCore);
         } catch (NoUnsatCoreException e) {
            logger.warn("UnsatCore is not available");
         }
      }
      return status;
   }

   /**
    * Returns the satisfiability value of the given expression.
    * 
    * @param expr
    *           a compressed expression, possibly in conjunctive normal form.
    * @return the satisfiability value of the input expression.
    */
   private CheckResult checkAux(IExpr expr) {
      logger.info("Checking expression {}", expr);
      CheckResult status = CheckResult.UNKNOWN;

      // Check the sat-hash-cache.
      if (this.checkHashCaches) {
         logger.info("Checking the sat-hash-cache");
         for (final UtopiaSolverListener listener : this.listeners) {
            listener.onBeforeCheckSatHashCache();
         }
         status = checkSatHashCache(expr);
         for (final UtopiaSolverListener listener : this.listeners) {
            listener.onAfterCheckSatHashCache(status);
         }

         if (!status.equals(CheckResult.UNKNOWN)) {
            logger.info("The sat-hash-cache produced a hit");
            return status;
         }
      }

      // Check the unsat-hash-cache.
      if (this.checkHashCaches) {
         logger.info("Checking the unsat-hash-cache");
         for (final UtopiaSolverListener listener : this.listeners) {
            listener.onBeforeCheckUnsatHashCache();
         }
         status = checkUnsatHashCache(expr);
         for (final UtopiaSolverListener listener : this.listeners) {
            listener.onAfterCheckUnsatHashCache(status);
         }

         if (!status.equals(CheckResult.UNKNOWN)) {
            logger.info("The unsat-hash-cache produced a hit");
            return status;
         }
      }

      // Get the number of variables in the formula.
      logger.info("Extracting variables");
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeVarsExtraction();
      }
      Set<Fun> vars = expr.getVars();
      Counter<ExprSort> varsCounter = new Counter<>();
      for (final Fun v : vars) {
         varsCounter.increment(v.sort());
      }
      logger.info("Extracted {} variables", vars.size());
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterVarsExtraction(vars.size());
      }

      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeTrivialCheck();
      }
      status = checkTrivialExpr(expr, vars.size());
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterTrivialCheck(status);
      }

      if (!status.equals(CheckResult.UNKNOWN)) {
         logger.info("The trivial check produced a hit");
         return status;
      }

      // Compute sat-delta.
      logger.info("Calculating sat-delta");
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeSatDeltaComputation();
      }
      BigRational satDelta = computeSatDelta(expr);
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterSatDeltaComputation(satDelta);
      }

      if (satDelta.signum() == 0) {
         logger.info("The sat-delta computation produced a hit");
         return CheckResult.SAT;
      }

      // Check the sat-delta-cache.
      logger.info("Checking the sat-delta-cache");
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeCheckSatDeltaCache();
      }
      status = checkSatDeltaCache(expr, satDelta, varsCounter);
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterCheckSatDeltaCache(status);
      }

      if (!status.equals(CheckResult.UNKNOWN)) {
         logger.info("The sat-delta-cache produced a hit");
         return status;
      }

      // Extract the conjuncts from the formula.
      logger.info("Calculating the number of clauses");
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeConjunctsExtraction();
      }
      Set<IExpr> conjuncts = Exprs.getConjuncts(expr);
      int nrClauses = conjuncts.size();
      logger.info("This expression has {} clauses", nrClauses);
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterConjunctsExtraction(nrClauses);
      }

      // Check the cex-cache.
      logger.info("Checking the cex-cache");
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeCheckCexCache();
      }
      status = checkCexCache(expr, conjuncts, nrClauses);
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterCheckCexCache(status);
      }

      if (!status.equals(CheckResult.UNKNOWN)) {
         logger.info("The cex-cache produced a hit");
         return status;
      }

      // Call the solver.
      logger.info("Calling the solver");
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeSolver();
      }
      status = callSolver(expr, satDelta, varsCounter, nrClauses);
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterSolver(status);
      }

      return status;
   }

   @Override
   public CheckResult check(IExpr expr) {
      this.setModel(null);
      this.setUnsatCore(null);

      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onBeforeCheck(expr);
      }
      CheckResult status = this.checkAux(expr);
      for (final UtopiaSolverListener listener : this.listeners) {
         listener.onAfterCheck(status);
      }
      return status;
   }
}
