package julia.solver.utopia;

import julia.expr.utils.BigRational;
import julia.solver.CheckResult;
import julia.solver.SolverListener;

/**
 * Interface of Utopia solver listeners.
 */
public interface UtopiaSolverListener extends SolverListener {
   /**
    * What to do before checking the sat hash cache.
    */
   public void onBeforeCheckSatHashCache();

   /**
    * What to do after checking the sat hash cache.
    * 
    * @param status
    *           the status of the checked expression after checking the cache.
    */
   public void onAfterCheckSatHashCache(CheckResult status);

   /**
    * What to do before checking the unsat hash cache.
    */
   public void onBeforeCheckUnsatHashCache();

   /**
    * What to do after checking the unsat hash cache.
    * 
    * @param status
    *           the status of the checked expression after checking the cache.
    */
   public void onAfterCheckUnsatHashCache(CheckResult status);

   /**
    * What to do before extracting the variables of the checked expression.
    */
   public void onBeforeVarsExtraction();

   /**
    * What to do after extracting the variables of the checked expression.
    * 
    * @param nrVars
    *           the number of variables of the checked expression.
    */
   public void onAfterVarsExtraction(int nrVars);

   /**
    * What to do before checking the triviality of the checked expression.
    */
   public void onBeforeTrivialCheck();

   /**
    * What to do after checking the triviality of the checked expression.
    * 
    * @param status
    *           the status of the checked expression after the triviality check.
    */
   public void onAfterTrivialCheck(CheckResult status);

   /**
    * What to do before calculating the sat-delta value of the checked expression.
    */
   public void onBeforeSatDeltaComputation();

   /**
    * What to do after calculating the sat-delta value of the checked expression.
    * 
    * @param delta
    *           the sat-delta value of the checked expression.
    */
   public void onAfterSatDeltaComputation(BigRational delta);

   /**
    * What to do before checking the sat-delta cache.
    */
   public void onBeforeCheckSatDeltaCache();

   /**
    * What to do after checking the sat-delta cache.
    * 
    * @param status
    *           the status of the checked expression after checking the cache.
    */
   public void onAfterCheckSatDeltaCache(CheckResult status);

   /**
    * What to do before extracting the conjuncts of the checked expression.
    */
   public void onBeforeConjunctsExtraction();

   /**
    * What to do After extracting the conjuncts of the checked expression.
    * 
    * @param nrClauses
    *           the number of conjuncts in the checked expression.
    */
   public void onAfterConjunctsExtraction(int nrClauses);

   /**
    * What to do before checking the counter-example cache.
    */
   public void onBeforeCheckCexCache();

   /**
    * What to do after checking the counter-example cache.
    * 
    * @param status
    *           the status of the checked expression after checking the cache.
    */
   public void onAfterCheckCexCache(CheckResult status);

   /**
    * What to do before calling the underlying solver of the listened solver.
    */
   public void onBeforeSolver();

   /**
    * What to do after calling the underlying solver of the listened solver.
    * 
    * @param status
    *           the status returned by the listened solver.
    */
   public void onAfterSolver(CheckResult status);
}
