package julia.solver.utopia;

import julia.expr.ast.IExpr;
import julia.expr.utils.BigRational;
import julia.solver.CheckResult;
import julia.utils.Counter;
import julia.utils.RunningStats;
import julia.utils.Timer;

/**
 * Utopia solver listener that collects statistics.
 */
public class UtopiaSolverStatsCollector implements UtopiaSolverListener {
   private final static Timer timer = new Timer();

   private Counter<String> counter;
   private final RunningStats satDeltaRunningStats;

   /**
    * Creates a new empty collector.
    */
   public UtopiaSolverStatsCollector() {
      this.counter = new Counter<>();
      this.satDeltaRunningStats = new RunningStats();
   }

   /**
    * Returns the reuse ratio of the listened solver.
    * <p>
    * By convention, the reuse ratio of solver that checked no expressions is 100%.
    * 
    * @return the reuse ratio of the listened solver.
    */
   public double getReuseRatio() {
      long checks = this.counter.get("checks").longValue();
      if (checks <= 0) {
         return 1d;
      }
      long solverCalls = this.counter.get("solver-calls").longValue();
      long cacheHits = (checks - solverCalls);
      return ((double) cacheHits) / checks;
   }

   @Override
   public void onBeforeCheck(IExpr expr) {
      this.counter.increment("checks");
   }

   @Override
   public void onAfterCheck(CheckResult status) {
      switch (status) {
         case SAT:
            this.counter.increment("sat-exprs");
            break;
         case UNSAT:
            this.counter.increment("unsat-exprs");
            break;
         case UNKNOWN:
            this.counter.increment("unknown-exprs");
            break;
      }
   }

   @Override
   public void onBeforeCheckSatHashCache() {
      timer.start();
   }

   @Override
   public void onAfterCheckSatHashCache(CheckResult status) {
      this.counter.update("sat-hash-cache-time", timer.stop());
      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("sat-hash-cache-hits");
      }
   }

   @Override
   public void onBeforeCheckUnsatHashCache() {
      timer.start();
   }

   @Override
   public void onAfterCheckUnsatHashCache(CheckResult status) {
      this.counter.update("unsat-hash-cache-time", timer.stop());
      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("unsat-hash-cache-hits");
      }
   }

   @Override
   public void onBeforeVarsExtraction() {
      timer.start();
   }

   @Override
   public void onAfterVarsExtraction(int nrVars) {
      this.counter.update("vars-extraction-time", timer.stop());
   }

   @Override
   public void onBeforeTrivialCheck() {
      timer.start();
   }

   @Override
   public void onAfterTrivialCheck(CheckResult status) {
      this.counter.update("trivial-check-time", timer.stop());

      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("trivial-expressions");
      }
   }

   @Override
   public void onBeforeSatDeltaComputation() {
      timer.start();
   }

   @Override
   public void onAfterSatDeltaComputation(BigRational delta) {
      this.counter.update("sat-delta-computation-time", timer.stop());

      if (delta.signum() == 0) {
         this.counter.increment("sat-delta-is-zero");
      }

      // Calculate the running statistics of the sat-delta values seen by this method.
      try {
         this.satDeltaRunningStats.add(delta.doubleValue());
      } catch (ArithmeticException e) {
         // Ignore infinite values.
      }
   }

   @Override
   public void onBeforeCheckSatDeltaCache() {
      timer.start();
   }

   @Override
   public void onAfterCheckSatDeltaCache(CheckResult status) {
      this.counter.update("sat-delta-cache-time", timer.stop());
      this.counter.increment("sat-delta-cache-calls");
      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("sat-delta-cache-hits");
      }
   }

   @Override
   public void onBeforeConjunctsExtraction() {
      timer.start();
   }

   @Override
   public void onAfterConjunctsExtraction(int nrClauses) {
      this.counter.update("conjuncts-extraction-time", timer.stop());
   }

   @Override
   public void onBeforeCheckCexCache() {
      timer.start();
   }

   @Override
   public void onAfterCheckCexCache(CheckResult status) {
      this.counter.update("cex-cache-time", timer.stop());
      this.counter.increment("cex-cache-calls");
      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("cex-cache-hits");
      }
   }

   @Override
   public void onBeforeSolver() {
      timer.start();
   }

   @Override
   public void onAfterSolver(CheckResult status) {
      this.counter.update("solver-time", timer.stop());
      this.counter.increment("solver-calls");
   }

   /**
    * Returns a string containing all the values collected by this listener.
    * 
    * @return a string containing all the values collected by this listener.
    */
   public String getStats() {
      StringBuilder b = new StringBuilder();
      b.append("[time(ns)]");
      b.append("\ncheck-sat-hash-cache = " + this.counter.get("sat-hash-cache-time"));
      b.append("\ncheck-unsat-hash-cache = " + this.counter.get("unsat-hash-cache-time"));
      b.append("\nextract-variables = " + this.counter.get("vars-extraction-time"));
      b.append("\ncheck-expr-is-trivial = " + this.counter.get("trivial-check-time"));
      b.append("\ncompute-satdelta = " + this.counter.get("sat-delta-computation-time"));
      b.append("\ncheck-satdelta-cache = " + this.counter.get("sat-delta-cache-time"));
      b.append("\nextract-conjuncts = " + this.counter.get("conjuncts-extraction-time"));
      b.append("\ncheck-cex-cache = " + this.counter.get("cex-cache-time"));
      b.append("\ncall-solver = " + this.counter.get("solver-time"));
      b.append("\n\n");

      b.append("[reuse]");
      b.append("\nsat-hash-cache-hits = " + this.counter.get("sat-hash-cache-hits"));
      b.append("\nunsat-hash-cache-hits = " + this.counter.get("unsat-hash-cache-hits"));
      b.append("\nsatdelta-is-zero = " + this.counter.get("sat-delta-is-zero"));
      b.append("\nsatdelta-cache-hits = " + this.counter.get("sat-delta-cache-hits"));
      b.append("\ncex-cache-hits = " + this.counter.get("cex-cache-hits"));
      b.append("\ncache-misses = " + this.counter.get("solver-calls"));
      b.append("\nreuse-ratio = " + this.getReuseRatio());
      b.append("\n\n");

      b.append("[satdelta]");
      b.append("\nmin = " + this.satDeltaRunningStats.min());
      b.append("\nmax = " + this.satDeltaRunningStats.max());
      b.append("\navg = " + this.satDeltaRunningStats.mean());
      b.append("\nstdev = " + this.satDeltaRunningStats.stdev());
      b.append("\n\n");

      b.append("[expressions]");
      b.append("\nchecked = " + this.counter.get("checks"));
      b.append("\nsat = " + this.counter.get("sat-exprs"));
      b.append("\nunsat = " + this.counter.get("unsat-exprs"));
      b.append("\nunknown = " + this.counter.get("unknown-exprs"));
      b.append("\n\n");

      b.append("[others]");
      b.append("\nnr-satdelta-cache-calls = " + this.counter.get("sat-delta-cache-calls"));
      b.append("\nnr-cex-cache-calls = " + this.counter.get("cex-cache-calls"));

      return b.toString();
   }
}
