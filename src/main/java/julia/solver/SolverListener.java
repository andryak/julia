package julia.solver;

import julia.expr.ast.IExpr;

/**
 * Interface of solver listeners.
 */
public interface SolverListener {
   /**
    * What to do before checking an expression.
    * 
    * @param expr
    *           the expression being checked.
    */
   public void onBeforeCheck(IExpr expr);

   /**
    * What to do after checking an expression.
    * 
    * @param expr
    *           the status of the expression after the check.
    */
   public void onAfterCheck(CheckResult status);
}