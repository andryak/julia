package julia.solver.random;

import julia.solver.CheckResult;
import julia.solver.SolverListener;

/**
 * Interface of random solver listeners.
 */
public interface RandomSolverListener extends SolverListener {
   /**
    * What to do before checking the sat cache.
    */
   public void onBeforeCheckSatCache();

   /**
    * What to do after checking the sat cache.
    * 
    * @param status
    *           the status of the checked expression after the cache check.
    */
   public void onAfterCheckSatCache(CheckResult status);

   /**
    * What to do before checking the unsat cache.
    */
   public void onBeforeCheckUnsatCache();

   /**
    * What to do after checking the unsat cache.
    * 
    * @param status
    *           the status of the checked expression after the cache check.
    */
   public void onAfterCheckUnsatCache(CheckResult status);

   /**
    * What to do before extracting conjuncts from the checked expression.
    */
   public void onBeforeConjunctsExtraction();

   /**
    * What to do after extracting conjuncts from the checked expression.
    * 
    * @param nrClauses
    *           the number of conjuncts in the checked expression.
    */
   public void onAfterConjunctsExtraction(int nrClauses);

   /**
    * What to do before calling the underlying solver of the listened solver.
    */
   public void onBeforeSolver();

   /**
    * What to do after calling the underlying solver of the listened solver.
    * 
    * @param status
    *           the status returned by the listened solver.
    */
   public void onAfterSolver(CheckResult status);
}
