package julia.solver.random;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.BoolVal;
import julia.expr.model.Model;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.exc.NoModelException;
import julia.solver.exc.NoUnsatCoreException;
import julia.utils.Exprs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A solver backed by a cache storing solutions of formulas.
 * <p>
 * When a new formula is checked, this solver tries to reuse some randomly chosen
 * solutions from its cache to prove that it is satisfiable or unsatisfiable.
 */
public class RandomSolver extends Solver {
   final static Logger logger = LoggerFactory.getLogger(RandomSolver.class);

   private final Random random;
   private final List<Model> models;
   private final List<Set<IExpr>> cores;
   private final Solver solver;
   private int nrCandidates;

   private final List<RandomSolverListener> listeners;

   /**
    * Creates a new solver.
    * 
    * @param seed
    *           the seed for the random number generator of this solver.
    * @param solver
    *           the solver used to solve missed expressions.
    * @param nrCandidates
    *           the number of candidate solutions extracted from the caches.
    */
   public RandomSolver(long seed, Solver solver, int nrCandidates) {
      this.random = new Random(seed);
      this.models = new ArrayList<>();
      this.cores = new ArrayList<>();
      this.solver = solver;
      this.nrCandidates = nrCandidates;
      this.listeners = new ArrayList<>();
   }

   /**
    * Adds a new listener to this solver.
    * 
    * @param listener
    *           the listener to add.
    */
   public void addListener(RandomSolverListener listener) {
      this.listeners.add(listener);
   }

   /**
    * Returns {@code nrCandidates} random models from the cache.
    * 
    * @return {@code nrCandidates} random models from the cache.
    */
   private List<Model> getCandidateModels() {
      List<Model> result = new ArrayList<>();
      for (int i = 0; i < Math.min(this.nrCandidates, this.models.size()); ++i) {
         int index = this.random.nextInt(Integer.MAX_VALUE) % models.size();
         result.add(models.get(index));
      }
      return result;
   }

   /**
    * Returns {@code nrCandidates} random unsat-cores from the cache.
    * 
    * @return {@code nrCandidates} random unsat-cores from the cache.
    */
   private List<Set<IExpr>> getCandidateCores() {
      List<Set<IExpr>> result = new ArrayList<>();
      for (int i = 0; i < Math.min(this.nrCandidates, this.cores.size()); ++i) {
         int index = this.random.nextInt(Integer.MAX_VALUE) % this.cores.size();
         result.add(cores.get(index));
      }
      return result;
   }

   /**
    * Tries to reuse models to prove the given expression satisfiable.
    * 
    * @param expr
    *           the expression to solve.
    * @return the status of the given expression after the check.
    */
   private CheckResult checkSatCache(IExpr expr) {
      for (final Model model : this.getCandidateModels()) {
         IExpr eExpr = null;
         try {
            eExpr = expr.eval(model);
         } catch (Exception e) {
            // If evaluating a model throws an exception we consider it a cache miss.
            continue;
         }

         if (eExpr.equals(BoolVal.TRUE)) {
            this.setModel(model);
            return CheckResult.SAT;
         }
      }
      return CheckResult.UNKNOWN;
   }

   /**
    * Tries to reuse unsat-cores to prove the given set of conjuncts unsatisfiable.
    * 
    * @param conjuncts
    *           the conjuncts in the expression to solve.
    * @return the status of the set of conjuncts after the check.
    */
   private CheckResult checkUnsatCache(Set<IExpr> conjuncts) {
      for (final Set<IExpr> core : this.getCandidateCores()) {
         if (conjuncts.containsAll(core)) {
            this.setUnsatCore(core);
            return CheckResult.UNSAT;
         }
      }
      return CheckResult.UNKNOWN;
   }

   /**
    * Finds a solution for the given expression.
    * 
    * @param expr
    *           the expression to solve.
    * @return the status of the given expression.
    */
   private CheckResult checkAux(IExpr expr) {
      logger.info("Checking expression {}", expr);
      CheckResult status = CheckResult.UNKNOWN;

      logger.info("Checking the sat-cache");
      for (final RandomSolverListener listener : this.listeners) {
         listener.onBeforeCheckSatCache();
      }
      status = this.checkSatCache(expr);
      for (final RandomSolverListener listener : this.listeners) {
         listener.onAfterCheckSatCache(status);
      }

      if (status.equals(CheckResult.SAT)) {
         logger.info("The sat-cache produced a hit");
         return CheckResult.SAT;
      }

      logger.info("Extracting conjuncts");
      for (final RandomSolverListener listener : this.listeners) {
         listener.onBeforeConjunctsExtraction();
      }
      Set<IExpr> conjuncts = Exprs.getConjuncts(expr);
      logger.info("This expression has {} conjuncts", conjuncts.size());
      for (final RandomSolverListener listener : this.listeners) {
         listener.onAfterConjunctsExtraction(conjuncts.size());
      }

      logger.info("Checking the unsat-cache");
      for (final RandomSolverListener listener : this.listeners) {
         listener.onBeforeCheckUnsatCache();
      }
      status = this.checkUnsatCache(conjuncts);
      for (final RandomSolverListener listener : this.listeners) {
         listener.onAfterCheckUnsatCache(status);
      }

      if (status.equals(CheckResult.UNSAT)) {
         logger.info("The unsat-cache produced a hit");
         return CheckResult.UNSAT;
      }

      logger.info("Calling the solver");
      for (final RandomSolverListener listener : this.listeners) {
         listener.onBeforeSolver();
      }
      status = this.solver.check(expr);
      logger.info("The solver returned {}", status);
      for (final RandomSolverListener listener : this.listeners) {
         listener.onAfterSolver(status);
      }

      if (status.equals(CheckResult.SAT)) {
         try {
            Model model = solver.getModel();
            this.models.add(model);
            this.setModel(model);
         } catch (NoModelException e) {
            logger.warn("Model is not available");
         }
      } else if (status.equals(CheckResult.UNSAT)) {
         try {
            Set<IExpr> unsatCore = solver.getUnsatCore();
            this.cores.add(unsatCore);
            this.setUnsatCore(unsatCore);
         } catch (NoUnsatCoreException e) {
            logger.warn("UnsatCore is not available");
         }
      }

      return status;
   }

   @Override
   public CheckResult check(IExpr expr) {
      this.setModel(null);
      this.setUnsatCore(null);

      for (final RandomSolverListener listener : this.listeners) {
         listener.onBeforeCheck(expr);
      }
      CheckResult status = this.checkAux(expr);
      for (final RandomSolverListener listener : this.listeners) {
         listener.onAfterCheck(status);
      }
      return status;
   }
}