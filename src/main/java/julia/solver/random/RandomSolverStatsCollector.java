package julia.solver.random;

import julia.expr.ast.IExpr;
import julia.solver.CheckResult;
import julia.utils.Counter;
import julia.utils.Timer;

/**
 * Random solver listener that collects statistics.
 */
public class RandomSolverStatsCollector implements RandomSolverListener {
   private final static Timer timer = new Timer();

   private final Counter<String> counter;

   /**
    * Creates a new empty collector.
    */
   public RandomSolverStatsCollector() {
      this.counter = new Counter<>();
   }

   @Override
   public void onBeforeCheck(IExpr expr) {
      this.counter.increment("checks");
   }

   @Override
   public void onAfterCheck(CheckResult status) {
      switch (status) {
         case SAT:
            this.counter.increment("sat-exprs");
            break;
         case UNSAT:
            this.counter.increment("unsat-exprs");
            break;
         case UNKNOWN:
            this.counter.increment("unknown-exprs");
            break;
      }
   }

   @Override
   public void onBeforeCheckSatCache() {
      timer.start();
   }

   @Override
   public void onAfterCheckSatCache(CheckResult status) {
      this.counter.update("sat-cache-time", timer.stop());
      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("sat-cache-hits");
      }
   }

   @Override
   public void onBeforeCheckUnsatCache() {
      timer.start();
   }

   @Override
   public void onAfterCheckUnsatCache(CheckResult status) {
      this.counter.update("unsat-cache-time", timer.stop());
      if (!status.equals(CheckResult.UNKNOWN)) {
         this.counter.increment("unsat-cache-hits");
      }
   }

   @Override
   public void onBeforeConjunctsExtraction() {
      timer.start();
   }

   @Override
   public void onAfterConjunctsExtraction(int nrClauses) {
      this.counter.update("extract-conjuncts-time", timer.stop());
   }

   @Override
   public void onBeforeSolver() {
      timer.start();
   }

   @Override
   public void onAfterSolver(CheckResult status) {
      this.counter.update("solver-time", timer.stop());
      this.counter.increment("solver-calls");
   }

   /**
    * Returns the reuse ratio of the listened solver.
    * <p>
    * By convention, the reuse ratio of solver that checked no expressions is 100%.
    * 
    * @return the reuse ratio of the listened solver.
    */
   public double getReuseRatio() {
      long checks = this.counter.get("checks").longValue();
      if (checks <= 0) {
         return 1d;
      }
      long solverCalls = this.counter.get("solver-calls").longValue();
      long cacheHits = (checks - solverCalls);
      return ((double) cacheHits) / checks;
   }

   public String getStats() {
      StringBuilder builder = new StringBuilder();
      builder.append("[time(ns)]");
      builder.append("\ncheck-sat-cache = " + this.counter.get("sat-cache-time"));
      builder.append("\ncheck-unsat-cache = " + this.counter.get("unsat-cache-time"));
      builder.append("\ncall-solver = " + this.counter.get("solver-time"));
      builder.append("\n\n");

      builder.append("[reuse]");
      builder.append("\nnr-sat-cache-hits = " + this.counter.get("sat-cache-hits"));
      builder.append("\nnr-unsat-cache-hits = " + this.counter.get("unsat-cache-hits"));
      builder.append("\nnr-cache-misses = " + this.counter.get("solver-calls"));
      builder.append("\nreuse-ratio = " + this.getReuseRatio());
      builder.append("\n\n");

      builder.append("[expressions]");
      builder.append("\nchecked = " + this.counter.get("checks"));
      builder.append("\nsat = " + this.counter.get("sat-exprs"));
      builder.append("\nunsat = " + this.counter.get("unsat-exprs"));
      builder.append("\nunknown = " + this.counter.get("unknown-exprs"));

      return builder.toString();
   }
}