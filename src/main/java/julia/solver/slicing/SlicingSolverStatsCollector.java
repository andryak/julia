package julia.solver.slicing;

import julia.expr.ast.IExpr;
import julia.solver.CheckResult;
import julia.utils.Counter;
import julia.utils.RunningStats;
import julia.utils.Timer;

/**
 * Slicing solver listener that collects statistics.
 */
public class SlicingSolverStatsCollector implements SlicingSolverListener {
   private final static Timer timer = new Timer();

   private final Counter<String> counter;
   private final RunningStats stats;

   /**
    * Creates a new empty collector.
    */
   public SlicingSolverStatsCollector() {
      this.counter = new Counter<>();
      this.stats = new RunningStats();
   }

   @Override
   public void onBeforeCheck(IExpr expr) {
      this.counter.increment("checks");
   }

   @Override
   public void onAfterCheck(CheckResult status) {
      switch (status) {
         case SAT:
            this.counter.increment("sat-exprs");
            break;
         case UNSAT:
            this.counter.increment("unsat-exprs");
            break;
         case UNKNOWN:
            this.counter.increment("unknown-exprs");
            break;
      }
   }

   @Override
   public void onBeforeSlicing() {
      timer.start();
   }

   @Override
   public void onAfterSlicing(int nrExprs) {
      this.counter.update("slicing-time", timer.stop());
      this.stats.add(nrExprs);
   }

   @Override
   public void onBeforeSolver() {
      timer.start();
   }

   @Override
   public void onAfterSolver(CheckResult status) {
      this.counter.update("solver-time", timer.stop());
      this.counter.increment("solver-calls");
   }

   public String getStats() {
      StringBuilder builder = new StringBuilder();
      builder.append("[time(ns)]");
      builder.append("\nslicing = " + this.counter.get("slicing-time"));
      builder.append("\ncall-solver = " + this.counter.get("solver-time"));
      builder.append("\n\n");

      builder.append("[expressions]");
      builder.append("\nchecked = " + this.counter.get("checks"));
      builder.append("\nsat = " + this.counter.get("sat-exprs"));
      builder.append("\nunsat = " + this.counter.get("unsat-exprs"));
      builder.append("\nunknown = " + this.counter.get("unknown-exprs"));
      builder.append("\n\n");

      builder.append("[slice-size]");
      builder.append("\nmin = " + this.stats.min());
      builder.append("\nmax = " + this.stats.max());
      builder.append("\navg = " + this.stats.mean());
      builder.append("\nstdev = " + this.stats.stdev());

      return builder.toString();
   }
}