package julia.solver.slicing;

import julia.solver.CheckResult;
import julia.solver.SolverListener;

/**
 * Interface of slicing solver listeners.
 */
public interface SlicingSolverListener extends SolverListener {
   /**
    * What to do before slicing the checked expression.
    */
   public void onBeforeSlicing();

   /**
    * What to do after slicing the checked expression.
    * 
    * @param nrExprs
    *           the number of sliced sub-expressions of the checked expression.
    */
   public void onAfterSlicing(int nrExprs);

   /**
    * What to do before calling the underlying solver of the listened solver.
    */
   public void onBeforeSolver();

   /**
    * What to do after calling the underlying solver of the listened solver.
    * 
    * @param status
    *           the status returned by the listened solver.
    */
   public void onAfterSolver(CheckResult status);
}
