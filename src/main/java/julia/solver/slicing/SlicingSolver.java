package julia.solver.slicing;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.slicer.ExprSlicer;
import julia.solver.CheckResult;
import julia.solver.Solver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A solver which slices formulas before solving them.
 */
public class SlicingSolver extends Solver {
   final Logger logger = LoggerFactory.getLogger(SlicingSolver.class);

   /**
    * The underlying solver used to solve sliced sub-formulas.
    */
   private final Solver solver;

   private final List<SlicingSolverListener> listeners;

   /**
    * Creates a new slicing solver.
    * 
    * @param solver
    *           the underlying solver used to solve sliced sub-formulas.
    */
   public SlicingSolver(Solver solver) {
      this.solver = solver;
      this.listeners = new ArrayList<>();
   }

   /**
    * Adds a new listener to this solver.
    * 
    * @param listener
    *           the listener to add.
    */
   public void addListener(SlicingSolverListener listener) {
      this.listeners.add(listener);
   }

   /**
    * Finds a solution for the given expression.
    * 
    * @param expr
    *           the expression to solve.
    * @return the status of the given expression.
    */
   private CheckResult checkAux(IExpr expr) {
      logger.info("Checking expression {}", expr);

      for (final SlicingSolverListener listener : this.listeners) {
         listener.onBeforeSlicing();
      }
      Set<IExpr> slice = ExprSlicer.selfSlice(expr);
      logger.info("Slicing produced {} subformulas", slice.size());
      for (final SlicingSolverListener listener : this.listeners) {
         listener.onAfterSlicing(slice.size());
      }

      long unknowns = 0;
      for (final IExpr slicedExpr : slice) {
         // Compress the current sliced sub-expression.
         logger.info("Compressing subformula {}", slicedExpr);
         IExpr cexpr = slicedExpr.compress();

         logger.info("Checking subformula {}", cexpr);
         for (final SlicingSolverListener listener : this.listeners) {
            listener.onBeforeSolver();
         }
         CheckResult status = this.solver.check(cexpr);
         logger.info("The solver returned {}", status);
         for (final SlicingSolverListener listener : this.listeners) {
            listener.onAfterSolver(status);
         }

         // If any expression in the slice is unsatisfiable, then the
         // overall expression is also unsatisfiable.
         if (status.equals(CheckResult.UNSAT)) {
            logger.info("This subformula is unsat, return unsat");
            return CheckResult.UNSAT;
         } else if (status.equals(CheckResult.UNKNOWN)) {
            ++unknowns;
         }
      }
      // If some expressions in the slice are sat and others are unknown, then
      // the satisfiability of the overall expression is also unknown.
      if (unknowns > 0) {
         logger.info("Some subformulas are unknowns and none are unsat, return unknown");
         return CheckResult.UNKNOWN;
      }

      // If all expressions in the slice are sat, then the overall expression is also sat.
      logger.info("All subformulas are sat, return sat");
      return CheckResult.SAT;
   }

   @Override
   public CheckResult check(IExpr expr) {
      this.setModel(null);
      this.setUnsatCore(null);

      for (final SlicingSolverListener listener : this.listeners) {
         listener.onBeforeCheck(expr);
      }
      CheckResult status = this.checkAux(expr);
      for (final SlicingSolverListener listener : this.listeners) {
         listener.onAfterCheck(status);
      }
      return status;
   }
}
