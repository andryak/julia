// Generated from Z3Model.g4 by ANTLR 4.5.3
package julia.solver.z3.parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Z3ModelLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, USORT_VAR_ID=22, BOOL_VAR_ID=23, 
		BOOL_RES_VAR_ID=24, INT_VAR_ID=25, REAL_VAR_ID=26, STR_VAR_ID=27, BIT_VEC_VAR_ID=28, 
		ARR_VAR_ID=29, FUN_VAR_ID=30, GEN_VAR_ID=31, DECLARE_SORT=32, INT=33, 
		REAL=34, STR=35, BIN=36, HEX=37, WS=38, LINE_COMMENT=39, DECLARE_FUN=40, 
		FORALL=41;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "USORT_VAR_ID", "BOOL_VAR_ID", "BOOL_RES_VAR_ID", 
		"INT_VAR_ID", "REAL_VAR_ID", "STR_VAR_ID", "BIT_VEC_VAR_ID", "ARR_VAR_ID", 
		"FUN_VAR_ID", "GEN_VAR_ID", "DECLARE_SORT", "INT", "REAL", "STR", "BIN", 
		"HEX", "INT_DIGIT", "STRING_CHR", "BIN_DIGIT", "HEX_DIGIT", "WS", "LINE_COMMENT", 
		"DECLARE_FUN", "FORALL"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'(model'", "')'", "'('", "'define-fun'", "'()'", "'_'", "'as-array'", 
		"'Bool'", "'Int'", "'Real'", "'String'", "'BitVec'", "'Array'", "'!val!'", 
		"'true'", "'false'", "'-'", "'/'", "'ite'", "'='", "'and'", "'usort'", 
		"'bool'", "'_c'", "'int'", "'real'", "'string'", "'bitvec'", "'array'", 
		"'fun'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, "USORT_VAR_ID", 
		"BOOL_VAR_ID", "BOOL_RES_VAR_ID", "INT_VAR_ID", "REAL_VAR_ID", "STR_VAR_ID", 
		"BIT_VEC_VAR_ID", "ARR_VAR_ID", "FUN_VAR_ID", "GEN_VAR_ID", "DECLARE_SORT", 
		"INT", "REAL", "STR", "BIN", "HEX", "WS", "LINE_COMMENT", "DECLARE_FUN", 
		"FORALL"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public Z3ModelLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Z3Model.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2+\u0168\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13"+
		"\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20"+
		"\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24"+
		"\3\24\3\24\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\33\3\33"+
		"\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3 \6"+
		" \u00f3\n \r \16 \u00f4\3 \3 \3 \3!\6!\u00fb\n!\r!\16!\u00fc\3\"\6\"\u0100"+
		"\n\"\r\"\16\"\u0101\3#\6#\u0105\n#\r#\16#\u0106\3#\3#\6#\u010b\n#\r#\16"+
		"#\u010c\3$\3$\3$\3$\7$\u0113\n$\f$\16$\u0116\13$\3$\3$\3%\3%\3%\3%\6%"+
		"\u011e\n%\r%\16%\u011f\3&\3&\3&\3&\6&\u0126\n&\r&\16&\u0127\3\'\3\'\3"+
		"(\3(\3)\3)\3*\3*\3+\6+\u0133\n+\r+\16+\u0134\3+\3+\3,\3,\3,\3,\7,\u013d"+
		"\n,\f,\16,\u0140\13,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\7"+
		"-\u0152\n-\f-\16-\u0155\13-\3-\3-\3.\3.\3.\3.\3.\3.\3.\3.\3.\7.\u0162"+
		"\n.\f.\16.\u0165\13.\3.\3.\2\2/\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23"+
		"\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31"+
		"\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M\2O\2Q\2S\2U(W)Y*["+
		"+\3\2\13\3\2c|\3\2C\\\3\2\62;\3\2\60\60\5\2\f\f\17\17$$\3\2\62\63\5\2"+
		"\62;CHch\5\2\13\f\17\17\"\"\4\2\f\f\17\17\u0170\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2"+
		"\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\3]\3\2\2\2\5d\3\2\2\2\7"+
		"f\3\2\2\2\th\3\2\2\2\13s\3\2\2\2\rv\3\2\2\2\17x\3\2\2\2\21\u0081\3\2\2"+
		"\2\23\u0086\3\2\2\2\25\u008a\3\2\2\2\27\u008f\3\2\2\2\31\u0096\3\2\2\2"+
		"\33\u009d\3\2\2\2\35\u00a3\3\2\2\2\37\u00a9\3\2\2\2!\u00ae\3\2\2\2#\u00b4"+
		"\3\2\2\2%\u00b6\3\2\2\2\'\u00b8\3\2\2\2)\u00bc\3\2\2\2+\u00be\3\2\2\2"+
		"-\u00c2\3\2\2\2/\u00c8\3\2\2\2\61\u00cd\3\2\2\2\63\u00d0\3\2\2\2\65\u00d4"+
		"\3\2\2\2\67\u00d9\3\2\2\29\u00e0\3\2\2\2;\u00e7\3\2\2\2=\u00ed\3\2\2\2"+
		"?\u00f2\3\2\2\2A\u00fa\3\2\2\2C\u00ff\3\2\2\2E\u0104\3\2\2\2G\u010e\3"+
		"\2\2\2I\u0119\3\2\2\2K\u0121\3\2\2\2M\u0129\3\2\2\2O\u012b\3\2\2\2Q\u012d"+
		"\3\2\2\2S\u012f\3\2\2\2U\u0132\3\2\2\2W\u0138\3\2\2\2Y\u0143\3\2\2\2["+
		"\u0158\3\2\2\2]^\7*\2\2^_\7o\2\2_`\7q\2\2`a\7f\2\2ab\7g\2\2bc\7n\2\2c"+
		"\4\3\2\2\2de\7+\2\2e\6\3\2\2\2fg\7*\2\2g\b\3\2\2\2hi\7f\2\2ij\7g\2\2j"+
		"k\7h\2\2kl\7k\2\2lm\7p\2\2mn\7g\2\2no\7/\2\2op\7h\2\2pq\7w\2\2qr\7p\2"+
		"\2r\n\3\2\2\2st\7*\2\2tu\7+\2\2u\f\3\2\2\2vw\7a\2\2w\16\3\2\2\2xy\7c\2"+
		"\2yz\7u\2\2z{\7/\2\2{|\7c\2\2|}\7t\2\2}~\7t\2\2~\177\7c\2\2\177\u0080"+
		"\7{\2\2\u0080\20\3\2\2\2\u0081\u0082\7D\2\2\u0082\u0083\7q\2\2\u0083\u0084"+
		"\7q\2\2\u0084\u0085\7n\2\2\u0085\22\3\2\2\2\u0086\u0087\7K\2\2\u0087\u0088"+
		"\7p\2\2\u0088\u0089\7v\2\2\u0089\24\3\2\2\2\u008a\u008b\7T\2\2\u008b\u008c"+
		"\7g\2\2\u008c\u008d\7c\2\2\u008d\u008e\7n\2\2\u008e\26\3\2\2\2\u008f\u0090"+
		"\7U\2\2\u0090\u0091\7v\2\2\u0091\u0092\7t\2\2\u0092\u0093\7k\2\2\u0093"+
		"\u0094\7p\2\2\u0094\u0095\7i\2\2\u0095\30\3\2\2\2\u0096\u0097\7D\2\2\u0097"+
		"\u0098\7k\2\2\u0098\u0099\7v\2\2\u0099\u009a\7X\2\2\u009a\u009b\7g\2\2"+
		"\u009b\u009c\7e\2\2\u009c\32\3\2\2\2\u009d\u009e\7C\2\2\u009e\u009f\7"+
		"t\2\2\u009f\u00a0\7t\2\2\u00a0\u00a1\7c\2\2\u00a1\u00a2\7{\2\2\u00a2\34"+
		"\3\2\2\2\u00a3\u00a4\7#\2\2\u00a4\u00a5\7x\2\2\u00a5\u00a6\7c\2\2\u00a6"+
		"\u00a7\7n\2\2\u00a7\u00a8\7#\2\2\u00a8\36\3\2\2\2\u00a9\u00aa\7v\2\2\u00aa"+
		"\u00ab\7t\2\2\u00ab\u00ac\7w\2\2\u00ac\u00ad\7g\2\2\u00ad \3\2\2\2\u00ae"+
		"\u00af\7h\2\2\u00af\u00b0\7c\2\2\u00b0\u00b1\7n\2\2\u00b1\u00b2\7u\2\2"+
		"\u00b2\u00b3\7g\2\2\u00b3\"\3\2\2\2\u00b4\u00b5\7/\2\2\u00b5$\3\2\2\2"+
		"\u00b6\u00b7\7\61\2\2\u00b7&\3\2\2\2\u00b8\u00b9\7k\2\2\u00b9\u00ba\7"+
		"v\2\2\u00ba\u00bb\7g\2\2\u00bb(\3\2\2\2\u00bc\u00bd\7?\2\2\u00bd*\3\2"+
		"\2\2\u00be\u00bf\7c\2\2\u00bf\u00c0\7p\2\2\u00c0\u00c1\7f\2\2\u00c1,\3"+
		"\2\2\2\u00c2\u00c3\7w\2\2\u00c3\u00c4\7u\2\2\u00c4\u00c5\7q\2\2\u00c5"+
		"\u00c6\7t\2\2\u00c6\u00c7\7v\2\2\u00c7.\3\2\2\2\u00c8\u00c9\7d\2\2\u00c9"+
		"\u00ca\7q\2\2\u00ca\u00cb\7q\2\2\u00cb\u00cc\7n\2\2\u00cc\60\3\2\2\2\u00cd"+
		"\u00ce\7a\2\2\u00ce\u00cf\7e\2\2\u00cf\62\3\2\2\2\u00d0\u00d1\7k\2\2\u00d1"+
		"\u00d2\7p\2\2\u00d2\u00d3\7v\2\2\u00d3\64\3\2\2\2\u00d4\u00d5\7t\2\2\u00d5"+
		"\u00d6\7g\2\2\u00d6\u00d7\7c\2\2\u00d7\u00d8\7n\2\2\u00d8\66\3\2\2\2\u00d9"+
		"\u00da\7u\2\2\u00da\u00db\7v\2\2\u00db\u00dc\7t\2\2\u00dc\u00dd\7k\2\2"+
		"\u00dd\u00de\7p\2\2\u00de\u00df\7i\2\2\u00df8\3\2\2\2\u00e0\u00e1\7d\2"+
		"\2\u00e1\u00e2\7k\2\2\u00e2\u00e3\7v\2\2\u00e3\u00e4\7x\2\2\u00e4\u00e5"+
		"\7g\2\2\u00e5\u00e6\7e\2\2\u00e6:\3\2\2\2\u00e7\u00e8\7c\2\2\u00e8\u00e9"+
		"\7t\2\2\u00e9\u00ea\7t\2\2\u00ea\u00eb\7c\2\2\u00eb\u00ec\7{\2\2\u00ec"+
		"<\3\2\2\2\u00ed\u00ee\7h\2\2\u00ee\u00ef\7w\2\2\u00ef\u00f0\7p\2\2\u00f0"+
		">\3\2\2\2\u00f1\u00f3\t\2\2\2\u00f2\u00f1\3\2\2\2\u00f3\u00f4\3\2\2\2"+
		"\u00f4\u00f2\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f7"+
		"\7#\2\2\u00f7\u00f8\5C\"\2\u00f8@\3\2\2\2\u00f9\u00fb\t\3\2\2\u00fa\u00f9"+
		"\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd"+
		"B\3\2\2\2\u00fe\u0100\5M\'\2\u00ff\u00fe\3\2\2\2\u0100\u0101\3\2\2\2\u0101"+
		"\u00ff\3\2\2\2\u0101\u0102\3\2\2\2\u0102D\3\2\2\2\u0103\u0105\t\4\2\2"+
		"\u0104\u0103\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0104\3\2\2\2\u0106\u0107"+
		"\3\2\2\2\u0107\u0108\3\2\2\2\u0108\u010a\t\5\2\2\u0109\u010b\t\4\2\2\u010a"+
		"\u0109\3\2\2\2\u010b\u010c\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2"+
		"\2\2\u010dF\3\2\2\2\u010e\u0114\7$\2\2\u010f\u0113\5O(\2\u0110\u0111\7"+
		"$\2\2\u0111\u0113\7$\2\2\u0112\u010f\3\2\2\2\u0112\u0110\3\2\2\2\u0113"+
		"\u0116\3\2\2\2\u0114\u0112\3\2\2\2\u0114\u0115\3\2\2\2\u0115\u0117\3\2"+
		"\2\2\u0116\u0114\3\2\2\2\u0117\u0118\7$\2\2\u0118H\3\2\2\2\u0119\u011a"+
		"\7%\2\2\u011a\u011b\7d\2\2\u011b\u011d\3\2\2\2\u011c\u011e\5Q)\2\u011d"+
		"\u011c\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u011d\3\2\2\2\u011f\u0120\3\2"+
		"\2\2\u0120J\3\2\2\2\u0121\u0122\7%\2\2\u0122\u0123\7z\2\2\u0123\u0125"+
		"\3\2\2\2\u0124\u0126\5S*\2\u0125\u0124\3\2\2\2\u0126\u0127\3\2\2\2\u0127"+
		"\u0125\3\2\2\2\u0127\u0128\3\2\2\2\u0128L\3\2\2\2\u0129\u012a\t\4\2\2"+
		"\u012aN\3\2\2\2\u012b\u012c\n\6\2\2\u012cP\3\2\2\2\u012d\u012e\t\7\2\2"+
		"\u012eR\3\2\2\2\u012f\u0130\t\b\2\2\u0130T\3\2\2\2\u0131\u0133\t\t\2\2"+
		"\u0132\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134\u0132\3\2\2\2\u0134\u0135"+
		"\3\2\2\2\u0135\u0136\3\2\2\2\u0136\u0137\b+\2\2\u0137V\3\2\2\2\u0138\u0139"+
		"\7=\2\2\u0139\u013a\7=\2\2\u013a\u013e\3\2\2\2\u013b\u013d\n\n\2\2\u013c"+
		"\u013b\3\2\2\2\u013d\u0140\3\2\2\2\u013e\u013c\3\2\2\2\u013e\u013f\3\2"+
		"\2\2\u013f\u0141\3\2\2\2\u0140\u013e\3\2\2\2\u0141\u0142\b,\3\2\u0142"+
		"X\3\2\2\2\u0143\u0144\7*\2\2\u0144\u0145\7f\2\2\u0145\u0146\7g\2\2\u0146"+
		"\u0147\7e\2\2\u0147\u0148\7n\2\2\u0148\u0149\7c\2\2\u0149\u014a\7t\2\2"+
		"\u014a\u014b\7g\2\2\u014b\u014c\7/\2\2\u014c\u014d\7h\2\2\u014d\u014e"+
		"\7w\2\2\u014e\u014f\7p\2\2\u014f\u0153\3\2\2\2\u0150\u0152\n\n\2\2\u0151"+
		"\u0150\3\2\2\2\u0152\u0155\3\2\2\2\u0153\u0151\3\2\2\2\u0153\u0154\3\2"+
		"\2\2\u0154\u0156\3\2\2\2\u0155\u0153\3\2\2\2\u0156\u0157\b-\3\2\u0157"+
		"Z\3\2\2\2\u0158\u0159\7*\2\2\u0159\u015a\7h\2\2\u015a\u015b\7q\2\2\u015b"+
		"\u015c\7t\2\2\u015c\u015d\7c\2\2\u015d\u015e\7n\2\2\u015e\u015f\7n\2\2"+
		"\u015f\u0163\3\2\2\2\u0160\u0162\n\n\2\2\u0161\u0160\3\2\2\2\u0162\u0165"+
		"\3\2\2\2\u0163\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166\3\2\2\2\u0165"+
		"\u0163\3\2\2\2\u0166\u0167\b.\3\2\u0167\\\3\2\2\2\20\2\u00f4\u00fc\u0101"+
		"\u0106\u010c\u0112\u0114\u011f\u0127\u0134\u013e\u0153\u0163\4\2\3\2\b"+
		"\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}