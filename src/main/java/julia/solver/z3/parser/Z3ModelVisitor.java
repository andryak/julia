// Generated from Z3Model.g4 by ANTLR 4.5.3
package julia.solver.z3.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Z3ModelParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface Z3ModelVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#model}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModel(Z3ModelParser.ModelContext ctx);
	/**
	 * Visit a parse tree produced by the {@code USortDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUSortDef(Z3ModelParser.USortDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolDef(Z3ModelParser.BoolDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolResDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolResDef(Z3ModelParser.BoolResDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntDef(Z3ModelParser.IntDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealDef(Z3ModelParser.RealDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitVecDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecDef(Z3ModelParser.BitVecDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArrayDecl}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayDecl(Z3ModelParser.ArrayDeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArrayDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayDef(Z3ModelParser.ArrayDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FunDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunDef(Z3ModelParser.FunDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StrDef}
	 * labeled alternative in {@link Z3ModelParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrDef(Z3ModelParser.StrDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#declareSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareSort(Z3ModelParser.DeclareSortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#boolSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolSort(Z3ModelParser.BoolSortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#intSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntSort(Z3ModelParser.IntSortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#realSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealSort(Z3ModelParser.RealSortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#strSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrSort(Z3ModelParser.StrSortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#bitVecSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecSort(Z3ModelParser.BitVecSortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#concreteSort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcreteSort(Z3ModelParser.ConcreteSortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ConcreteArraySort}
	 * labeled alternative in {@link Z3ModelParser#arraySort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcreteArraySort(Z3ModelParser.ConcreteArraySortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AbstractArraySort}
	 * labeled alternative in {@link Z3ModelParser#arraySort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbstractArraySort(Z3ModelParser.AbstractArraySortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#anySort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnySort(Z3ModelParser.AnySortContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#uSortVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUSortVal(Z3ModelParser.USortValContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#boolVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolVal(Z3ModelParser.BoolValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link Z3ModelParser#intVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt(Z3ModelParser.IntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntNeg}
	 * labeled alternative in {@link Z3ModelParser#intVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntNeg(Z3ModelParser.IntNegContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Real}
	 * labeled alternative in {@link Z3ModelParser#realVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReal(Z3ModelParser.RealContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealRational}
	 * labeled alternative in {@link Z3ModelParser#realVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealRational(Z3ModelParser.RealRationalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RealNeg}
	 * labeled alternative in {@link Z3ModelParser#realVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealNeg(Z3ModelParser.RealNegContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#strVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrVal(Z3ModelParser.StrValContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#bitVecVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitVecVal(Z3ModelParser.BitVecValContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#concreteVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcreteVal(Z3ModelParser.ConcreteValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ConstArray}
	 * labeled alternative in {@link Z3ModelParser#arrayVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstArray(Z3ModelParser.ConstArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NonConstArray}
	 * labeled alternative in {@link Z3ModelParser#arrayVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonConstArray(Z3ModelParser.NonConstArrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link Z3ModelParser#arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg(Z3ModelParser.ArgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SingleFunArg}
	 * labeled alternative in {@link Z3ModelParser#argsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleFunArg(Z3ModelParser.SingleFunArgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ManyFunArgs}
	 * labeled alternative in {@link Z3ModelParser#argsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitManyFunArgs(Z3ModelParser.ManyFunArgsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ConstFun}
	 * labeled alternative in {@link Z3ModelParser#funVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstFun(Z3ModelParser.ConstFunContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NonConstFun}
	 * labeled alternative in {@link Z3ModelParser#funVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonConstFun(Z3ModelParser.NonConstFunContext ctx);
}