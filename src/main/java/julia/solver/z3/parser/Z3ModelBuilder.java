package julia.solver.z3.parser;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.array.Array;
import julia.expr.ast.array.ArrayVal;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bv.BitVec;
import julia.expr.ast.bv.BitVecVal;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntNeg;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.Real;
import julia.expr.ast.reals.RealNeg;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.str.Str;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.uf.Fun;
import julia.expr.ast.uf.FunVal;
import julia.expr.ast.usort.USort;
import julia.expr.ast.usort.USortVal;
import julia.expr.model.MapModel;
import julia.expr.model.Model;
import julia.expr.model.SortMapModel;
import julia.expr.sort.ArraySort;
import julia.expr.sort.BitVecSort;
import julia.expr.sort.BoolSort;
import julia.expr.sort.DeclareSort;
import julia.expr.sort.ExprSort;
import julia.expr.sort.FunSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.RealSort;
import julia.expr.sort.StrSort;
import julia.expr.utils.BigRational;
import julia.expr.utils.BitVector;
import julia.expr.utils.ConstArray;
import julia.expr.utils.FunInter;
import julia.solver.z3.parser.Z3ModelParser.ArgContext;
import julia.utils.Z3Utils;

/**
 * Builder to construct total models from strings produced by Microsoft Z3.
 */
public class Z3ModelBuilder extends Z3ModelBaseVisitor<Object> {
   private Map<Fun, IConst> vars = new HashMap<>();

   /**
    * Returns the model built from the visited model tree.
    * <p>
    * The returned model is extended with default assignments to make it total.
    * 
    * @return the model built from the visited model tree.
    * 
    * @see SortMapModel
    */
   public Model getModel() {
      return new SortMapModel(new MapModel(vars));
   }

   @Override
   public Void visitUSortDef(Z3ModelParser.USortDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      USortVal value = (USortVal) super.visit(ctx.uSortVal());
      vars.put(USort.create(index, value.sort()), value);
      return null;
   }

   @Override
   public Void visitBoolDef(Z3ModelParser.BoolDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      BoolVal value = (BoolVal) super.visit(ctx.boolVal());
      vars.put(Bool.create(index), value);
      return null;
   }

   @Override
   public Void visitIntDef(Z3ModelParser.IntDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      IntVal value = (IntVal) super.visit(ctx.intVal());
      vars.put(Int.create(index), value);
      return null;
   }

   @Override
   public Void visitRealDef(Z3ModelParser.RealDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      RealVal value = (RealVal) super.visit(ctx.realVal());
      vars.put(Real.create(index), value);
      return null;
   }

   @Override
   public Void visitBitVecDef(Z3ModelParser.BitVecDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      BitVecVal value = (BitVecVal) super.visit(ctx.bitVecVal());
      vars.put(BitVec.create(index, value.sort().getSize()), value);
      return null;
   }

   @Override
   public Void visitStrDef(Z3ModelParser.StrDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      StrVal value = (StrVal) super.visit(ctx.strVal());
      vars.put(Str.create(index), value);
      return null;
   }

   @Override
   public Void visitFunDef(Z3ModelParser.FunDefContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      FunInter inter = (FunInter) super.visit(ctx.funVal());

      List<ExprSort> sorts = ctx
            .anySort()
            .stream()
            .map(e -> (ExprSort) super.visit(e))
            .collect(Collectors.toList());

      // The last sort is the one of the range.
      ExprSort range = sorts.remove(sorts.size() - 1);
      // All the others are part of the domain.
      List<ExprSort> domain = sorts;

      FunSort sort = FunSort.create(domain, range);
      FunVal value = FunVal.create(inter, sort);

      vars.put(Fun.create(index, sort), value);
      return null;
   }

   @Override
   public USortVal visitUSortVal(Z3ModelParser.USortValContext ctx) {
      DeclareSort sort = DeclareSort.create(ctx.sortName.getText());
      Integer index = Integer.parseInt(ctx.index.getText());
      return USortVal.create(index, sort);
   }

   @Override
   public BoolVal visitBoolVal(Z3ModelParser.BoolValContext ctx) {
      if (Boolean.parseBoolean(ctx.getText())) {
         return BoolVal.TRUE;
      } else {
         return BoolVal.FALSE;
      }
   }

   @Override
   public StrVal visitStrVal(Z3ModelParser.StrValContext ctx) {
      String expr = ctx.getText().substring(1, ctx.getText().length() - 1);
      return StrVal.create(Z3Utils.escape(expr));
   }

   @Override
   public StrSort visitStrSort(Z3ModelParser.StrSortContext ctx) {
      return StrSort.create();
   }

   @Override
   public IntVal visitInt(Z3ModelParser.IntContext ctx) {
      return IntVal.create(BigInteger.valueOf(Long.parseLong(ctx.INT().getText())));
   }

   @Override
   public RealVal visitReal(Z3ModelParser.RealContext ctx) {
      BigRational v = BigRational.create(ctx.REAL().toString());
      return RealVal.create(v);
   }

   @Override
   public RealVal visitRealRational(Z3ModelParser.RealRationalContext ctx) {
      BigRational p = BigRational.create(ctx.p.getText());
      BigRational q = BigRational.create(ctx.q.getText());
      return RealVal.create(p.divide(q));
   }

   @Override
   public IntVal visitIntNeg(Z3ModelParser.IntNegContext ctx) {
      IntVal expr = (IntVal) super.visit(ctx.intVal());
      return (IntVal) IntNeg.create(expr);
   }

   @Override
   public RealVal visitRealNeg(Z3ModelParser.RealNegContext ctx) {
      RealVal expr = (RealVal) super.visit(ctx.realVal());
      return (RealVal) RealNeg.create(expr);
   }

   @Override
   public BitVecVal visitBitVecVal(Z3ModelParser.BitVecValContext ctx) {
      return BitVecVal.create(BitVector.valueOf(ctx.getText()));
   }

   /**
    * Backup map to track the declarations and definitions of array variables.
    */
   private final Map<String, Integer> arrayRefToIndex = new HashMap<>();

   @Override
   public Void visitArrayDecl(Z3ModelParser.ArrayDeclContext ctx) {
      Integer index = Integer.parseInt(ctx.index.getText());
      String ref = ctx.ref.getText();
      arrayRefToIndex.put(ref, index);
      return null;
   }

   @Override
   public Void visitArrayDef(Z3ModelParser.ArrayDefContext ctx) {
      String arrRef = ctx.arrRef.getText();
      ExprSort indexSort = (ExprSort) super.visit(ctx.indexSort);
      ExprSort valueSort = (ExprSort) super.visit(ctx.valueSort);
      ConstArray arrayExpr = (ConstArray) super.visit(ctx.arrayVal());

      ArrayVal value = ArrayVal.create(arrayExpr, indexSort, valueSort);

      Integer index = arrayRefToIndex.get(arrRef);
      vars.put(Array.create(index, indexSort, valueSort), value);

      return null;
   }

   @Override
   public DeclareSort visitDeclareSort(Z3ModelParser.DeclareSortContext ctx) {
      return DeclareSort.create(ctx.DECLARE_SORT().getText());
   }

   @Override
   public BoolSort visitBoolSort(Z3ModelParser.BoolSortContext ctx) {
      return BoolSort.create();
   }

   @Override
   public IntSort visitIntSort(Z3ModelParser.IntSortContext ctx) {
      return IntSort.create();
   }

   @Override
   public RealSort visitRealSort(Z3ModelParser.RealSortContext ctx) {
      return RealSort.create();
   }

   @Override
   public BitVecSort visitBitVecSort(Z3ModelParser.BitVecSortContext ctx) {
      BigInteger size = new BigInteger(ctx.size.getText());
      return BitVecSort.create(size);
   }

   @Override
   public ArraySort visitConcreteArraySort(Z3ModelParser.ConcreteArraySortContext ctx) {
      ExprSort indexSort = (ExprSort) super.visit(ctx.indexSort);
      ExprSort valueSort = (ExprSort) super.visit(ctx.valueSort);
      return ArraySort.create(indexSort, valueSort);
   }

   @Override
   public ArraySort visitAbstractArraySort(Z3ModelParser.AbstractArraySortContext ctx) {
      ExprSort indexSort = (ExprSort) super.visit(ctx.indexSort);
      ExprSort valueSort = (ExprSort) super.visit(ctx.valueSort);
      return ArraySort.create(indexSort, valueSort);
   }

   @Override
   public ConstArray visitConstArray(Z3ModelParser.ConstArrayContext ctx) {
      IConst value = (IConst) super.visit(ctx.concreteVal());
      return new ConstArray(value);
   }

   @Override
   public ConstArray visitNonConstArray(Z3ModelParser.NonConstArrayContext ctx) {
      IConst index = (IConst) super.visit(ctx.index);
      IConst value = (IConst) super.visit(ctx.value);
      return ((ConstArray) super.visit(ctx.arrayVal())).set(index, value);
   }

   @Override
   public List<IExpr> visitArg(Z3ModelParser.ArgContext ctx) {
      return Arrays.asList((IExpr) super.visit(ctx.concreteVal()));
   }

   @Override
   public List<IExpr> visitSingleFunArg(Z3ModelParser.SingleFunArgContext ctx) {
      return Arrays.asList((IExpr) super.visit(ctx.arg()));
   }

   @SuppressWarnings("unchecked")
   @Override
   public List<IExpr> visitManyFunArgs(Z3ModelParser.ManyFunArgsContext ctx) {
      List<IExpr> args = new ArrayList<>();
      for (final ArgContext arg : ctx.arg()) {
         args.addAll((List<IExpr>) super.visit(arg));
      }
      return args;
   }

   @Override
   public FunInter visitConstFun(Z3ModelParser.ConstFunContext ctx) {
      IConst value = (IConst) super.visit(ctx.concreteVal());
      return new FunInter(value);
   }

   @Override
   public FunInter visitNonConstFun(Z3ModelParser.NonConstFunContext ctx) {
      @SuppressWarnings("unchecked")
      List<IConst> args = (List<IConst>) super.visit(ctx.args);
      IConst value = (IConst) super.visit(ctx.value);
      return ((FunInter) super.visit(ctx.funVal())).set(args, value);
   }
}