package julia.solver.z3;

import julia.solver.SolverListener;

/**
 * Interface of the listeners of the Microsoft Z3 SMT solver.
 */
public interface Z3SolverListener extends SolverListener {
}
