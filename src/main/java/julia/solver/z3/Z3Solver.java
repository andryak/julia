package julia.solver.z3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.solver.z3.parser.Z3ModelBuilder;
import julia.solver.z3.parser.Z3ModelLexer;
import julia.solver.z3.parser.Z3ModelParser;
import julia.utils.Exprs;
import julia.utils.Exprs.SmtDecl;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service to communicate with the Z3 SMT solver.
 * <p>
 * This class bridges the <tt>expr</tt> package with the external Microsoft Z3 executable
 * allowing determining the satisfiability of boolean expressions.
 */
public class Z3Solver extends Solver {
   final static Logger logger = LoggerFactory.getLogger(Z3Solver.class);

   private static final String[] ARGUMENTS = { "-in", "-smt2" };
   private static final String[] COMMANDS = { "(set-option :produce-models true)",
         "(set-option :produce-unsat-cores true)" };

   private Process process;
   private BufferedReader processIn;
   private BufferedWriter processOut;

   private final List<Z3SolverListener> listeners;

   /**
    * Creates a new solver object to communicate with the Z3 SMT solver.
    * 
    * @param executable
    *           the path to the Z3 executable.
    */
   public Z3Solver(String executable) {
      // Build the command to run the Z3 SMT solver.
      List<String> commands = new ArrayList<String>();
      commands.add(executable);
      for (final String arg : ARGUMENTS) {
         commands.add(arg);
      }

      ProcessBuilder processBuilder = new ProcessBuilder(commands);
      try {
         this.process = processBuilder.start();
         this.processIn = new BufferedReader(new InputStreamReader(process.getInputStream()));
         this.processOut = new BufferedWriter(
               new OutputStreamWriter(process.getOutputStream()));

         // Push initial queries to the solver.
         for (final String cmd : COMMANDS) {
            this.communicate(cmd);
         }
      } catch (IOException e) {
         e.printStackTrace();
      }

      this.listeners = new ArrayList<>();
   }

   /**
    * Adds a new listener to this solver.
    * 
    * @param listener
    *           the listener to add.
    */
   public void addListener(Z3SolverListener listener) {
      this.listeners.add(listener);
   }

   /**
    * Sends the given command to the solver without flushing the output stream.
    * <p>
    * The command might not be received by the solver until a flush is explicitly invoked.
    * 
    * @param command
    *           the command to send to the Z3 SMT solver.
    */
   private void communicate(String command) throws IOException {
      this.communicate(command, false);
   }

   /**
    * Sends the given command to the solver.
    * <p>
    * If flush is set to true, the output stream is flushed.
    * 
    * @param command
    *           the command to send to the Z3 SMT solver.
    * 
    * @param flush
    *           if true, the command is sent to the solver immediately, otherwise it might
    *           be delayed.
    */
   private void communicate(String command, boolean flush) throws IOException {
      this.processOut.write(command + "\n");
      if (flush) {
         this.processOut.flush();
      }
   }

   /**
    * Reads the result of a check-sat command from the solver.
    * 
    * @return the result of the check-sat command read from the solver.
    */
   private CheckResult parseStatus() throws IOException {
      String line = this.processIn.readLine();
      logger.info("Status line read from the solver is {}", line);

      if (line.equals("sat")) {
         return CheckResult.SAT;
      } else if (line.equals("unsat")) {
         return CheckResult.UNSAT;
      } else if (line.equals("unknown")) {
         return CheckResult.UNKNOWN;
      }

      final String format = "Cannot parse the solver status, expected \"sat\", \"unsat\" or \"unknown\", got %s.";
      throw new IOException(String.format(format, line));
   }

   /**
    * Read character by character a lisp-like object written by the solver.
    * <p>
    * All character until (and including) the newline following the end of the read
    * lisp-like object are skipped so that it is possible to keep communicating with the
    * solver with a line-based approach.
    * <p>
    * <b>Notice:</b> a lisp-like object is an object consistently wrapped into parentheses
    * potentially contained other lisp-objects. The models produced by Z3 are lisp-like
    * objects.
    * 
    * @return the read lisp-like object as a string.
    */
   private String readLispLikeObject() throws IOException {
      StringBuilder builder = new StringBuilder();
      int paras = 0;
      do {
         int c = this.processIn.read();

         // If the end of the stream is reached, return.
         if (c < 0) {
            return builder.toString();
         }

         builder.append((char) c);
         if (c == '(') {
            ++paras;
         } else if (c == ')') {
            --paras;
         }
      } while (paras > 0);

      // Skip the rest of the line.
      this.processIn.readLine();

      return builder.toString();
   }

   /**
    * Reads the model produced by Z3 and transforms it into a total model using an ANTLR
    * parser.
    * 
    * @return the model produced by Z3, or null if such model cannot be decoded.
    */
   private Model parseModel() throws IOException {
      logger.info("Reading the model from the solver");

      String modelString = this.readLispLikeObject();
      logger.info("The model is {}", modelString);

      logger.info("Initializing ANTLR to parse the model");
      ANTLRInputStream input = new ANTLRInputStream(modelString);
      Z3ModelLexer lex = new Z3ModelLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lex);
      Z3ModelParser parser = new Z3ModelParser(tokens);

      logger.info("Parsing the model to build a parse tree");
      ParseTree tree = parser.model();

      // In case of syntax errors, return null.
      if (parser.getNumberOfSyntaxErrors() > 0) {
         logger.error("The model cannot be parsed");
         return null;
      }

      logger.info("Building the model from the parse tree");
      Z3ModelBuilder modelBuilder = new Z3ModelBuilder();
      modelBuilder.visit(tree);

      Model model = modelBuilder.getModel();
      logger.info("Model built is {}", model);

      return model;
   }

   /**
    * Reads the unsat-core produced by Z3 and transforms it into a set of conjuncts.
    * 
    * @param conjuncts
    *           an array of conjuncts in the order they were submitted as queries to the
    *           Z3 SMT solver.
    */
   private Set<IExpr> parseUnsatCore(IExpr[] conjuncts) throws IOException {
      logger.info("Reading the unsat-core from the solver");

      String unsatCoreString = this.readLispLikeObject();
      logger.info("The unsat-core is {}", unsatCoreString);

      // Drop the opening and closing parentheses.
      unsatCoreString = unsatCoreString.substring(1, unsatCoreString.length() - 1);

      Set<IExpr> unsatCore = new HashSet<>();
      for (final String clauseId : unsatCoreString.split(" ")) {
         logger.info("Extracting conjunct named {}", clauseId);
         assert (clauseId.startsWith("_c"));
         int i = Integer.valueOf(clauseId.substring(2));
         unsatCore.add(conjuncts[i]);
      }

      return unsatCore;
   }

   /**
    * Sets the logic used by this solver.
    * 
    * @param logic
    *           the name of a logic in SMT-LIB v2 format.
    */
   public void setLogic(String logic) throws IOException {
      this.communicate("(set-logic " + logic + ")");
   }

   /**
    * Returns a new solver specialized for the given logic.
    * 
    * @param executable
    *           the path to the Z3 executable.
    * @param logic
    *           the name of a logic in SMT-LIB v2 format.
    * @throws UncheckedIOException
    *            if the logic cannot be set.
    */
   public static Z3Solver getSolverFor(String executable, String logic) {
      Z3Solver solver = new Z3Solver(executable);
      try {
         solver.setLogic(logic);
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }
      return solver;
   }

   private CheckResult checkAux(IExpr expr) {
      logger.info("Checking expression {}", expr);

      // Extract all conjuncts and build a set of declarations and assertions.
      logger.info("Extracting conjuncts from the expression");
      Set<IExpr> cs = Exprs.getConjuncts(expr);

      // Treat the conjuncts as a sequence since they will potentially be
      // retrieved later to build an unsat-core and in that case the order will
      // be relevant.
      logger.info("Transforming the conjuncts set into an array");
      IExpr[] conjuncts = cs.toArray(new IExpr[cs.size()]);

      Set<String> sortDecls = new HashSet<>();
      Set<String> varDecls = new HashSet<>();
      Set<String> asserts = new HashSet<>();
      int i = 0;
      for (final IExpr conjunct : conjuncts) {
         logger.info("Considering conjunct {}", conjunct);
         SmtDecl decl = Exprs.toSmtDecl(conjunct);
         sortDecls.addAll(decl.getSortDecls());
         varDecls.addAll(decl.getFunDecls());
         asserts.add(String.format("(assert (! %s :named _c%s))", decl.getQuery(), i++));
      }

      CheckResult status = CheckResult.UNKNOWN;
      try {
         // Push a new context.
         logger.info("Pushing a new context");
         this.communicate("(push)");

         // Push declarations to the solver.
         for (final String decl : sortDecls) {
            logger.info("Pushing sort declaration {}", decl);
            this.communicate(decl);
         }

         for (final String decl : varDecls) {
            logger.info("Pushing variable declaration {}", decl);
            this.communicate(decl);
         }

         // Push named assertions to the solver.
         for (final String assertion : asserts) {
            logger.info("Pushing named assert {}", assertion);
            this.communicate(assertion);
         }

         // Try to solve the expression.
         this.communicate("(check-sat)", true);

         logger.info("Parsing the output of the solver");
         status = this.parseStatus();
         logger.info("The status is {}", status);

         if (status.equals(CheckResult.SAT)) {
            logger.info("Retrieving the model");
            this.communicate("(get-model)", true);
            Model model = this.parseModel();
            this.setModel(model);
         } else if (status.equals(CheckResult.UNSAT)) {
            this.communicate("(get-unsat-core)", true);
            Set<IExpr> unsatCore = this.parseUnsatCore(conjuncts);
            this.setUnsatCore(unsatCore);
         }

         logger.info("Dropping the context of the solver");
         this.communicate("(pop)", true);
      } catch (IOException e) {
         e.printStackTrace();
      }

      return status;
   }

   @Override
   public CheckResult check(IExpr expr) {
      this.setModel(null);
      this.setUnsatCore(null);

      for (final Z3SolverListener listener : this.listeners) {
         listener.onBeforeCheck(expr);
      }
      CheckResult status = this.checkAux(expr);
      for (final Z3SolverListener listener : this.listeners) {
         listener.onAfterCheck(status);
      }
      return status;
   }
}
