package julia.solver.z3;

import julia.expr.ast.IExpr;
import julia.solver.CheckResult;
import julia.utils.Counter;
import julia.utils.Timer;

/**
 * Z3 solver listener that collects statistics.
 */
public class Z3SolverStatsCollector implements Z3SolverListener {
   private final static Timer timer = new Timer();

   private final Counter<String> counter;

   public Z3SolverStatsCollector() {
      this.counter = new Counter<>();
   }

   @Override
   public void onBeforeCheck(IExpr expr) {
      this.counter.increment("checks");
      timer.start();
   }

   @Override
   public void onAfterCheck(CheckResult status) {
      this.counter.update("check-time", timer.stop());
      switch (status) {
         case SAT:
            this.counter.increment("sat-exprs");
            break;
         case UNSAT:
            this.counter.increment("unsat-exprs");
            break;
         case UNKNOWN:
            this.counter.increment("unknown-exprs");
            break;
      }
   }

   public String getStats() {
      StringBuilder builder = new StringBuilder();
      builder.append("[time(ns)]");
      builder.append("\nsolve = " + this.counter.get("check-time"));
      builder.append("\n\n");

      builder.append("[expressions]");
      builder.append("\nchecked = " + this.counter.get("checks"));
      builder.append("\nsat = " + counter.get("sat-exprs"));
      builder.append("\nunsat = " + counter.get("unsat-exprs"));
      builder.append("\nunknown = " + counter.get("unknown-exprs"));

      return builder.toString();
   }
}
