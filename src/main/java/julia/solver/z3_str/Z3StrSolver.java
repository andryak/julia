package julia.solver.z3_str;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.solver.CheckResult;
import julia.solver.Solver;
import julia.utils.Exprs;
import julia.utils.ProcUtils;
import julia.utils.ProcUtils.ExecResult;
import julia.utils.Z3StrUtils;
import julia.utils.Z3StrUtils.Z3StrDecl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service to communicate with the Z3-Str solver.
 * <p>
 * This class bridges the <tt>expr</tt> package with the external Z3-str.py script
 * allowing determining the satisfiability of boolean expressions.
 */
public class Z3StrSolver extends Solver {
   final static Logger logger = LoggerFactory.getLogger(Z3StrSolver.class);

   private final static String TIMEOUT_BINARY = "/usr/local/bin/gtimeout";

   private final String executable;
   private final int timeout;
   private final List<Z3StrSolverListener> listeners;

   /**
    * Creates a new solver object to communicate with the Z3 SMT solver.
    * 
    * @param executable
    *           the path to the Z3 executable.
    * @param timeout
    *           the timeout in seconds for each query.
    */
   public Z3StrSolver(String executable, int timeout) {
      this.executable = executable;
      this.timeout = timeout;
      this.listeners = new ArrayList<>();
   }

   /**
    * Adds a new listener to this solver.
    * 
    * @param listener
    *           the listener to add.
    */
   public void addListener(Z3StrSolverListener listener) {
      this.listeners.add(listener);
   }

   /**
    * Reads the result of a check-sat command from the solver.
    * 
    * @return the result of the check-sat command read from the solver.
    */
   private CheckResult parseStatus(String output) throws IOException {
      if (output.contains(">> UNSAT")) {
         return CheckResult.UNSAT;
      } else if (output.contains(">> SAT")) {
         return CheckResult.SAT;
      } else {
         return CheckResult.UNKNOWN;
      }
   }

   private CheckResult checkAux(IExpr expr) {
      logger.info("Checking expression {}", expr);

      // Transform the given expression into a string and save it into the pivot file.
      Z3StrDecl decl = Z3StrUtils.toZ3StrDecl(expr);

      File pivotFile = null;
      try {
         pivotFile = File.createTempFile("z3str-pivot-file", ".tmp");
      } catch (IOException e) {
         logger.error("Cannot create pivot file.", e);
         return CheckResult.UNKNOWN;
      }

      try {
         StringBuilder sb = new StringBuilder();
         sb.append(decl.getFullQuery());
         sb.append("\n(check-sat)");
         sb.append("\n(get-model)");

         logger.info("Expression that will be saved to the pivot file: {}", sb.toString());
         Files.write(pivotFile.toPath(), sb.toString().getBytes());
      } catch (IOException e) {
         logger.error("Cannot save expression to pivot file.", e);
         return CheckResult.UNKNOWN;
      }

      // Call Z3-Str to solve the formula in the pivot file.
      CheckResult status = CheckResult.UNKNOWN;
      try {
         String cmd = String.format(
               "%s %d python %s -f %s",
               TIMEOUT_BINARY,
               this.timeout,
               this.executable,
               pivotFile.getAbsolutePath());

         ExecResult r = ProcUtils.exec(cmd);
         String output = r.getOutput();

         logger.info("Parsing the output of the solver");
         logger.info("Output produced by the solver: {}", output);
         status = this.parseStatus(output);
         logger.info("The status is {}", status);

         if (status.equals(CheckResult.SAT)) {
            logger.info("Retrieving the model");

            String modelStr = Z3StrUtils.getModelBlock(output);
            logger.info("The model extracted from the output is: {}", modelStr);

            Model model = Z3StrUtils.parseModel(modelStr);
            logger.info("The parsed model is: {}", model);

            this.setModel(model);
         } else if (status.equals(CheckResult.UNSAT)) {
            // Z3-Str does not produce unsat-cores, so the whole formula is considered.
            // It might be interesting to exploiting some minimization techniques here.
            Set<IExpr> unsatCore = Exprs.getConjuncts(expr);
            this.setUnsatCore(unsatCore);
         }
         return status;
      } catch (Exception e) {
         logger.error("An error occurred while communicating with Z3-Str.", e);
         return CheckResult.UNKNOWN;
      } finally {
         // Delete the pivot file after the formula it contains has been solved.
         pivotFile.delete();
      }
   }

   @Override
   public CheckResult check(IExpr expr) {
      this.setModel(null);
      this.setUnsatCore(null);

      for (final Z3StrSolverListener listener : this.listeners) {
         listener.onBeforeCheck(expr);
      }
      CheckResult status = this.checkAux(expr);
      for (final Z3StrSolverListener listener : this.listeners) {
         listener.onAfterCheck(status);
      }
      return status;
   }
}
