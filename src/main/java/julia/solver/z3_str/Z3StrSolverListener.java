package julia.solver.z3_str;

import julia.solver.SolverListener;

/**
 * Interface of the listeners of the Z3-Str solver.
 */
public interface Z3StrSolverListener extends SolverListener {
}
