package julia.solver;

import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.model.Model;
import julia.solver.exc.NoModelException;
import julia.solver.exc.NoUnsatCoreException;

/**
 * Solver interface.
 */
public interface ISolver {
   /**
    * Finds a solution for the given expression.
    * 
    * @param expr
    *           the expression to solve.
    * @return the status of the given expression.
    */
   public CheckResult check(IExpr expr);

   /**
    * Returns the model of the last checked expression.
    * 
    * @throws NoModelException
    *            if no expression has been checked or no model was produced.
    */
   public Model getModel() throws NoModelException;

   /**
    * Sets the model stored by this solver.
    * 
    * @param model
    *           the model to store.
    */
   public void setModel(Model model);

   /**
    * Returns the unsat-core of the last checked expression.
    * 
    * @throws NoModelException
    *            if no expression has been checked or no unsat-core was produced.
    */
   public Set<IExpr> getUnsatCore() throws NoUnsatCoreException;

   /**
    * Sets the unsat-core stored by this solver.
    * 
    * @param unsatCore
    *           the unsat-core to store.
    */
   public void setUnsatCore(Set<IExpr> unsatCore);
}
