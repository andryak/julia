package julia.solver.exc;

/**
 * Exception thrown when a model is requested but not available.
 */
public class NoModelException extends Exception {
   private static final long serialVersionUID = 3867805574746273596L;

   public NoModelException() {
      super();
   }
}
