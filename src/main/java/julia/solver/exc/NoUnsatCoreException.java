package julia.solver.exc;

/**
 * Exception thrown when a unsat-core is requested but not available.
 */
public class NoUnsatCoreException extends Exception {
   private static final long serialVersionUID = 2097256976929278115L;

   public NoUnsatCoreException() {
      super();
   }
}
