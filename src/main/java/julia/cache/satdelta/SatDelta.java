package julia.cache.satdelta;

import julia.expr.utils.BigRational;
import com.google.common.base.Preconditions;

/**
 * Sat-delta value of a boolean expression (and its negation) from a reference model.
 */
public class SatDelta {
   private final BigRational dir;
   private final BigRational inv;

   /**
    * Creates a new sat-delta value.
    * 
    * @param dir
    *           the distance of the expression producing this sat-delta from a reference
    *           model.
    * @param inv
    *           the distance of the negation of the expression producing this sat-delta
    *           from a reference model.
    */
   public SatDelta(long dir, long inv) {
      this(BigRational.create(dir), BigRational.create(inv));
   }

   /**
    * Creates a new sat-delta value.
    * 
    * @param dir
    *           the distance of the expression producing this sat-delta from a reference
    *           model.
    * @param inv
    *           the distance of the negation of the expression producing this sat-delta
    *           from a reference model.
    */
   public SatDelta(BigRational dir, BigRational inv) {
      Preconditions.checkNotNull(dir);
      Preconditions.checkNotNull(inv);

      this.dir = dir;
      this.inv = inv;
   }

   /**
    * Returns the distance of the boolean expression that generated this sat-delta from
    * the related reference model.
    * 
    * @return the distance of the boolean expression that generated this sat-delta from
    *         the related reference model.
    */
   public BigRational dir() {
      return this.dir;
   }

   /**
    * Returns the distance of the negation of the boolean expression that generated this
    * sat-delta from the related reference model.
    * 
    * @return the distance of the negation of the boolean expression that generated this
    *         sat-delta from the related reference model.
    */
   public BigRational inv() {
      return this.inv;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + this.dir.hashCode();
      result = prime * result + this.inv.hashCode();
      return result;
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         SatDelta o = (SatDelta) other;
         boolean sameDir = this.dir.equals(o.dir);
         boolean sameInv = this.inv.equals(o.inv);
         return (sameDir && sameInv);
      }
   }

   @Override
   public String toString() {
      return String.format("SatDelta(%s, %s)", this.dir, this.inv);
   }
}
