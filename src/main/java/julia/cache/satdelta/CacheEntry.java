package julia.cache.satdelta;

import julia.expr.model.Model;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BigRational;
import julia.utils.Counter;

/**
 * Entry stored in a SatDeltaCache.
 * 
 * @see SatDeltaCache
 */
public class CacheEntry {
   private BigRational score;
   private Counter<ExprSort> vars;
   private Model model;

   /**
    * Creates a new entry.
    * 
    * @param score
    *           the score of the entry, that is, the sat-delta value of the satisfiable
    *           boolean expression that is solved by the given model.
    * @param vars
    *           a counter mapping variable sorts to the number of variables of that sort
    *           in the expression that produced this entry.
    * @param model
    *           the model associated with this entry.
    */
   public CacheEntry(BigRational score, Counter<ExprSort> vars, Model model) {
      this.score = score;
      this.vars = vars;
      this.model = model;
   }

   /**
    * Returns the score of this entry.
    * 
    * @return the score of this entry.
    */
   public BigRational getScore() {
      return score;
   }

   /**
    * Returns the counter of this entry.
    * 
    * @return the counter of this entry.
    */
   public Counter<ExprSort> getVars() {
      return this.vars;
   }

   /**
    * Returns the model of this entry.
    * 
    * @return the model of this entry.
    */
   public Model getModel() {
      return model;
   }

   @Override
   public String toString() {
      return String.format(
            "Entry(score=%s, vars=%s, model=%s)",
            this.score,
            this.vars,
            this.model);
   }
}
