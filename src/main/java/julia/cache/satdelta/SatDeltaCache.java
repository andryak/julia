package julia.cache.satdelta;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import julia.expr.sort.ExprSort;
import julia.expr.utils.BigRational;
import julia.utils.Counter;
import julia.utils.Pair;

/**
 * Cache to filter likely reusable models.
 * <p>
 * This cache stores models of satisfiable boolean expressions keeping them sorted by
 * increasing distance from a set of reference models.
 * 
 * It allows to retrieve the models of the expressions the distances of which are the
 * closest to a given value.
 * 
 * <p>
 * The distance of an expression from a set of reference models is called "SatDelta" and
 * is defined in the paper:
 * "Heuristically Matching Formula Solution Spaces To Efficiently Reuse Solutions"
 * published at the International Conference on Software Engineering (ICSE'17) by Andrea
 * Aquino, Giovanni Denaro and Mauro Pezze'.
 * 
 * @author Andrea Aquino <andrex.aquino@gmail.com>
 */
public class SatDeltaCache {
   /**
    * The entries stored in this cache.
    */
   private List<CacheEntry> entries;

   /**
    * Creates a new, empty sat-delta cache.
    */
   public SatDeltaCache() {
      this.entries = new ArrayList<>();
   }

   /**
    * Stores a new entry in this cache.
    * 
    * @param entry
    *           the entry to store in this cache.
    */
   public void store(CacheEntry entry) {
      this.entries.add(entry);
   }

   /**
    * Returns the number of entries stored in this cache.
    * 
    * @return the number of entries stored in this cache.
    */
   public int size() {
      return this.entries.size();
   }

   /**
    * Returns true if the counter rc contains at most as many variables as the counter ec
    * for each expression sort.
    * 
    * @param ec
    *           the counter representing the variables in an entry.
    * @param rc
    *           the counter representing the variables in the reference expression.
    * @return true if the counter rc contains at most as many variables as the counter ec
    *         for each expression sort.
    */
   boolean isCompatible(Counter<ExprSort> ec, Counter<ExprSort> rc) {
      for (final Entry<ExprSort, BigInteger> e : rc.getData().entrySet()) {
         if (e.getValue().compareTo(ec.get(e.getKey())) > 0) {
            return false;
         }
      }
      return true;
   }

   class CompareToReferenceScore implements Comparator<CacheEntry> {
      private final BigRational referenceScore;

      public CompareToReferenceScore(BigRational referenceScore) {
         this.referenceScore = referenceScore;
      }

      @Override
      public int compare(CacheEntry e1, CacheEntry e2) {
         BigRational e1Delta = referenceScore.subtract(e1.getScore()).abs();
         BigRational e2Delta = referenceScore.subtract(e2.getScore()).abs();
         return e1Delta.compareTo(e2Delta);
      }
   }

   /**
    * Returns at most k entries that are the closest to the given score.
    * <p>
    * Returned entries are sorted by increasing distance from the given score.
    * 
    * @param score
    *           the reference score according to which the extracted entries are sorted.
    * @param vars
    *           a counter that maps variable sorts into the number of variables of that
    *           sort.
    * @param k
    *           the maximum number of extracted entries.
    * @return the list of the entries matching the given parameters.
    */
   private List<CacheEntry> filterByProximity(BigRational score, Counter<ExprSort> vars, int k) {
      if (this.entries.size() <= k) {
         // If the number of requested entries exceeds the available entries,
         // return them all in the right order.
         List<CacheEntry> entriesCopy = new ArrayList<>(this.entries);
         return entriesCopy
               .stream()
               .filter(e -> isCompatible(e.getVars(), vars))
               .sorted(new CompareToReferenceScore(score))
               .collect(Collectors.toList());
      } else {
         PriorityQueue<Pair<BigRational, CacheEntry>> queue = new PriorityQueue<>(
               k,
               (p1, p2) -> ((-1) * (p1.getL().compareTo(p2.getL()))));

         // Load the first k entries in the queue, then keep updating the queue
         // inserting elements whose distance from satDelta is smaller than the
         // distance of the maximum element in the queue. The maximum is removed
         // whenever a new elements is inserted so that the overall complexity
         // of this method is O(n*log(k)).
         int i = 0;
         for (final CacheEntry entry : this.entries) {
            if (!isCompatible(entry.getVars(), vars)) {
               // Entries containing models with less variables than
               // the reference expression are immediately discarded.
               continue;
            }

            BigRational delta = entry.getScore().subtract(score).abs();
            if (i++ < k) {
               queue.add(new Pair<>(delta, entry));
            } else {
               Pair<BigRational, CacheEntry> head = queue.peek();
               if (delta.compareTo(head.getL()) < 0) {
                  queue.poll();
                  queue.add(new Pair<>(delta, entry));
               }
            }
         }

         List<CacheEntry> closest = new ArrayList<>(k);
         while (!queue.isEmpty()) {
            closest.add(queue.remove().getR());
         }
         Collections.reverse(closest);
         return closest;
      }
   }

   /**
    * Returns at most k entries that are the closest to the given score.
    * <p>
    * Returned entries are sorted by increasing distance from the given score.
    * 
    * @param score
    *           the reference score according to which the extracted entries are sorted.
    * @param vars
    *           a counter that maps variable sorts into the number of variables of that
    *           sort.
    * @param k
    *           the maximum number of extracted entries.
    * @return the list of the entries matching the given parameters.
    */
   public List<CacheEntry> filter(BigRational score, Counter<ExprSort> vars, int k) {
      if (k <= 0) {
         return new ArrayList<CacheEntry>();
      }
      return this.filterByProximity(score, vars, k);
   }
}
