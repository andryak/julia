package julia.cache.cex;

import julia.expr.ast.IExpr;
import julia.utils.BloomSet;

/**
 * Cache to filter likely reusable unsatisfiable cores.
 * <p>
 * This cache is a BloomSet (based on a single hashing function) of unsatisfiable cores,
 * represented as sets of boolean expressions.
 * 
 * @author Andrea Aquino <andrex.aquino@gmail.com>
 * 
 * @see BloomSet
 */
public class CexCache extends BloomSet<IExpr> {
}
