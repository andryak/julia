package julia.utils;

import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Stack;
import julia.expr.ast.Expr;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.slicer.ExprSlicer;

/**
 * A path condition generated during the symbolic execution of a program.
 */
public class AssumptionSet {
   private Stack<IExpr> assumptions;

   /**
    * Creates a new empty assumption set.
    */
   public AssumptionSet() {
      this.assumptions = new Stack<>();
   }

   /**
    * Returns the number of expressions in this assumption set.
    * 
    * @return the number of expressions in this assumption set.
    */
   public int size() {
      return this.assumptions.size();
   }

   /**
    * Drops all assumptions in this assumption set.
    */
   public void clear() {
      this.assumptions.clear();
   }

   /**
    * Adds an assumption to this assumption set.
    * 
    * @param expr
    *           the assumption to add to the assumption set.
    */
   public void push(IExpr expr) {
      this.assumptions.push(expr);
   }

   /**
    * Retracts the last assumption pushed to this assumption set.
    * 
    * @return the popped assumption.
    * @throws EmptyStackException
    *            if the assumption stack is empty.
    */
   public IExpr pop() {
      return this.assumptions.pop();
   }

   /**
    * Returns the expression obtained slicing the expressions in this assumption set with
    * respect to the given expression.
    * 
    * @param expr
    *           the expression to slice with respect to this assumption set.
    * @return the resulting sliced expression.
    */
   public IExpr slice(IExpr expr) {
      return ExprSlicer.slice(new HashSet<>(this.assumptions), expr);
   }

   /**
    * Returns the conjunction of all expressions in this assumption set and the given
    * expression.
    * 
    * @param expr
    *           the expression to add in conjunction to this assumption set.
    * @return the conjunction of all expressions in this assumption set and the given
    *         expression.
    */
   public IExpr merge(IExpr expr) {
      IExpr[] exprs = new Expr[this.assumptions.size() + 1];
      this.assumptions.copyInto(exprs);
      exprs[this.assumptions.size()] = expr;
      return And.create(exprs);
   }

   @Override
   public String toString() {
      return this.assumptions.toString();
   }
}
