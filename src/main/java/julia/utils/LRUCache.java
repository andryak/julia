package julia.utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A LRU cache.
 * 
 * Code originally taken by http://chriswu.me/blog/a-lru-cache-in-10-lines-of-java/ and
 * adapted by Andrea Aquino <andrex.aquino@gmail.com>.
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {
   private static final long serialVersionUID = -5337273599963776031L;

   private int maxSize;

   /**
    * Creates a new cache with the given maximum capacity.
    * 
    * @param maxSize
    *           the maximum capacity of this cache.
    */
   public LRUCache(int maxSize) {
      super(16, 0.75f, true);
      this.maxSize = maxSize;
   }

   @Override
   protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
      return this.size() >= maxSize;
   }
}