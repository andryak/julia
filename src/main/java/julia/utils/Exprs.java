package julia.utils;

import java.util.HashSet;
import java.util.Set;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.reals.RealVal;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.uf.Fun;
import julia.expr.parser.infix.InfixExprBuilder;
import julia.expr.parser.infix.InfixExprLexer;
import julia.expr.parser.infix.InfixExprParser;
import julia.expr.sort.DeclareSort;
import julia.expr.sort.ExprSort;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Utility class implementing static methods to manipulate expressions.
 */
public final class Exprs {
   /**
    * Do not instantiate me!
    */
   private Exprs() {};

   /**
    * Returns the list of all conjuncts in the given expression.
    * <p>
    * The conjunction of all returned expressions is equivalent to the given expression.
    *
    * @param expr
    *           a expression.
    * @return the list of all conjuncts in the given expression.
    */
   public static Set<IExpr> getConjuncts(IExpr expr) {
      Set<IExpr> conjuncts = new HashSet<>();
      if (expr instanceof And) {
         conjuncts.addAll(expr.getExprs());
      } else {
         conjuncts.add(expr);
      }
      return conjuncts;
   }

   /**
    * Returns a new expression from a string in infix notation.
    * 
    * @param s
    *           a string representing an expression in infix notation.
    * @return the expression corresponding to the given string.
    * @throws IllegalArgumentException
    *            if the string s cannot be parsed.
    */
   public static IExpr create(String s) {
      ANTLRInputStream input = new ANTLRInputStream(s);
      InfixExprLexer lexer = new InfixExprLexer(input);
      lexer.removeErrorListener(ConsoleErrorListener.INSTANCE);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      InfixExprParser parser = new InfixExprParser(tokens);
      parser.removeErrorListener(ConsoleErrorListener.INSTANCE);
      ParseTree tree = parser.expr();

      // In case of syntax errors throw an exception.
      if (parser.getNumberOfSyntaxErrors() > 0) {
         throw new IllegalArgumentException(String.format("Cannot parse expression %s.", s));
      }

      InfixExprBuilder infixExprBuilder = new InfixExprBuilder();
      return infixExprBuilder.visit(tree);
   }

   /**
    * Returns the type safe cast of the given expression to the given class.
    * <p>
    * Returns expr casted to the given class if possible, otherwise returns null.
    * 
    * @param expr
    *           the expression to cast.
    * @param cls
    *           the target class.
    * @return the type safe cast of the given expression to the given class.
    */
   public static <T extends IExpr> T cast(IExpr expr, Class<T> cls) {
      if (cls.isInstance(expr)) {
         return cls.cast(expr);
      }
      return cls.cast(null);
   }

   /**
    * Inner class providing access to the three main components of the SMT-LIB v2
    * declaration of an expression.
    * <p>
    * An SMT-LIB v2 declaration of an expression is composed of:
    * <ul>
    * <li>a list of uninterpreted sort declarations,</li>
    * <li>a list of uninterpreted functions declarations,</li>
    * <li>a boolean assertion.</li>
    * </ul>
    */
   public static final class SmtDecl {
      private final Set<String> sortDecls;
      private final Set<String> funDecls;
      private final String query;

      public SmtDecl(Set<String> sortDecls, Set<String> funDecls, String query) {
         this.sortDecls = sortDecls;
         this.funDecls = funDecls;
         this.query = query;
      }

      public Set<String> getSortDecls() {
         return this.sortDecls;
      }

      public Set<String> getFunDecls() {
         return this.funDecls;
      }

      public String getQuery() {
         return this.query;
      }

      public String getFullQuery() {
         StringBuilder builder = new StringBuilder();
         for (final String sortDecl : this.sortDecls) {
            builder.append(sortDecl).append("\n");
         }
         for (final String decl : this.funDecls) {
            builder.append(decl).append("\n");
         }
         builder.append("(assert " + this.getQuery() + ")");
         return builder.toString();
      }
   }

   /**
    * Returns an SMT-LIB v2 declaration of the given expression for SMT-LIB v2 compliant
    * SMT solvers.
    * 
    * @param expr
    *           the expression to translate to SMT-LIB v2 format.
    * @return an SMT-LIB v2 declaration of the given expression for SMT-LIB v2 compliant
    *         SMT solvers.
    */
   public static SmtDecl toSmtDecl(IExpr expr) {
      Set<Fun> vars = expr.getVars();
      Set<String> sortDecls = new HashSet<>();
      Set<String> funDecls = new HashSet<>();
      for (final Fun f : vars) {
         // Declare uninterpreted sorts.
         for (final ExprSort sort : f.signature()) {
            if (sort instanceof DeclareSort) {
               DeclareSort s = (DeclareSort) sort;
               sortDecls.add(String.format("(declare-sort %s)", s.getName()));
            }
         }
         // Then declare the function.
         funDecls.add(f.toSmtDecl());
      }
      String query = expr.toSmt();
      return new SmtDecl(sortDecls, funDecls, query);
   }

   /**
    * Returns a constant function of the given sort.
    * 
    * @return a constant function of the given sort.
    */
   public static Fun mkConst(int index, ExprSort sort) {
      return Fun.create(index, sort);
   }

   /**
    * Returns a node representing the given boolean constant.
    * 
    * @param value
    *           the value to convert to a node.
    * @return a node representing the given boolean constant.
    */
   public static IExpr toExpr(Boolean value) {
      return BoolVal.create(value);
   }

   /**
    * Returns a node representing the given integer constant.
    * 
    * @param value
    *           the value to convert to a node.
    * @return a node representing the given integer constant.
    */
   public static IExpr toExpr(Integer value) {
      return IntVal.create(value);
   }

   /**
    * Returns a node representing the given real constant.
    * 
    * @param value
    *           the value to convert to a node.
    * @return a node representing the given real constant.
    */
   public static IExpr toExpr(Float value) {
      return RealVal.create(value.toString());
   }

   /**
    * Returns a node representing the given real constant.
    * 
    * @param value
    *           the value to convert to a node.
    * @return a node representing the given real constant.
    */
   public static IExpr toExpr(Double value) {
      return RealVal.create(value.toString());
   }

   /**
    * Returns a node representing the given string constant.
    * 
    * @param value
    *           the value to convert to a node.
    * @return a node representing the given string constant.
    */
   public static IExpr toExpr(String value) {
      return StrVal.create(value);
   }
}
