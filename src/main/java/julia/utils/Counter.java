package julia.utils;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import com.google.common.base.Preconditions;

/**
 * This class implements a Python-like counter, that is, a map from objects to numbers
 * that can be easily updated or incremented.
 */
public class Counter<T> {
   private Map<T, BigInteger> data;
   private BigInteger _default;

   /**
    * Creates a new counter with default value set to zero.
    */
   public Counter() {
      this(BigInteger.ZERO);
   }

   /**
    * Creates a new counter with the given default value.
    * 
    * @param _default
    *           the default value of the counter.
    */
   public Counter(long _default) {
      this(BigInteger.valueOf(_default));
   }

   /**
    * Creates a new counter with the given default value.
    * 
    * @param _default
    *           the default value of the counter.
    */
   public Counter(BigInteger _default) {
      Preconditions.checkNotNull(_default);

      this.data = new HashMap<>();
      this._default = _default;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + this._default.hashCode();
      result = prime * result + this.data.hashCode();
      return result;
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      } else if (other == null) {
         return false;
      } else if (getClass() != other.getClass()) {
         return false;
      } else {
         Counter<?> o = (Counter<?>) other;
         boolean sameData = this.data.equals(o.data);
         boolean sameDefault = this._default.equals(o._default);
         return (sameData && sameDefault);
      }
   }

   /**
    * Returns the count associated with the given key, or the default value of this
    * counter if it contains no such key.
    * 
    * @param key
    *           the key identifying the value to extract.
    * @return the number associated to the given key, or the default value of this counter
    *         if it contains no such key.
    */
   public BigInteger get(T key) {
      return this.data.getOrDefault(key, this._default);
   }

   /**
    * Returns the underlying map of this counter.
    * 
    * @return the underlying map of this counter.
    */
   public Map<T, BigInteger> getData() {
      return this.data;
   }

   /**
    * Updates the count associated with the given key adding n to it.
    * 
    * @param key
    *           the key identifying the mapping to update.
    * @param n
    *           the amount by which the given mapping will be incremented.
    * @return the new count associated with the given key.
    */
   public BigInteger update(T key, BigInteger n) {
      BigInteger value = this.get(key).add(n);
      this.data.put(key, value);
      return value;
   }

   /**
    * Updates the count associated with the given key adding n to it.
    * 
    * @param key
    *           the key identifying the mapping to update.
    * @param n
    *           the amount by which the given mapping will be incremented.
    * @return the new count associated with the given key.
    */
   public BigInteger update(T key, long n) {
      return this.update(key, BigInteger.valueOf(n));
   }

   /**
    * Updates the count associated with the given key adding one to it.
    * 
    * @param key
    *           the key identifying the mapping to update.
    * @return the new count associated with the given key.
    */
   public BigInteger increment(T key) {
      return this.update(key, BigInteger.ONE);
   }

   @Override
   public String toString() {
      return this.data.toString();
   }
}
