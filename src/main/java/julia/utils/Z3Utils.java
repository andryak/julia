package julia.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class implementing static methods to ease the communication with Microsoft Z3.
 */
public final class Z3Utils {
   /**
    * Do not instantiate me!
    */
   private Z3Utils() {};

   /**
    * Pattern matching an hexadecimal character of the form \xXY.
    */
   final static Pattern pattern = Pattern.compile("\\\\x[0-9a-fA-F]{2}");

   /**
    * Convert a string of the form \xXY into the corresponding ascii character.
    * <p>
    * This method returns the character corresponding to the hexadecimal character XY.
    * 
    * @param s
    *           the string to convert.
    * @return the ascii character corresponding to s.
    */
   private static String hexToAscii(String s) {
      StringBuilder output = new StringBuilder("");
      output.append((char) Integer.parseInt(s.replaceAll("\\\\x", ""), 16));
      return output.toString();
   }

   /**
    * Converts a string into a sequence of hexadecimal characters.
    * 
    * @param s
    *           the input string.
    * @return a string of hexadecimal characters encoding the input string.
    */
   public static String unescape(String s) {
      StringBuilder result = new StringBuilder();
      for (char c : s.toCharArray()) {
         result.append("\\x" + Integer.toHexString(c));
      }
      return result.toString();
   }

   /**
    * Translates all occurrences of the pattern \xXY in the given string with the
    * corresponding ascii character.
    * 
    * @param s
    *           a string.
    * @return the escaped input string.
    */
   public static String escape(String s) {
      StringBuffer output = new StringBuffer();
      Matcher matcher = pattern.matcher(s);
      while (matcher.find()) {
         String rep = matcher.group();
         matcher.appendReplacement(output, hexToAscii(rep));
      }
      matcher.appendTail(output);
      return output.toString();
   }
}
