package julia.utils;

/**
 * Helper class to compute the running average, variance and standard deviation of data
 * points as they are collected.
 */
public class RunningStats {
   private int n;
   private double mean;
   private double cvar;
   private double min;
   private double max;

   public RunningStats() {
      this.n = 0;
      this.mean = 0;
      this.cvar = 0;
      this.min = 0;
      this.max = 0;
   }

   /**
    * Resets the data points added so far.
    */
   public void clear() {
      this.n = 0;
   }

   /**
    * Adds a new data point.
    * 
    * @param x
    *           the data point to add to the collection.
    */
   public void add(double x) {
      ++this.n;

      if (this.n == 1) {
         this.mean = x;
         this.cvar = 0;
         this.min = x;
         this.max = x;
      } else {
         double new_mean = this.mean + (x - this.mean) / this.n;
         double new_cvar = this.cvar + (x - this.mean) * (x - new_mean);

         this.mean = new_mean;
         this.cvar = new_cvar;
         this.min = Math.min(this.min, x);
         this.max = Math.max(this.max, x);
      }
   }

   /**
    * Returns the number of data points added so far.
    * 
    * @return the number of data points added so far.
    */
   public int count() {
      return this.n;
   }

   /**
    * Returns the average of the data points added to this class.
    * 
    * @return the average of the data points added to this class.
    */
   public double mean() {
      return (this.n > 0) ? this.mean : 0d;
   }

   /**
    * Returns the variance of the data points added to this class.
    * 
    * @return the variance of the data points added to this class.
    */
   public double var() {
      return (this.n > 1) ? (this.cvar / (this.n - 1)) : 0d;
   }

   /**
    * Returns the standard deviation of the data points added to this class.
    * 
    * @return the standard deviation of the data points added to this class.
    */
   public double stdev() {
      return Math.sqrt(this.var());
   }

   /**
    * Returns the minimum of the data points added to this class.
    * 
    * @return the minimum of the data points added to this class.
    */
   public double min() {
      return (this.n > 0) ? this.min : 0d;
   }

   /**
    * Returns the maximum of the data points added to this class.
    * 
    * @return the maximum of the data points added to this class.
    */
   public double max() {
      return (this.n > 0) ? this.max : 0d;
   }
}
