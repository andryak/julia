package julia.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import julia.expr.ast.IConst;
import julia.expr.ast.IExpr;
import julia.expr.ast.bool.And;
import julia.expr.ast.bool.Bool;
import julia.expr.ast.bool.BoolVal;
import julia.expr.ast.bool.Distinct;
import julia.expr.ast.bool.Equal;
import julia.expr.ast.bool.Not;
import julia.expr.ast.bool.Or;
import julia.expr.ast.ints.Int;
import julia.expr.ast.ints.IntAdd;
import julia.expr.ast.ints.IntDiv;
import julia.expr.ast.ints.IntGe;
import julia.expr.ast.ints.IntGt;
import julia.expr.ast.ints.IntLe;
import julia.expr.ast.ints.IntLt;
import julia.expr.ast.ints.IntMul;
import julia.expr.ast.ints.IntVal;
import julia.expr.ast.mixed.If;
import julia.expr.ast.re.Match;
import julia.expr.ast.re.ReConcat;
import julia.expr.ast.re.ReStar;
import julia.expr.ast.re.ReUnion;
import julia.expr.ast.re.StrToRe;
import julia.expr.ast.str.Str;
import julia.expr.ast.str.StrConcat;
import julia.expr.ast.str.StrContains;
import julia.expr.ast.str.StrIndexOf;
import julia.expr.ast.str.StrLastIndexOf;
import julia.expr.ast.str.StrLen;
import julia.expr.ast.str.StrPrefixOf;
import julia.expr.ast.str.StrReplace;
import julia.expr.ast.str.StrSuffixOf;
import julia.expr.ast.str.StrVal;
import julia.expr.ast.str.SubStr;
import julia.expr.ast.uf.Fun;
import julia.expr.model.MapModel;
import julia.expr.model.Model;
import julia.expr.model.SortMapModel;
import julia.expr.model.SortModel;
import julia.expr.sort.BoolSort;
import julia.expr.sort.IntSort;
import julia.expr.sort.StrSort;

/**
 * Utility class implementing static methods to ease the communication with Z3-Str.
 */
public final class Z3StrUtils {
   /**
    * Pattern matching lines representing interpretations in the Z3-Str solver output.
    */
   private final static Pattern pattern = Pattern.compile("([^ ]+) : ([^ ]+) -> (.+)");

   /**
    * Do not instantiate me!
    */
   private Z3StrUtils() {};

   /**
    * Inner class providing access to the two main components of the declaration of an
    * expression in the format accepted by the Z3-Str solver.
    * <p>
    * The declaration of an expression is composed of:
    * <ul>
    * <li>a list of uninterpreted functions declarations of sort Int, Bool or String,</li>
    * <li>a boolean assertion.</li>
    * </ul>
    */
   public static final class Z3StrDecl {
      private final Set<String> varDecls;
      private final String query;

      public Z3StrDecl(Set<String> varDecls, String query) {
         this.varDecls = varDecls;
         this.query = query;
      }

      public Set<String> getVarDecls() {
         return this.varDecls;
      }

      public String getQuery() {
         return this.query;
      }

      public String getFullQuery() {
         StringBuilder builder = new StringBuilder();
         for (final String decl : this.varDecls) {
            builder.append(decl).append("\n");
         }
         builder.append("(assert " + this.getQuery() + ")");
         return builder.toString();
      }
   }

   private static String toZ3StrQuery(IExpr expr) {
      if (expr.opcode() == And.OPCODE) {
         List<String> params = expr
               .getExprs()
               .stream()
               .map(e -> toZ3StrQuery(e))
               .collect(Collectors.toList());
         return String.format("(and %s)", String.join("\n", params));
      }

      if (expr.opcode() == Or.OPCODE) {
         List<String> params = expr
               .getExprs()
               .stream()
               .map(e -> toZ3StrQuery(e))
               .collect(Collectors.toList());
         return String.format("(or %s)", String.join("\n", params));
      }

      if (expr.opcode() == Not.OPCODE) {
         String e = toZ3StrQuery(expr.getExpr(0));
         return String.format("(not %s)", e);
      }

      if (expr.opcode() == BoolVal.OPCODE) {
         // The format is the same as SMT-LIB v2.
         return expr.toSmt();
      }

      if (expr.opcode() == Equal.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(= %s %s)", lhs, rhs);
      }

      if (expr.opcode() == Distinct.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(not (= %s %s))", lhs, rhs);
      }

      if (expr.opcode() == If.OPCODE) {
         String c = toZ3StrQuery(expr.getExpr(0));
         String t = toZ3StrQuery(expr.getExpr(1));
         String e = toZ3StrQuery(expr.getExpr(2));
         return String.format("(ite %s %s %s)", c, t, e);
      }

      if (expr.opcode() == StrConcat.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(Concat %s %s)", lhs, rhs);
      }

      if (expr.opcode() == StrContains.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(Contains %s %s)", lhs, rhs);
      }

      if (expr.opcode() == StrSuffixOf.OPCODE) {
         // Parameters are reversed on purpose.
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(EndsWith %s %s)", rhs, lhs);
      }

      if (expr.opcode() == StrIndexOf.OPCODE) {
         assert ((StrIndexOf) expr).getOffset() == 0;

         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(Indexof %s %s)", lhs, rhs);
      }

      if (expr.opcode() == StrLen.OPCODE) {
         return String.format("(Length %s)", toZ3StrQuery(expr.getExpr(0)));
      }

      if (expr.opcode() == StrReplace.OPCODE) {
         String subject = toZ3StrQuery(expr.getExpr(0));
         String target = toZ3StrQuery(expr.getExpr(1));
         String repl = toZ3StrQuery(expr.getExpr(2));
         return String.format("(Replace %s %s %s)", subject, target, repl);
      }

      if (expr.opcode() == StrPrefixOf.OPCODE) {
         // Parameters are reversed on purpose.
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(StartsWith %s %s)", rhs, lhs);
      }

      if (expr.opcode() == SubStr.OPCODE) {
         String subject = toZ3StrQuery(expr.getExpr(0));
         String offset = toZ3StrQuery(expr.getExpr(1));
         String length = toZ3StrQuery(expr.getExpr(2));
         return String.format("(Substring %s %s %s)", subject, offset, length);
      }

      if (expr.opcode() == StrLastIndexOf.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(LastIndexof %s %s)", lhs, rhs);
      }

      if (expr.opcode() == StrLastIndexOf.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(LastIndexof %s %s)", lhs, rhs);
      }

      if (expr.opcode() == StrVal.OPCODE) {
         return StringUtils.quote(escape((String) expr.getArg(0)));
      }

      if (expr.opcode() == ReConcat.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(RegexConcat %s %s)", lhs, rhs);
      }

      if (expr.opcode() == Match.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(RegexIn %s %s)", lhs, rhs);
      }

      if (expr.opcode() == ReStar.OPCODE) {
         return String.format("(RegexStar %s)", toZ3StrQuery(expr.getExpr(0)));
      }

      if (expr.opcode() == ReUnion.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(RegexUnion %s %s)", lhs, rhs);
      }

      if (expr.opcode() == StrToRe.OPCODE) {
         return String.format("(Str2Reg %s)", toZ3StrQuery(expr.getExpr(0)));
      }

      if (expr.opcode() == IntAdd.OPCODE) {
         List<String> params = expr
               .getExprs()
               .stream()
               .map(e -> toZ3StrQuery(e))
               .collect(Collectors.toList());
         return String.format("(+ %s)", String.join(" ", params));
      }

      if (expr.opcode() == IntMul.OPCODE) {
         List<String> params = expr
               .getExprs()
               .stream()
               .map(e -> toZ3StrQuery(e))
               .collect(Collectors.toList());
         return String.format("(* %s)", String.join(" ", params));
      }

      if (expr.opcode() == IntDiv.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(div %s %s)", lhs, rhs);
      }

      if (expr.opcode() == IntLt.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(< %s %s)", lhs, rhs);
      }

      if (expr.opcode() == IntLe.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(<= %s %s)", lhs, rhs);
      }

      if (expr.opcode() == IntGt.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(> %s %s)", lhs, rhs);
      }

      if (expr.opcode() == IntGe.OPCODE) {
         String lhs = toZ3StrQuery(expr.getExpr(0));
         String rhs = toZ3StrQuery(expr.getExpr(1));
         return String.format("(>= %s %s)", lhs, rhs);
      }

      if (expr.opcode() == IntVal.OPCODE) {
         // The format is the same as SMT-LIB v2.
         return expr.toSmt();
      }

      if (expr.opcode() == Fun.OPCODE) {
         // The format is the same as SMT-LIB v2.
         return expr.toSmt();
      }

      throw new IllegalArgumentException(String.format(
            "Cannot translate class %s to Z3-Str format.",
            expr.getClass().getSimpleName()));
   }

   /**
    * Returns a declaration of the given expression compliant with the Z3-Str solver.
    * 
    * @param expr
    *           the expression to translate.
    * @return a declaration of the given expression compliant with the Z3-Str solver.
    */
   public static Z3StrDecl toZ3StrDecl(IExpr expr) {
      Set<Fun> vars = expr.getVars();
      Set<String> varDecls = new HashSet<>();
      for (final Fun f : vars) {
         String decl = String.format("(declare-variable %s %s)", f.toSmt(), f.range().toSmt());
         varDecls.add(decl);
      }
      String query = toZ3StrQuery(expr);
      return new Z3StrDecl(varDecls, query);
   }

   /**
    * Returns the block representing the model from the output of Z3-Str.
    * 
    * @param output
    *           the output of the Z3-Str solver.
    * @return the block representing a model from the output of Z3-Str.
    */
   public static String getModelBlock(String output) {
      List<String> lines = Arrays.asList(output.split("\n"));
      int i = lines.indexOf("------------------------");
      int j = lines.lastIndexOf("************************");
      return String.join("\n", lines.subList(i + 1, j));
   }

   /**
    * Inner class representing a single interpretation in a Z3-Str model.
    */
   private static final class Z3StrInter {
      private String var;
      private String sort;
      private String value;

      public Z3StrInter(String var, String sort, String value) {
         this.var = var;
         this.sort = sort;
         this.value = value;
      }
   }

   /**
    * Returns an interpretation from a line in a Z3-Str model.
    * 
    * @param line
    *           a Z3-Str model line.
    * @return an interpretation from a line in a Z3-Str model.
    */
   private static Z3StrInter parseModelLine(String line) {
      Matcher matcher = pattern.matcher(line);
      if (!matcher.matches()) {
         throw new IllegalArgumentException(String.format(
               "Input line `%s` does not match interpretation format.",
               line));
      }
      return new Z3StrInter(matcher.group(1), matcher.group(2), matcher.group(3));
   }

   private static String[] Z3STR_ESCAPE_SEQUENCES = { "\\", "\"", "\t", "\r", "\n" };
   private static String[] Z3STR_UNESCAPE_SEQUENCES = { "\\\\", "\\\"", "\\t", "\\r", "\\n" };

   /**
    * Returns the input string escaped according to the Z3-Str format.
    * 
    * @param s
    *           the string to escape.
    * @return the input string escaped according to the Z3-Str format.
    */
   public static String escape(String s) {
      return org.apache.commons.lang3.StringUtils.replaceEach(
            s,
            Z3STR_ESCAPE_SEQUENCES,
            Z3STR_UNESCAPE_SEQUENCES);
   }

   /**
    * Returns the input string unescaped according to the Z3-Str format.
    * 
    * @param s
    *           the string to unescape.
    * @return the input string unescaped according to the Z3-Str format.
    */
   public static String unescape(String s) {
      return org.apache.commons.lang3.StringUtils.replaceEach(
            s,
            Z3STR_UNESCAPE_SEQUENCES,
            Z3STR_ESCAPE_SEQUENCES);
   }

   /**
    * Returns an in-memory sort model from a Z3-Str model string.
    * 
    * @param s
    *           a Z3-Str model.
    * @return an in-memory sort model from a Z3-Str model string.
    */
   public static Model parseModel(String s) {
      if (s.trim().isEmpty()) {
         return new SortModel();
      }

      Map<Fun, IConst> vars = new HashMap<>();
      for (String line : s.split("\n")) {
         Z3StrInter inter = parseModelLine(line);
         Fun var = null;
         IConst value = null;
         if (inter.sort.equals("bool")) {
            String prefix = BoolSort.create().prefix();
            int index = Integer.parseInt(inter.var.replace(prefix, ""));
            var = Bool.create(index);
            value = BoolVal.create(Boolean.parseBoolean(inter.value));
         } else if (inter.sort.equals("int")) {
            String prefix = IntSort.create().prefix();
            int index = Integer.parseInt(inter.var.replace(prefix, ""));
            var = Int.create(index);
            value = IntVal.create(Integer.parseInt(inter.value));
         } else if (inter.sort.equals("string")) {
            String prefix = StrSort.create().prefix();
            int index = Integer.parseInt(inter.var.replace(prefix, ""));
            var = Str.create(index);
            value = StrVal.create(unescape(StringUtils.unquote(inter.value)));
         } else {
            throw new IllegalArgumentException(String.format(
                  "Unrecognized sort %s while parsing Z3-Str model.",
                  inter.sort));
         }
         vars.put(var, value);
      }
      return new SortMapModel(new MapModel(vars));
   }
}
