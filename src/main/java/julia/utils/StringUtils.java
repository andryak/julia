package julia.utils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import julia.utils.ProcUtils.ExecResult;

/**
 * Utility class implementing static methods to manipulate strings.
 */
public final class StringUtils {
   /**
    * Do not instantiate me!
    */
   private StringUtils() {};

   /**
    * Returns the Hamming distance of the heads of the strings s and t.
    * <p>
    * The strings are considered up to the length of the shortest of the two.
    * 
    * @param s
    *           a string.
    * @param t
    *           another string.
    * @return the Hamming distance of the heads of the strings s and t.
    */
   public static int hammingHead(String s, String t) {
      int result = 0;
      for (int i = 0; i < Math.min(s.length(), t.length()); ++i) {
         if (s.charAt(i) != t.charAt(i)) {
            ++result;
         }
      }
      return result;
   }

   /**
    * Returns the Hamming distance of the tails of the strings s and t.
    * <p>
    * The strings are considered up to the length of the shortest of the two.
    * 
    * @param s
    *           a string.
    * @param t
    *           another string.
    * @return the Hamming distance of the tails of the strings s and t.
    */
   public static int hammingTail(String s, String t) {
      int result = 0;
      for (int i = 1; i <= Math.min(s.length(), t.length()); ++i) {
         if (s.charAt(s.length() - i) != t.charAt(t.length() - i)) {
            ++result;
         }
      }
      return result;
   }

   /**
    * Returns the list of all substrings of the string {@code s} of length {@code length}.
    * 
    * @param s
    *           a string.
    * @param length
    *           the length of the substrings to extract from {@code s}.
    * @return the list of all substrings of the string {@code s} of length {@code length}.
    */
   public static List<String> substrings(String s, int length) {
      List<String> result = new ArrayList<>();
      for (int i = 0; i < s.length() - length; ++i) {
         result.add(s.substring(i, i + length));
      }
      return result;
   }

   /**
    * Returns the input string wrapped in double quotes.
    * 
    * @param s
    *           the string to wrap in double quotes.
    * @return the input string wrapped in double quotes.
    */
   public static String quote(String s) {
      return "\"" + s + "\"";
   }

   /**
    * Returns the input string without its first and last double quotes.
    * <p>
    * If the string is not quoted it is returned as is.
    * 
    * @param s
    *           a string.
    * @return the input string without its first and last double quotes.
    */
   public static String unquote(String s) {
      assert s.length() >= 2;
      int i = (s.charAt(0) == '"') ? 1 : 0;
      int j = (s.charAt(s.length() - 1) == '"') ? 1 : 0;
      return s.substring(i, s.length() - j);
   }

   /**
    * Returns the edit distance between the given string and all the strings represented
    * by the given pattern.
    * <p>
    * The parameter pattern must be a POSIX compliant regular expression.
    * 
    * @param s
    *           a string.
    * @param pattern
    *           a POSIX regular expression.
    * @return the edit distance between the given string and all the strings represented
    *         by the given pattern.
    */
   // TODO update this code with a Java library implementing fuzzy regular expression
   // matching.
   public static int regexDelta(String s, String pattern) {
      final int MAX_DELTA = 100;

      File file = null;
      try {
         file = File.createTempFile("agrep-pivot-file", ".tmp");
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }

      try {
         Files.write(file.toPath(), (s + "\n").getBytes());
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }

      String cmd = String.format(
            "/usr/local/bin/agrep -E %d -s -e ^%s$ %s",
            MAX_DELTA,
            pattern,
            file.getAbsolutePath());

      ExecResult r = ProcUtils.exec(cmd);
      String output = r.getOutput();
      file.delete();

      String[] blocks = output.split(":");
      if (blocks.length <= 1) {
         return MAX_DELTA;
      }
      return Integer.parseInt(blocks[0]);
   }
}
