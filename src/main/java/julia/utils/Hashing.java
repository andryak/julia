package julia.utils;

/**
 * Utility class to calculate the hash code of sequences of objects.
 */
public final class Hashing {
   private final static long MASK = 0x9e3779b9;

   private Hashing() {};

   private static long combineWithSeed(long seed, Object o) {
      return ((long) o.hashCode()) + MASK + (seed << 6l) + (seed >>> 2l);
   }

   /**
    * Combines the hash codes of the given objects into a single hash code.
    * 
    * @param first
    *           the first object to hash with the others.
    * @param args
    *           the other objects to hash together.
    * @return an hash code describing the given objects.
    */
   public static int combine(Object first, Object... args) {
      long hash = first.hashCode();
      for (final Object arg : args) {
         hash ^= Hashing.combineWithSeed(hash, arg);
      }
      return (int) hash;
   }

   /**
    * Returns a reasonable hash code for a 32-bit integer as suggested by Knuth.
    * 
    * @param n
    *           the integer to hash.
    * @return an hash code describing the input integer.
    */
   public static int hash32(int n) {
      return (int) (n * 2654435761l);
   }
}