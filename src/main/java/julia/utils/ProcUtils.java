package julia.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import org.apache.commons.io.IOUtils;
import com.google.common.util.concurrent.UncheckedExecutionException;

/**
 * Utility class implementing static methods to run processes.
 */
public final class ProcUtils {
   /**
    * Do not instantiate me!
    */
   private ProcUtils() {};

   /**
    * The result of a call to exec.
    * <p>
    * Instances of this class include the exit status, the stdout and the stderr of the
    * process that produced them.
    */
   public static class ExecResult {
      private final int status;
      private final String out;
      private final String err;

      private ExecResult(int status, String out, String err) {
         this.status = status;
         this.out = out;
         this.err = err;
      }

      public int getStatus() {
         return this.status;
      }

      public String getOutput() {
         return this.out;
      }

      public String getError() {
         return this.err;
      }
   }

   /**
    * Runs the given command in a separate process and returns its exit status, its stdout
    * and its stderr.
    * 
    * @param cmd
    *           the command to run.
    * @return the exit status, stdout and stderr of the process executing the given
    *         command.
    */
   public static ExecResult exec(String cmd) {
      Runtime rt = Runtime.getRuntime();
      Process process = null;
      try {
         process = rt.exec(cmd);
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }
      try {
         process.waitFor();
      } catch (InterruptedException e) {
         throw new UncheckedExecutionException(e);
      }

      int status = process.exitValue();

      InputStream out = process.getInputStream();
      InputStream err = process.getErrorStream();

      String sout = null;
      String serr = null;
      try {
         sout = IOUtils.toString(out);
         serr = IOUtils.toString(err);
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }

      return new ExecResult(status, sout, serr);
   }
}
