package julia.utils;

import com.google.common.base.Preconditions;

/**
 * Utility class to time the execution of code fragments.
 */
public class Timer {
   private long start = -1;

   /**
    * Creates a new timer.
    */
   public Timer() {}

   /**
    * Starts this timer and returns the current time.
    * 
    * @return the current time in nanoseconds. This value can only be used
    *         relatively to other values obtained calling methods of this class
    *         or System.nanoTime.
    */
   public long start() {
      this.start = System.nanoTime();
      return this.start;
   }

   /**
    * Stops the timer and returns the number of nanoseconds elapsed since its
    * start.
    * 
    * @return the number of nanoseconds elapsed since this timer was started.
    */
   public long stop() {
      Preconditions.checkState(
            this.start >= 0,
            "Cannot stop a timer that was never started");

      long elapsed = (System.nanoTime() - this.start);
      this.start = -1;
      return elapsed;
   }
}
