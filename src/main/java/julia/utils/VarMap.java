package julia.utils;

import java.util.HashMap;
import java.util.Map;
import julia.expr.ast.uf.Fun;

/**
 * A map from variables to their new indexes.
 */
public class VarMap extends HashMap<Fun, Integer> {
   private static final long serialVersionUID = -8428625976329366953L;

   /**
    * Creates a new do-nothing map.
    */
   public VarMap() {
      super();
   }

   /**
    * Creates a new map.
    * 
    * @param map
    *           a map from variables to their new indexes.
    */
   public VarMap(Map<Fun, Integer> map) {
      super(map);
   }

   /**
    * Returns the index associated with the given variable, or null if no association
    * exists.
    * 
    * @param v
    *           a variable.
    * @return the index associated with the given variable, or null if no association
    *         exists.
    */
   public Integer getIndexFor(Fun v) {
      return this.get(v);
   }
}
