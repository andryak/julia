package julia.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to map unique variable names to unique indexes.
 * <p>
 * The same name is always mapped to the same index.
 */
public class VarRenamer {
   private Map<String, Integer> map;
   private int lastIndex;

   /**
    * Creates a new variable renamer that will map variable names to incremental
    * indexes starting from zero.
    */
   public VarRenamer() {
      this.map = new HashMap<>();
      this.lastIndex = 0;
   }

   /**
    * Returns an index for the given variable name.
    * <p>
    * If this variable has been renamed in the past the same name will be
    * returned.
    * 
    * @param name
    *           a variable name.
    * @return the index corresponding to the given variable name.
    */
   public Integer getIndexFor(String name) {
      Integer index = this.map.get(name);
      if (index == null) {
         index = this.lastIndex;
         this.map.put(name, index);
         ++this.lastIndex;
      }
      return index;
   }
}
