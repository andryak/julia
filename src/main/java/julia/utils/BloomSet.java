package julia.utils;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * An efficient data structure to filter a collection of collections by containment.
 */
public class BloomSet<T> {
   private final Map<BitSet, List<Set<T>>> entries;

   private final int sigsize;

   /**
    * Creates a new empty bloom set based on 512 bits long signatures.
    */
   public BloomSet() {
      this(512);
   }

   /**
    * Creates a new empty bloom set based on {@code sigsize} bits long signatures.
    * 
    * @param sigsize
    *           the size of the bloom filters used by this bloom set.
    */
   public BloomSet(int sigsize) {
      this.entries = new HashMap<>();
      this.sigsize = sigsize;
   }

   /**
    * Calculates a one hashing function based signature (sig) of the given collection.
    * <p>
    * It holds that collection A is likely contained in collection B if sig(A) & ~sig(B) =
    * 0.
    * 
    * @param c
    *           the collection to calculate the signature of.
    */
   protected BitSet getHashSig(Set<T> c) {
      BitSet sig = new BitSet(this.sigsize);
      for (final T o : c) {
         sig.set(Integer.remainderUnsigned(o.hashCode(), this.sigsize));
      }
      return sig;
   }

   /**
    * Returns the number of signatures stored in this set.
    * 
    * @return the number of signatures stored in this set.
    */
   public int size() {
      return this.entries.size();
   }

   /**
    * Stores a new collection in this set.
    * 
    * @param c
    *           the collection to store in this set.
    */
   public void put(Set<T> c) {
      BitSet sig = this.getHashSig(c);
      List<Set<T>> l = this.entries.get(sig);
      if (l == null) {
         l = new ArrayList<>();
         this.entries.put(sig, l);
      }
      l.add(c);
   }

   /**
    * Returns the list of the first k collections in the set that are likely contained
    * into the given reference collection.
    * 
    * @param c
    *           the reference collection.
    * @param k
    *           the maximum number of extracted collections.
    * @return the list of the first k collections in the set that are likely contained
    *         into the given reference collection.
    */
   public List<Set<T>> getSubsets(Set<T> c, int k) {
      if (k <= 0) {
         return new ArrayList<>();
      }

      BitSet sig = this.getHashSig(c);

      List<Set<T>> result = new ArrayList<>(Math.min(k, this.entries.size()));
      int extracted = 0;
      for (final Entry<BitSet, List<Set<T>>> e : this.entries.entrySet()) {
         BitSet esig = (BitSet) e.getKey().clone();
         esig.andNot(sig);
         if (esig.isEmpty()) {
            List<Set<T>> l = e.getValue();

            int n = Math.min(k - extracted, l.size());
            for (int i = 0; i < n; ++i) {
               result.add(l.get(i));
            }
            extracted += n;

            if (extracted >= k) {
               break;
            }
         }
      }
      return result;
   }
}
