# Julia

---

![](https://codeship.com/projects/dc5eeb10-f5e0-0134-27f6-1ebf8f342cfb/status?branch=master "CI status")

## Abstract

Julia is a general purpose caching framework for formulas from many 
SMT theories relying on the Z3 SMT solver. Although originally designed 
for the quantifier-free linear integer arithmetic logic, it now supports: 

+ the core theory (that is, propositional logic),
+ the theory of reals, 
+ the theory of reals-ints,
+ the theory of fixed-size bit-vectors,
+ the theory of strings,
+ the theory of arrays,
+ the empty theory (that is, the theory of uninterpreted functions).

Julia implements quick hash-based caches, a one-hash bloom filter 
based counter-example cache and a sat-delta based cache to reuse 
models of formulas solved in the past to assess the satisfiability 
of new formulas. 

Julia is particularly suited to be used in conjunction with symbolic 
execution tools written in Java, like Java Path Finder (JPF) or the 
Java Bytecode Symbolic Executor (JBSE).

## Other caching frameworks

Julia is a complementary caching framework that can be used together 
with or in substitution of Green, GreenTrie, Recal and Recal+. 

Julia can also be used in conjunction with KLEE's branch and counter-example 
caches.

## Installation

To install and run Julia you need to do three things:

1. download and build Julia dependencies,
2. download and build the Microsoft Z3 SMT solver,
3. tell Julia about the location where you installed Z3.

Don't panic! 
Julia is shipped with a Gradle build script to help you perform all three tasks. 

Please, notice that to enable support for the theory of strings in Julia requires 
further steps as detailed in a later section of this document.

### Building Julia dependencies

To download and install Julia dependencies simply run: `./gradlew build`

Please refer to the section "Dependencies" of this file to learn about Julia 
dependencies and why Julia needs them.

### Building the Microsoft Z3 SMT solver

Julia depends on the Microsoft Z3 SMT solver. 

This software takes a long time to build so it has been excluded from the default 
build chain of the Gradle script. This means that you will have to install it separately 
from the other dependencies of Julia. 

You have two options:

1. you can either download and compile Z3 manually and to place it anywhere you like, or
2. you can use our dedicated Gradle task running: `gradlew buildZ3`

If you choose the second option, our Gradle task will download and compile Microsoft Z3 from 
source in the **src/main/resources** folder. 

### Telling Julia about Z3 location.

If you used the Gradle task to install Z3 you don't need to do anything else.
Otherwise, simply modify the file **src/main/resources/config.properties** updating the *z3* property
to the path of the Microsoft Z3 binary on your file system.

### Testing that Julia can access Z3.

To check whether Julia can access the Microsoft Z3 SMT solver and communicate with it, 
use our gradle script to run the related tests: `gradlew runZ3SolverTests`

### Enable support for the theory of strings

Julia relies on the Z3-Str SMT solver to solve formulas from the theory of strings.
Currently, our Gradle script does not include directives to build Z3-Str.

To make Julia work with formulas from this theory you need to download and 
compile Z3-Str from its [git-hub](https://github.com/z3str/Z3-str) repository.
Please, notice that this requires downloading, installing and patching an older 
version of the Microsoft Z3 SMT solver (4.1.1) as detailed in the Z3-Str README 
file.

You can then place the folder containing the compiled binaries and the Z3-Str main 
python script in the folder **src/main/resources/Z3-str**. Alternatively, you can 
place such folder anywhere on your machine and tell Julia about its location by 
updating the *z3-str* property in the file **src/main/resources/config.properties**.

To check whether Julia can access the Z3-Str SMT solver and communicate with it, 
use our gradle script to run the related tests: `gradlew runZ3StrSolverTests`

### Replicating the experiments.

To replicate the experiments reported in the paper "Heuristically Matching Solution Spaces of Arithmetic Formulas to Efficiently Reuse Solutions" 
you can simply run our dedicated gradle tasks: 

1. `gradlew runJuliaExperiments`
2. `gradlew runRandomExperiments`
3. `gradlew runZ3Experiments`

These tasks will automatically fetch the raw data needed to perform the experiments and save them under **src/test/resources/benchmarks**.
The results of the experiments are placed in the folder **src/test/resources/logs**.

**Beware!** Running all experiments can be very costly, it takes approximately three hours on our machines. 
Moreover, the results reported in the paper were produced by using another system called **Utopia**, 
which shares the same goal of Julia but is quite different from it. Because of this, the results you 
will get with Julia might vary (even by a large amount) by the ones reported in the paper.

## Dependencies

Julia depends on: 

+ the [Microsoft Z3 SMT solver](https://github.com/Z3Prover/z3) to determine the satisfiability or the unsatisfiability of formulas not found in the cache. Julia is compatible with Z3 version 4.4.2 and 4.5.1, compatibility with older versions is not guaranteed.
+ the [ANTLR4 parser generator](http://www.antlr.org) to generate the parsers used to parse the models produced by Z3 and to build expressions (formulas) from strings in infix notation. The latter parser is not required for Julia to work but is used to simplify testing.
+ the [Google Gson library](https://github.com/google/gson) to transform objects into JSON strings. This library is currently used to print statistics collected by the Julia caches in JSON format.
+ the [SLF4J logging library](http://www.slf4j.org) to log messages. For efficiency reasons it is recommented to use the nop (no operation) binding of the library which forces Julia to print nothing on stdout but for debug purposes other bindings can be used to make Julia print a lot of information about what it is going on in the cache.
+ the [Guava library](https://github.com/google/guava) to check sort-related preconditions when expressions are built. 
+ the [Apache commons-cli library](https://commons.apache.org/proper/commons-cli/download_cli.cgi) to parse command line arguments.
+ the [Apache commons-lang3 library](https://commons.apache.org/proper/commons-lang/download_lang.cgi) to calculate the Levenshtein distance of two strings which is used to calculate the sat-delta value of some expressions in the theory of strings.

## Logo

Julia's logo has been designed by [Freepik](http://www.freepik.com/free-photos-vectors/face).
